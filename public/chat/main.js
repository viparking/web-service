var sinchClient = new SinchClient({
    applicationKey: '23bc4426-9058-446a-a6c5-13d4f79f631a',
    capabilities: {messaging: true},
    supportActiveConnection:true,
});

var CSID = "AliceCS";
var messageClient = sinchClient.getMessageClient();
var isRegistered = false;
var username = "";
var email = "";
var phone = "";
var fullName = "";

var sinchMessageListener = {
	onMessageDelivered: function(msgDeliveryInfo)
	{
		console.log("delivered");
		console.log(msgDeliveryInfo);
	},
	onIncomingMessage: function(msg)
	{
		console.log(msg);
		if(msg.senderId==username)
		{
			if(msg.textBody.indexOf("#register:")==-1)
				document.getElementById("chatTable").innerHTML+="<tr class='outgoing'><td>"+msg.textBody+"</td></tr>";
			else
				document.getElementById("chatTable").innerHTML+="<tr class='incoming'><td>"+getWelcomeMessage(msg.senderId)+"</td></tr>";
		}
		else
		{
			if(msg.textBody.indexOf("#register:")==-1)
			{
				document.getElementById("chatTable").innerHTML+="<tr class='incoming'><td>"+msg.textBody+"</td></tr>";
				notifyMe(msg.textBody);
			}
		}
		$("#chats").each( function() 
		{
		   // certain browsers have a bug such that scrollHeight is too small
		   // when content does not fill the client area of the element
		   var scrollHeight = Math.max(this.scrollHeight, this.clientHeight);
		   this.scrollTop = scrollHeight - this.clientHeight;
		});
	}
};

messageClient.addEventListener(sinchMessageListener);

function chatLoad()
{
	document.getElementById('chatBox').style.display = "none";
	console.log("load");
}
function toggleChat(show)
{
	if(show==1)
	{
		var height = window.innerHeight;
		height -= 85;
		height-=50;
		$("#register").css("max-height",height+"px");
		$("#chats").css("max-height",height+"px");
		document.getElementById("imgChat").style.display = "none";
		document.getElementById('chatBox').style.display = "block";
		if(!isRegistered)
		{
			document.getElementById("chats").style.display = "none";
			document.getElementById("register").style.display = "block";
		}
		else
		{
			document.getElementById("chats").style.display = "block";
			document.getElementById("register").style.display = "none";
		}
	}
	else
	{
		document.getElementById("imgChat").style.display = "block";
		document.getElementById('chatBox').style.display = "none";
	}
}
var isMinimized = false;
function toggleMinimize()
{
	if(isMinimized)
	{
		document.getElementById('chatContainer').style.display = "block";
		isMinimized = false;
		$("#chats").each( function() 
		{
		   // certain browsers have a bug such that scrollHeight is too small
		   // when content does not fill the client area of the element
		   var scrollHeight = Math.max(this.scrollHeight, this.clientHeight);
		   this.scrollTop = scrollHeight - this.clientHeight;
		});
	}
	else
	{
		document.getElementById('chatContainer').style.display = "none";
		isMinimized = true;
	}
}
function btnSubmitClick()
{
	username = document.getElementById("txtUsername").value;
	email = document.getElementById("txtEmail").value;
	fullName = document.getElementById("txtFullName").value;
	phone = document.getElementById("txtPhone").value;
	if(username!=""&&email!=""&&fullName!=""&&phone!="")
	{
		var error = false;
		if(!validateEmail(email))
		{
			error = true;
			document.getElementById("errorMessage").innerHTML = "Invalid email address format";
		}
		if(!validatePhone(phone))
		{
			error = true;
			console.log("wrong phone format");
			document.getElementById("errorMessage").innerHTML = "Invalid phone number format";
		}
		if(username.length>3&&username.length<20&&username.indexOf(" ")!=-1)
		{
			error = true;
			document.getElementById("errorMessage").innerHTML = "Username should between 3 and 20 characters and does not contain space.";
		}
		if(!error)
		{
			document.getElementById("btnSubmit").style.cursor = "wait";
			document.getElementById("txtUsername").disabled = true;
			document.getElementById("txtEmail").disabled = true;
			document.getElementById("txtFullName").disabled = true;
			document.getElementById("txtPhone").disabled = true;
			sinchClient.newUser({username:username, password: 'somethingSecure'});
			setTimeout(function(){sinchClient.start({username: username, password: 'somethingSecure'},onSinchConnected,onSinchFailedToConnect);},3000);
			
		}
	}
	else
	{
		document.getElementById("errorMessage").innerHTML = "Please fill out all of the field.";
	}
}
function validateEmail(mail)
{  
 if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))  
  {  
    return (true)  
  }
  return (false)  
}  
function validatePhone(phone)
{
	var regex = /^(?:[0-9] ?){6,14}[0-9]$/;
	return regex.test(phone);
}
function onSinchConnected()
{
	console.log("sinch connected");
	sinchClient.startActiveConnection();
	isRegistered = true;
	var message = messageClient.newMessage(CSID,"#register:"+username+"|"+fullName+"|"+email+"|"+phone);
	messageClient.send(message);
	toggleChat(1);

	document.getElementById("btnSubmit").style.cursor = "pointer";
	document.getElementById("txtUsername").disabled = false;
	document.getElementById("txtEmail").disabled = false;
	document.getElementById("txtFullName").disabled = false;
	document.getElementById("txtPhone").disabled = false;
}
function onSinchFailedToConnect()
{
	console.log("failed");
	document.getElementById("errorMessage").innerHTML = "Something wrong with our server, please try again later";
	document.getElementById("btnSubmit").style.cursor = "pointer";
	document.getElementById("txtUsername").disabled = false;
	document.getElementById("txtEmail").disabled = false;
	document.getElementById("txtFullName").disabled = false;
	document.getElementById("txtPhone").disabled = false;
}
function btnSendClick()
{
	var strMsg = document.getElementById("txtChat").value;
	var message = messageClient.newMessage(CSID,strMsg);
	messageClient.send(message);
	document.getElementById("txtChat").value="";
}
function checkKey(e)
{
	var keynum;
	if(window.event){
		keynum = e.keyCode;
	}else
	    if(e.which){		
			keynum = e.which;
	     }
	if(keynum==13)
	{
		btnSendClick();
	}
}

function notifyMe(msg) {
  if (!("Notification" in window)) {
    alert("This browser does not support desktop notification");
  }
  else if (Notification.permission === "granted") {
    var notification = new Notification("New Message From Customer Service",{body:msg, icon:'favicon.ico'});
  }
  else if (Notification.permission !== 'denied') {
    Notification.requestPermission(function (permission) {
      if (permission === "granted") {
        var notification = new Notification("New Message From Customer Service",{body:msg, icon:'favicon.ico'});
      }
    });
  }
}

function getWelcomeMessage(name)
{
	var date = new Date();
	var hour = date.getHours();
	var msg = "Good ";
	if(hour<11)
		msg+="morning"
	else if(hour<16)
		msg+="afternoon";
	else if(hour<19)
		msg+="evening";
	else
		msg+="night";
	msg+=" "+name;
	msg+="! Feel free to talk with our customer service agent for suggestions and more information. We are here to assist you.";
	return msg;
}