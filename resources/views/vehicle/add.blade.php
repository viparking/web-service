@extends('admin')

@section('content')

<?php 
	$vehicleTypes = $vehicleTypes->data;
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Add Vehicle</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<form method="post" action="<?= URL::to('vehicle/post'); ?>">
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Vehicle Type</label>
					</div>
					<div class="col-md-10">
						<select name="vehicleTypeID" class="form-control">
							@foreach($vehicleTypes as $vehicleType)
								<option value="{{ $vehicleType->id }}">
									{{ $vehicleType->vehicleType }}
								</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Member ID</label>
					</div>
					<div class="col-md-10">
						<input type="text" name="memberID" value="<?= isset($member) ? $member->id : "" ?>" required="required" class="form-control">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Card ID</label>
					</div>
					<div class="col-md-10">
						<input type="text" name="cardID" required="required" class="form-control">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Member Username</label>
					</div>
					<div class="col-md-10">
						<input type="text" name="name" value="<?= isset($member) ? $member->username : "" ?>" required="required" class="form-control">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Plate Number</label>
					</div>
					<div class="col-md-10">
						<input type="text" name="plateNumber" required="required" class="form-control">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>STNK Address</label>
					</div>
					<div class="col-md-10">
						<input type="text" name="address" required="required" class="form-control">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>No STNK</label>
					</div>
					<div class="col-md-10">
						<input type="text" name="noSTNK" required="required" class="form-control">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Brand</label>
					</div>
					<div class="col-md-10">
						<input type="text" name="brand" required="required" class="form-control">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Color</label>
					</div>
					<div class="col-md-10">
						<input type="text" name="color" required="required" class="form-control">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Type</label>
					</div>
					<div class="col-md-10">
						<input type="text" name="type" required="required" class="form-control">
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="pull-right">
					<button class="btn btn-danger" type="reset">Reset</button> 
					<button class="btn btn-primary" type="submit">Submit</button>
				</div>
			</div>
		</form>
	</div>
</div>

@stop
