@extends('admin')

@section('content')

    <?php $data = $vehicles->items(); function makeMoney($input)
    {
        $ret = "Rp " . number_format($input,2,',','.');
        return $ret;
    } function getFormattedDate($timeString)
    {
        $t = strtotime($timeString);
        $ret = "";
        $ret = date('H:i d-M-Y',$t);
        return $ret;
    } ?>

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Vehicle List
                <a href="{{ URL::to('vehicle/add') }}">
                    <button class="btn btn-primary pull-right">Add Vehicle</button>
                </a>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <form method="post" action="<?= URL::to('vehicle/search'); ?>">
                <div class="form-group">
                    <div class="row">                   
                        <div class="col-md-11">
                            <input type="text" name="username" 
                            placeholder="Search by Plate Number" class="form-control"
                            value="<?= isset($search) ? $search : "" ?>">
                        </div>
                        <div class="col-md-1">
                            <button class="btn btn-primary" type="submit">Submit</button>
                            </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-hover table-striped tablesorter">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Vehicle Type</th>
                    <th>User ID</th>
                    <th>Name</th>
                    <th>Plate Number</th>
                    <th>Balance</th>
                    <th>VIP Due Date</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($data as $vehicle)
                    <tr style="cursor: pointer" onclick="document.location='<?= URL::to('vehicle/'.$vehicle->id.'/detail') ?>'">
                        <td>{{$vehicle->id}}</td>
                        <td>{{$vehicle->vehicleType}}</td>
                        <td>{{$vehicle->member_id}}</td>
                        <td>{{$vehicle->name}}</td>
                        <td>{{$vehicle->plateNumber}}</td>
                        <td>{{makeMoney($vehicle->balance)}}</td>
                        <td>{{ $vehicle->due_date == NULL ? '-' : $vehicle->due_date  }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <!-- Pagination -->
            <div class="pull-right">
                {{ $vehicles->render() }}
            </div>
        </div>
    </div>

@stop

@section('script')

    <script type="text/javascript">
        $(document).ready(function(){
            $(".tablesorter").tablesorter();
        });
    </script>

@stop