@extends('admin')

@section('content')

    <?php
    $vehicle = $detail->data;
    $parkings = $parkings->data;
    $topUps = $topUps->data;
    function getFormattedDate($timeString)
    {
        $t = strtotime($timeString);
        $ret = "";
        $ret = date('H:i d-M-Y',$t);
        return $ret;
    }
    function makeMoney($input)
    {
        $ret = "Rp " . number_format($input,2,',','.');
        return $ret;
    }
    ?>

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Vehicle Detail
                <a href="{{ URL::to('vehicle/'.$vehicle->id.'/edit') }}"><button class="btn btn-default pull-right" style="margin-left:5px">Edit</button></a>
                <button class="btn btn-danger pull-right" onclick="$('#removeModal').modal('show');" style="margin-left:5px">Remove</button>
                <button class="btn btn-default pull-right" onclick="$('#vipModal').modal('show');" style="margin-left:5px">
                    {{ $vehicle->due_date == NULL ? "Set VIP" : "Update VIP Due Date" }}
                </button>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <img src="<?= URL::to('stnk/'.$vehicle->id.'.jpg') ?>">
            <table>
                <tr>
                    <td style="width:50%"><h4>Vehicle ID</h4></td>
                    <td style="width:5%">:</td>
                    <td>{{ $vehicle->id }}</td>
                </tr>
                <tr>
                    <td style="width:50%"><h4>Vehicle Type</h4></td>
                    <td style="width:5%">:</td>
                    <td>{{ $vehicle->vehicleType }}</td>
                </tr>
                <tr>
                    <td><h4>Member ID</h4></td>
                    <td>:</td>
                    <td>{{ $vehicle->member_id }}</td>
                </tr>
                <tr>
                    <td><h4>Card ID</h4></td>
                    <td>:</td>
                    <td>{{ $vehicle->card_id }}</td>
                </tr>
                <tr>
                    <td><h4>VIP Due Date</h4></td>
                    <td>:</td>
                    <td>{{ $vehicle->due_date == NULL ? "-" : getFormattedDate($vehicle->due_date) }}</td>
                </tr>
                <tr>
                    <td><h4>Name</h4></td>
                    <td>:</td>
                    <td>{{ $vehicle->name }}</td>
                </tr>
                <tr>
                    <td><h4>Plate Number</h4></td>
                    <td>:</td>
                    <td>{{ $vehicle->plateNumber }}</td>
                </tr>
                <tr>
                    <td><h4>Balance</h4></td>
                    <td>:</td>
                    <td>Rp. {{ $vehicle->balance }},-</td>
                </tr>
                <tr>
                    <td><h4>Address</h4></td>
                    <td>:</td>
                    <td>{{ $vehicle->address }}</td>
                </tr>
                <tr>
                    <td><h4>Nomor STNK</h4></td>
                    <td>:</td>
                    <td>{{ $vehicle->noSTNK }}</td>
                </tr>
                <tr>
                    <td><h4>Brand</h4></td>
                    <td>:</td>
                    <td>{{ $vehicle->brand }}</td>
                </tr>
                <tr>
                    <td><h4>Color</h4></td>
                    <td>:</td>
                    <td>{{ $vehicle->color }}</td>
                </tr>
                <tr>
                    <td><h4>Type</h4></td>
                    <td>:</td>
                    <td>{{ $vehicle->type }}</td>
                </tr>
            </table>
        </div>

        <!-- Last Parking -->
        <div class="col-md-12">
            <h2 style="text-align:center">Last Parking</h2>
            <table class="table table-hover table-striped tablesorter">
                <thead>
                <tr>
                    <th>Member ID</th>
                    <th>Price</th>
                    <th>Time In</th>
                    <th>Time Out</th>
                    <th>Gate In</th>
                    <th>Gate Out</th>
                    <th>Parking Lot Name</th>
                    <th>Starting Balance</th>
                    <th>Balance</th>
                </tr>
                </thead>
                <tbody>
                @foreach($parkings as $parking)
                    <tr>
                        <td>{{ $parking->member_id }}</td>
                        <td>{{ makeMoney($parking->price) }}</td>
                        <td>{{ getFormattedDate($parking->inTime) }}</td>
                        <td>{{ getFormattedDate($parking->outTime) }}</td>
                        <td>{{ $parking->inGate }}</td>
                        <td>{{ $parking->outGate }}</td>
                        <td>{{ $parking->parkingLotName }}</td>
                        <td>{{ makeMoney($parking->startingBalance) }}</td>
                        <td>{{ makeMoney($parking->balance) }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <!-- Last Top Up -->
        <div class="col-md-12">
            <h2 style="text-align:center">Last Top Up</h2>
            <table class="table table-hover table-striped tablesorter">
                <thead>
                <tr>
                    <th>Member ID</th>
                    <th>Pay Amount</th>
                    <th>Credit Amount</th>
                    <th>Starting Balance</th>
                    <th>Balance</th>
                </tr>
                </thead>
                <tbody>
                @foreach($topUps as $topUp)
                    <tr>
                        <td>{{ $topUp->member_id }}</td>
                        <td>{{ makeMoney($topUp->payAmount) }}</td>
                        <td>{{ money($topUp->creditAmount) }}</td>
                        <td>{{ money($parking->startingBalance) }}</td>
                        <td>{{ money($parking->balance) }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="modal fade" id="removeModal" tabindex="-1" role="dialog" aria-labelledby="banAdminModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Confirmation</h4>
                </div>
                <div class="modal-body">
                    <p id="msg">Are you sure want to remove this vehicle?</p>
                </div>
                <div class="modal-footer">
                    <form action="{{ URL::to('vehicle/remove') }}" method="post">
                        <input type="hidden" name="id" value="{{ $vehicle->id }}">
                        <button type="reset" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-danger">Remove</button>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal fade" id="vipModal" tabindex="-1" role="dialog" aria-labelledby="banAdminModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{{ $vehicle->due_date == NULL ? "Set VIP" : "Update VIP Due Date" }}</h4>
                </div>
                <form action="{{ URL::to('vehicle/vip') }}" method="post">
                    <div class="modal-body">
                        <p id="msg">Set the VIP Due Date</p>
                        <input type="hidden" name="vehicleID" value="{{ $vehicle->id }}">
                        <input type="date" name="dueDate" class="form-control" placeholder="due date">
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-danger">{{ $vehicle->due_date == NULL ? "Set" : "Update" }}</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@stop