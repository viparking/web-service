<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?= isset($title) ? $title : "ViParking Admin" ?></title>

    <link href="<?= URL::to('bower_components/bootstrap/dist/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?= URL::to('bower_components/metisMenu/dist/metisMenu.min.css'); ?>" rel="stylesheet">
    <link href="<?= URL::to('dist/css/timeline.css');?>" rel="stylesheet">
    <link href="<?= URL::to('dist/css/sb-admin-2.css');?>" rel="stylesheet">
    <link href="<?= URL::to('css/admin.css');?>" rel="stylesheet">
    <link href="<?= URL::to('css/style.css');?>" rel="stylesheet">
    <link href="<?= URL::to('bower_components/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css">
@yield('style')
<!-- ini css kalau mau pake morris -->
<!-- <link href="<?= URL::to('bower_components/morrisjs/morris.css'); ?>" rel="stylesheet"> -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="" href="<?= URL::to('/');?>"><img class="logo" src="<?= URL::to('images/logo.png');?>"></a>
        </div>
        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="<?= URL::to('logout');?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-top-links -->

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    @if(in_array("Dashboard", $adminMenuMapping))
                        <li>
                            <a href="<?= URL::to('dashboard');?>"><i class="fa fa-home fa-fw"></i> Dashboard</a>
                        </li>
                    @endif
                    @if(in_array("Company", $adminMenuMapping))
                        <li>
                            <a href="<?= URL::to('company');?>"><i class="fa fa-building fa-fw"></i> Company</a>
                        </li>
                    @endif
                    @if(in_array("Problem", $adminMenuMapping))
                        <li>
                            <a href="<?= URL::to('problem');?>"><i class="fa fa-warning fa-fw"></i> Problem</a>
                        </li>
                    @endif
                    @if(in_array("Casshier", $adminMenuMapping))
                        <li>
                            <a href="<?= URL::to('casshier');?>"><i class="fa fa-building fa-fw"></i> Casshier</a>
                        </li>
                    @endif
                    @if(in_array("Vehicle", $adminMenuMapping))
                        <li>
                            <a href="<?= URL::to('vehicle');?>"><i class="fa fa-car fa-fw"></i> Vehicle</a>
                        </li>
                    @endif
                    @if(in_array("Member", $adminMenuMapping))
                        <li>
                            <a href="<?= URL::to('member');?>"><i class="fa fa-group fa-fw"></i> Member</a>
                        </li>
                    @endif
                    @if(in_array("Admin", $adminMenuMapping))
                        <li>
                            <a href="<?= URL::to('admin');?>"><i class="fa fa-user-md fa-fw"></i> Admin</a>
                        </li>
                    @endif
                    @if(in_array("Admin Level", $adminMenuMapping))
                        <li>
                            <a href="<?= URL::to('admin-level');?>"><i class="fa fa-user-md fa-fw"></i> Admin Level</a>
                        </li>
                    @endif
                    @if(in_array("Vehicle Type", $adminMenuMapping))
                        <li>
                            <a href="<?= URL::to('vehicleType');?>"><i class="fa fa-car fa-fw"></i> Vehicle Type</a>
                        </li>
                    @endif
                    @if(in_array("Problem Type", $adminMenuMapping))
                        <li>
                            <a href="<?= URL::to('problem-type');?>"><i class="fa fa-warning fa-fw"></i> Problem Type</a>
                        </li>
                    @endif
                    @if(in_array("Top Up Amount", $adminMenuMapping))
                        <li>
                            <a href="<?= URL::to('topUpAmount');?>"><i class="fa fa-warning fa-fw"></i> Top Up Amount</a>
                        </li>
                    @endif
                    @if(in_array("Contact Forms", $adminMenuMapping))
                        <li>
                            <a href="<?= URL::to('contact/all');?>"><i class="fa fa-comments fa-fw"></i> Contact Forms</a>
                        </li>
                    @endif
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>

    <div id="page-wrapper">
        @yield('content')
    </div>

</div>

<?php if(isset($error)) { ?>
<div class="modal fade" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Error</h4>
            </div>
            <div class="modal-body">
                <p id="msg">{{ $error }}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php } ?>

<script src="<?= URL::to('bower_components/jquery/dist/jquery.min.js');?>"></script>
<script src="<?= URL::to('bower_components/bootstrap/dist/js/bootstrap.min.js');?>"></script>
<script src="<?= URL::to('bower_components/metisMenu/dist/metisMenu.min.js');?>"></script>
<script src="<?= URL::to('dist/js/sb-admin-2.js');?>"></script>
<script type="text/javascript" src="<?= URL::to('js/jquery.tablesorter.min.js');?>"></script>
<!-- <script src="<?= URL::to('bower_components/raphael/raphael-min.js');?>"></script>
    <script src="<?= URL::to('bower_components/morrisjs/morris.min.js');?>"></script> -->
<?php if(isset($error)) { ?>
<script type="text/javascript">
    $(document).ready(function(){
        $("#errorModal").modal('show');
    });
</script>
<?php } ?>
@yield('script')
</body>

</html>