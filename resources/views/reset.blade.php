<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<?= URL::to('bower_components/bootstrap/dist/css/bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?= URL::to('css/history.css')?>" rel="stylesheet">
    <?php function getFormattedDate($timeString)
    {
        $t = strtotime($timeString);
        $ret = "";
        $ret = date('H:i d-M-Y',$t);
        return $ret;
    } function makeMoney($input)
    {
        $ret = "Rp " . number_format($input,2,',','.');
        return $ret;
    } ?>
</head>
<body>
    <div class="container">
        <h1 style="text-align: center">Password Reset Request</h1>
        <br><br>
        <p>
            Your password has been reset to: <strong>{{ $user->id }}</strong>
        </p>
    </div>
</body>
<script src="<?= URL::to('bower_components/jquery/dist/jquery.min.js') ?>"></script>
<script src="<?= URL::to('bower_components/bootstrap/dist/js/bootstrap.min.js') ?>"></script>

</html>