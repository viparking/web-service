@extends('admin')

@section('content')

<?php 
	$vehicleTypes = $vehicleTypes->data;
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Add New Price</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<form method="post" action="<?= URL::to('price/post') ?>">
			<input type="hidden" name="parkingLotID" value="{{ $parkingLot->id }}">
			
			<div class="form-group">
				<div class="row">
					<div class="col-md-3">
						<label>Parking Lot Name</label>
					</div>
					<div class="col-md-9">
						<input type="text" class="form-control" value="{{ $parkingLot->name }}" disabled="disabled">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-3">
						<label>Vehicle Type</label>
					</div>
					<div class="col-md-9">
						<select class="form-control" name="vehicle_type_id">
							@foreach($vehicleTypes as $type)
								<option value="{{ $type->id }}">{{ $type->vehicleType }}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-3">
						<label>Initial Price</label>
					</div>
					<div class="col-md-9">
						<input type="text" name="initialPrice" required="required" class="form-control">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-3">
						<label>Initial Length</label>
					</div>
					<div class="col-md-9">
						<input type="text" name="initialLength" required="required" class="form-control">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-3">
						<label>Free Duration</label>
					</div>
					<div class="col-md-9">
						<input type="text" name="freeDuration" required="required" class="form-control">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-3">
						<label>Price</label>
					</div>
					<div class="col-md-9">
						<input type="text" name="price" required="required" class="form-control">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-3">
						<label>Overnight Price</label>
					</div>
					<div class="col-md-9">
						<input type="text" name="overnightPrice" required="required" class="form-control">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-3">
						<label>Max Price</label>
					</div>
					<div class="col-md-9">
						<input type="text" name="maxPrice" required="required" class="form-control">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-3">
						<label>Free Start</label>
					</div>
					<div class="col-md-9">
						<input type="text" name="freeStart" required="required" class="form-control">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-3">
						<label>Free End</label>
					</div>
					<div class="col-md-9">
						<input type="text" name="freeEnd" required="required" class="form-control">
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="pull-right">
					<button class="btn btn-danger" type="reset">Reset</button> 
					<button class="btn btn-primary" type="submit">Submit</button>
				</div>
			</div>
		</form>
	</div>
</div>

@stop