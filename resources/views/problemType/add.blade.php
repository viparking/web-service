@extends('admin')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= isset($problemType) ? "Edit" : "Add New"; ?> Problem Type</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<form method="post" action="<?= URL::to('problem-type/post') ?>">
			<?php if (isset($problemType)) { ?>
				<input type="hidden" name="id" value="{{ $problemType->id }}">
			<?php } ?>

			<div class="form-group">
				<div class="row">
					<div class="col-md-3">
						<label>Problem</label>
					</div>
					<div class="col-md-9">
						<input type="text" name="problem" required="required" class="form-control" value="<?= isset($problemType) ? $problemType->problem : "" ?>">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-3">
						<label>Description</label>
					</div>
					<div class="col-md-9">
						<input type="text" name="description" required="required" class="form-control" value="<?= isset($problemType) ? $problemType->description : "" ?>">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-3">
						<label>Fine</label>
					</div>
					<div class="col-md-9">
						<input type="text" name="fine" required="required" class="form-control" value="<?= isset($problemType) ? $problemType->fine : "" ?>">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-3">
						<label>Parking Related?</label>
					</div>
					<div class="col-md-9">
						<select name="isParkingRelated" class="form-control">
							<option value="1" {{ isset($problemType) && $problemType->isParkingRelated == 1 ? 'selected="selected"' : '' }}>Yes</option>
							<option value="0" {{ isset($problemType) && $problemType->isParkingRelated == 0 ? 'selected="selected"' : '' }}>No</option>
						</select>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="pull-right">
					<button class="btn btn-danger" type="reset">Reset</button> 
					<button class="btn btn-primary" type="submit">Submit</button>
				</div>
			</div>
		</form>
	</div>
</div>

@stop