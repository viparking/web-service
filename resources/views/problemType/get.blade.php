@extends('admin')

@section('content')

<?php $data = $problemTypes->data; function makeMoney($input)
    {
        $ret = "Rp " . number_format($input,2,',','.');
        return $ret;
    } function getFormattedDate($timeString)
    {
        $t = strtotime($timeString);
        $ret = "";
        $ret = date('H:i d-M-Y',$t);
        return $ret;
    } ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Problem Type
            <a href="<?= URL::to('problem-type/add') ?>">
                <button class="btn btn-primary pull-right">
                    <span class="fa fa-plus"></span> Add Problem Type
                </button>
            </a> 
        </h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<table class="table table-hover table-striped tablesorter">
			<thead>
				<tr>
                    <th>ID</th>
					<th>Problem</th>
					<th>Description</th>
					<th>Fine</th>
                    <th>Parking Related?</th>
                    <th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($data as $problemType)
					<tr>
                        <td>{{ $problemType->id }}</td>
						<td>{{ $problemType->problem }}</td>
						<td>{{ $problemType->description }}</td>
                        <td>{{makeMoney($problemType->fine)}},-</td>
                        <td>{{ $problemType->isParkingRelated ? 'Yes' : 'No' }}</td>
						<td>
							<a href="<?= URL::to('problem-type/'.$problemType->id.'/edit') ?>"><button class="btn btn-default">Edit</button></a>
							<button class="btn btn-danger" onclick="prepareDelete('{{ $problemType->id }}')">Delete</button>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                <p id="msg">Are you sure want to delete this problem type?</p>
            </div>
            <div class="modal-footer">
                <form action="<?= URL::to('problem-type/delete') ?>" method="post">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <input type="hidden" name="id" id="problemID">
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@stop

@section('script')

<script type="text/javascript">
	function prepareDelete(id)
	{
		$("#problemID").val(id);
		$("#myModal").modal('show');
	}
    $(document).ready(function(){
        $(".tablesorter").tablesorter({
            headers: {5: {sorter: false}}
        });
    });
</script>

@stop