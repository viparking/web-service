@extends('admin')

@section('style')
    <link href="<?= URL::to('bower_components/morrisjs/morris.css'); ?>" rel="stylesheet">
@stop

@section('content')

	<?php
	$company = $company->data;
	$parkingLotList = $parkingLotList->data;
	$adminListData = $adminList->items();
	$weeklyRevenue = $weeklyRevenue->data->summary;
    $monthlyRevenue = $monthlyRevenue->data->summary;
    $monthlyParking = $monthlyParking->data->summary;
    $weeklyParking = $weeklyParking->data->summary;
	?>

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">{{ $company->name }}</h1>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<table>
				<tr>
					<td><h4>Company ID</h4></td>
					<td>: {{ $company->id }}</td>
				</tr>
				<tr>
					<td><h4>Representative Name</h4></td>
					<td>: {{ $company->representativeName }}</td>
				</tr>
				<tr>
					<td><h4>Phone Number</h4></td>
					<td>: {{ $company->phoneNumber }} </td>
				</tr>
				<tr>
					<td><h4>Email Address</h4></td>
					<td>: {{ $company->emailAddress }}</td>
				</tr>
				<tr>
					<td><h4>Parking Lot Count</h4></td>
					<td>: {{ $company->parkingLotCount }}</td>
				</tr>
			</table>
		</div>

		<div class="col-md-12">
			<div class="col-md-6">
				<h2 style="text-align: center">Parking Lot List
					<a href="<?= URL::to('company/'.$company->id.'/parkingLot/add'); ?>">
						<button class="btn btn-primary pull-right">
							<span class="fa fa-plus"></span> Add Parking Lot
						</button>
					</a>
				</h2>
				<table class="table table-hover table-striped tablesorter">
					<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Address</th>
					</tr>
					</thead>
					<tbody>
					@foreach($parkingLotList as $parkingLot)
						<tr style="cursor: pointer" onclick="document.location='<?= URL::to('parkingLot/'.$parkingLot->id.'/detail'); ?>'">
							<td>{{ $parkingLot->id }}</td>
							<td>{{ $parkingLot->name }}</td>
							<td>{{ $parkingLot->address }}</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>

			@if($level > 3)
			<div class="col-md-6">
				<h2 style="text-align:center">Admin List</h2>
				<table class="table table-hover table-striped tablesorter">
					<thead>
					<tr>
						<th>Username</th>
						<th>Level</th>
						<th>Is Banned</th>
					</tr>
					</thead>
					<tbody>
					@foreach($adminListData as $admin)
						<tr style="cursor:pointer" onclick="document.location='<?= URL::to('admin/'.$admin->id.'/detail'); ?>'">
							<td>{{ $admin->username }}</td>
							<td>{{ $admin->level }}</td>
							<td>{{ $admin->isBanned ? "YES" : "NO" }}</td>
						</tr>
					@endforeach
					</tbody>
				</table>
				<!-- Pagination -->
				<div class="pull-right">
					{{ $adminList->render() }}
				</div>
			</div>
			@endif
		</div>
	</div>

	<!-- Revenue -->
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i> <span id="lblRevenue">Revenue</span>
                    <div class="pull-right">
                        <div class="btn-group">
                            <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                <span id="btnRevenue">Actions</span>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu pull-right" role="menu">
                                <li><a onclick="monthlyRevenue()">Monthly Revenue</a>
                                </li>
                                <li><a onclick="weeklyRevenue()">Weekly Revenue</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div id="morris-area-chart"></div>
                </div>
                <!-- /.panel-body -->
            </div>
        </div>

        <!-- Parking -->
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i> <span id="lblRevenue">Parking</span>
                    <div class="pull-right">
                        <div class="btn-group">
                            <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                <span id="btnParking">Actions</span>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu pull-right" role="menu">
                                <li><a onclick="monthlyParking()">Monthly Parking</a>
                                </li>
                                <li><a onclick="weeklyParking()">Weekly Parking</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div id="parkingChart"></div>
                </div>
                <!-- /.panel-body -->
            </div>
        </div>

@stop

@section('script')

	<script src="<?= URL::to('bower_components/raphael/raphael-min.js'); ?>"></script>
    <script src="<?= URL::to('bower_components/morrisjs/morris.min.js'); ?>"></script>

	<script>
		var revenueGraph;
        var parkingGraph;

		$(document).ready(function(){
			$(".tablesorter").tablesorter({
				headers: {5: {sorter: false}}
			});

			revenueGraph = Morris.Area({
                element: 'morris-area-chart',
                xkey: 'period',
                ykeys: ['revenue'],
                labels: ['revenue'],
                pointSize: 2,
                hideHover: 'auto',
                resize: true
            });

            parkingGraph = Morris.Area({
                element: 'parkingChart',
                xkey: 'period',
                ykeys: ['parking'],
                labels: ['parking'],
                pointSize: 2,
                hideHover: 'auto',
                resize: true
            });

            monthlyRevenue();
            monthlyParking();
		});

		function monthlyRevenue()
        {
            revenueGraph.setData([
                    @foreach($monthlyRevenue as $revenue)
                {
                    period: '<?= $revenue->year."-".$revenue->monthNumber; ?>',
                    revenue: <?= ($revenue->revenue); ?>
                },
                @endforeach
            ]);

            $("#lblRevenue").html("Monthly Revenue");
            $("#btnRevenue").html("Monthly");
        }

        function weeklyRevenue()
        {
            revenueGraph.setData([
                    @foreach($weeklyRevenue as $revenue)
                {
                    period: '<?= $revenue->year."-".$revenue->weekNumber; ?>',
                    revenue: <?= ($revenue->revenue); ?>
                },
                @endforeach
            ]);

            $("#lblRevenue").html("Weekly Revenue");
            $("#btnRevenue").html("Weekly");
        }

        function weeklyParking()
        {
            parkingGraph.setData([
                    @foreach($weeklyParking as $parking)
                {
                    period: '<?= $parking->year."-".$parking->weekNumber; ?>',
                    parking: <?= ($parking->parking); ?>
                },
                @endforeach
            ]);

            $("#lblParking").html("Weekly Parking");
            $("#btnParking").html("Weekly");
        }

        function monthlyParking()
        {
            parkingGraph.setData([
                    @foreach($monthlyParking as $parking)
                {
                    period: '<?= $parking->year."-".$parking->monthNumber; ?>',
                    parking: <?= ($parking->parking); ?>
                },
                @endforeach
            ]);

            $("#lblParking").html("Monthly Parking");
            $("#btnParking").html("Monthly");
        }
	</script>

@stop