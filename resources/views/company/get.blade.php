@extends('admin')

@section('content')

<?php $data = $companies->data; ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Company List
            <a href="<?= URL::to('company/add') ?>">
                <button class="btn btn-primary pull-right">
                    <span class="fa fa-plus"></span> Add Company
                </button>
            </a> 
        </h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<table class="table table-hover table-striped tablesorter">
			<thead>
				<tr>
                    <th>ID</th>
					<th>Name</th>
					<th>Representative Name</th>
					<th>Phone Number</th>
                    <th>Email</th>
                    <th>Parking Lot</th>
                    <th>Action</th>
				</tr>
			</thead>
			<tbody>
            <?php if(count($data) == 0) { ?>
                <tr>
                    <td colspan="6" style="text-align: center">No Data Available</td>
                </td>
            <?php } ?>
				@foreach ($data as $company)
					<tr style="cursor: pointer">
                        <td onclick="document.location='<?= URL::to('company/'.$company->id.'/detail') ?>'">{{$company->id}}</td>
						<td onclick="document.location='<?= URL::to('company/'.$company->id.'/detail') ?>'">{{$company->name}}</td>
						<td onclick="document.location='<?= URL::to('company/'.$company->id.'/detail') ?>'">{{$company->representativeName}}</td>
                        <td onclick="document.location='<?= URL::to('company/'.$company->id.'/detail') ?>'">{{$company->phoneNumber}}</td>
                        <td onclick="document.location='<?= URL::to('company/'.$company->id.'/detail') ?>'">{{$company->emailAddress}}</td>
                        <td onclick="document.location='<?= URL::to('company/'.$company->id.'/detail') ?>'">{{$company->ParkingLotCount}}</td>
						<td>
							<a href="<?= URL::to('company/'.$company->id.'/edit') ?>"><button class="btn btn-default">Update</button></a>
							<button class="btn btn-danger" onclick="prepareDelete('{{ $company->id }}')">Delete</button>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                <p id="msg">Are you sure want to delete this company?</p>
            </div>
            <div class="modal-footer">
                <form action="<?= URL::to('company/delete') ?>" method="post">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <input type="hidden" name="id" id="companyID">
                <button type="submit" class="btn btn-danger">Delete</button>
            </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@stop

@section('script')

<script type="text/javascript">
	function prepareDelete(id)
	{
		$("#companyID").val(id);
		$("#myModal").modal('show');
	}
    $(document).ready(function(){
        $(".tablesorter").tablesorter({
            headers: {5: {sorter: false}}
        });
    });
</script>

@stop