@extends('admin')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= isset($company) ? "Edit" : "Add New"; ?> Company</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<form method="post" action="<?= URL::to('company/post') ?>">
			<?php if (isset($company)) { ?>
				<input type="hidden" name="id" value="{{ $company->id }}">
			<?php } ?>
			<div class="form-group">
				<div class="row">
					<div class="col-md-3">
						<label>Name</label>
					</div>
					<div class="col-md-9">
						<input type="text" name="name" required="required" class="form-control" value="<?= isset($company) ? $company->name : "" ?>">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-3">
						<label>Representative Name</label>
					</div>
					<div class="col-md-9">
						<input type="text" name="representativeName" required="required" class="form-control" value="<?= isset($company) ? $company->representativeName : "" ?>">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-3">
						<label>Phone Number</label>
					</div>
					<div class="col-md-9">
						<input type="text" name="phoneNumber" required="required" class="form-control" value="<?= isset($company) ? $company->phoneNumber : "" ?>">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-3">
						<label>Email</label>
					</div>
					<div class="col-md-9">
						<input type="text" name="email" required="required" class="form-control" value="<?= isset($company) ? $company->emailAddress : "" ?>">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-3">
						<label>Address</label>
					</div>
					<div class="col-md-9">
						<input type="text" name="address" required="required" class="form-control" value="<?= isset($company) ? $company->address : "" ?>">
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="pull-right">
					<button class="btn btn-danger" type="reset">Reset</button> 
					<button class="btn btn-primary" type="submit">Submit</button>
				</div>
			</div>
		</form>
	</div>
</div>

@stop