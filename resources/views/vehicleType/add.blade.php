@extends('admin')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= isset($vehicleType) ? "Edit" : "Add"; ?> Vehicle Type</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<form method="post" action="<?= URL::to('vehicleType/post') ?>">
			<?php if(isset($vehicleType)) { ?>
				<input type="hidden" name="id" value="{{$vehicleType->id}}">
			<?php } ?>
			<div class="form-group">
				<div class="row">
					<div class="col-md-3">
						<label>Vehicle Type</label>
					</div>
					<div class="col-md-9">
						<input type="text" name="vehicleType" required="required" class="form-control" value="<?= isset($vehicleType)?$vehicleType->vehicleType:"" ?>">
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<button class="btn btn-danger col-md-6" type="reset">Reset</button>
					<button class="btn btn-primary col-md-6" type="submit">Submit</button>
				</div>
			</div>
		</form>
	</div>
</div>

@stop