<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>login</title>
	<link rel="stylesheet" type="text/css" href="<?= URL::to('css/bootstrap.min.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?= URL::to('css/login.css') ?>">
	<script type="text/javascript" src="<?= URL::to('js/jquery.js') ?>"></script>

</head>
<body>
	<div class="container">
		<div class="login-container">
        	<div id="output"></div>
        	<img src="<?= URL::to('images/logo.png') ?>" class= "avatar">
            <div class="form-box">
            	<form action="<?= URL::to('/login') ?>" method="post">
            		<input type="text" placeholder="username" name="username">
            		<input type="password" name="password" placeholder="Password">
            		<button class="btn btn-primary login" type="submit" style="margin-top: 10px">Login</button>
            	</form>
            </div>
        </div>  
	</div>
</body>
<script type="text/javascript">
<?php if(isset($error)) { ?>
	$(document).ready(function(){
		alert("<?= $error ?>")
	});
<?php } ?>
</script>
</html>