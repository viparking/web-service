@extends('admin')

@section('content')

<?php 
	$companies = $companies->data;
	if(isset($parkingLots))
		$parkingLots = $parkingLots->data;
	if(isset($generalProblem))
		$generalProblem = $generalProblem->items();
	if(isset($sessions))
	{
		$sessionPager = $sessions;
		$sessions = $sessions->items();
	}
?>

<?php function makeMoney($input)
{
	$ret = "Rp " . number_format($input,2,',','.');
	return $ret;
}
function getFormattedDate($timeString)
{
	$t = strtotime($timeString);
	$ret = "";
	$ret = date('H:i d-M-Y',$t);
	return $ret;
}?>

<div class="row">
	<div class="col-md-12">
	    <h1 class="page-header">Casshier Session</h1>
    </div>
</div>

<div class="row col-md-12">
	<form id="problemForm">
		<div class="form-group">
			<div class="row">
				<div class="col-md-3">
					<label>Company</label>
				</div>
				<div class="col-md-9">
					<select class="form-control" name="companyID" onchange="$('#problemForm').submit()">
						<option disabled="disabled" selected="selected">--Choose Company--</option>
						@foreach($companies as $company)
							<option value="{{ $company->id }}" {{ (isset($companyID) && $companyID == $company->id)?"selected":"" }}>{{ $company->name }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-3">
					<label>Parking Lot</label>
				</div>
				<div class="col-md-9">
					<select class="form-control" name="parkingLotID" onchange="$('#problemForm').submit()">
						<option disabled="disabled" selected="selected">--Choose Parking Lot--</option>
						@if(isset($parkingLots))
							@foreach($parkingLots as $parkingLot)
								<option value="{{ $parkingLot->id }}" {{ (isset($parkingLotID) && $parkingLotID == $parkingLot->id)?"selected":"" }}>{{ $parkingLot->name }}</option>
							@endforeach
						@endif
					</select>
				</div>
			</div>
		</div>
	</form>
</div>

<div class="row col-md-12">
	<table class="table table-hover table-striped tablesorter">
		<thead>
			<tr>
				<th>ID</th>
				<th>Username</th>
				<th>Income</th>
				<th>Login Time</th>
				<th>Logout Time</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
			@if(isset($sessions))
				@foreach($sessions as $session)
					<tr>
						<td>{{ $session->sessionID }}</td>
						<td>{{ $session->username }}</td>
						<td>{{ makeMoney($session->income) }}</td>
						<td>{{ getFormattedDate($session->loginTime) }}</td>
						<td>{{ $session->logoutTime>$session->loginTime?getFormattedDate($session->logoutTime):"" }}</td>
						<td>
							<div class="btn-group">
		                        <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
		                            <span>Actions</span>
		                            <span class="caret"></span>
		                        </button>
		                        <ul class="dropdown-menu pull-right" role="menu">
		                        	<li><a href="{{ URL::to('casshier/'.$session->sessionID.'/detail') }}">View Detail</a></li>
		                            <li><a href="{{ URL::to('parkingLot/'.$session->parking_lot_id.'/detail') }}">View Parking Lot</a></li>
		                            <li><a href="{{ URL::to('admin/'.$session->admin_id."/detail") }}">View Admin</a></li>
		                            <li><a href="{{ URL::to('admin/'.$session->login_manager_id."/detail") }}">View Login Manager</a></li>
		                            @if($session->logout_manager_id > 0)
			                            <li><a href="{{ URL::to('admin/'.$session->logout_manager_id."/detail") }}">View Logout Manager</a></li>
		                            @endif
		                        </ul>
		                    </div>
						</td>
					</tr>
				@endforeach
			@else
				<tr>
					<td colspan="5">No Data Available</td>
				</tr>
			@endif
		</tbody>
	</table>
</div>

<div class="col-md-12">
	@if(isset($sessions))
		<div class="pull-right">
			{{ $sessionPager->render() }}
		</div>
	@endif
</div>

@stop

@section('script')

<script type="text/javascript">
    $(document).ready(function(){
        $(".tablesorter").tablesorter();
    });
</script>

@stop