@extends('admin')

@section('content')

<?php
	$detailPager = $detail;
	$detail = $detail->items();
?>

<?php function makeMoney($input)
{
    $ret = "Rp " . number_format($input,2,',','.');
    return $ret;
}
function getFormattedDate($timeString)
{
    $t = strtotime($timeString);
    $ret = "";
    $ret = date('H:i d-M-Y',$t);
    return $ret;
}?>

 <div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Cashier Session Detail</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<table>
            <tr>
                <td><h4>Session ID</h4></td>
                <td>:</td>
                <td>{{ $summary->sessionID }}</td>
            </tr>
			<tr>
				<td><h4>Username</h4></td>
				<td>:</td>
				<td><a href="{{ URL::to('admmin/'.$summary->admin_id."/detail") }}">{{ $summary->username }}</a></td>
			</tr>
			<tr>
				<td><h4>Income</h4></td>
				<td>:</td>
				<td>{{ makeMoney($summary->income) }}</td>
			</tr>
			<tr>
				<td><h4>Login Time</h4></td>
				<td>:</td>
				<td>{{ getFormattedDate($summary->loginTime) }}</td>
			</tr>
			<tr>
				<td><h4>Login Manager Username</h4></td>
				<td>:</td>
				<td><a href="{{ URL::to('admin/'.$summary->login_manager_id."/detail") }}">{{ $summary->loginManagerUsername }}</a></td>
			</tr>
			<tr>
				<td><h4>Logout Time</h4></td>
				<td>:</td>
				<td>{{ $summary->logoutTime>$summary->loginTime?getFormattedDate($summary->logoutTime):"-" }}</td>
			</tr>
			<tr>
				<td><h4>Logout Manager Username</h4></td>
				<td>:</td>
				<td>
					@if(isset($summary->logoutManagerUsername))
					<a href="{{ URL::to('admin/'.$summary->admin_id."/detail") }}">{{ $summary->logoutManagerUsername }}</a>
					@endif
				</td>
			</tr>
			<tr>
				<td><h4>Starting Balance</h4></td>
				<td>:</td>
				<td>{{ makeMoney($summary->startingBalance) }}</td>
			</tr>
			<tr>
				<td><h4>Final Balance</h4></td>
				<td>:</td>
				<td>{{ $summary->finalBalance==0?"-":makeMoney($summary->finalBalance) }}</td>
			</tr>
			<tr>
				<td><h4>Taken Balance</h4></td>
				<td>:</td>
				<td>{{ $summary->takenBalance==0?"-":makeMoney($summary->takenBalance) }}</td>
			</tr>
		</table>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<h3>Parking Detail</h3>
		<table class="table table-striped table-hover tablesorter">
			<thead>
				<tr>
                    <th>ID</th>
					<th>In Time</th>
					<th>Out Time</th>
					<th>In Gate</th>
					<th>Out Gate</th>
					<th>Price</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				@foreach($detail as $data)
					<tr>
                        <td>{{ $data->id }}</td>
						<td>{{ getFormattedDate($data->inTime) }}</td>
						<td>{{ getFormattedDate($data->outTime) }}</td>
						<td>{{ $data->inGate }}</td>
						<td>{{ $data->outGate }}</td>
						<td>{{ makeMoney($data->price) }}</td>
						<td>
							<div class="btn-group">
		                        <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
		                            <span>Actions</span>
		                            <span class="caret"></span>
		                        </button>
		                        <ul class="dropdown-menu pull-right" role="menu">
		                        	<li><a href="#">View Parking</a></li>
		                            <li><a href="{{ URL::to('vehicle/'.$data->vehicle_id.'/detail') }}">View Vehicle</a></li>
		                            <li><a href="{{ URL::to('member/'.$data->member_id."/detail") }}">View Member</a></li>
		                        </ul>
		                    </div>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
		<div class="pull-right">
			{{ $detailPager->render() }}
		</div>
	</div>
</div>

@stop

@section('script')

<script type="text/javascript">
    $(document).ready(function(){
        $(".tablesorter").tablesorter();
    });
</script>

@stop