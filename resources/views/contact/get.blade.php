@extends('admin')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Contact Forms
        </h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<table class="table table-hover table-striped tablesorter">
			<thead>
				<tr>
					<th>Email</th>
					<th>Name</th>
					<th>Subject</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($contacts as $contact)
					<tr style="cursor: pointer" onclick="document.location='<?= URL::to('contact/detail/'.$contact->id) ?>'">
						<td>{{$contact->email}}</td>
                        <td>{{$contact->name}}</td>
                        <td>{{$contact->email}}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
		<!-- Pagination -->
		<div class="pull-right">
			{{ $contacts->render() }}
		</div>
	</div>
</div>

@stop

@section('script')

<script type="text/javascript">
    $(document).ready(function(){
        $(".tablesorter").tablesorter();
    });
</script>

@stop