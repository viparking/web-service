@extends('admin')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Contact Detail
        </h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="col-md-5">
			<table>
				<tr>
					<td><h4>Name</h4></td>
					<td>:</td>
					<td>{{ $contact->name }}</td>
				</tr>
				<tr>
					<td><h4>Email</h4></td>
					<td style="width:5%;">:</td>
					<td>{{ $contact->email }}</td>
				</tr>
				<tr>
					<td><h4>Subject</h4></td>
					<td>:</td>
					<td>{{ $contact->subject }}</td>
				</tr>
				<tr>
					<td><h4>Message</h4></td>
					<td>:</td>
					<td>{{ $contact->message }}</td>
				</tr>
			</table>
		</div>

	</div>
</div>

@stop