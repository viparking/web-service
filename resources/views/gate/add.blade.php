@extends('admin')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= isset($gate) ? "Edit" : "Add New"; ?> Gate</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<form method="post" action="<?= URL::to('gate/post') ?>">
			@if (isset($parkingLotID))
				<input type="hidden" name="parkingLotID" value="{{ $parkingLotID }}">
			@endif

			@if (isset($gate))
				<input type="hidden" name="parkingLotID" value="{{ $gate->parking_lot_id }}">
				<input type="hidden" name="id" value="{{ $gate->id }}">
			@endif
			
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Name</label>
					</div>
					<div class="col-md-10">
						<input type="text" name="name" required="required" class="form-control" value="{{ isset($gate) ? $gate->name : "" }}">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Type</label>
					</div>
					<div class="col-md-10">
						<select name="type" class="form-control">
							@foreach($types as $type)
								<option value="{{ $type->value }}" {{ isset($selectedType) && $selectedType == $type->value ? 'selected="selected"' : '' }}>
									{{ $type->name }}
								</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="pull-right">
					<button class="btn btn-danger" type="reset">Reset</button> 
					<button class="btn btn-primary" type="submit">Submit</button>
				</div>
			</div>
		</form>
	</div>
</div>

@stop