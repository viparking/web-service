@extends('admin')

@section('content')

<?php $topUps = $topUps->data; ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Top Up Amount
        	<a href="<?= URL::to('topUpAmount/add') ?>">
        		<button class="btn btn-primary pull-right">
        			<span class="fa fa-plus"></span> Add Top Up Amount
        		</button>
        	</a>
        </h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<table class="table table-hover table-striped tablesorter">
			<thead>
				<th>ID</th>
				<th>Pay Amount</th>
				<th>Credit Amount</th>
				<th>Action</th>
			</thead>
			<tbody>
				<?php if(count($topUps) == 0) { ?>
		          <tr>
		            <td colspan="3">No Data Available</td>
		          </tr>
		        <?php } ?>
				@foreach($topUps as $type)
					<tr>
						<td>{{$type->id}}</td>
						<td>{{$type->payAmount}}</td>
						<td>{{$type->creditAmount}}</td>
						<td>
							<a href="<?= URL::to('topUpAmount/edit/'.$type->id) ?>"><button class="btn btn-default">Update</button></a>
							<button onclick="prepareDelete('{{ $type->id }}')" class="btn btn-danger">Delete</button>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Confirmation</h4>
      </div>
      <div class="modal-body">
        <p id="msg">Are you sure want to delete this Vehicle Type?</p>
      </div>
      <div class="modal-footer">
      <form action="<?= URL::to('topUpAmount/delete') ?>" method="post">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        	<input type="hidden" name="id" id="vehicleTypeID">
	        <button type="submit" class="btn btn-danger">Delete</button>
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@stop

@section('script')

<script type="text/javascript">
	function prepareDelete(id)
	{
		$("#vehicleTypeID").val(id);
		$("#myModal").modal('show');
	}
	$(document).ready(function(){
	    $(".tablesorter").tablesorter({
	        headers: {2: {sorter: false}}
	    });
	});
</script>

@stop