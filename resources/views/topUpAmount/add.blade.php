@extends('admin')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= isset($topUp) ? "Edit" : "Add"; ?> Top Up Amount</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<form method="post" action="<?= URL::to('topUpAmount/post') ?>">
			<?php if(isset($topUp)) { ?>
				<input type="hidden" name="id" value="{{$topUp->id}}">
			<?php } ?>
			<div class="form-group">
				<div class="row">
					<div class="col-md-3">
						<label>Pay Amount</label>
					</div>
					<div class="col-md-9 input-group">
						<span class="input-group-addon">Rp. </span>
						<input type="number" name="payAmount" required="required" class="form-control" value="<?= isset($topUp)?$topUp->payAmount:"" ?>">
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-md-3">
						<label>Credit Amount</label>
					</div>
					<div class="col-md-9 input-group">
						<span class="input-group-addon">Rp. </span>
						<input type="number" name="creditAmount" required="required" class="form-control" value="<?= isset($topUp)?$topUp->creditAmount:"" ?>">
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<button class="btn btn-danger col-md-6" type="reset">Reset</button>
					<button class="btn btn-primary col-md-6" type="submit">Submit</button>
				</div>
			</div>
		</form>
	</div>
</div>

@stop