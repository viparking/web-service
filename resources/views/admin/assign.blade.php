@extends('admin')

@section('content')

<?php 
	$companies = $companies->data;
	$parkingLots = $parkingLots->data;
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Assign Admin</h1>
    </div>
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="form-group">
			<select id="ddlCompany" class="form-control">
				@foreach($companies as $company)
					<option value="{{ $company->id }}" {{ $companyID == $companyID ? 'selected=	' : '' }}>{{ $company->name }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<button class="btn btn-primary" onclick="document.location='{{ URL::to('admin/'.$adminID.'/assign') }}'+'/'+$('#ddlCompany').val()">Search</button>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<table class="table table-hover table-striped tablesorter">
			<thead>
				<tr>
					<th>ID</th>
					<th>Name</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($parkingLots as $parkingLot)
					<tr>
						<td>{{ $parkingLot->id }}</td>
						<td>{{ $parkingLot->name }}</td>
						<td>
							<button class="btn btn-default" onclick="prepareAssign('{{ $parkingLot->id }}')">Assign</button>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>

<div class="modal fade" id="assignModal" tabindex="-1" role="dialog" aria-labelledby="removeAccessModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        	<form action="{{ URL::to('admin/access/assign') }}" method="post">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                <h4 class="modal-title">Detail Assignation</h4>
	            </div>
	            <div class="modal-body">
	                <p id="msg">Choose access type below (can be more than 1): </p>
	                <div class="checkbox">
	                	<label><input type="checkbox" name="view">View</label>
	                </div>
	                <div class="checkbox">
	                	<label><input type="checkbox" name="add">Add</label>
	                </div>
	                <div class="checkbox">
	                	<label><input type="checkbox" name="edit">Edit</label>
	                </div>
	                <div class="checkbox">
	                	<label><input type="checkbox" name="delete">Delete</label>
	                </div>
	            </div>
	            <div class="modal-footer">
	                <input type="hidden" name="id" value="{{ $adminID }}">
	                <input type="hidden" name="parkingLotID" id="parkingLotID">
	                <button type="submit" class="btn btn-primary">Assign</button>
	                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	            </div>
			</form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@stop

@section('script')

<script>
	function prepareAssign(id)
	{
		$('#parkingLotID').val(id);
		$('#assignModal').modal('show');
	}

	$(document).ready(function(){
	    $(".tablesorter").tablesorter({
	        headers: {5: {sorter: false}}
	    });
	});
</script>

@stop