@extends('admin')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Add New Admin</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<form method="post" action="<?= URL::to('admin/post') ?>">
			<div class="form-group">
				<div class="row">
					<div class="col-md-3">
						<label>Username</label>
					</div>
					<div class="col-md-9">
						<input type="text" name="username" required="required" class="form-control">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-3">
						<label>Level</label>
					</div>
					<div class="col-md-9">
						<input type="text" name="level" required="required" class="form-control">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-3">
						<label>Password</label>
					</div>
					<div class="col-md-9">
						<input type="password" name="password" required="required" class="form-control">
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="pull-right">
					<button class="btn btn-danger" type="reset">Reset</button> 
					<button class="btn btn-primary" type="submit">Submit</button>
				</div>
			</div>
		</form>
	</div>
</div>

@stop