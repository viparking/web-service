@extends('admin')

@section('content')

<?php $data = $admins->items(); ?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Admin List
            <a href="<?= URL::to('admin/add') ?>">
                <button class="btn btn-primary pull-right">
                    <span class="fa fa-plus"></span> Add Admin
                </button>
            </a> 
        </h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<table class="table table-hover table-striped tablesorter">
			<thead>
				<tr>
					<th>ID</th>
					<th>Username</th>
					<th>Level</th>
					<th>Access Count</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($data as $admin)
					<tr style="cursor: pointer" onclick="document.location='<?= URL::to('admin/'.$admin->id.'/detail') ?>'">
						<td>{{$admin->id}}</td>
						<td>{{$admin->username}}</td>
                        <td>{{$admin->level}}</td>
                        <td>{{$admin->accessCount}}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
		<!-- Pagination -->
		<div class="pull-right">
			{{ $admins->render() }}
		</div>
	</div>
</div>

@stop

@section('script')

<script type="text/javascript">
    $(document).ready(function(){
        $(".tablesorter").tablesorter();
    });
</script>

@stop