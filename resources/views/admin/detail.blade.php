@extends('admin')

@section('content')

<?php
	$admin = $detail->data->admin;
	$access = $detail->data->access;
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Admin Detail
        	<?php if($admin->isBanned == 0) { ?>
	            <button class="btn btn-danger pull-right" onclick="$('#banAdminModal').modal('show');" style="margin-left:5px">Ban</button>
            <?php } else { ?>
            	<button class="btn btn-primary pull-right" onclick="$('#banAdminModal').modal('show');" style="margin-left:5px">Un-Ban</button>
        	<?php } ?>
            <a href="{{ URL::to('admin/'.$admin->id.'/assign') }}">
            	<button class="btn btn-default pull-right">Assign</button>
            </a>
        </h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="col-md-5">
			<table>
				<tr>
					<td><h4>Admin ID</h4></td>
					<td>:</td>
					<td>{{ $admin->id }}</td>
				</tr>
				<tr>
					<td><h4>Username</h4></td>
					<td>:</td>
					<td>{{ $admin->username }}</td>
				</tr>
				<tr>
					<td><h4>Level</h4></td>
					<td style="width:5%;">:</td>
					<td>{{ $admin->level }}</td>
				</tr>
				<tr>
					<td><h4>Access Count</h4></td>
					<td>:</td>
					<td>{{ $admin->accessCount }}</td>
				</tr>
				<tr>
					<td><h4>Is Banned</h4></td>
					<td>:</td>
					<td>{{ empty($admin->isBanned) ? 'No' : 'Yes' }}</td>
				</tr>
			</table>
		</div>

		<!-- Parking Lot table -->
		<div class="col-md-7">
			<h3 style="text-align: center">Assigned Parking Lot</h3>
			<table class="table table-hover table-striped tablesorter">
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($access as $item)
						<tr>
							<td>{{ $item->id }}</td>
							<td>{{ $item->name }}</td>
							<td>
								<button class="btn btn-danger" onclick="prepareRemove('{{ $item->parking_lot_id }}')">Remove Access</button>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="modal fade" id="banAdminModal" tabindex="-1" role="dialog" aria-labelledby="banAdminModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                <p id="msg">Are you sure want to <?= ($admin->isBanned==0)?"ban":"un-ban" ?> this admin?</p>
            </div>
            <div class="modal-footer">
                <form action="{{ ($admin->isBanned==0)?URL::to('admin/ban'):URL::to('admin/unban') }}" method="post">
	                <input type="hidden" name="id" value="{{ $admin->id }}">
	                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	                <button type="submit" class="btn btn-<?= ($admin->isBanned==0)?"danger":"primary" ?>"><?= ($admin->isBanned==0)?"Ban":"Un-Ban" ?></button>
	            </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="removeAccessModal" tabindex="-1" role="dialog" aria-labelledby="removeAccessModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                <p id="msg">Are you sure want to remove access of this admin?</p>
            </div>
            <div class="modal-footer">
                <form action="{{ URL::to('admin/access/remove') }}" method="post">
	                <input type="hidden" name="id" value="{{ $admin->id }}">
	                <input type="hidden" name="parkingLotID" id="parkingLotID">
	                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	                <button type="submit" class="btn btn-danger">Remove</button>
	            </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@stop

@section('script')

<script>
	function prepareRemove(id)
	{
		$("#parkingLotID").val(id);
		$("#removeAccessModal").modal('show');
	}

	$(document).ready(function(){
	    $(".tablesorter").tablesorter({
	        headers: {1: {sorter: false}}
	    });
	});
</script>

@stop