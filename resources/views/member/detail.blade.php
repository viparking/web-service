@extends('admin')

@section('content')

	<?php
	$member = $detail->data[0];
	$vehicles = $vehicles->data;
	$transactions = $transactions->data;
	function makeMoney($input)
	{
		$ret = "Rp " . number_format($input,2,',','.');
		return $ret;
	}
	function getFormattedDate($timeString)
	{
		$t = strtotime($timeString);
		$ret = "";
		$ret = date('H:i d-M-Y',$t);
		return $ret;
	}
	?>

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Member Detail
				<a href="{{ URL::to("member/".$member->id.'/edit') }}"><button class="btn btn-default pull-right" style="margin-left:5px">Edit</button></a>
				<?php if($member->isBanned == 0) { ?>
				<button class="btn btn-danger pull-right" onclick="$('#banMemberModal').modal('show');" style="margin-left:5px">Ban</button>
				<?php } else { ?>
				<button class="btn btn-primary pull-right" onclick="$('#banMemberModal').modal('show');" style="margin-left:5px">Un-Ban</button>
				<?php } ?>
			</h1>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<img src="<?= URL::to('ktp/'.$member->id.'.jpg') ?>">
			<table>
				<tr>
					<td><h4>Username</h4></td>
					<td style="width:5%;">:</td>
					<td>{{ $member->username }}</td>
				</tr>
				<tr>
					<td><h4>Member ID</h4></td>
					<td style="width:5%;">:</td>
					<td>{{ $member->id }}</td>
				</tr>
				<tr>
					<td><h4>Member Type</h4></td>
					<td>:</td>
					<td>{{ $member->member_type_id }}</td>
				</tr>
				<tr>
					<td><h4>Email</h4></td>
					<td>:</td>
					<td>{{ $member->email }}</td>
				</tr>
				<tr>
					<td><h4>Alamat Tinggal</h4></td>
					<td>:</td>
					<td>{{ $member->alamatTinggal }}</td>
				</tr>
				<tr>
					<td><h4>Alamat KTP</h4></td>
					<td>:</td>
					<td>{{ $member->alamatKTP }}</td>
				</tr>
				<tr>
					<td><h4>Nomor KTP</h4></td>
					<td>:</td>
					<td>{{ $member->noKTP }}</td>
				</tr>
				<tr>
					<td><h4>Point</h4></td>
					<td>:</td>
					<td>{{ $member->point }}</td>
				</tr>
				<tr>
					<td><h4>Parking Count</h4></td>
					<td>:</td>
					<td>{{ $member->parkingCount }}</td>
				</tr>
				<tr>
					<td><h4>Vehicle Count</h4></td>
					<td>:</td>
					<td>{{ $member->vehicleCount }}</td>
				</tr>
				<tr>
					<td><h4>Is Banned</h4></td>
					<td>:</td>
					<td>{{ empty($member->isBanned) ? 'No' : 'Yes' }}</td>
				</tr>
			</table>
		</div>

		<!-- Vehicle List table -->
		<div class="col-md-12">
			<h3 style="text-align: center">Vehicle List
				<a href="<?= URL::to('vehicle/add/'.$member->id) ?>"><button class="btn btn-primary pull-right">Add Vehicle</button></a>
			</h3>
			<table class="table table-hover table-striped tablesorter">
				<thead>
				<tr>
					<th>Vehicle Type</th>
					<th>Name</th>
					<th>Plate Number</th>
					<th>Balance</th>
				</tr>
				</thead>
				<tbody>
				@foreach($vehicles as $vehicle)
					<tr style="cursor: pointer" onclick="document.location='<?= URL::to('vehicle/'.$vehicle->id.'/detail') ?>'">
						<td>{{ $vehicle->vehicleType }}</td>
						<td>{{ $vehicle->name }}</td>
						<td>{{ $vehicle->plateNumber }}</td>
						<td>{{ makeMoney($vehicle->balance) }}</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>

		<!-- Transaction History table -->
		<div class="col-md-12">
			<h3 style="text-align: center">Transaction History</h3>
			<table class="table table-hover table-striped tablesorter">
				<thead>
				<tr>
					<th>Vehicle</th>
					<th>Amount</th>
					<th>Created At</th>
					<th>Type</th>
					<th>Starting Balance</th>
					<th>Balance</th>
				</tr>
				</thead>
				<tbody>
				@foreach($transactions as $transaction)
					<tr>
						<td>{{ $transaction->vehicle_id }}</td>
						<td>{{ makeMoney($transaction->Amount) }}</td>
						<td>{{ getFormattedDate($transaction->created_at) }}</td>
						<td>{{ $transaction->Type }}</td>
						<td>{{ makeMoney($transaction->startingBalance) }}</td>
						<td>{{ makeMoney($transaction->balance) }}</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>

	<div class="modal fade" id="banMemberModal" tabindex="-1" role="dialog" aria-labelledby="banAdminModal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Confirmation</h4>
				</div>
				<div class="modal-body">
					<p id="msg"><?= ($member->isBanned==0)?"Are you sure want to ban this member?":"Are you sure want to un-ban this member?" ?></p>
				</div>
				<div class="modal-footer">
					<form action="{{ ($member->isBanned==0)?URL::to('member/ban'):URL::to('member/unban') }}" method="post">
						<input type="hidden" name="id" value="{{ $member->id }}">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						<button type="submit" class="btn btn-<?= ($member->isBanned==0)?"danger":"primary" ?>"><?= ($member->isBanned==0)?"Ban":"Un-Ban" ?></button>
					</form>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

@stop

@section('script')

	<script>
		$(document).ready(function(){
			$(".tablesorter").tablesorter({
				headers: {1: {sorter: false}}
			});
		});
	</script>

@stop