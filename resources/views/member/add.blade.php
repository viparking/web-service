@extends('admin')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Registration</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<form method="post" action="<?= !isset($detail)?URL::to('member/post'):URL::to('member/'.$memberID.'/edit'); ?>">
			<div class="form-group">
				<div class="row">
					<div class="col-md-3">
						<label>Username</label>
					</div>
					<div class="col-md-9">
						<input type="text" name="username" required="required" class="form-control" value="<?= isset($detail)?$detail->username:'' ?>">
					</div>
				</div>
			</div>
			<?php if(!isset($detail)) { ?>
				<div class="form-group">
					<div class="row">
						<div class="col-md-3">
							<label>Password</label>
						</div>
						<div class="col-md-9">
							<input type="password" name="password" required="required" class="form-control">
						</div>
					</div>
				</div>
			<?php } ?>
			<div class="form-group">
				<div class="row">
					<div class="col-md-3">
						<label>Email</label>
					</div>
					<div class="col-md-9">
						<input type="email" name="email" required="required" class="form-control" value="<?= isset($detail)?$detail->email:'' ?>">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-3">
						<label>Phone Number</label>
					</div>
					<div class="col-md-9">
						<input type="text" name="phoneNumber" required="required" class="form-control" value="<?= isset($detail)?$detail->phoneNumber:'' ?>"> 
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-3">
						<label>Alamat Tinggal</label>
					</div>
					<div class="col-md-9">
						<input type="text" name="alamatTinggal" required="required" class="form-control" value="<?= isset($detail)?$detail->alamatTinggal:'' ?>">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-3">
						<label>Alamat KTP</label>
					</div>
					<div class="col-md-9">
						<input type="text" name="alamatKTP" required="required" class="form-control" value="<?= isset($detail)?$detail->alamatKTP:'' ?>"> 
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-3">
						<label>Nomor KTP</label>
					</div>
					<div class="col-md-9">
						<input type="text" name="noKTP" required="required" class="form-control" value="<?= isset($detail)?$detail->noKTP:'' ?>">
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="pull-right">
					<button class="btn btn-danger" type="reset">Reset</button> 
					<button class="btn btn-primary" type="submit">Submit</button>
				</div>
			</div>
		</form>
	</div>
</div>

@stop