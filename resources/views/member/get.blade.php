@extends('admin')

@section('content')

<?php $data = $members->items(); ?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Member List
            <a href="<?= URL::to('member/add') ?>">
                <button class="btn btn-primary pull-right">
                    <span class="fa fa-plus"></span> Registration
                </button>
            </a> 
        </h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<form method="post" action="<?= URL::to('member/search') ?>">
			<div class="form-group">
				<div class="row">					
					<div class="col-md-11">
						<input type="text" name="username" 
						placeholder="Search by Name or Email" class="form-control"
						value="<?= isset($search) ? $search : "" ?>">
					</div>
					<div class="col-md-1">
						<button class="btn btn-primary" type="submit">Submit</button>
						</div>
				</div>
			</div>
			</form>
			</div>
			</div>
<div class="row">
	<div class="col-md-12">
		<table class="table table-hover table-striped tablesorter">
			<thead>
				<tr>
					<th>ID</th>
					<th>Username</th>
					<th>Member Type</th>
					<th>Email</th>
					<th>Point</th>
					<th>Parking Count</th>
					<th>Vehicle Count</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($data as $member)
					<tr style="cursor: pointer; <?php if($member->isBanned == 1) echo "color: red" ?>" onclick="document.location='<?= URL::to('member/'.$member->id.'/detail') ?>'">
						<td>{{$member->id}}</td>
						<td>{{$member->username}}</td>
						<td>{{$member->member_type_id}}</td>
						<td>{{$member->email}}</td>
						<td>{{$member->point}}</td>
                        <td>{{$member->parkingCount}}</td>
                        <td>{{$member->vehicleCount}}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
		<!-- Pagination -->
		<div class="pull-right">
			{{ $members->render() }}
		</div>
	</div>
</div>

@stop

@section('script')

<script type="text/javascript">
    $(document).ready(function(){
        $(".tablesorter").tablesorter();
    });
</script>

@stop