@extends('admin')

@section('content')

<?php 
	$companies = $companies->data;
	$parkingLots = isset($parkingLots) ? $parkingLots->data : null;
	$problems = $problems->data;
	$isParkingRelated = isset($problemDetail) ? $problemDetail->isParkingRelated : null;
	function getFormattedDate($timeString)
    {
        $t = strtotime($timeString);
        $ret = "";
        $ret = date('H:i d-M-Y',$t);
        return $ret;
    }
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Input Problem</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<form method="post" action="<?= URL::to('problem/post') ?>">
			<!-- Problem Drop Down -->
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Problem</label>
					</div>
					<div class="col-md-10">
						<select id="ddlProblem" name="problemID" class="form-control">
							<option disabled="disabled" selected="selected">--Choose Problem--</option>
							@foreach($problems as $problem)
								<option value="{{ $problem->id }}" {{ (isset($problemID) && $problem->id == $problemID) ? "selected" : "" }}>{{ $problem->problem }}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>

			<!-- Is Parking Related Check Box -->
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Is Parking Related</label>
					</div>
					<div class="col-md-10">
						<select name="isParkingRelated" class="form-control" readonly>
							<option value="1" {{ $isParkingRelated == 1 ? 'selected="selected"' : '' }}>Yes</option>
							<option value="0" {{ $isParkingRelated == 0 ? 'selected="selected"' : '' }}>No</option>
						</select>
					</div>
				</div>
			</div>

			<!-- Company Drop Down -->
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Company</label>
					</div>
					<div class="col-md-10">
						<select id="ddlCompany" class="form-control" name="companyID">
							<option disabled="disabled" selected="selected">--Choose Company--</option>
							@foreach($companies as $company)
								<option value="{{ $company->id }}" {{ (isset($companyID) && $companyID == $company->id)?"selected":"" }}>{{ $company->name }}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>

			<!-- Parking Lot Drop Down -->
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Parking Lot</label>
					</div>
					<div class="col-md-10">
						<select id="ddlParkingLot" name="parkingLotID" class="form-control">
							<option disabled="disabled" selected="selected">--Choose Parking Lot--</option>
							@if(isset($parkingLots))
								@foreach($parkingLots as $parkingLot)
									<option value="{{ $parkingLot->id }}" {{ (isset($parkingLotID) && $parkingLotID == $parkingLot->id)?"selected":"" }}>{{ $parkingLot->name }}</option>
								@endforeach
							@endif
						</select>
					</div>
				</div>
			</div>

			<!-- Parking Drop Down -->
			<div class="form-group {{ $isParkingRelated != 1 ? 'hidden' : '' }}">
				<div class="row">
					<div class="col-md-2">
						<label>Parking</label>
					</div>
					<div class="col-md-10">
						<select id="ddlParking" name="parkingID" class="form-control">
							<option disabled="disabled" selected="selected">--Choose Parking--</option>
							@if(isset($parkings))
								@foreach($parkings as $parking)
									<option value="{{ $parking->id }}">
										{{ $parking->id." - ".$parking->vehicle->card_id." - ".getFormattedDate($parking->inTime) }}
									</option>
								@endforeach
							@endif
						</select>
					</div>
				</div>
			</div>

			<!-- Card ID Text Field -->
			<div class="form-group {{ $isParkingRelated != 1 ? 'hidden' : '' }}">
				<div class="row">
					<div class="col-md-2">
						<label>Card ID</label>
					</div>
					<div class="col-md-10">
						<input type="text" name="cardID" class="form-control">
					</div>
				</div>
			</div>

			<!-- Description Text Field -->
			<div class="form-group {{ $isParkingRelated == 0 ? '' : 'hidden' }}">
				<div class="row">
					<div class="col-md-2">
						<label>Description</label>
					</div>
					<div class="col-md-10">
						<input type="text" name="description" class="form-control" value="{{ $isParkingRelated == 0 ? $problem->description : '' }}" readonly>
					</div>
				</div>
			</div>

			<!-- Reason Text Field -->
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Reason</label>
					</div>
					<div class="col-md-10">
						<input type="text" name="reason" required="required" class="form-control">
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="pull-right">
					<button class="btn btn-danger" type="reset">Reset</button> 
					<button class="btn btn-primary" type="submit">Submit</button>
				</div>
			</div>
		</form>
	</div>
</div>

@stop

@section('script')

<script type="text/javascript">	
	function generateLink(companyID, parkingLotID, problemID)
	{
		var problem = "problemID="+problemID;
		var company = "&companyID="+companyID;
		var parkingLot = "&parkingLotID="+parkingLotID; 

		return "{{ URL::to('problem/add') }}?"+problem+company+parkingLot;
	}

	$(document).ready(function() {
		$('select').change(function(e) {
			if(e.target == document.getElementById("ddlParking"))
				return;
			var problemID = $('#ddlProblem').val();
			var companyID = $('#ddlCompany').val();
			var parkingLotID = $('#ddlParkingLot').val();
			location.href = generateLink(companyID, parkingLotID, problemID);
		});
	});
</script>

@stop
