@extends('admin')

@section('content')
<?php function makeMoney($input)
    {
        $ret = "Rp " . number_format($input,2,',','.');
        return $ret;
    }
    function getFormattedDate($timeString)
    {
        $t = strtotime($timeString);
        $ret = "";
        $ret = date('H:i d-M-Y',$t);
        return $ret;
    }?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Parking Problem Detail
        	@if($transaction==null)
	        	<button class="btn btn-primary pull-right" onclick="$('#payFineModal').modal('show');" style="margin-left:5px">Pay Fine</button>
        	@endif
        </h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<table>
			<tr>
				<td><h4>Problem ID</h4></td>
				<td>:</td>
				<td>{{ $parkingProblem->id  }}</td>
			</tr>
			<tr>
				<td><h4>Username</h4></td>
				<td>:</td>
				<td><a href="{{ URL::to('member/'.$member->id.'/detail') }}">{{ $member->username }}</a></td>
			</tr>
			<tr>
				<td><h4>Reason</h4></td>
				<td>:</td>
				<td>{{ $parkingProblem->reason }}</td>
			</tr>
			<tr>
				<td><h4>Problem Name</h4></td>
				<td>:</td>
				<td>{{ $problem->problem }}</td>
			</tr>
			<tr>
				<td><h4>Problem Description</h4></td>
				<td>:</td>
				<td>{{ $problem->description }}</td>
			</tr>
			<tr>
				<td><h4>Fine</h4></td>
				<td>:</td>
				<td>{{ (!isset($fine))?$problem->fine:$fine->fines }}</td>
			</tr>
			<tr>
				<td><h4>Is Fine Paid?</h4></td>
				<td>:</td>
				<td>{{ $transaction == null?"NO":"YES" }}</td>
			</tr>
			<tr>
				<td><h4>In Time</h4></td>
				<td>:</td>
				<td>{{ getFormattedDate($parking->inTime) }}</td>
			</tr>
			<tr>
				<td><h4>In Gate</h4></td>
				<td>:</td>
				<td>{{ $parking->inGate }}</td>
			</tr>
			<tr>
				<td><h4>Out Time</h4></td>
				<td>:</td>
				<td>{{ $parking->outTime>$parking->inTime?getFormattedDate($parking->outTime):"-" }}</td>
			</tr>
			<tr>
				<td><h4>Out Gate</h4></td>
				<td>:</td>
				<td>{{ $parking->outGate }}</td>
			</tr>
			<tr>
				<td><h4>Price</h4></td>
				<td>:</td>
				<td>{{ $parking->price }}</td>
			</tr>
		</table>
	</div>
</div>

<div class="modal fade" id="payFineModal" tabindex="-1" role="dialog" aria-labelledby="payFineModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Pay Fine</h4>
            </div>
            <form action="{{ URL::to('problem/payFine') }}" method="post">
	            <div class="modal-body">
	               	<input type="hidden" name="parkingProblemID" value="{{ $parkingProblem->id }}"> 
	               	<input type="hidden" name="parkingLotID" value="{{ $parking->parking_lot_id }}">
	               	<select name="payment_type_id" required="required" class="form-control">
	               		<option disabled="disabled" selected="selected">--Choose Payment Type--</option>
	               		@foreach($paymentType as $type)
	               			<option value="{{ $type->id }}">{{ $type->paymentType }}</option>
	               		@endforeach
	               </select>
	            </div>
	            <div class="modal-footer">
		            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	                <button type="submit" class="btn btn-primary">Pay</button>
	            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@stop