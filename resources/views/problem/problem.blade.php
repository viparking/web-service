@extends('admin')

@section('content')

<?php 
	$companies = $companies->data;
	if(isset($parkingLots))
		$parkingLots = $parkingLots->data;
	if(isset($generalProblem))
		$generalProblem = $generalProblem->items();
	if(isset($parkingProblem))
	{
		$parkingPager = $parkingProblem;
		$parkingProblem = $parkingProblem->items();
	} function makeMoney($input)
    {
        $ret = "Rp " . number_format($input,2,',','.');
        return $ret;
    } function getFormattedDate($timeString)
    {
        $t = strtotime($timeString);
        $ret = "";
        $ret = date('H:i d-M-Y',$t);
        return $ret;
    }
?>
<div class="row">
	<div class="col-md-12">
	    <h1 class="page-header">Problem
		    <a href="<?= URL::to('/problem/add') ?>">
                <button class="btn btn-primary pull-right">
                    <span class="fa fa-plus"></span> Report Problem
                </button>
            </a> 
	    </h1>
    </div>
</div>

<div class="row col-md-12">
	<form id="problemForm">
		<div class="form-group">
			<div class="row">
				<div class="col-md-3">
					<label>Company</label>
				</div>
				<div class="col-md-9">
					<select class="form-control" name="companyID" onchange="$('#problemForm').submit()">
						<option disabled="disabled" selected="selected">--Choose Company--</option>
						@foreach($companies as $company)
							<option value="{{ $company->id }}" {{ (isset($companyID) && $companyID == $company->id)?"selected":"" }}>{{ $company->name }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-3">
					<label>Parking Lot</label>
				</div>
				<div class="col-md-9">
					<select class="form-control" name="parkingLotID" onchange="$('#problemForm').submit()">
						<option disabled="disabled" selected="selected">--Choose Parking Lot--</option>
						@if(isset($parkingLots))
							@foreach($parkingLots as $parkingLot)
								<option value="{{ $parkingLot->id }}" {{ (isset($parkingLotID) && $parkingLotID == $parkingLot->id)?"selected":"" }}>{{ $parkingLot->name }}</option>
							@endforeach
						@endif
					</select>
				</div>
			</div>
		</div>
	</form>
</div>

<div class="row">
	<div class="col-md-12">
		<h3>Parking Problem
			@if(isset($parkingProblem))
				<div class="pull-right">
					{{ $parkingPager->render() }}
				</div>
			@endif
		</h3>
		<table class="table table-hover table-striped tablesorter">
			<thead>
				<tr>
					<th>ID</th>
					<th>Problem Name</th>
					<th>Reason</th>
					<th>Fine</th>
					<th>Time</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@if(isset($parkingProblem))
					@foreach($parkingProblem as $problem)
						<tr>
							<td>{{ $problem->id }}</td>
							<td>{{ $problem->problem }}</td>
							<td>{{ $problem->reason }}</td>
							<td>{{ makeMoney($problem->localFine>0?$problem->localFine:$problem->globalFine) }}</td>
							<td>{{ getFormattedDate($problem->updated_at) }}</td>
							<td>
								<div class="btn-group">
			                        <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
			                            <span>Actions</span>
			                            <span class="caret"></span>
			                        </button>
			                        <ul class="dropdown-menu pull-right" role="menu">
				                        <li><a href="{{ URL::to('problem/'.$problem->id.'/detail') }}">View Detail</a></li>
			                            <li><a href="{{ URL::to('problem-type') }}">View Problem</a></li>
			                            <li><a href="{{ URL::to('parking/'.$problem->parking_id.'/detail') }}">View Parking</a></li>
			                            <li><a href="{{ URL::to('vehicle/'.$problem->vehicle_id."/detail") }}">View Vehicle</a></li>
			                            <li><a href="{{ URL::to('member/'.$problem->member_id."/detail") }}">View Member</a></li>
			                        </ul>
			                    </div>
							</td>
						</tr>
					@endforeach
				@else
					<tr>
						<td colspan="5">No Data Available</td>
					</tr>
				@endif
			</tbody>
		</table>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<h3>General Problem</h3>
		<table class="table table-hover table-striped tablesorter">
			<thead>
				<tr>
					<th>ID</th>
					<th>Problem</th>
					<th>Reason</th>
					<th>Description</th>
					<th>Time</th>
				</tr>
			</thead>
			<tbody>
				@if(isset($generalProblem))
					@foreach($generalProblem as $problem)
						<tr>
							<td>{{ $problem->id }}</td>
							<td>{{ $problem->problem }}</td>
							<td>{{ $problem->reason }}</td>
							<td>{{ $problem->description }}</td>
							<td>{{ getFormattedDate($problem->updated_at) }}</td>
						</tr>
					@endforeach
				@else
					<tr>
						<td colspan="4">No Data Available</td>
					</tr>
				@endif
			</tbody>
		</table>
	</div>
</div>

@stop

@section('script')

<script type="text/javascript">
    $(document).ready(function(){
        $(".tablesorter").tablesorter();
    });
</script>

@stop