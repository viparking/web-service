@extends('admin')

@section('content')

<?php $data = $level->data; ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Admin Level 
        </h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<table class="table table-hover table-striped">
			<thead>
				<tr>
					<th>ID</th>
					<th>AdminLevel</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
        <?php if(count($data) == 0) { ?>
          <tr>
            <td colspan="3">No Data Available</td>
          </tr>
        <?php } ?>
				@foreach ($data as $adminLevel)
					<tr>
						<td>{{$adminLevel->id}}</td>
						<td>{{$adminLevel->adminLevel}}</td>
						<td>
							<a href="<?= URL::to('admin-level/edit/'.$adminLevel->id) ?>"><button class="btn btn-default">Update</button></a>
							<button class="btn btn-danger" onclick="prepareDelete('{{ $adminLevel->id }}')">Delete</button>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Confirmation</h4>
      </div>
      <div class="modal-body">
        <p id="msg">Are you sure want to delete this admin level?</p>
      </div>
      <div class="modal-footer">
      <form action="<?= URL::to('admin-level/delete') ?>" method="post">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        	<input type="hidden" name="id" id="adminLevelID">
	        <button type="submit" class="btn btn-danger">Delete</button>
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@stop

@section('script')

<script type="text/javascript">
	function prepareDelete(id)
	{
		$("#adminLevelID").val(id);
		$("#myModal").modal('show');
	}
</script>

@stop