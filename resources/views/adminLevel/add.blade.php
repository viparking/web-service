@extends('admin')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= isset($adminLevel) ? "Edit" : "Add"; ?> Admin Level</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<form method="post" action="<?= URL::to('admin-level/post') ?>">
			<?php if(isset($adminLevel)) { ?>
				<input type="hidden" name="id" value="{{$adminLevel->id}}">
			<?php } ?>
			<div class="form-group">
				<div class="row">
					<div class="col-md-3">
						<label>Admin Level</label>
					</div>
					<div class="col-md-9">
						<input type="text" name="adminLevel" required="required" class="form-control" value="<?= isset($adminLevel)?$adminLevel->adminLevel:"" ?>">
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<button class="btn btn-danger col-md-6" type="reset">Reset</button>
					<button class="btn btn-primary col-md-6" type="submit">Submit</button>
				</div>
			</div>
		</form>
	</div>
</div>

@stop