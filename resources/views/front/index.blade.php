<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>ViParking</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script src="<?= URL::to('js/slippry.min.js') ?>"></script>
<link rel="stylesheet" type="text/css" href="<?= URL::to('css/viparking.css') ?>" />
<link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700' rel='stylesheet' type='text/css'>

</head>

<body onload="initialize()">
  <div align="center">
    <div class="bg-mobile-indexpage">
      <div class="mobile-show">
        <a href="<?= URL::to('/') ?>"><img src="<?= URL::to('img/logo-viparking.png') ?>"></a>
      </div>   
    </div>  
    <div align="center" class="cangkang">
      <div class="box-absolute">
        <div class="make-it-center"> 
          <a href="<?= URL::to('/') ?>"><div class="header-left"></div></a>
          <div class="navnav">
            <ul align="right" class="nav">
                <li onclick=""><a class="btn">&#9776;</a>
                    <ul class="menu">
                        <li><a href="<?= URL::to('/') ?>" class="active">Home</a></li>
                        <li><a href="<?= URL::to('/about') ?>">About Us</a></li>
                        <li><a href="<?= URL::to('/product') ?>">Product</a></li> 
                        <li><a href="<?= URL::to('/contact') ?>">Contact Us</a></li> 
                        @if(Auth::user() != null)<li><a href="<?= URL::to('user/history') ?>">History</a></li>
                          <li><a href="<?= URL::to('/userLogout') ?>">Logout</a></li>  @else
                        <li><a href="<?= URL::to('/login') ?>">Login</a></li>@endif
                    </ul>         
                </li>
            </ul>   
          </div>  
        </div>
      </div>
    </div>
  

    <section class="demo_wrapper">
      <article class="demo_block">
        <ul id="demo1">
          <li><a href="#slide1"><img src="<?= URL::to('img/slide01.jpg') ?>" alt="Integrated Parking System"></a></li>
          <li><a href="#slide2"><img src="<?= URL::to('img/slide02.jpg') ?>" alt="Connect Each Other"></a></li>
          <!--<li><a href="#slide3"><img src="static/img/slide03.jpg" alt="Surgical"></a></li>
          <li><a href="#slide4"><img src="static/img/slide04.jpg" alt="Critical Care"></a></li>-->
        </ul>
      </article>
    </section>    

    <script>
      $(function() {
        var demo1 = $("#demo1").slippry({
        });
      });
    </script>

   <div class="cangkang">

        <div align="center" class="four-side-content">
            <h4>OUR SERVICES</h4>
            <h5>Learn more about our services</h5>

            <div align="center" class="four-side-left">
                <figure><img src="<?= URL::to('img/icon-01.png') ?>"></figure>
                <h3>Automatic Parking System</h3>
                <!--<h4>We provided the most sophisticated solution yet cost</h4>-->
            </div>          
            <div align="center" class="four-side-left">
                <figure><img src="<?= URL::to('img/icon-02.png') ?>"></figure>
                <h3>Auto Tracking Parking System</h3>
                <!--<h4>We provided the most sophisticated solution yet cost</h4>-->
            </div>
            <div align="center" class="four-side-left">
                <figure><img src="<?= URL::to('img/icon-03.png') ?>"></figure>
                <h3>Mobile Apps Integration System</h3>
                <!--<h4>We provided the most sophisticated solution yet cost</h4>-->
            </div>          
            <div align="center" class="four-side-left">
                <figure><img src="<?= URL::to('img/icon-04.png') ?>"></figure>
                <h3>Integrated Human Resource</h3>
                <!--<h4>We provided the most sophisticated solution yet cost</h4>-->
            </div>         
            <div class="cf"></div>
        </div> 
    </div>

    <div align="center" class="bg-gray quote">
      <h4>"Supported with the best resources, we’d like to invite you to experience the new experiences with us."</h4>
      <p><span>William Winarto</span> - ViParking Owner</p>
    </div>

    <div class="cangkang">    
        <div align="center" class="four-side-content">
          <h4>SAFE &amp; SECURE</h4>
          <h5>Learn how SAFE our system is</h5>

          <div class="contact-us-left bg-gray" style="height:360px;">
              
          </div> 

          <div align="center" class="contact-us-right">
            <a href=""><p>Download <br>Vision Parking Apps</p></a>
            <a href=""><img src="<?= URL::to('img/icon-apps.png') ?>"></a>
            
            <p>Available on:</p>
            <a href=""><img src="<?= URL::to('img/google-play-button.png') ?>"></a>
            <a href=""><img src="<?= URL::to('img/apps-store-button.png') ?>"></a>
          </div>   
          <div class="cf"></div>                 
        </div>

    </div>
  
    <div align="left" class="footer">
        <div class="footer-left">
          <img src="<?= URL::to('img/logo-mkg.png') ?>" width="100">
          
        </div>  
        <div class="footer-middle">
          <p>PT Magnivisi Kreasi Gemilang is a national scale company headquartered in North Jakarta. This company core business is in Information Technology. In accordance with the motto easy cozy simply, We run all the business line using efficient technology which believed can helps their customers in many ways.</p>
        </div>  
        <div class="footer-middle">
          <a href="<?= URL::to('/') ?>">Home</a><br>
          <a href="<?= URL::to('/about') ?>">About Us</a><br>
          <a href="<?= URL::to('/product') ?>">Product</a><br>
          <a href="<?= URL::to('contact') ?>">Contact Us</a><br>
          @if(Auth::user()!=null)<a href="<?= URL::to('user/history') ?>">Transaction History</a><br>@else
          <a href="<?= URL::to('login') ?>">Login</a><br>@endif
        </div>  
        <div class="footer-right">
          <p><span> 
              PT Magnivisi Kreasi Gemilang</span><br>
              Regus Grha Sentra<br>
              5th Floor, Grha Sentra Building<br>
              Jl. Agung Perkasa IX Blok K-1 No.26-27 <br> Sunter, Jakarta Utara 14350<br>
              TEL (62) 21 2963 8211 <br>
              FAX (62) 21 2963 8201</p>
        </div>      
        <div class="cf"></div>
    </div>        


</div>

</body>
</html>
