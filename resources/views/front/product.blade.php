<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>ViParking</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="<?= URL::to('css/viparking.css') ?>" />
<link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700' rel='stylesheet' type='text/css'>

</head>

<body onload="initialize()">
  <div align="center">
    <div class="bg-template">
      <div class="mobile-show">
        <a href="<?= URL::to('/') ?>"><img src="<?= URL::to('img/logo-viparking.png') ?>"></a>
      </div> 
      <div align="center" class="cangkang">
        <div class="box-absolute">
          <div class="make-it-center"> 
            <a href="<?= URL::to('/') ?>"><div class="header-left"></div></a>
            <div class="navnav">
              <ul align="right" class="nav">
                  <li onclick=""><a class="btn">&#9776;</a>
                      <ul class="menu">
                          <li><a href="<?= URL::to('/') ?>">Home</a></li>
                        <li><a href="<?= URL::to('/about') ?>">About Us</a></li>
                        <li><a href="<?= URL::to('/product') ?>" class="active">Product</a></li> 
                        <li><a href="<?= URL::to('/contact') ?>">Contact Us</a></li>  
                        @if(Auth::user() != null)<li><a href="<?= URL::to('user/history') ?>">History</a></li>
                          <li><a href="<?= URL::to('/userLogout') ?>">Logout</a></li>  @else
                        <li><a href="<?= URL::to('/login') ?>">Login</a></li>@endif
                      </ul>         
                  </li>
              </ul>   
            </div>  
          </div>
        </div>
      </div>
    
    </div>

    <div class="cangkang">
        <div class="breadcrumb mobile-show">Product</div>
        
        <div align="center" class="three-side-content">
            <div align="center" class="three-side-left">
                <figure><img src="<?= URL::to('img/integrated.jpg') ?>"></figure>
                <h2>Viparking Integrated Parking Systems</h2>
                <h4>This system will help our customer to access buildings which use our services by using one intelligent ID card.</h4>
            </div>          
            <div align="center" class="three-side-left">
                <figure><img src="<?= URL::to('img/valet.jpg') ?>"></figure>
                <h2>ViParking Valet Intelligent Parking</h2>
                <h4>We develop technology that will facilitate our loyal valet parking user to access their vehicle, to reserve parking lot, and many more.</h4>
            </div>
            <div align="center" class="three-side-left">
                <figure><img src="<?= URL::to('img/rfid.jpg') ?>"></figure>
                <h2>RFID Technology</h2>
                <h4>This long range RFID technology will help driver to ride into building and open the entrance barrier gate without tagging a card.</h4>
            </div>
            <div class="cf"></div>       
            <div align="center" class="three-side-left">
                <figure><img src="<?= URL::to('img/consulting.jpg') ?>"></figure>
                <h2>Parking Management Consulting</h2>
                <h4>This consultation services will help our partners to maximizing the parking area potency and will help its customers to get the best services.</h4>
            </div>          
            <div align="center" class="three-side-left">
                <figure><img src="<?= URL::to('img/history.jpg') ?>"></figure>
                <h2>History Recall</h2>
                <h4>In our system, you can check your transaction history within certain time period, send the e-Billing Statement to email and print it.</h4>
            </div>
            <div align="center" class="three-side-left">
                <figure><img src="<?= URL::to('img/apps.jpg') ?>"></figure>
                <h2>ViParking Mobile Apps</h2>
                <h4>We also develop ViParking Mobile Apps to add more options for customer to check their transaction history. </h4>
            </div> 
            <div class="cf"></div>
            <div align="center" class="three-side-left">
                <figure><img src="<?= URL::to('img/payment.jpg') ?>"></figure>
                <h2>ViParking Virtual Payment</h2>
                <h4>By using Virtual Payment, we are targeting more efficient payment time at the exit gate. </h4>
            </div>
            <div class="cf"></div> 
        </div> 
    </div>

    <div align="left" class="footer">
        <div class="footer-left">
          <img src="<?= URL::to('img/logo-mkg.png') ?>" width="100">
          
        </div>  
        <div class="footer-middle">
          <p>PT Magnivisi Kreasi Gemilang is a national scale company headquartered in North Jakarta. This company core business is in Information Technology. In accordance with the motto easy cozy simply, We run all the business line using efficient technology which believed can helps their customers in many ways.</p>
        </div>  
        <div class="footer-middle">
          <a href="<?= URL::to('/') ?>">Home</a><br>
          <a href="<?= URL::to('/about') ?>">About Us</a><br>
          <a href="<?= URL::to('/product') ?>">Product</a><br>
          <a href="<?= URL::to('contact') ?>">Contact Us</a><br>
          @if(Auth::user()!=null)<a href="<?= URL::to('user/history') ?>">Transaction History</a><br>@else
          <a href="<?= URL::to('login') ?>">Login</a><br>@endif
        </div>  
        <div class="footer-right">
          <p><span> 
              PT Magnivisi Kreasi Gemilang</span><br>
              Regus Grha Sentra<br>
              5th Floor, Grha Sentra Building<br>
              Jl. Agung Perkasa IX Blok K-1 No.26-27 <br> Sunter, Jakarta Utara 14350<br>
              TEL (62) 21 2963 8211 <br>
              FAX (62) 21 2963 8201</p>
        </div>      
        <div class="cf"></div>
    </div>        


</div>

</body>
</html>
