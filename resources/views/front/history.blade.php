<?php function getFormattedDate($timeString)
    {
        $t = strtotime($timeString);
        $ret = "";
        $ret = date('H:i d-M-Y',$t);
        return $ret;
    }
    function getDateString($timeString)
    {
        $t = strtotime($timeString);
        $ret = "";
        $ret = date('d-M-Y',$t);
        return $ret;
    }
    function getTimeString($timeString)
    {
        $t = strtotime($timeString);
        $ret = "";
        $ret = date('H:i',$t);
        return $ret;
    }
    function makeMoney($input)
    {
        $ret = "Rp " . number_format($input,2,',','.');
        return $ret;
    } ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>ViParking</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="<?= URL::to('css/viparking.css') ?>" />
<link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
  
  <script>
  $(document).ready(function() {
    $("#datepicker").datepicker();
    $("#datepicker2").datepicker();
  });
  </script>
  <style type="text/css">
    .pagination {
      list-style: none;
      display: inline;
      width:auto!important;
    }
    .pagination li {
      display: inline!important;
      margin-left: 1em!important;
    }
  </style>
</head>

<body onload="initialize()">
  <div align="center">
    <div class="bg-template">
      <div class="mobile-show">
        <a href="<?= URL::to('/') ?>"><img src="static/img/logo-viparking.png"></a>
      </div> 

      <div align="center" class="cangkang">
        <div class="box-absolute">
          <div class="make-it-center">
              <a href="<?= URL::to('/') ?>"><div class="header-left"></div></a>
            <div class="navnav">
              <ul align="right" class="nav">
                  <li onclick=""><a class="btn">&#9776;</a>
                      <ul class="menu">
                          <li><a href="<?= URL::to('/') ?>">Home</a></li>
                          <li><a href="<?= URL::to('/about') ?>">About Us</a></li>
                          <li><a href="<?= URL::to('/product') ?>">Product</a></li> 
                          <li><a href="<?= URL::to('/contact') ?>">Contact Us</a></li>  
                          @if(Auth::user() != null)<li><a href="<?= URL::to('user/history') ?>" class="active">History</a></li>
                          <li><a href="<?= URL::to('/userLogout') ?>">Logout</a></li>  @else
                        <li><a href="<?= URL::to('/login') ?>">Login</a></li>@endif
                      </ul>         
                  </li>
              </ul>   
            </div>  
          </div>
        </div>
      </div>
    
    </div>

    <div align="center" class="cangkang about">
        <div class="breadcrumb mobile-show">Transaction History</div>
        <form class="contact" action="<?= URL::to('user/history') ?>" method="get" id="contactForm">
          <div class="half">
            <p>Start from:</p><input id="datepicker" name="startTime" onchange="dateChanged()" value="{{ $startTime }}" />
          </div>
          <div class="half">
            <p>To:</p><input id="datepicker2" name="endTime" onchange="dateChanged()" value="{{ $endTime }}" />
          </div>
          <div class="cf"></div>
        </form>
        
        <div class="history">
          <!--start::table-header-->
            <h4 class="table-header column1">Tanggal</h4>
            <h4 class="table-header column2">Tempat</h4>
            <h4 class="table-header column3">Waktu</h4>
            <h4 class="table-header column4">Durasi</h4>
            <h4 class="table-header column5">Harga</h4>
            <div class="cf"></div>
          <!--end::table-header-->

          @foreach($history as $h)
            <?php $inTime = Carbon\Carbon::parse($h->inTime);
              $outTime = Carbon\Carbon::parse($h->outTime); ?>
            <div class="table-row">
              <span class="mobile-show">Tanggal</span>
              <h4 class="column1">{{ getDateString($h->inTime) }}</h4>
              <span class="mobile-show">Tempat</span>
              <h4 class="column2">{{ $h->name }}</h4>
              <span class="mobile-show">Waktu</span>
              <h4 class="column3">{{ getTimeString($h->inTime) }}-{{ getTimeString($h->outTime) }}</h4>
              <span class="mobile-show">Durasi</span>
              <h4 class="column4">{{ $inTime->diffInHours($outTime, false) }} jam {{ $inTime->diffInMinutes($outTime) % 60 }} menit</h4>
              <span class="mobile-show">Harga</span>
              <h4 class="column5">{{ makeMoney($h->price) }}</h4>
              <div class="cf"></div>
            </div>
          @endforeach

        </div>
        
        <div>
          {{ $history->render() }}
        </div>
    </div>

  

    <div align="left" class="footer">
        <div class="footer-left">
          <img src="<?= URL::to('img/logo-mkg.png') ?>" width="100">
          
        </div>  
        <div class="footer-middle">
          <p>PT Magnivisi Kreasi Gemilang is a national scale company headquartered in North Jakarta. This company core business is in Information Technology. In accordance with the motto easy cozy simply, We run all the business line using efficient technology which believed can helps their customers in many ways.</p>
        </div>  
        <div class="footer-middle">
          <a href="<?= URL::to('/') ?>">Home</a><br>
          <a href="<?= URL::to('/about') ?>">About Us</a><br>
          <a href="<?= URL::to('/product') ?>">Product</a><br>
          <a href="<?= URL::to('contact') ?>">Contact Us</a><br>
          @if(Auth::user()!=null)<a href="<?= URL::to('user/history') ?>">Transaction History</a><br>@else
          <a href="<?= URL::to('login') ?>">Login</a><br>@endif
        </div>  
        <div class="footer-right">
          <p><span> 
              PT Magnivisi Kreasi Gemilang</span><br>
              Regus Grha Sentra<br>
              5th Floor, Grha Sentra Building<br>
              Jl. Agung Perkasa IX Blok K-1 No.26-27 <br> Sunter, Jakarta Utara 14350<br>
              TEL (62) 21 2963 8211 <br>
              FAX (62) 21 2963 8201</p>
        </div>      
        <div class="cf"></div>
    </div>     


</div>
<script type="text/javascript">
  function dateChanged(){
    if($("#datepicker").val() != "" && $("#datepicker2").val() != "")
      $("#contactForm").submit();
  }
</script>
</body>
</html>
