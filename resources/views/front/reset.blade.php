<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>ViParking</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="static/css/viparking.css" />
<link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700' rel='stylesheet' type='text/css'>

</head>

<body onload="initialize()">
  <div align="center">
    <div class="bg-login">
      <div align="center" class="cangkang">
        <div class="box-absolute">
          <div class="make-it-center">
              <a href="<?= URL::to('/') ?>"><div class="header-left"></div></a>
            <div class="navnav">
              <ul align="right" class="nav">
                  <li onclick=""><a class="btn">&#9776;</a>
                      <ul class="menu">
                          <li><a href="<?= URL::to('/') ?>" class="active">Home</a></li>
                        <li><a href="<?= URL::to('/about') ?>">About Us</a></li>
                        <li><a href="<?= URL::to('/product') ?>">Product</a></li> 
                        <li><a href="<?= URL::to('/contact') ?>">Contact Us</a></li>  
                        @if(Auth::user() != null)<li><a href="<?= URL::to('user/history') ?>">History</a></li>
                          <li><a href="<?= URL::to('/userLogout') ?>">Logout</a></li>  @else
                        <li><a href="<?= URL::to('/login') ?>">Login</a></li>@endif
                      </ul>         
                  </li>
              </ul>   
            </div>  
          </div>
        </div>
      </div>

      <div class="mobile-show">
        <a href="<?= URL::to('/') ?>"><img src="<?= URL::to('img/logo-viparking.png') ?>"></a>
      </div>

      <br>
      <div class="login-rules">
        <h4>Reset Password</h4>
        <p>Your password has been successfully reset. <br> Please check your inbox. If you haven't receive our mail, click <a href="#">here</a> to resend.</p>
      </div>  
    
    </div>

    <div align="left" class="footer">
        <div class="footer-left">
          <img src="<?= URL::to('img/logo-mkg.png') ?>" width="100">
          
        </div>  
        <div class="footer-middle">
          <p>PT Magnivisi Kreasi Gemilang is a national scale company headquartered in North Jakarta. This company core business is in Information Technology. In accordance with the motto easy cozy simply, We run all the business line using efficient technology which believed can helps their customers in many ways.</p>
        </div>  
        <div class="footer-middle">
          <a href="<?= URL::to('/') ?>">Home</a><br>
          <a href="<?= URL::to('/about') ?>">About Us</a><br>
          <a href="<?= URL::to('/product') ?>">Product</a><br>
          <a href="<?= URL::to('contact') ?>">Contact Us</a><br>
          @if(Auth::user()!=null)<a href="<?= URL::to('user/history') ?>">Transaction History</a><br>@else
          <a href="<?= URL::to('login') ?>">Login</a><br>@endif
        </div>  
        <div class="footer-right">
          <p><span> 
              PT Magnivisi Kreasi Gemilang</span><br>
              Regus Grha Sentra<br>
              5th Floor, Grha Sentra Building<br>
              Jl. Agung Perkasa IX Blok K-1 No.26-27 <br> Sunter, Jakarta Utara 14350<br>
              TEL (62) 21 2963 8211 <br>
              FAX (62) 21 2963 8201</p>
        </div>      
        <div class="cf"></div>
    </div>        


</div>

</body>
</html>
