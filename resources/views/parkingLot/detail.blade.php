@extends('admin')

@section('content')

<?php 
	$parkingLot = $parkingLot->data; 
	$gates = $gates->data;
	$fines = $fines->data;
	$prices = $prices->data;
	$pending = $pending->items();
	$parkingData = $parking->items();
	function getFormattedDate($timeString)
    {
        $t = strtotime($timeString);
        $ret = "";
        $ret = date('H:i d-M-Y',$t);
        return $ret;
    }
    function makeMoney($input)
    {
        $ret = "Rp " . number_format($input,2,',','.');
        return $ret;
    }
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Parking Lot - {{ $parkingLot->name }}
            <button class="btn btn-danger pull-right" onclick="$('#deleteParkingLotModal').modal('show');" style="margin-left:5px">Delete</button> 
            <a href="{{ URL::to('parkingLot/'.$parkingLot->id.'/edit') }}">
            	<button class="btn btn-default pull-right">Update</button>
            </a>
        </h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="col-md-6">
			<table>
				<tr>
					<td><h4>Parking Lot ID</h4></td>
					<td>:</td>
					<td>{{ $parkingLot->id }}</td>
				</tr>
				<tr>
					<td><h4>Name</h4></td>
					<td>:</td>
					<td>{{ $parkingLot->name }}</td>
				</tr>
				<tr>
					<td><h4>Latitude</h4></td>
					<td>:</td>
					<td>{{ $parkingLot->latitude }}</td>
				</tr>
				<tr>
					<td><h4>Longitude</h4></td>
					<td>:</td>
					<td>{{ $parkingLot->longitude }}</td>
				</tr>
				<tr>
					<td><h4>Address</h4></td>
					<td>:</td>
					<td>{{ $parkingLot->address }}</td>
				</tr>
			</table>
		</div>

		<!-- Gates table -->
		<div class="col-md-6">
			<h2 style="text-align: center">Gates
				<a href="{{ URL::to('parkingLot/'.$parkingLot->id.'/gate/add') }}">
	                <button class="btn btn-primary pull-right">
	                    <span class="fa fa-plus"></span> Add Gate
	                </button>
	            </a> 
			</h2>
			<table class="table table-hover table-striped tablesorter">
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Type</th>
						<th style="width:33%">Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($gates as $gate)
						<tr>
							<td>{{ $gate->id }}</td>
							<td>{{ $gate->name }}</td>
							<td>{{ $gate->type }}</td>
							<td>
								<a href="<?= URL::to('gate/'.$gate->id.'/edit') ?>"><button class="btn btn-default">Update</button></a>
								<button class="btn btn-danger" onclick="prepareDelete('{{ $gate->id }}')">Delete</button>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>

	<!-- Fines table -->
	<div class="col-md-12">
		<h2 style="text-align: center">Fines
			<a href="<?= URL::to('parkingLot/'.$parkingLot->id.'/fine/add') ?>">
                <button class="btn btn-primary pull-right">
                    <span class="fa fa-plus"></span> Set Fine
                </button>
            </a> 
		</h2>
		<table class="table table-hover table-striped tablesorter">
			<thead>
				<tr>
					<th>ID</th>
					<th>Problem</th>
					<th>Description</th>
					<th>Local Fine</th>
					<th>Global Fine</th>
					<th>Parking Related?</th>
				</tr>
			</thead>
			<tbody>
				@foreach($fines as $fine)
					<tr>
						<td>{{ $fine->id }}</td>
						<td>{{ $fine->problem }}</td>
						<td>{{ $fine->description }}</td>
						<td>{{ makeMoney($fine->localFine) }},-</td>
						<td>{{ makeMoney($fine->globalFine) }},-</td>
						<td>{{ $fine->isParkingRelated == 1 ? 'Yes' : 'No' }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>

	<!-- Price table -->
	<div class="col-md-12">
		<h2 style="text-align: center">Price
			<a href="<?= URL::to('parkingLot/'.$parkingLot->id.'/price/add') ?>">
                <button class="btn btn-primary pull-right">
                    <span class="fa fa-plus"></span> Add Price
                </button>
            </a> 
		</h2>
		<table class="table table-hover table-striped tablesorter">
			<thead>
				<tr>
					<th>Vehicle Type</th>
					<th>Initial Price</th>
					<th>Initial Length</th>
					<th>Free Duration</th>
					<th>Price</th>
					<th>Overnight Price</th>
					<th>Max Price</th>
					<th>Free Start</th>
					<th>Free End</th>
				</tr>
			</thead>
			<tbody>
				@foreach($prices as $price)
					<tr>
						<td>{{ $price->vehicleType }}</td>
						<td>{{ makeMoney($price->initialPrice) }},-</td>
						<td>{{ $price->initialLength }}</td>
						<td>{{ $price->freeDuration }}</td>
						<td>{{ makeMoney($price->price) }},-</td>
						<td>{{ makeMoney($price->overnightPrice) }},-</td>
						<td>{{ makeMoney($price->maxPrice) }},-</td>
						<td>{{ $price->freeStart }}</td>
						<td>{{ $price->freeEnd }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>

	<!-- Pending table -->
	<div class="col-md-12">
		<h2 style="text-align: center">Pending</h2>
		<table class="table table-hover table-striped tablesorter">
			<thead>
				<tr>
					<th>ID</th>
					<th>Username</th>
					<th>Time In</th>
					<th>Time Out</th>
					<th>Gate In</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($pending as $item)
					<tr>
						<td>{{ $item->id }}</td>
						<td>{{ $item->username }}</td>
						<td>{{ getFormattedDate($item->inTime) }}</td>
						<td>{{ getFormattedDate($item->outTime) }}</td>
						<td>{{ $item->inGateName }}</td>
						<td>
							<a href="<?= URL::to('parking/'.$parkingLot->id.'/'.$item->id.'/edit') ?>"><button class="btn btn-default">Edit</button></a>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>

	<!-- Parking table -->
	<div class="col-md-12">
		<h2 style="text-align: center">Parking
			<a href="<?= URL::to('parkingLot/'.$parkingLot->id.'/parking/add') ?>">
                <button class="btn btn-primary pull-right">
                    <span class="fa fa-plus"></span> Add Parking
                </button>
            </a> 
		</h2>
		<table class="table table-hover table-striped tablesorter">
			<thead>
				<tr>
					<th>ID</th>
					<th>Plate Number</th>
					<th>Payment Type</th>
					<th>Username</th>
					<th>Price</th>
					<th>Time In</th>
					<th>Time Out</th>
					<th>Gate In</th>
					<th>Gate Out</th>
				</tr>
			</thead>
			<tbody>
				@foreach($parkingData as $item)
					<tr onclick="document.location='{{ URL::to('parkingLot/'.$parkingLot->id.'/parking/'.$item->id.'/detail') }}'" style="cursor:pointer">
						<td>{{ $item->id }}</td>
						<td>{{ $item->plateNumber }}</td>
						<td>{{ $item->paymentType }}</td>
						<td>{{ $item->username }}</td>
						<td>{{ makeMoney($item->price) }}</td>
						<td>{{ getFormattedDate($item->inTime) }}</td>
						<td>{{ getFormattedDate($item->outTime) }}</td>
						<td>{{ $item->inGateName }}</td>
						<td>{{ $item->outGateName }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
		<!-- Pagination -->
		<div class="pull-right">
			{{ $parking->render() }}
		</div>
	</div>
</div>

<div class="modal fade" id="deleteParkingLotModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                <p id="msg">Are you sure want to delete this parking lot?</p>
            </div>
            <div class="modal-footer">
                <form action="{{ URL::to('parkingLot/delete') }}" method="post">
	                <input type="hidden" name="id" value="{{ $parkingLot->id }}">
	                <input type="hidden" name="companyID" value="{{ $parkingLot->company_id }}">
	                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	                <button type="submit" class="btn btn-danger">Delete</button>
	            </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="deleteGateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                <p id="msg">Are you sure want to delete this gate?</p>
            </div>
            <div class="modal-footer">
                <form action="{{ URL::to('gate/delete') }}" method="post">
	                <input type="hidden" name="id" id="gateID">
	                <input type="hidden" name="parkingLotID" value="{{ $parkingLot->id }}">
	                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	                <button type="submit" class="btn btn-danger">Delete</button>
	            </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@stop

@section('script')

<script>
	function prepareDelete(id)
	{
		$("#gateID").val(id);
		$("#deleteGateModal").modal('show');
	}

	$(document).ready(function(){
	    $(".tablesorter").tablesorter({
	        headers: {5: {sorter: false}}
	    });
	});
</script>

@stop