@extends('admin')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= isset($parkingLot) ? "Edit" : "Add New"; ?> Parking Lot</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<form method="post" action="<?= URL::to('parkingLot/post') ?>">
			@if (isset($company))
				<input type="hidden" name="companyID" value="{{ $company->id }}">
			@endif

			@if (isset($parkingLot))
				<input type="hidden" name="companyID" value="{{ $parkingLot->company_id }}">
				<input type="hidden" name="id" value="{{ $parkingLot->id }}">
			@endif
			
			<div class="form-group">
				<div class="row">
					<div class="col-md-3">
						<label>Company Name</label>
					</div>
					<div class="col-md-9">
						<input type="text" required="required" class="form-control" value="{{ isset($company) ? $company->name : "" }}" disabled="disabled">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-3">
						<label>Name</label>
					</div>
					<div class="col-md-9">
						<input type="text" name="name" required="required" class="form-control" value="{{ isset($parkingLot) ? $parkingLot->name : "" }}">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-3">
						<label>Latitude</label>
					</div>
					<div class="col-md-9">
						<input type="text" name="latitude" required="required" class="form-control" value="{{ isset($parkingLot) ? $parkingLot->latitude : "" }}">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-3">
						<label>Longitude</label>
					</div>
					<div class="col-md-9">
						<input type="text" name="longitude" required="required" class="form-control" value="{{ isset($parkingLot) ? $parkingLot->longitude : "" }}">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-3">
						<label>Address</label>
					</div>
					<div class="col-md-9">
						<input type="text" name="address" required="required" class="form-control" value="{{ isset($parkingLot) ? $parkingLot->address : "" }}">
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="pull-right">
					<button class="btn btn-danger" type="reset">Reset</button> 
					<button class="btn btn-primary" type="submit">Submit</button>
				</div>
			</div>
		</form>
	</div>
</div>

@stop