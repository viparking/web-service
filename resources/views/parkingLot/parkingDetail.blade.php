@extends('admin')

@section('content')

<?php $parking = $parking->data;
	function makeMoney($input)
    {
        $ret = "Rp " . number_format($input,2,',','.');
        return $ret;
    }
    function getFormattedDate($timeString)
    {
        $t = strtotime($timeString);
        $ret = "";
        $ret = date('H:i d-M-Y',$t);
        return $ret;
    } ?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Parking Detail</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<table>
			<tr>
				<td><h4>In Time</h4></td>
				<td>:</td>
				<td><?= getFormattedDate($parking->inTime) ?></td>
			</tr>
			<tr>
				<td><h4>In Gate</h4></td>
				<td>:</td>
				<td><?= $parking->inGate ?></td>
			</tr>
			<tr>
				<td><h4>Out Time</h4></td>
				<td>:</td>
				<td><?= getFormattedDate($parking->outTime) ?></td>
			</tr>
			<tr>
				<td><h4>Out Gate</h4></td>
				<td>:</td>
				<td><?= $parking->outGate ?></td>
			</tr>
			<tr>
				<td><h4>Price</h4></td>
				<td>:</td>
				<td><?= makeMoney($parking->price) ?></td>
			</tr>
			<tr>
				<td><h4>Payment Type</h4></td>
				<td>:</td>
				<td><?= $parking->paymentType ?></td>
			</tr>
			<tr>
				<td><h4>Company Name</h4></td>
				<td>:</td>
				<td><?= $parking->companyName ?></td>
			</tr>
			<tr>
				<td><h4>Parking Lot Name</h4></td>
				<td>:</td>
				<td><?= $parking->parkingLotName ?></td>
			</tr>
			<tr>
				<td><h4>Username</h4></td>
				<td>:</td>
				<td><?= $parking->username ?></td>
			</tr>
			<tr>
				<td><h4>Plate Number</h4></td>
				<td>:</td>
				<td><?= $parking->plateNumber ?></td>
			</tr>
			<tr>
				<td><h4>Brand</h4></td>
				<td>:</td>
				<td><?= $parking->brand ?></td>
			</tr>
			<tr>
				<td><h4>Type</h4></td>
				<td>:</td>
				<td><?= $parking->type ?></td>
			</tr>
			<tr>
				<td><h4>Color</h4></td>
				<td>:</td>
				<td><?= $parking->color ?></td>
			</tr>
		</table>
	</div>
</div>

@stop