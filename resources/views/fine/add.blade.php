@extends('admin')

@section('content')

<?php 
	$problems = $problems->data;
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Set Fine</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<table class="table table-hover table-striped tablesorter">
			<thead>
				<tr>
					<th>Problem</th>
					<th>Description</th>
					<th>Global Fine</th>
					<th>Parking Related?</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($problems as $problem)
					<tr>
						<td>{{ $problem->problem }}</td>
						<td>{{ $problem->description }}</td>
						<td>Rp. {{ $problem->fine }},-</td>
						<td>{{ $problem->isParkingRelated == 1 ? 'Yes' : 'No' }}</td>
						<td>
							<button class="btn btn-primary" onclick="prepareSet('{{ $problem->id }}')">Set</button>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>

<div class="modal fade" id="setModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        	<form action="{{ URL::to('fine/post') }}" method="post">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                <h4 class="modal-title">Set Local Fine</h4>
	            </div>
	            <div class="modal-body">
	                <p id="msg">Fill local fine for this problem:</p>
	                <div class="form-group">
	                	<input type="text" name="fines" class="form-control">
	                </div>
	            </div>
	            <div class="modal-footer">
	                <input type="hidden" name="id" id="problemID">
	                <input type="hidden" name="parkingLotID" value="{{ $parkingLotID }}">
	                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	                <button type="submit" class="btn btn-primary">Submit</button>
	            </div>
	        </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@stop

@section('script')

<script>
	function prepareSet(id)
	{
		$("#problemID").val(id);
		$("#setModal").modal('show');
	}

	$(document).ready(function(){
	    $(".tablesorter").tablesorter({
	        headers: {5: {sorter: false}}
	    });
	});
</script>

@stop