<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<?= URL::to('bower_components/bootstrap/dist/css/bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?= URL::to('css/history.css')?>" rel="stylesheet">
    <?php function getFormattedDate($timeString)
    {
        $t = strtotime($timeString);
        $ret = "";
        $ret = date('H:i d-M-Y',$t);
        return $ret;
    } function makeMoney($input)
    {
        $ret = "Rp " . number_format($input,2,',','.');
        return $ret;
    } ?>
</head>
<body>
    <div class="container">
        <h1 style="text-align: center">Parking History for <?= $vehicle->plateNumber ?></h1>
        <br><br>
        <div class="col-md-10 col-md-offset-1">
            <center>
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th style="padding:8px">Location</th>
                            <th style="padding:8px">Price</th>
                            <th style="padding:8px">In Time</th>
                            <th style="padding:8px">Out Time</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($parking as $p)
                            <tr>
                                <td style="padding:8px"><?= $p->parkingLotName ?></td>
                                <td style="padding:8px"><?= makeMoney($p->price) ?></td>
                                <td style="padding:8px"><?= getFormattedDate($p->inTime) ?></td>
                                <td style="padding:8px"><?= getFormattedDate($p->outTime) ?></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </center>
        </div>
    </div>
</body>
<script src="<?= URL::to('bower_components/jquery/dist/jquery.min.js') ?>"></script>
<script src="<?= URL::to('bower_components/bootstrap/dist/js/bootstrap.min.js') ?>"></script>

</html>