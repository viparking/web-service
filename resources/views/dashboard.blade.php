@extends('admin')

@section('style')
    <link href="<?= URL::to('bower_components/morrisjs/morris.css'); ?>" rel="stylesheet">
@stop

@section('content')

    <?php
    $todayRevenue = $todayRevenue->data->summary;
    $weeklyProblem = $weeklyProblem->data;
    $parkingLotCapacity = $parkingLotCapacity->data;
    $weeklyRevenue = $weeklyRevenue->data->summary;
    $monthlyRevenue = $monthlyRevenue->data->summary;
    $monthlyParking = $monthlyParking->data->summary;
    $weeklyParking = $weeklyParking->data->summary;
    $pendingParking = $pendingParking->data;
    if($allTimeTopMember != null)
        $allTimeTopMember = $allTimeTopMember->data;
    else
        $allTimeTopMember = array();
    if($weeklyTopMember != null)
        $weeklyTopMember = $weeklyTopMember->data;
    else
        $weeklyTopMember = array();
    if($monthlyTopMember != null)
        $monthlyTopMember = $monthlyTopMember->data;
    else
        $monthlyTopMember = array();
    function makeMoney($input)
    {
        $ret = "Rp " . number_format($input,2,',','.');
        return $ret;
    }
    function getFormattedDate($timeString)
    {
        $t = strtotime($timeString);
        $ret = "";
        $ret = date('H:i d-M-Y',$t);
        return $ret;
    }
    ?>

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Dashboard</h1>
        </div>
    </div>

    <div class="row">
        <!-- Total Pending Parking -->
        <div class="col-lg-6 col-md-6">
            <div class="panel panel-red">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-clock-o fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{ isset($pendingParking) ? $pendingParking->pendingCount:'0' }}</div>
                            <div>Pending Parking</div>
                        </div>
                    </div>
                </div>
                <a href="#">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>

        <!-- Today Revenue -->
        <div class="col-lg-6 col-md-6">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{ makeMoney($todayRevenue->revenue) ? makeMoney($todayRevenue->revenue):'0' }}</div>
                            <div>Today Revenue</div>
                        </div>
                    </div>
                </div>
                <a href="#">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>

        {{-- <div class="col-md-12">
            <form method="get" class="form-inline">
                <div class="col-md-3">
                    <h4>Filter Report</h4>
                </div>
                <div class="col-md-3">
                    <label for="reportStart">From</label>
                    <input id="reportStart" class="form-control" style="width:80%!important" type="date" placeholder="Start Date" name="startDate">
                </div>
                <div class="col-md-3">
                    <label for="reportEnd">To</label>
                    <input id="reportEnd" class="form-control" type="date" placeholder="End Date" name="endDate">
                </div>
                <div class="col-md-3">
                    <button class="col-md-12 btn btn-default">Filter</button>
                </div>
            </form>
        </div> --}}
        <!-- Revenue -->
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i> <span id="lblRevenue">Revenue</span>
                    <div class="pull-right">
                        <div class="btn-group">
                            <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                <span id="btnRevenue">Actions</span>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu pull-right" role="menu">
                                <li><a onclick="monthlyRevenue()">Monthly Revenue</a>
                                </li>
                                <li><a onclick="weeklyRevenue()">Weekly Revenue</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div id="morris-area-chart"></div>
                </div>
                <!-- /.panel-body -->
            </div>
        </div>

        <!-- Parking -->
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-bar-chart-o fa-fw"></i> <span id="lblRevenue">Parking</span>
                    <div class="pull-right">
                        <div class="btn-group">
                            <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                <span id="btnParking">Actions</span>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu pull-right" role="menu">
                                <li><a onclick="monthlyParking()">Monthly Parking</a>
                                </li>
                                <li><a onclick="weeklyParking()">Weekly Parking</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div id="parkingChart"></div>
                </div>
                <!-- /.panel-body -->
            </div>
        </div>

        <!-- Parking Lot Capacity -->
        <div class="col-lg-6 col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong>Parking Lot Capacity</strong>
                </div>
                <div class="panel-body">
                    <table class="table table-hover table-striped tablesorter">
                        <thead>
                        <tr>
                            <th>Parking Lot Name</th>
                            <th>Company Name</th>
                            <th>Capacity</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($parkingLotCapacity as $capacity)
                            <tr>
                                <td><a href="<?= URL::to('parkingLot/'.$capacity->id.'/detail'); ?>"><?= $capacity->parkingLotName; ?></td>
                                <td><a href="<?= URL::to('company/'.$capacity->company_id.'/detail'); ?>"><?= $capacity->companyName; ?></td>
                                <td><?= $capacity->capacity; ?></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <!-- Weekly Problem -->
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Weekly Problem
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div id="weeklyProblemChart"></div>
                </div>
                <!-- /.panel-body -->
            </div>
        </div>

    @if($allTimeTopMember != NULL)
        <!-- All Time Top Member -->
            <div class="col-lg-12 col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong id="lblTopMember">All Time Top Member</strong>
                        <div class="pull-right">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                    <span id="btnTopMember">Actions</span>
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li><a onclick="topMember(1)">All Time</a>
                                    </li>
                                    <li><a onclick="topMember(2)">Monthly</a>
                                    </li>
                                    <li><a onclick="topMember(3)">Weekly</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table class="table table-hover table-striped tablesorter">
                            <thead>
                            <tr>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Total Transaction</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($allTimeTopMember as $item)
                                <tr class="allTimeTopMember" onclick="document.location='<?= URL::to("member/".$item->member_id."/detail"); ?>'" style="cursor: pointer">
                                    <td>{{ $item->username }}</td>
                                    <td>{{ $item->email }}</td>
                                    <td>{{ makeMoney($item->totalTransaction) }}</td>
                                </tr>
                            @endforeach
                            @foreach($weeklyTopMember as $item)
                                <tr class="weeklyTopMember" onclick="document.location='<?= URL::to("member/".$item->member_id."/detail"); ?>'" style="cursor: pointer">
                                    <td>{{ $item->username }}</td>
                                    <td>{{ $item->email }}</td>
                                    <td>{{ makeMoney($item->totalTransaction) }}</td>
                                </tr>
                            @endforeach
                            @foreach($monthlyTopMember as $item)
                                <tr class="monthlyTopMember" onclick="document.location='<?= URL::to("member/".$item->member_id."/detail"); ?>'" style="cursor: pointer">
                                    <td>{{ $item->username }}</td>
                                    <td>{{ $item->email }}</td>
                                    <td>{{ makeMoney($item->totalTransaction) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endif
    </div>

@stop

@section('script')

    <script src="<?= URL::to('bower_components/raphael/raphael-min.js'); ?>"></script>
    <script src="<?= URL::to('bower_components/morrisjs/morris.min.js'); ?>"></script>

    <script type="text/javascript">
        var revenueGraph;
        var parkingGraph;

        $(document).ready(function(){
            $(".tablesorter").tablesorter();
            topMember(1);

            revenueGraph = Morris.Area({
                element: 'morris-area-chart',
                xkey: 'period',
                ykeys: ['revenue'],
                labels: ['revenue'],
                pointSize: 2,
                hideHover: 'auto',
                resize: true
            });

            parkingGraph = Morris.Area({
                element: 'parkingChart',
                xkey: 'period',
                ykeys: ['parking'],
                labels: ['parking'],
                pointSize: 2,
                hideHover: 'auto',
                resize: true
            });

            monthlyRevenue();
            weeklyProblem();
            monthlyParking();
        });
        function topMember(q)
        {
            switch(q)
            {
                case 1:
                    $(".allTimeTopMember").show();
                    $(".weeklyTopMember").hide();
                    $(".monthlyTopMember").hide();
                    $("#lblTopMember").html("All Time Top Member");
                    $("#btnTopMember").html("All Time");
                    break;
                case 2:
                    $(".allTimeTopMember").hide();
                    $(".weeklyTopMember").hide();
                    $(".monthlyTopMember").show();
                    $("#lblTopMember").html("This Month Top Member");
                    $("#btnTopMember").html("Montly");
                    break;
                case 3:
                    $(".allTimeTopMember").hide();
                    $(".weeklyTopMember").show();
                    $(".monthlyTopMember").hide();
                    $("#lblTopMember").html("This Week Top Member");
                    $("#btnTopMember").html("Weekly");
                    break;
            }
        }

        function monthlyRevenue()
        {
            revenueGraph.setData([
                    @foreach($monthlyRevenue as $revenue)
                {
                    period: '<?= $revenue->year."-".$revenue->monthNumber; ?>',
                    revenue: <?= ($revenue->revenue); ?>
                },
                @endforeach
            ]);

            $("#lblRevenue").html("Monthly Revenue");
            $("#btnRevenue").html("Monthly");
        }

        function weeklyRevenue()
        {
            revenueGraph.setData([
                    @foreach($weeklyRevenue as $revenue)
                {
                    period: '<?= $revenue->year."-".$revenue->weekNumber; ?>',
                    revenue: <?= ($revenue->revenue); ?>
                },
                @endforeach
            ]);

            $("#lblRevenue").html("Weekly Revenue");
            $("#btnRevenue").html("Weekly");
        }

        function weeklyProblem()
        {
            Morris.Area({
                element: 'weeklyProblemChart',
                data: [
                        @foreach($weeklyProblem as $problem)
                    {
                        period: '<?= $problem->year."-".$problem->weekNumber; ?>',
                        GeneralProblem: <?= $problem->generalProblemCount; ?>,
                        ParkingProblem: <?= $problem->parkingProblemCount; ?>
                    },
                    @endforeach
                ],
                xkey: 'period',
                ykeys: ['GeneralProblem','ParkingProblem'],
                labels: ['General Problem','Parking Problem'],
                pointSize: 2,
                hideHover: 'auto',
                resize: true
            });
        }

        function weeklyParking()
        {
            parkingGraph.setData([
                    @foreach($weeklyParking as $parking)
                {
                    period: '<?= $parking->year."-".$parking->weekNumber; ?>',
                    parking: <?= ($parking->parking); ?>
                },
                @endforeach
            ]);

            $("#lblParking").html("Weekly Parking");
            $("#btnParking").html("Weekly");
        }

        function monthlyParking()
        {
            parkingGraph.setData([
                    @foreach($monthlyParking as $parking)
                {
                    period: '<?= $parking->year."-".$parking->monthNumber; ?>',
                    parking: <?= ($parking->parking); ?>
                },
                @endforeach
            ]);

            $("#lblParking").html("Monthly Parking");
            $("#btnParking").html("Monthly");
        }

    </script>

@stop