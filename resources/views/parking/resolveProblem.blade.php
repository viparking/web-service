@extends('admin')

@section('content')

<?php 
	$problem = $problem->data;
	$paymentTypes = $paymentTypes->data;

	function makeMoney($input)
    {
        $ret = "Rp " . number_format($input,2,',','.');
        return $ret;
    }
    function getFormattedDate($timeString)
    {
        $t = strtotime($timeString);
        $ret = "";
        $ret = date('H:i d-M-Y',$t);
        return $ret;
    }
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Parking Problem Payment</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<table>
			<tr>
				<td><h4>Problem Name</h4></td>
				<td style="width:5%;">:</td>
				<td>{{ $problem->problem }}</td>
			</tr>
			<tr>
				<td><h4>Problem Fine</h4></td>
				<td style="width:5%;">:</td>
				<td>{{ makeMoney($problem->localFine == null ? $problem->globalFine : $problem->localFine) }}</td>
			</tr>
			<tr>
				<td><h4>Problem Description</h4></td>
				<td style="width:5%;">:</td>
				<td>{{ $problem->description }}</td>
			</tr>
			<tr>
				<td><h4>Problem Reason</h4></td>
				<td style="width:5%;">:</td>
				<td>{{ $reason }}</td>
			</tr>
			<form method="post" action="{{ URL::to('problem/parking/payFine') }}">
			<input type="hidden" name="parkingProblemID" value="{{ $parkingProblemID }}">
			<input type="hidden" name="parkingLotID" value="{{ $parkingLotID }}">
			<input type="hidden" name="parkingID" value="{{ $parkingID }}">
			<tr>
				<td><h4>Payment Type</h4></td>
				<td style="width:5%;">:</td>
				<td>
					<select name="payment_type_id" class="form-control" required="required">
						@foreach($paymentTypes as $type)
							<option value="{{ $type->id }}">{{ $type->paymentType }}</option>
						@endforeach
					</select>
				</td>
			</tr>
			<tr>
				<td>
					<button type="submit" class="btn btn-primary">Pay Parking</button></a>
				</td>
			</tr>
			</form>
		</table>
	</div>
</div>

@stop