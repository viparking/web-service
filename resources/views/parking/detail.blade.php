@extends('admin')

@section('content')

<?php 
	$parking = $parking->data;
	$parkingLot = $parkingLot->data;
	$vehicle = $vehicle->data;

	function makeMoney($input)
    {
        $ret = "Rp " . number_format($input,2,',','.');
        return $ret;
    }
    function getFormattedDate($timeString)
    {
        $t = strtotime($timeString);
        $ret = "";
        $ret = date('H:i d-M-Y',$t);
        return $ret;
    }
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Parking Detail</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<table>
			<tr>
				<td><h4>Parking ID</h4></td>
				<td style="width:5%;">:</td>
				<td>{{ $parking->id }}</td>
			</tr>
			<tr>
				<td><h4>Company Name</h4></td>
				<td style="width:5%;">:</td>
				<td>{{ $parking->companyName }}</td>
			</tr>
			<tr>
				<td><h4>Parking Lot Name</h4></td>
				<td style="width:5%;">:</td>
				<td>{{ $parkingLot->name }}</td>
			</tr>
			<tr>
				<td><h4>Parking ID</h4></td>
				<td style="width:5%;">:</td>
				<td>{{ $parking->id }}</td>
			</tr>
			<tr>
				<td><h4>Vehicle ID</h4></td>
				<td style="width:5%;">:</td>
				<td><a href="{{ URL::to('vehicle/'.$parking->vehicle_id.'/detail') }}">{{ $parking->vehicle_id }}</a></td>
			</tr>
			<tr>
				<td><h4>Username</h4></td>
				<td style="width:5%;">:</td>
				<td><a href="{{ URL::to('member/'.$parking->member_id.'/detail') }}">{{ $parking->username }}</a></td>
			</tr>
			<tr>
				<td><h4>Card ID</h4></td>
				<td style="width:5%;">:</td>
				<td>{{ $vehicle->card_id }}</td>
			</tr>
			<tr>
				<td><h4>Vehicle Name</h4></td>
				<td style="width:5%;">:</td>
				<td>{{ $vehicle->name }}</td>
			</tr>
			<tr>
				<td><h4>Plate Number</h4></td>
				<td style="width:5%;">:</td>
				<td>{{ $vehicle->plateNumber }}</td>
			</tr>
			<tr>
				<td><h4>Balance</h4></td>
				<td style="width:5%;">:</td>
				<td>{{ makeMoney($vehicle->balance) }}</td>
			</tr>
			<tr>
				<td><h4>Brand</h4></td>
				<td style="width:5%;">:</td>
				<td>{{ $vehicle->brand }}</td>
			</tr>
			<tr>
				<td><h4>Type</h4></td>
				<td style="width:5%;">:</td>
				<td>{{ $vehicle->type }}</td>
			</tr>
			<tr>
				<td><h4>Color</h4></td>
				<td style="width:5%;">:</td>
				<td>{{ $vehicle->color }}</td>
			</tr>
			<tr>
				<td><h4>Price</h4></td>
				<td style="width:5%;">:</td>
				<td>{{ makeMoney($parking->price) }}</td>
			</tr>
			<tr>
				<td><h4>In Time</h4></td>
				<td style="width:5%;">:</td>
				<td>{{ getFormattedDate($parking->inTime) }}</td>
			</tr>
			<tr>
				<td><h4>In Gate</h4></td>
				<td style="width:5%;">:</td>
				<td>{{ $parking->inGate }}</td>
			</tr>
			<tr>
				<td><h4>Out Time</h4></td>
				<td style="width:5%;">:</td>
				<td>{{ getFormattedDate($parking->outTime) }}</td>
			</tr>
			<tr>
				<td><h4>Out Gate</h4></td>
				<td style="width:5%;">:</td>
				<td>{{ $parking->inGate }}</td>
			</tr>
		</table>
	</div>
</div>

@stop