@extends('admin')

@section('content')

<?php 
	$result = $result->data;
	$paymentTypes = $paymentTypes->data;

	function makeMoney($input)
    {
        $ret = "Rp " . number_format($input,2,',','.');
        return $ret;
    }
    function getFormattedDate($timeString)
    {
        $t = strtotime($timeString);
        $ret = "";
        $ret = date('H:i d-M-Y',$t);
        return $ret;
    }
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Parking Checkout</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<table>
			<tr>
				<td><h4>Vehicle Name</h4></td>
				<td style="width:5%;">:</td>
				<td>{{ $result->vehicle->name }}</td>
			</tr>
			<tr>
				<td><h4>Plate Number</h4></td>
				<td style="width:5%;">:</td>
				<td>{{ $result->vehicle->plateNumber }}</td>
			</tr>
			<tr>
				<td><h4>Balance</h4></td>
				<td style="width:5%;">:</td>
				<td>{{ makeMoney($result->vehicle->balance) }}</td>
			</tr>
			<tr>
				<td><h4>Price</h4></td>
				<td style="width:5%;">:</td>
				<td>{{ makeMoney($result->price) }}</td>
			</tr>
			<tr>
				<td><h4>In Time</h4></td>
				<td style="width:5%;">:</td>
				<td>{{ getFormattedDate($result->inTime) }}</td>
			</tr>
			<tr>
				<td><h4>Out Time</h4></td>
				<td style="width:5%;">:</td>
				<td>{{ getFormattedDate($result->outTime->date) }}</td>
			</tr>
			<form method="post" action="{{ URL::to('parking/payParking') }}">
			<input type="hidden" name="parkingID" value="{{ $result->id }}">
			<input type="hidden" name="parkingLotID" value="{{ $parkingLotID }}">
			<tr>
				<td><h4>Payment Type</h4></td>
				<td style="width:5%;">:</td>
				<td>
					<select name="payment_type_id" class="form-control" required="required">
						@foreach($paymentTypes as $type)
							<option value="{{ $type->id }}">{{ $type->paymentType }}</option>
						@endforeach
					</select>
				</td>
			</tr>
			<tr>
				<td>
					<button type="submit" class="btn btn-primary">Pay Parking</button></a>
				</td>
			</tr>
			</form>
		</table>
	</div>
</div>

@stop