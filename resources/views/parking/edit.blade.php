@extends('admin')

@section('content')

<?php 
	$gates = $gates->data;
	$vehicleTypes = $vehicleTypes->data;
	$parking = $parking->data;

	function getFormattedDate($timeString)
    {
        $t = strtotime($timeString);
        $ret = "";
        $ret = date('H:i d-M-Y',$t);
        return $ret;
    }
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Edit Parking</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<form method="post" action="<?= URL::to('parking/post') ?>">
			<input type="hidden" name="parkingLotID" value="{{ $parkingLotID }}"/>
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Parking ID</label>
					</div>
					<div class="col-md-10">
						<input type="text" class="form-control" disabled="disabled" value="<?= $parking->id ?>">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Card ID</label>
					</div>
					<div class="col-md-10">
						<input type="text" class="form-control" disabled="disabled" value="<?= $parking->card_id ?>">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Gate In</label>
					</div>
					<div class="col-md-10">
						<input type="text" class="form-control" disabled="disabled" name="gateIn" value="<?= $parking->inGateName ?>">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Username</label>
					</div>
					<div class="col-md-10">
						<input type="text" name="username" class="form-control" disabled="disabled" value="<?= $parking->username ?>">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Plate Number</label>
					</div>
					<div class="col-md-10">
						<input type="text" name="plateNumber" class="form-control" disabled="disabled" value="<?= $parking->plateNumber ?>">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Vehicle Type</label>
					</div>
					<div class="col-md-10">
						<input type="text" class="form-control" name="vehicleType" disabled="disabled" value="{{ $parking->vehicleType }}">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Time In</label>
					</div>
					<div class="col-md-10">
						<input type="txt" name="inDate" required="required" class="form-control" disabled="disabled" value="{{ getFormattedDate($parking->inTime) }}">
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="pull-right">
					<a href="{{ URL::to('parking/'.$parking->parking_lot_id.'/'.$parking->id.'/gate/out') }}"><button class="btn btn-primary" type="button">Complete Transaction</button></a>
				</div>
			</div>
		</form>
	</div>
</div>

@stop

@section('script')

@stop