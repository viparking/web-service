@extends('admin')

@section('content')

<?php 
	$gates = $gates->data;
	$vehicleTypes = $vehicleTypes->data;
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Add Parking</h1>
    </div>
</div>

<div class="row">
	<div class="col-md-12">
		<form method="post" action="<?= URL::to('parking/post') ?>">
			<input type="hidden" name="parkingLotID" value="{{ $parkingLotID }}"/>
			<input type="hidden" name="isMember" value="1" />
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Gate In</label>
					</div>
					<div class="col-md-10">
						<select name="inGate" class="form-control">
							@foreach($gates as $gate)
								<option value="{{ $gate->id }}">
									{{ $gate->name }}
								</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Card ID</label>
					</div>
					<div class="col-md-10">
						<input type="text" name="cardID" class="form-control">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Plate Number</label>
					</div>
					<div class="col-md-10">
						<input type="text" name="plateNumber" class="form-control">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Vehicle Type</label>
					</div>
					<div class="col-md-10">
						<select name="vehicleTypeID" class="form-control">
							@foreach($vehicleTypes as $vehicleType)
								<option value="{{ $vehicleType->id }}">
									{{ $vehicleType->vehicleType }}
								</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Time In</label>
					</div>
					<div class="col-md-7">
						<input type="date" name="inDate" required="required" class="form-control" placeholder="YYYY-MM-DD">
					</div>
					<div class="col-md-3">
						<input type="time" name="inTime" required="required" class="form-control" placeholder="HH:MM:SS">
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-2">
						<label>Photo</label>
					</div>
					<div class="col-md-10">
						<input type="hidden" name="photo" id="inputFileString">
						<input type="file" id="inputFilePhoto">
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="pull-right">
					<button class="btn btn-danger" type="reset">Reset</button> 
					<button class="btn btn-primary" type="submit">Submit</button>
				</div>
			</div>
		</form>
	</div>
</div>

@stop

@section('script')

<script>
	$(document).ready(function() {
		$('#inputFilePhoto').change(function() {
			var file = this.files[0];
			var reader = new FileReader();
			reader.onloadend = function() {
				var result = reader.result.substring(reader.result.indexOf(',')+1);
				$('#inputFileString').val(result);
			}
			reader.readAsDataURL(file);
		});
	});
</script>

@stop