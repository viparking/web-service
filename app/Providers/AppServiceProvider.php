<?php

namespace App\Providers;

use App\AdminMenuMapping;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //$menu = AdminMenuMapping::where('admin_level_id','=', Auth::guard('admin')->user()->level)->get();

        View::composer('*', 'App\Http\ViewComposers\AdminComposer');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
