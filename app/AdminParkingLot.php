<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminParkingLot extends Model
{
	public $incrementing = false;
    public function admin(){
    	return $this->belongsTo('App\Admin');
    }

    public function parkingLot() {
    	return $this->belongsTo('App\ParkingLot');
    }
}
