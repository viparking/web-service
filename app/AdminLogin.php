<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminLogin extends Model
{
	public $incrementing = false;
    public function admin()
    {
    	return $this->belongsTo('App\Admin');
    }
}
