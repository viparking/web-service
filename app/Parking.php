<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parking extends Model
{
    public $incrementing = false;
    protected $hidden = [
        'updated_at','auditedActivity','auditedUser'
    ];
    public function parkingLot() {
    	return $this->belongsTo('App\ParkingLot');
    }
    public function paymentType() {
    	return $this->belongsTo('App\PaymentType');
    }
    public function parkingProblem() {
    	return $this->hasMany('App\ParkingProblem');
    }
    public function member() {
    	return $this->belongsTo('App\Member');
    }
    public function vehicle() {
    	return $this->belongsTo('App\Vehicle');
    }
}
