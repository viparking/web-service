<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    protected $hidden = [
        'password','updated_at','created_at','auditedActivity','auditedUser','remember_token'
    ];

    public function adminLogin() {
    	return $this->hasMany('App\AdminLogin');
    }
    public function adminParkingLot() {
    	return $this->hasMany('App\AdminParkingLot');
    }

    public function adminLevel(){
    	return $this->belongsTo('App\AdminLevel');
    }
}
