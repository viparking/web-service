<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fine extends Model
{
	protected $hidden = [
        'updated_at','created_at','auditedActivity','auditedUser'
    ];
	public $incrementing = false;
   	public function problem() {
   		return $this->belongsTo('App\Problem');
   	}
   	public function parkinglot() {
   		return $this->belongsTo('App\ParkingLot');
   	}
}
