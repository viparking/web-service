<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gate extends Model
{
	protected $hidden = [
        'updated_at','created_at','auditedActivity','auditedUser'
    ];
    public $incrementing = false;
    public function parkingLot() {
    	return $this->belongsTo('App\ParkingLot');
    }
}
