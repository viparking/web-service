<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParkingProblem extends Model
{
	public $incrementing = false;
    protected $hidden = [
        'created_at','auditedActivity','auditedUser'
    ];
    protected $table = "parking_problem";

    public function problem() {
    	return $this->belongsTo('App\Problem');
    }
    public function parking() {
    	return $this->belongsTo('App\Parking');
    }
    public function member() {
    	return $this->belongsTo('App\Member');
    }
}
