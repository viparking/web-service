<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleHistory extends Model
{
	public $incrementing = false;
    public function vehicle() {
    	return $this->belongsTo('App\Vehicle');
    }
}
