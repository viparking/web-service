<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CasshierSession extends Model
{
    protected $hidden = [
        'updated_at','created_at','auditedActivity','auditedUser'
    ];
}
