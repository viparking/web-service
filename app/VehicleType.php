<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleType extends Model
{
	public $timestamps = false;
    public function price() {
    	return $this->hasMany('App\Price');
    }
    public function vehicle() {
    	return $this->hasMany('App\Vehicle');
    }
}
