<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
	public $incrementing = false;
    public function transactionType() {
    	return $this->belongsTo('App\TransactionType');
    }
}
