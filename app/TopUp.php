<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TopUp extends Model
{
	protected $hidden = [
        'updated_at','auditedActivity','auditedUser'
    ];
	public $incrementing = false;
    public function member() {
    	return $this->belongsTo('App\Member');
    }
    public function vehicle() {
    	return $this->belongsTo('App\Vehicle');
    }
}
