<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    public $incrementing = false;
    protected $hidden = [
        'updated_at','created_at','auditedActivity','auditedUser'
    ];
    public function member() {
    	return $this->belongsTo('App\Member');
    }
    public function vehicleType() {
    	return $this->belongsTo('App\VehicleType');
    }
    public function parking() {
    	return $this->hasMany('App\Parking');
    }
    public function topUp() {
    	return $this->hasMany('App\TopUp');
    }
    public function vehicleHistory() {
    	return $this->hasOne('App\VehicleHistory');
    }
    public function vip(){
        return $this->hasOne('App\VipMember');
    }
}
