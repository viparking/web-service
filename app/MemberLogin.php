<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemberLogin extends Model
{
	public $incrementing = false;
    public function member() {
    	return $this->belongsTo('App\Member');
    }
}
