<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TopUpAmount extends Model
{
	public $timestamps = false;
    protected $hidden = [
        'updated_at','auditedActivity','auditedUser','created_at'
    ];
}
