<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
	protected $hidden = [
        'updated_at','created_at','auditedActivity','auditedUser','id'
    ];
    public function parkingLot() {
    	return $this->belongsTo('App\ParkingLot');
    }
    public function vehicleType() {
    	return $this->belongsTo('App\VehicleType');
    }
}
