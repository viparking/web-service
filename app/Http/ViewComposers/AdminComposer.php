<?php

namespace App\Http\ViewComposers;

use App\AdminMenuMapping;
use Illuminate\View\View;
use Illuminate\Support\Facades\Auth;

class AdminComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $menu;

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct()
    {
        $this->menu = array();

        if(Auth::guard('admin')->user() == NULL)
            return;
        $temp = AdminMenuMapping::where('admin_level_id','=', Auth::guard('admin')->user()->level)->get();
        foreach($temp as $t) {
            array_push($this->menu, $t->menu);
        }
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('adminMenuMapping', $this->menu);
    }
}