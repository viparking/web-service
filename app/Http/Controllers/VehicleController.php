<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\VehicleType;
use App\Vehicle;
use App\Parking;
use App\TopUp;
use App\VehicleHistory;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use Mail;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\PDFController;

class VehicleController extends Controller
{
	/**
	 * Get all vehicle types stored in database
	 * @param  Request $request basic request parameter
	 * @return            
	 */
    public function getVehicleType(Request $request)
    {
    	$vehicleType = VehicleType::selectRaw('vehicle_types.*, Count(a.id) AS vehicleCount')
    		->leftJoin(DB::raw('(SELECT * FROM vehicles WHERE auditedActivity <> \'D\') a'), 'a.vehicle_type_id','=','vehicle_types.id')
    		->groupBy('vehicle_types.id')
    		->get();
    	$ret = new \stdClass();
    	$ret->success =true;
    	$ret->data = $vehicleType;

    	return response()->json($ret);
    }

    /**
     * Add new vehicle type and save it to database
     * @param Request $request request with vehicleType parameter
     */
    public function addVehicleType(Request $request)
    {
        if(Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::vehicle/type/add:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
    	if($request->has("data"))
    	{
    		$data = $request->input("data");
    		if(array_key_exists("vehicleType", $data))
    		{
    			$type = new VehicleType();
    			$type->vehicleType = $data["vehicleType"];
    			try
    			{
    				if($type->save())
    				{
    					$ret = new \stdClass();
	    				$ret->success = true;
	    				$ret->data = $type;
	    				\Log::info("200::vehicle/type/add:: vehicle type created with ID: ".$type->id);
	    				return response()->json($ret);
    				}
    				else
    				{
    					$ret = new \stdClass();
	                    $ret->success = false;
	                    $ret->error = "Something wrong with our server, please try again later";
	                    \Log::error("500::vehicle/type/add:: failed to save");
	                    return response()->json($ret);
    				}
    			}
    			catch (\Illuminate\Database\QueryException $ex)
    			{
    				$ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::vehicle/type/add:: failed to save: ".$ex);
                    return response()->json($ret);
    			}
    		}
    		else
    		{
    			\Log::info("400::vehicle/type/add:: vehicle type not provided");
                return $this->returnBadRequest("vehicleType field is required");
    		}
    	}
    	\Log::info("400::vehicle/type/add:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * Edit vehicle type stored in database with specified id
     * @param  Request $request request with id and vehicleType parameter
     * @return            
     */
    public function editVehicleType(Request $request)
    {
        if(Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::vehicle/type/edit:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
    	if($request->has("data"))
    	{
    		$data = $request->input("data");
    		if(array_key_exists("vehicleType",$data) && array_key_exists("id",$data))
    		{
    			$type = VehicleType::find($data["id"]);
    			$type->vehicleType = $data["vehicleType"];
    			try
    			{
    				if($type->save())
    				{
    					$ret = new \stdClass();
    					$ret->success = true;
    					$ret->data = $type;
    					\Log::info("200::vehicle/type/edit:: vehicle type with ID: ".$type->id." modified to: ".$type->vehicleType);
	    				return response()->json($ret);
    				}
    				else
    				{
    					$ret = new \stdClass();
	                    $ret->success = false;
	                    $ret->error = "Something wrong with our server, please try again later";
	                    \Log::error("500::vehicle/type/edit:: failed to save");
	                    return response()->json($ret);
    				}
    			}
    			catch (\Illuminate\Database\QueryException $ex)
    			{
    				$ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::vehicle/type/edit:: failed to save: ".$ex);
                    return response()->json($ret);
    			}
    		}
    		else
    		{
    			\Log::info("400::vehicle/type/edit:: vehicle type or id not provided");
                return $this->returnBadRequest("vehicleType and id field are required");
    		}
    	}
    	\Log::info("400::vehicle/type/edit:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * Remove specified vehicleType with sent id from database
     * @param  Request $request request with id parameter
     * @return            
     */
    public function deleteVehicleType(Request $request)
    {
        if(Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::vehicle/type/delete:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
    	if($request->has("data"))
    	{
    		$data = $request->input('data');
    		if(array_key_exists("id",$data))
    		{
    			$type = VehicleType::find($data['id']);
    			try
    			{
    				if($type->delete())
    				{
    					$ret = new \stdClass();
    					$ret->success = true;
    					\Log::info("200::vehicle/type/delete:: vehicle type with ID: ".$type->id." removed");
	    				return response()->json($ret);
    				}
    				else
    				{
    					$ret = new \stdClass();
	                    $ret->success = false;
	                    $ret->error = "Something wrong with our server, please try again later";
	                    \Log::error("500::vehicle/type/delete:: failed to save");
	                    return response()->json($ret);
    				}
    			}
    			catch (\Illuminate\Database\QueryException $ex)
    			{
    				$ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Can't delete vehicle type with vehicle(s) assigned to it";
                    \Log::error("500::vehicle/type/delete:: failed to delete: ".$ex);
                    return response()->json($ret);
    			}
    		}
    		else
    		{
    			\Log::info("400::vehicle/type/delete:: id not provided");
                return $this->returnBadRequest("id field is required");
    		}
    	}
    	\Log::info("400::vehicle/type/delete:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * Get vehicle list of specified memberID
     * @param  Request $request  basic request parameter
     * @param  string  $memberID id of the member
     * @return             
     */
    public function getVehicleForMember(Request $request, $memberID)
    {
        if(Auth::guard('admin')->user()->level < 2)
        {
            \Log::info("401::vehicle/type/add:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
    	$vehicleList = Vehicle::selectRaw("vehicles.*, vehicle_types.vehicleType")
	    	->join('vehicle_types','vehicle_types.id','=','vehicles.vehicle_type_id')
    		->where('member_id',$memberID)
    		->where('auditedActivity','<>','D')
    		->get();
    	$ret = new \stdClass();
    	$ret->success = true;
    	$ret->data = $vehicleList;

    	\Log::info("200::vehicle/get/{memberID}:: Get vehicle for memberID: ".$memberID);
        return response()->json($ret);
    }

    /**
     * Get all vehicle from the database based on specified page
     * @param  Request $request basic request parameter
     * @param  integer $page    page offset of the vehicle list
     * @return            
     */
    public function getAllVehicle(Request $request, $page = 0, $search='')
    {
        if(Auth::guard('admin')->user()->level < 3)
        {
            \Log::info("401::vehicle/type/add:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
    	$skip = $page * config('app.OFFSET');
    	$vehicles = Vehicle::selectRaw("vehicles.*, vehicle_types.vehicleType, due_date")
    		->join('vehicle_types','vehicle_types.id','=','vehicles.vehicle_type_id')
            ->leftJoin('vip_members','vip_members.vehicle_id','=','vehicles.id')
			->where('auditedActivity','<>','D')
            ->where('vehicles.plateNumber','like','%'.$search.'%')
			->skip($skip)
            ->take(config('app.OFFSET'))
    		->get();

        $vehicleCount = Vehicle::join('vehicle_types','vehicle_types.id','=','vehicles.vehicle_type_id')
            ->where('auditedActivity','<>','D')
            ->count();

    	$ret = new \stdClass();
    	$ret->success = true;
        $ret->total = $vehicleCount;
    	$ret->data = $vehicles;

    	\Log::info("200::vehicle/getAll:: Get Member page ".$page);
        return response()->json($ret);
    }

    /**
     * Delete vehicle by specified ID
     * @param  Request $request request with id parameter
     * @return            
     */
    public function deleteVehicle(Request $request)
    {
        if(Auth::guard('admin')->user()->level < 2)
        {
            \Log::info("401::vehicle/type/add:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
    	if($request->has("data"))
    	{
    		$data = $request->input('data');
    		if(array_key_exists("id",$data))
    		{
    			$vehicle = Vehicle::find($data['id']);
    			try
    			{
    				$vehicle->auditedActivity = 'D';
    				$vehicle->auditedUser = Auth::guard('admin')->user()->id;
    				if($vehicle->save())
    				{
    					$ret = new \stdClass();
    					$ret->success = true;
    					\Log::info("200::vehicle/delete:: vehicle with ID: ".$vehicle->id." removed");
	    				return response()->json($ret);
    				}
    				else
    				{
    					$ret = new \stdClass();
	                    $ret->success = false;
	                    $ret->error = "Something wrong with our server, please try again later";
	                    \Log::error("500::vehicle/delete:: failed to save");
	                    return response()->json($ret);
    				}
    			}
    			catch (\Illuminate\Database\QueryException $ex)
    			{
    				$ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::vehicle/delete:: failed to delete: ".$ex);
                    return response()->json($ret);
    			}
    		}
    		else
    		{
    			\Log::info("400::vehicle/delete:: id not provided");
                return $this->returnBadRequest("id field is required");
    		}
    	}
    	\Log::info("400::vehicle/delete:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * Get vehicle list of logged in member
     * @param  Request $request basic request parameter
     * @return            
     */
    public function getMyVehicle(Request $request)
    {
    	if(Auth::user() != null)
    	{
	    	$vehicleList = Vehicle::selectRaw("vehicles.*, vehicle_types.vehicleType")
		    	->join('vehicle_types','vehicle_types.id','=','vehicles.vehicle_type_id')
	    		->where('member_id',Auth::user()->id)
	    		->where('auditedActivity','<>','D')
	    		->get();
	    	$ret = new \stdClass();
	    	$ret->success = true;
	    	$ret->data = $vehicleList;

	    	\Log::info("200::vehicle/me/:: Get vehicle for memberID: ".Auth::user()->id);
	        return response()->json($ret);
	    }
	    else
	    {
	    	$ret = new \stdClass();
	    	$ret->success = false;
	    	$ret->error = "You have to be a member to get your vehicle list";

	    	\Log::info("400::vehicle/me/:: Invalid! admin trying to get vehicle");
	        return response()->json($ret);
	    }
    }

    /**
     * Add vehicle for specified memberID
     * @param Request $request request with vehicleTypeID, memberID, cardID, name, and plateNumber parameter
     */
    public function addVehicle(Request $request)
    {
//        if(Auth::guard('admin')->user()->level < 2)
//        {
//            \Log::info("401::vehicle/type/add:: Insufficient level");
//            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
//        }
    	if($request->has("data"))
    	{
    		$data = $request->input("data");
    		if(array_key_exists("vehicleTypeID",$data) && array_key_exists("memberID",$data) && array_key_exists("cardID", $data) && array_key_exists("name", $data) && array_key_exists("plateNumber", $data) && array_key_exists("address",$data) && array_key_exists("noSTNK", $data) && array_key_exists("brand", $data) && array_key_exists("color", $data) && array_key_exists("type", $data))
    		{
    			$vehicle = new Vehicle();
    			$vehicle->id = "V".substr($data["plateNumber"],1,2).date('Ymdhis').substr(microtime(), 2,3);
    			$vehicle->vehicle_type_id = $data["vehicleTypeID"];
    			$vehicle->member_id = $data["memberID"];
    			$vehicle->card_id = $data["cardID"];
    			$vehicle->name = $data["name"];
    			$vehicle->plateNumber = $data["plateNumber"];
    			$vehicle->address = $data["address"];
    			$vehicle->noSTNK = $data["noSTNK"];
    			$vehicle->brand = $data["brand"];
    			$vehicle->color = $data["color"];
    			$vehicle->type = $data["type"];
    			$vehicle->balance = 0;
    			$vehicle->auditedUser = Auth::guard('admin')->user()->id;
    			$vehicle->auditedActivity = 'I';

                if(array_key_exists("photo", $data))
                {
                    $tempImage = $this->base64_to_jpeg($data["photo"],"stnk/t".$vehicle->id.".jpg");
                    $this->compress_image($tempImage, "stnk/".$vehicle->id.".jpg", 60);
                    unlink("stnk/t".$vehicle->id.".jpg");
                }
    			try
    			{
    				if($vehicle->save())
    				{
    					$ret = new \stdClass();
    					$ret->success = true;
    					$ret->data = $vehicle;
    					\Log::info("200::vehicle/add/:: vehicle with ID: ".$vehicle->id." added for memberID: ".$vehicle->member_id);
    					return response()->json($ret);
    				}
    				else
    				{
    					$ret = new \stdClass();
	                    $ret->success = false;
	                    $ret->error = "Something wrong with our server, please try again later";
	                    \Log::error("500::vehicle/add/:: failed to save");
	                    return response()->json($ret);
    				}
    			}
    			catch (\Illuminate\Database\QueryException $ex)
    			{
    				$ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::vehicle/add/:: failed to save: ".$ex);
                    return response()->json($ret);
    			}
    		}
    		else
    		{
    			\Log::info("400::vehicle/add/:: required fields not provided");
                return $this->returnBadRequest("vehicleTypeID, memberID, cardID, name, address, noSTNK, brand, color, type and plateNumber field are required");
    		}
    	}
    	\Log::info("400::vehicle/add/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * restore deleted vehicle from specified vehicleID
     * @param  Request $request request with id parameter
     * @return            
     */
    public function restoreVehicle(Request $request)
    {
        if(Auth::guard('admin')->user()->level < 2)
        {
            \Log::info("401::vehicle/type/add:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
    	if($request->has("data"))
    	{
    		$data = $request->input("data");
    		if(array_key_exists("id", $data))
    		{
    			$vehicle = Vehicle::find($data["id"]);
    			$vehicle->auditedActivity = 'U';
    			$vehicle->auditedUser = Auth::guard('admin')->user()->id;
    			try
    			{
    				if($vehicle->save())
    				{
    					$ret = new \stdClass();
    					$ret->success = true;
    					$ret->data = $vehicle;
    					\Log::info("200::vehicle/restore/:: vehicle with ID: ".$vehicle->id." restored");
    					return response()->json($ret);
    				}
    				else
    				{
    					$ret = new \stdClass();
	                    $ret->success = false;
	                    $ret->error = "Something wrong with our server, please try again later";
	                    \Log::error("500::vehicle/add/:: failed to save");
	                    return response()->json($ret);
    				}
    			}
    			catch(\Illuminate\Database\QueryException $ex)
    			{
    				$ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::vehicle/add/:: failed to save: ".$ex);
                    return response()->json($ret);
    			}
    		}
    		else
    		{
    			\Log::info("400::vehicle/restore/:: id field not provided");
                return $this->returnBadRequest("id field is required");
    		}
    	}
    	\Log::info("400::vehicle/restore/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * Get vehicle history for memberID
     * @param  Request $request  basic request parameter
     * @param  string  $memberID id of the member
     * @return             
     */
    public function getHistoryForMember(Request $request, $memberID)
    {
        if(Auth::guard('admin')->user()->level < 2)
        {
            \Log::info("401::vehicle/type/add:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
    	$vehicleList = Vehicle::selectRaw("vehicles.*, vehicle_types.vehicleType")
	    	->join('vehicle_types','vehicle_types.id','=','vehicles.vehicle_type_id')
    		->where('member_id',$memberID)
    		->where('auditedActivity','=','D')
    		->get();
    	$ret = new \stdClass();
    	$ret->success = true;
    	$ret->data = $vehicleList;

    	\Log::info("200::vehicle/history/{memberID}:: Get vehicle history for memberID: ".$memberID);
        return response()->json($ret);
    }

    /**
     * Get vehicle history for specific member
     * @param  Request $request basic request parameter
     * @return            
     */
    public function getMyHistory(Request $request)
    {
    	if(Auth::user() != null)
    	{
	    	$vehicleList = Vehicle::selectRaw("vehicles.*, vehicle_types.vehicleType")
		    	->join('vehicle_types','vehicle_types.id','=','vehicles.vehicle_type_id')
	    		->where('member_id',Auth::user()->id)
	    		->where('auditedActivity','=','D')
	    		->get();
	    	$ret = new \stdClass();
	    	$ret->success = true;
	    	$ret->data = $vehicleList;

	    	\Log::info("200::vehicle/history/me/:: Get vehicle for memberID: ".Auth::user()->id);
	        return response()->json($ret);
	    }
	    else
	    {
	    	$ret = new \stdClass();
	    	$ret->success = false;
	    	$ret->error = "You have to be a member to get your vehicle history list";

	    	\Log::info("400::vehicle/history/me/:: Invalid! admin trying to get vehicle history");
	        return response()->json($ret);
	    }
    }

    /**
     * get paginated all vehicle history
     * @param  Request $request basic request parameter
     * @param  integer $page    optional page offset
     * @return            
     */
    public function getAllHistory(Request $request, $page = 0)
    {
        if(Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::vehicle/type/add:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
    	$skip = $page * config('app.OFFSET');
    	$vehicles = Vehicle::selectRaw("vehicles.*, vehicle_types.vehicleType")
    		->join('vehicle_types','vehicle_types.id','=','vehicles.vehicle_type_id')
			->where('auditedActivity','=','D')
			->skip($skip)
            ->take(config('app.OFFSET'))
    		->get();

        $vehicleCount = Vehicle::join('vehicle_types','vehicle_types.id','=','vehicles.vehicle_type_id')
            ->where('auditedActivity','=','D')
            ->count();

    	$ret = new \stdClass();
    	$ret->success = true;
        $ret->total = $vehicleCount;
    	$ret->data = $vehicles;

    	\Log::info("200::vehicle/getAll:: Get Member page ".$page);
        return response()->json($ret);
    }

    /**
     * Get vehicle parking history as admin -> can get for any vehicle
     * @param  Request $request   basic request parameter
     * @param  string  $vehicleID id of the vehicle
     * @param  integer $page      page offset
     * @return              
     */
    public function getAdminParkingHistory(Request $request, $vehicleID, $page = 0)
    {
        if(Auth::guard('admin')->user()->level < 2)
        {
            \Log::info("401::vehicle/type/add:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
    	$skip = $page * config('app.OFFSET');
    	$parking = Parking::selectRaw("parkings.*, parking_lots.name AS parkingLotName, transactions.startingBalance, transactions.balance")
            ->join('transactions','transactions.referenceID','=','parkings.id')
    		->join('parking_lots','parkings.parking_lot_id','=','parking_lots.id')
    		->where('vehicle_id',$vehicleID)
    		->where('parkings.auditedActivity','<>','D')
    		->skip($skip)
    		->take(config('app.OFFSET'))
    		->get();

        $parkingCount = Parking::join('transactions','transactions.referenceID','=','parkings.id')
            ->join('parking_lots','parkings.parking_lot_id','=','parking_lots.id')
            ->where('vehicle_id',$vehicleID)
            ->where('parkings.auditedActivity','<>','D')
            ->count();

    	$ret = new \stdClass();
    	$ret->success = true;
        $ret->total = $parkingCount;
    	$ret->data = $parking;

    	\Log::info("200::vehicle/{vehicleID}/parking/{page?}:: admin Get Vehicle parking history page ".$page." for vehicleID: ".$vehicleID);
        return response()->json($ret);
    }

    /**
     * Get vehicle parking history as member -> only can get his/her parking history
     * @param  Request $request   basic request parameter
     * @param  string  $vehicleID id of the vehicle
     * @param  integer $page      page offset
     * @return              
     */
    public function getMemberParkingHistory(Request $request, $vehicleID, $page = 0, $startTime = null, $endTime = null)
    {
    	if(Auth::user() != null)
    	{
	    	$skip = $page * config('app.OFFSET');

            if($startTime != null && $endTime != null)
            {
                $endTime = Carbon::parse($endTime);
                if($endTime->hour == 0)
                {
                    $endTime->hour = 23;
                    $endTime->minute = 59;
                }
                $parking = Parking::selectRaw("parkings.*, parking_lots.name AS parkingLotName, transactions.startingBalance, transactions.balance")
                    ->join('transactions','transactions.referenceID','=','parkings.id')
                    ->join('parking_lots','parkings.parking_lot_id','=','parking_lots.id')
                    ->where('vehicle_id',$vehicleID)
                    ->where('parkings.member_id', Auth::user()->id)
                    ->where('parkings.auditedActivity','<>','D')
                    ->where('parkings.outTime', '>=', Carbon::parse($startTime)->toDateTimeString())
                    ->where('parkings.outTime', '<=', $endTime->toDateTimeString())
                    ->skip($skip)
                    ->take(config('app.OFFSET'))
                    ->get();

                $parkingCount = Parking::join('transactions','transactions.referenceID','=','parkings.id')
                    ->join('parking_lots','parkings.parking_lot_id','=','parking_lots.id')
                    ->where('vehicle_id',$vehicleID)
                    ->where('parkings.member_id', Auth::user()->id)
                    ->where('parkings.auditedActivity','<>','D')
                    ->where('parkings.outTime', '>=', Carbon::parse($startTime)->toDateTimeString())
                    ->where('parkings.outTime', '<=', Carbon::parse($endTime)->toDateTimeString())
                    ->count();
            }
            else
            {
                $parking = Parking::selectRaw("parkings.*, parking_lots.name AS parkingLotName, transactions.startingBalance, transactions.balance")
                    ->join('transactions','transactions.referenceID','=','parkings.id')
                    ->join('parking_lots','parkings.parking_lot_id','=','parking_lots.id')
                    ->where('vehicle_id',$vehicleID)
                    ->where('parkings.member_id', Auth::user()->id)
                    ->where('parkings.auditedActivity','<>','D')
                    ->skip($skip)
                    ->take(config('app.OFFSET'))
                    ->get();

                $parkingCount = Parking::join('transactions','transactions.referenceID','=','parkings.id')
                    ->join('parking_lots','parkings.parking_lot_id','=','parking_lots.id')
                    ->where('vehicle_id',$vehicleID)
                    ->where('parkings.member_id', Auth::user()->id)
                    ->where('parkings.auditedActivity','<>','D')
                    ->count();
            }

	    	$ret = new \stdClass();
	    	$ret->success = true;
            $ret->total = $parkingCount;
	    	$ret->data = $parking;

	    	\Log::info("200::vehicle/me/{vehicleID}/parking/{page?}/:: Get vehicle parking history for memberID: ".Auth::user()->id." for vehicleID: ".$vehicleID." page:".$page);
	        return response()->json($ret);
	    }
	    else
	    {
	    	$ret = new \stdClass();
	    	$ret->success = false;
	    	$ret->error = "You have to be a member to use this API";

	    	\Log::info("400::vehicle/me/{vehicleID}/parking/{page?}/:: Invalid! admin trying to get vehicle parking history");
	        return response()->json($ret);
	    }
    }

    /**
     * Get vehicle top up history as member -> only can get history for his/her vehicle
     * @param  Request $request   basic request parameter
     * @param  string  $vehicleID id of the vehicle
     * @param  integer $page      page offset
     * @return              
     */
    public function getMemberTopUp(Request $request, $vehicleID, $page = 0)
    {
        if(Auth::user() != null)
        {
            $skip = $page * config('app.OFFSET');
            $skip = $page * config('app.OFFSET');
            $topUp = TopUp::selectRaw('top_ups.*, transactions.startingBalance, transactions.balance')
                ->join('transactions','transactions.referenceID','=','top_ups.id')
                ->where('vehicle_id',$vehicleID)
                ->where('top_ups.auditedActivity','<>','D')
                ->where('member_id', Auth::user()->id)
                ->skip($skip)
                ->take(config('app.OFFSET'))
                ->get();

            $topUpCount = TopUp::join('transactions','transactions.referenceID','=','top_ups.id')
                ->where('vehicle_id',$vehicleID)
                ->where('top_ups.auditedActivity','<>','D')
                ->where('member_id', Auth::user()->id)
                ->count();

            $ret = new \stdClass();
            $ret->success = true;
            $ret->total = $topUpCount;
            $ret->data = $topUp;

             \Log::info("200::vehicle/me/{vehicleID}/topUp/{page?}:: MemberID:".Auth::user()->id." Get Vehicle parking history page ".$page." for vehicleID: ".$vehicleID);
            return response()->json($ret);
        }
        else
        {
            $ret = new \stdClass();
            $ret->success = false;
            $ret->error = "You have to be a member to use this API";

            \Log::info("400::vehicle/me/{vehicleID}/parking/{page?}/:: Invalid! admin trying to get vehicle top up history");
            return response()->json($ret);
        }
    }

    /**
     * Get top up history for specific vehicleID as admin -> can view any vehicle
     * @param  Request $request   basic request parameter
     * @param  string  $vehicleID id of the vehicle
     * @param  integer $page      page offset
     * @return              
     */
    public function getAdminTopUp(Request $request, $vehicleID, $page = 0)
    {
        $skip = $page * config('app.OFFSET');
        $topUp = TopUp::selectRaw('top_ups.*, transactions.startingBalance, transactions.balance')
            ->join('transactions','transactions.referenceID','=','top_ups.id')
            ->where('vehicle_id',$vehicleID)
            ->where('top_ups.auditedActivity','<>','D')
            ->skip($skip)
            ->take(config('app.OFFSET'))
            ->get();

        $topUpCount = TopUp::join('transactions','transactions.referenceID','=','top_ups.id')
            ->where('vehicle_id',$vehicleID)
            ->where('top_ups.auditedActivity','<>','D')
            ->count();

        $ret = new \stdClass();
        $ret->success = true;
        $ret->total = $topUpCount;
        $ret->data = $topUp;

        \Log::info("200::vehicle/{vehicleID}/topUp/{page?}:: admin Get Vehicle parking history page ".$page." for vehicleID: ".$vehicleID);
        return response()->json($ret);
    }

    /**
     * get vehicle detail by cardID
     * @param  Request $request basic request parameter
     * @param  string  $cardID  id of the card
     * @return            
     */
    public function getVehicleByCardID(Request $request, $cardID)
    {
        $vehicle = Vehicle::selectRaw("vehicles.*, vehicle_types.vehicleType, members.username")
            ->join('vehicle_types','vehicle_types.id','=','vehicles.vehicle_type_id')
            ->join('members','vehicles.member_id','=','members.id')
            ->where('card_id', $cardID)
            ->where('vehicles.auditedActivity','<>','D')
            ->first();

        $ret = new \stdClass();
        $ret->success = true;
        $ret->data = $vehicle;

        \Log::info("200::vehicle/card/{cardI}:: get vehicle with card ID: ".$cardID);
        return response()->json($ret);
    }

    /**
     * get vehicle detail by the vehicle id
     * @param  Request $request   basic request parameter
     * @param  string  $vehicleID id of the vehicle
     * @return              
     */
    public function getVehicleByID(Request $request, $vehicleID)
    {
        $vehicleList = Vehicle::selectRaw("vehicles.*, vehicle_types.vehicleType, members.username, due_date")
            ->join('vehicle_types','vehicle_types.id','=','vehicles.vehicle_type_id')
            ->join('members','vehicles.member_id','=','members.id')
            ->leftJoin('vip_members','vip_members.vehicle_id','=','vehicles.id')
            ->where('vehicles.id',$vehicleID)
            ->where('vehicles.auditedActivity','<>','D')
            ->first();
        $ret = new \stdClass();
        $ret->success = true;
        $ret->data = $vehicleList;

        \Log::info("200::vehicle/detail/{vehicleID}:: Get vehicle detail for id: ".$vehicleID);
        return response()->json($ret);
    }

    /**
     * send email of the history to the member
     * @param  Request $request   basic request parameter
     * @param  string  $vehicleID id of the vehicle
     * @param  string  $startTime start time offset
     * @param  string  $endTime   end time offset
     * @return              
     */
    public function sendHistoryEmail(Request $request, $vehicleID, $startTime = null, $endTime = null)
    {
        $pdfController = new PDFController();
        if($startTime != null && $endTime != null)
        {
            $endTime = Carbon::parse($endTime);
            if($endTime->hour == 0)
            {
                $endTime->hour = 23;
                $endTime->minute = 59;
            }
            $parking = Parking::selectRaw("parkings.*, parking_lots.name AS parkingLotName, transactions.startingBalance, transactions.balance")
                ->join('transactions','transactions.referenceID','=','parkings.id')
                ->join('parking_lots','parkings.parking_lot_id','=','parking_lots.id')
                ->where('vehicle_id',$vehicleID)
                ->where('parkings.auditedActivity','<>','D')
                ->where('parkings.outTime', '>=', Carbon::parse($startTime)->toDateTimeString())
                ->where('parkings.outTime', '<=', $endTime->toDateTimeString())
                ->get();
        }
        else
        {
            $parking = Parking::selectRaw("parkings.*, parking_lots.name AS parkingLotName, transactions.startingBalance, transactions.balance")
                ->join('transactions','transactions.referenceID','=','parkings.id')
                ->join('parking_lots','parkings.parking_lot_id','=','parking_lots.id')
                ->where('vehicle_id',$vehicleID)
                ->where('parkings.auditedActivity','<>','D')
                ->get();
        }
        $vehicle = Vehicle::find($vehicleID);

        $user = Auth::user();
        Mail::send('history', ['vehicle' => $vehicle, 'parking' => $parking], function($m) use ($user, $vehicle) {
            $m->from('no-reply@vi-parking.com',"No-Reply Vi-Parking");
            $m->to($user->email, $user->username)->subject("Parking Report");
            $m->attach('parking/'.$vehicle->id.'.pdf');
        });

        $ret = new \stdClass();
        $ret->success = true;

        \Log::info("200::sendHistoryEmail:: email sent for ID: ".$vehicleID);
        return response()->json($ret);
    }

    /**
     * compress uploaded image
     * @param  [type] $source_url      [description]
     * @param  [type] $destination_url [description]
     * @param  [type] $quality         [description]
     * @return [type]                  [description]
     */
    private function compress_image($source_url, $destination_url, $quality) 
    {
        $info = getimagesize($source_url); 
        if ($info['mime'] == 'image/jpeg') 
            $image = imagecreatefromjpeg($source_url); 
        else if ($info['mime'] == 'image/gif') 
            $image = imagecreatefromgif($source_url); 
        else if ($info['mime'] == 'image/png')
            $image = imagecreatefrompng($source_url); 
        else
            $image = imagecreatefromjpeg($source_url);
        imagejpeg($image, $destination_url, $quality); 
        return $destination_url; 
    }

    /**
     * convert image to base64
     * @param  [type] $base64_string [description]
     * @param  [type] $output_file   [description]
     * @return [type]                [description]
     */
    private function base64_to_jpeg($base64_string, $output_file) {
        $ifp = fopen($output_file, "wb+");

        fwrite($ifp, base64_decode($base64_string)); 
        fclose($ifp); 

        return $output_file;
    }
}