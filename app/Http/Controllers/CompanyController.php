<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Company;
use App\ParkingLot;
use App\AdminParkingLot;
use App\Http\Requests;
use App\Admin;

class CompanyController extends Controller
{
    /**
     * create a company with supplied parameter
     * @param Request $request request with all required parameter
     */
    public function addCompany(Request $request)
    {
        if(Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::company/add/:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
        if($request->has('data'))
        {
            $data = $request->input('data');
            if(array_key_exists("name", $data) && array_key_exists("representativeName", $data) && array_key_exists("phoneNumber", $data) && array_key_exists("address", $data) && array_key_exists('email', $data))
            {
                $company = new Company();
                $company->name = $data["name"];
                $company->representativeName = $data["representativeName"];
                $company->phoneNumber = $data["phoneNumber"];
                $company->emailAddress = $data["email"];
                $company->address = $data["address"];
                $company->auditedUser = Auth::guard('admin')->user()->id;
                $company->auditedActivity = 'I';

                try
                {
                    if($company->save())
                    {
                        $ret = new \stdClass();
                        $ret->success = true;
                        $ret->data = $company;
                        \Log::info("200::company/add/:: Company created with id: ".$company->id);
                        return response()->json($ret);
                    }
                    else
                    {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->error = "Something wrong with our server, please try again later";
                        \Log::error("500::company/add/:: failed to save");
                        return response()->json($ret);
                    }
                }
                catch (\Illuminate\Database\QueryException $ex)
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::company/add/:: failed to save: ".$ex);
                    return response()->json($ret);
                }
            }
            else
            {
                \Log::info("400::company/add/:: required fields not provided");
                return $this->returnBadRequest("name, representativeName, phoneNumber, email, and address field are required");
            }
        }
        \Log::info("400::ucompany/add/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * Delete company from the list
     * @param  Request $request request with id data
     * @return            
     */
    public function deleteCompany(Request $request)
    {
        if(Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::company/delete/:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
        if($request->has('data'))
        {
            $data = $request->input('data');
            if(array_key_exists("id", $data))
            {
                $company = Company::find($data['id']);
                $company->auditedActivity = 'D';
                $company->auditedUser = Auth::guard('admin')->user()->id;

                try
                {
                    if($company->save())
                    {
                        $ret = new \stdClass();
                        $ret->success = true;
                        \Log::info("200::company/delete/:: Company deleted with id: ".$company->id);
                        return response()->json($ret);
                    }
                    else
                    {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->error = "Something wrong with our server, please try again later";
                        \Log::error("500::company/delete/:: failed to save");
                        return response()->json($ret);
                    }
                }
                catch (\Illuminate\Database\QueryException $ex)
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::company/delete/:: failed to save: ".$ex);
                    return response()->json($ret);
                }
            }
            else
            {
                \Log::info("400::company/delete/:: required fields not provided");
                return $this->returnBadRequest("id field is required");
            }
        }
        \Log::info("400::ucompany/delete/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * Edit company detail saved in DB
     * @param  Request $request request with name, representativeName, phoneNumber, address, email and id data
     * @return            
     */
    public function editCompany(Request $request)
    {
        if(Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::company/edit/:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
        if($request->has('data'))
        {
            $data = $request->input('data');
            if(array_key_exists("name", $data) && array_key_exists("representativeName", $data) && array_key_exists("phoneNumber", $data) && array_key_exists("address", $data) && array_key_exists('email', $data) && array_key_exists("id", $data))
            {
                $company = Company::find($data['id']);
                $company->name = $data["name"];
                $company->representativeName = $data["representativeName"];
                $company->phoneNumber = $data["phoneNumber"];
                $company->emailAddress = $data["email"];
                $company->address = $data["address"];
                $company->auditedUser = Auth::guard('admin')->user()->id;
                $company->auditedActivity = 'U';

                try
                {
                    if($company->save())
                    {
                        $ret = new \stdClass();
                        $ret->success = true;
                        $ret->data = $company;
                        \Log::info("200::company/edit/:: Company edited with id: ".$company->id);
                        return response()->json($ret);
                    }
                    else
                    {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->error = "Something wrong with our server, please try again later";
                        \Log::error("500::company/edit/:: failed to save");
                        return response()->json($ret);
                    }
                }
                catch (\Illuminate\Database\QueryException $ex)
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::company/edit/:: failed to save: ".$ex);
                    return response()->json($ret);
                }
            }
            else
            {
                \Log::info("400::company/edit/:: required fields not provided");
                return $this->returnBadRequest("id, name, representativeName, phoneNumber, email, and address field are required");
            }
        }
        \Log::info("400::ucompany/edit/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * Get list of company registered in the system
     * @param  Request $request basic request parameter
     * @return            
     */
    public function getCompanyList(Request $request)
    {
        if(Auth::guard('admin')->user()->level < 4) {
            $company = Company::selectRaw('companies.*, Count(a.id) AS ParkingLotCount')
                ->leftJoin(DB::raw('(SELECT x.id, x.company_id FROM parking_lots x JOIN admin_parking_lots y ON x.id = y.parking_lot_id WHERE y.admin_id = ' . (Auth::guard('admin')->user()->id) . ' AND x.auditedActivity <> \'D\') a'), 'a.company_id', '=', 'companies.id')
                ->join('admin_parking_lots','admin_parking_lots.parking_lot_id','=','a.id')
                ->where('companies.auditedActivity', '<>', 'D')
                //->havingRaw('COUNT(a.id) > 0')
                ->groupBy('id')
                ->groupBy('name')
                ->groupBy('representativeName')
                ->groupBy('phoneNumber')
                ->groupBy('emailAddress')
                ->groupBy('address')
                ->groupBy('auditedActivity')
                ->groupBy('auditedUser')
                ->groupBy('updated_at')
                ->groupBy('created_at')
                ->get();

            $ret = new \stdClass();
            $ret->success = true;
            $ret->data = $company;

            \Log::info("200::company/:: Get company list");
            return response()->json($ret);
        }
        else {
            $company = Company::selectRaw('companies.*, Count(a.id) AS ParkingLotCount')
                ->leftJoin(DB::raw('(SELECT x.id, x.company_id FROM parking_lots x WHERE x.auditedActivity <> \'D\') a'), 'a.company_id', '=', 'companies.id')
                //->join('admin_parking_lots','admin_parking_lots.parking_lot_id','=','a.id')
                ->where('companies.auditedActivity', '<>', 'D')
                //->havingRaw('COUNT(a.id) > 0')
                ->groupBy('id')
                ->groupBy('name')
                ->groupBy('representativeName')
                ->groupBy('phoneNumber')
                ->groupBy('emailAddress')
                ->groupBy('address')
                ->groupBy('auditedActivity')
                ->groupBy('auditedUser')
                ->groupBy('updated_at')
                ->groupBy('created_at')
                ->get();

            $ret = new \stdClass();
            $ret->success = true;
            $ret->data = $company;

            \Log::info("200::company/:: Get company list");
            return response()->json($ret);
        }
    }

    /**
     * Get detail of specific company saved in the DB
     * @param  Request $request   basic request parameter
     * @param  int  $companyID id of the company
     * @return              
     */
    public function getCompanyDetail(Request $request, $companyID)
    {
        $company = Company::selectRaw('companies.*, Count(a.id) AS parkingLotCount')
            ->leftJoin(DB::raw('(SELECT * FROM parking_lots WHERE parking_lots.auditedActivity <> \'D\') a'), 'a.company_id','=','companies.id')
            ->where('companies.auditedActivity','<>','D')
            ->where('companies.id',$companyID)
            ->first();

        $ret = new \stdClass();
        $ret->success = true;
        $ret->data = $company;

        \Log::info("200::/{companyID}/:: Get company detail for companyID: ".$companyID);
        return response()->json($ret);
    }

    /**
     * Get parking lot list for particular company
     * @param  Request $request   basic request parameter
     * @param  int  $companyID id of the company
     * @return              
     */
    public function getParkingLot(Request $request, $companyID)
    {
        if(Auth::guard('admin')->user()->level < 4)
        {
            //if(Auth::guard('admin')->user()->level == 3)
            //{
                $parkingLot = ParkingLot::selectRaw('parking_lots.*')
                    ->join('admin_parking_lots','admin_parking_lots.parking_lot_id','=','parking_lots.id')
                    ->where('parking_lots.company_id', $companyID)
                    ->where('view','1')
                    ->where('admin_id',Auth::guard('admin')->user()->id)
                    ->where('parking_lots.auditedActivity','<>','D')
                    ->where('admin_parking_lots.auditedActivity', '<>','D')
                    ->get();

                $ret = new \stdClass();
                $ret->success = true;
                $ret->data = $parkingLot;

                \Log::info("200::{companyID}/parkingLot/:: Get parkingLot for companyID: ".$companyID);
                return response()->json($ret);
            //}
        }

        $parkingLot = ParkingLot::where('company_id', $companyID)
            ->where('auditedActivity','<>','D')
            ->get();

        $ret = new \stdClass();
        $ret->success = true;
        $ret->data = $parkingLot;

        \Log::info("200::{companyID}/parkingLot/:: Get parkingLot for companyID: ".$companyID);
        return response()->json($ret);
    }

    /**
     * get admin list for particular company
     * @param  Request $request   basic request parameter
     * @param  int  $companyID id of the company
     * @param  integer $page      page offset of admin list to get
     * @return              
     */
    public function getAdmin(Request $request, $companyID, $page = 0)
    {
//        if(Auth::guard('admin')->user()->level < 4)
//        {
//            if(Auth::guard('admin')->user()->level == 3)
//            {
//                $admin = AdminParkingLot::where('admin_id',Auth::guard('admin')->user()->id)
//                ->where('parking_lot_id', $companyID)
//                ->first();
//                if($admin->view == 0)
//                {
//                    \Log::info("401::{companyID}/admin/{page?}:: Insufficient Access level");
//                    return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
//                }
//            }
//            else
//            {
//                \Log::info("401::{companyID}/admin/{page?}:: Insufficient level");
//                return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
//            }
//        }

        $skip = $page * config('app.OFFSET');
        $admin = Admin::selectRaw('admins.*')
            ->join('admin_parking_lots','admins.id','=','admin_parking_lots.admin_id')
            ->join('parking_lots','parking_lots.id','=','admin_parking_lots.parking_lot_id')
            ->where('parking_lots.company_id',$companyID)
            ->where('admins.auditedActivity','<>','D')
            ->orderBy('admins.created_at')
            ->skip($skip)
            ->take(config('app.OFFSET'))
            ->get();

        if($admin == null)
            $admin = array();

        $adminCount = Admin::join('admin_parking_lots','admins.id','=','admin_parking_lots.admin_id')
            ->join('parking_lots','parking_lots.id','=','admin_parking_lots.parking_lot_id')
            ->where('parking_lots.company_id',$companyID)
            ->where('admins.auditedActivity','<>','D')
            ->count();

        $ret = new \stdClass();
        $ret->success = true;
        $ret->total = $adminCount;
        $ret->data = $admin;

        \Log::info("200::{companyID}/admin/{page?}:: Get admins for companyID: ".$companyID);
        return response()->json($ret);
    }

    /**
     * get weekly revenue count
     * @param  Request $request    basic request parameter
     * @param  integer $weekNumber number of the week in the year
     * @param  integer $page       page offset for the detail
     * @return 
     */
    public function getWeeklyRevenue(Request $request, $companyID, $weekNumber = 0, $page = 0)
    {
        $skip = $page * config('app.OFFSET');
        $summary = DB::select("SELECT SUM(price) AS revenue, WEEKOFYEAR(outTime) AS weekNumber, YEAR(outTime) AS year 
          FROM parkings a JOIN parking_lots b ON a.parking_lot_id = b.id AND a.auditedActivity <> 'D' 
          WHERE b.company_id = ?
          GROUP BY weekNumber, year 
          ORDER BY year DESC, weekNumber DESC", [$companyID]);

        $ret = new \stdClass();
        $ret->success = true;
        $ret->data = new \stdClass();
        $ret->data->summary = $summary;

        \Log::info("200::dashboard/weeklyRevenue/{weekNumber?}/{page?}:: Get weekly revenue for week: ".$weekNumber." page: ".$page);
        return response()->json($ret);
    }

    /**
     * get monthly revenue detail and count
     * @param  Request $request     basic request parameter
     * @param  integer $monthNumber month number
     * @param  integer $page        page offset of the detail
     * @return                
     */
    public function getMonthlyRevenue(Request $request, $companyID, $monthNumber = 0, $page = 0)
    {
        $skip = $page * config('app.OFFSET');
        $summary = DB::select("SELECT SUM(price) AS revenue, MONTH(outTime) AS monthNumber, YEAR(outTime) AS year 
          FROM parkings a JOIN parking_lots b ON a.parking_lot_id = b.id AND a.auditedActivity <> 'D' 
          WHERE b.company_id = ?
          GROUP BY monthNumber, year 
          ORDER BY year DESC, monthNumber DESC", [$companyID]);

        $ret = new \stdClass();
        $ret->success = true;
        $ret->data = new \stdClass();
        $ret->data->summary = $summary;

        \Log::info("200::dashboard/monthlyRevenue/{monthNumber?}/{page?}:: Get weekly revenue for month: ".$monthNumber." page: ".$page);
        return response()->json($ret);
    }

    public function getMonthlyParking(Request $request, $companyID, $monthNumber = 0, $page = 0)
    {
        $skip = $page * config('app.OFFSET');
        $summary = DB::select("SELECT SUM(price) AS parking, MONTH(outTime) AS monthNumber, YEAR(outTime) AS year 
          FROM parkings a JOIN parking_lots b ON a.parking_lot_id = b.id AND a.auditedActivity <> 'D' 
          WHERE b.company_id = ?
          GROUP BY monthNumber, year 
          ORDER BY year DESC, monthNumber DESC", [$companyID]);

        $ret = new \stdClass();
        $ret->success = true;
        $ret->data = new \stdClass();
        $ret->data->summary = $summary;

        \Log::info("200::dashboard/monthlyParking/{monthNumber?}/{page?}:: Get weekly parking for month: ".$monthNumber." page: ".$page);
        return response()->json($ret);
    }

    public function getWeeklyParking(Request $request, $companyID, $weekNumber = 0, $page = 0)
    {
        $skip = $page * config('app.OFFSET');
        $summary = DB::select("SELECT COUNT(a.id) AS parking, WEEKOFYEAR(outTime) AS weekNumber, YEAR(outTime) AS year 
          FROM parkings a JOIN parking_lots b ON a.parking_lot_id = b.id AND a.auditedActivity <> 'D' 
          WHERE b.company_id = ?
          GROUP BY weekNumber, year 
          ORDER BY year DESC, weekNumber DESC", [$companyID]);

        $ret = new \stdClass();
        $ret->success = true;
        $ret->data = new \stdClass();
        $ret->data->summary = $summary;

        \Log::info("200::dashboard/weeklyParking/{weekNumber?}/{page?}:: Get weekly parking for week: ".$weekNumber." page: ".$page);
        return response()->json($ret);
    }
}
