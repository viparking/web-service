<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Member;
use App\Parking;
use App\Http\Requests;
use App\ParkingLot;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    /**
     * get dashboard page
     * @param  Request $request basic request parameter
     * @return            
     */
    public function getDashboard(Request $request)
    {
    	$time = $this->getFormattedDate("");
    	return view('dashboard', compact(["time"]));
    }

    /**
     * get today revenue count
     * @param  Request $request basic request parameter
     * @param  integer $page    page offset for revenue detail
     * @return            
     */
    public function getTodayRevenue(Request $request, $page = 0)
    {
    	$skip = $page * config('app.OFFSET');
    	if(Auth::guard('admin')->user()->level < 4)
    	{
    		$summary = DB::select("SELECT SUM(price) AS revenue FROM parkings a 
              JOIN parking_lots b ON a.parking_lot_id = b.id 
              JOIN admin_parking_lots c ON b.id = c.parking_lot_id 
              WHERE view = 1 AND admin_id = ? 
              AND DATE(outTime) = CURDATE() 
              AND a.auditedActivity <> 'D'", [Auth::guard('admin')->user()->id]);

    		$detail = DB::select("SELECT SUM(price) AS revenue, a.parking_lot_id, b.company_id, b.name AS parkingLotName, b.address, c.name as companyName 
              FROM parkings a JOIN parking_lots b ON a.parking_lot_id = b.id 
              JOIN companies c ON b.company_id = c.id 
              JOIN admin_parking_lots d ON b.id = d.parking_lot_id 
              WHERE DATE(outTime) = CURDATE() 
              AND a.auditedActivity <> 'D' 
              AND view = 1 AND admin_id = ? 
              GROUP BY a.parking_lot_id, b.company_id, parkingLotName, address, companyName", [Auth::guard('admin')->user()->id]);
    	}
    	else
    	{
    		$summary = DB::select("SELECT SUM(price) AS revenue FROM parkings 
              WHERE DATE(outTime) = CURDATE() AND auditedActivity <> 'D'");

    		$detail = DB::select("SELECT SUM(price) AS revenue, a.parking_lot_id, b.company_id, b.name AS parkingLotName, b.address, c.name as companyName 
              FROM parkings a JOIN parking_lots b ON a.parking_lot_id = b.id 
              JOIN companies c ON b.company_id = c.id 
              WHERE DATE(outTime) = CURDATE() 
              AND a.auditedActivity <> 'D' 
              GROUP BY a.parking_lot_id, b.company_id, parkingLotName, address, companyName");
    	}

    	$summary = $summary[0];
    	$ret = new \stdClass();
        $ret->success = true;
        $ret->data = new \stdClass();
        $ret->data->summary = $summary;
        $ret->data->detail = $detail;

        \Log::info("200::dashboard/todayRevenue:: Get today's revenue");
        return response()->json($ret);
    }

    /**
     * get weekly revenue count
     * @param  Request $request    basic request parameter
     * @param  integer $weekNumber number of the week in the year
     * @param  integer $page       page offset for the detail
     * @return 
     */
    public function getWeeklyRevenue(Request $request, $weekNumber = 0, $page = 0)
    {
        $skip = $page * config('app.OFFSET');
        if(Auth::guard('admin')->user()->level < 4)
        {
            $summary = DB::select("SELECT SUM(price) AS revenue, WEEKOFYEAR(outTime) AS weekNumber, YEAR(outTime) AS year 
              FROM parkings a JOIN parking_lots b ON a.parking_lot_id = b.id 
              JOIN admin_parking_lots c ON b.id = c.parking_lot_id 
              WHERE view = 1 
              AND a.auditedActivity <> 'D' 
              AND admin_id = ? 
              GROUP BY weekNumber, year 
              ORDER BY year DESC, weekNumber DESC", [Auth::guard('admin')->user()->id]);

            if($weekNumber > 0)
            {
                $detail = DB::select("SELECT SUM(price) AS revenue, WEEKOFYEAR(outTime) AS weekNumber, YEAR(outTime) AS year, 
                  a.parking_lot_id, b.company_id, b.name AS parkingLotName, b.address, c.name as companyName 
                  FROM parkings a JOIN parking_lots b ON a.parking_lot_id = b.id 
                  JOIN companies c ON b.company_id = c.id 
                  JOIN admin_parking_lots d ON b.id = d.parking_lot_id 
                  WHERE a.auditedActivity <> 'D' 
                  AND view = 1 
                  AND admin_id = ? 
                  AND WEEKOFYEAR(outTime) = ? 
                  GROUP BY a.parking_lot_id, b.company_id, parkingLotName, address, companyName, weekNumber, year 
                  ORDER BY companyName, parkingLotName LIMIT ? OFFSET ?",
                    [$weekNumber, Auth::guard('admin')->user()->id, config('app.OFFSET'), $skip]);
            }
        }
        else
        {
            $summary = DB::select("SELECT SUM(price) AS revenue, WEEKOFYEAR(outTime) AS weekNumber, YEAR(outTime) AS year 
              FROM parkings a JOIN parking_lots b ON a.parking_lot_id = b.id AND a.auditedActivity <> 'D' 
              GROUP BY weekNumber, year 
              ORDER BY year DESC, weekNumber DESC");
            if($weekNumber > 0)
            {
                $detail = DB::select("SELECT SUM(price) AS revenue, WEEKOFYEAR(outTime) AS weekNumber, YEAR(outTime) AS year, 
                  a.parking_lot_id, b.company_id, b.name AS parkingLotName, b.address, c.name as companyName 
                  FROM parkings a JOIN parking_lots b ON a.parking_lot_id = b.id 
                  JOIN companies c ON b.company_id = c.id 
                  WHERE a.auditedActivity <> 'D' 
                  AND WEEKOFYEAR(outTime) = ? 
                  GROUP BY a.parking_lot_id, b.company_id, parkingLotName, address, companyName, weekNumber, year 
                  ORDER BY companyName, parkingLotName LIMIT ? OFFSET ?", [$weekNumber, config('app.OFFSET'), $skip]);
            }
        }

        $ret = new \stdClass();
        $ret->success = true;
        $ret->data = new \stdClass();
        $ret->data->summary = $summary;
        if($weekNumber > 0)
            $ret->data->detail = $detail;

        \Log::info("200::dashboard/weeklyRevenue/{weekNumber?}/{page?}:: Get weekly revenue for week: ".$weekNumber." page: ".$page);
        return response()->json($ret);
    }

    /**
     * get monthly revenue detail and count
     * @param  Request $request     basic request parameter
     * @param  integer $monthNumber month number
     * @param  integer $page        page offset of the detail
     * @return                
     */
    public function getMonthlyRevenue(Request $request, $monthNumber = 0, $page = 0)
    {
        $skip = $page * config('app.OFFSET');
        if(Auth::guard('admin')->user()->level < 4)
        {
            $summary = DB::select("SELECT SUM(price) AS revenue, MONTH(outTime) AS monthNumber, YEAR(outTime) AS year 
              FROM parkings a JOIN parking_lots b ON a.parking_lot_id = b.id 
              JOIN admin_parking_lots c ON b.id = c.parking_lot_id 
              WHERE view = 1 
              AND a.auditedActivity <> 'D' 
              AND admin_id = ? 
              GROUP BY monthNumber, year 
              ORDER BY year DESC, monthNumber DESC", [Auth::guard('admin')->user()->id]);

            if($monthNumber > 0)
            {
                $detail = DB::select("SELECT SUM(price) AS revenue, MONTH(outTime) AS monthNumber, YEAR(outTime) AS year, 
                  a.parking_lot_id, b.company_id, b.name AS parkingLotName, b.address, c.name as companyName 
                  FROM parkings a JOIN parking_lots b ON a.parking_lot_id = b.id 
                  JOIN companies c ON b.company_id = c.id 
                  JOIN admin_parking_lots d ON b.id = d.parking_lot_id 
                  WHERE a.auditedActivity <> 'D' 
                  AND view = 1 
                  AND admin_id = ? 
                  AND MONTH(outTime) = ? 
                  GROUP BY a.parking_lot_id, b.company_id, parkingLotName, address, companyName, monthNumber, year 
                  ORDER BY companyName, parkingLotName LIMIT ? OFFSET ?",
                    [$monthNumber, Auth::guard('admin')->user()->id, config('app.OFFSET'), $skip]);
            }
        }
        else
        {
            $summary = DB::select("SELECT SUM(price) AS revenue, MONTH(outTime) AS monthNumber, YEAR(outTime) AS year 
              FROM parkings a JOIN parking_lots b ON a.parking_lot_id = b.id AND a.auditedActivity <> 'D' 
              GROUP BY monthNumber, year 
              ORDER BY year DESC, monthNumber DESC");
            if($monthNumber > 0)
            {
                $detail = DB::select("SELECT SUM(price) AS revenue, MONTH(outTime) AS monthNumber, YEAR(outTime) AS year, 
                  a.parking_lot_id, b.company_id, b.name AS parkingLotName, b.address, c.name as companyName 
                  FROM parkings a JOIN parking_lots b ON a.parking_lot_id = b.id 
                  JOIN companies c ON b.company_id = c.id 
                  WHERE a.auditedActivity <> 'D' 
                  AND MONTH(outTime) = ? 
                  GROUP BY a.parking_lot_id, b.company_id, parkingLotName, address, companyName, monthNumber, year 
                  ORDER BY companyName, parkingLotName LIMIT ? OFFSET ?", [$monthNumber, config('app.OFFSET'), $skip]);
            }
        }

        $ret = new \stdClass();
        $ret->success = true;
        $ret->data = new \stdClass();
        $ret->data->summary = $summary;
        if($monthNumber > 0)
            $ret->data->detail = $detail;

        \Log::info("200::dashboard/monthlyRevenue/{monthNumber?}/{page?}:: Get weekly revenue for month: ".$monthNumber." page: ".$page);
        return response()->json($ret);
    }

    public function getMonthlyParking(Request $request, $monthNumber = 0, $page = 0)
    {
        $skip = $page * config('app.OFFSET');
        if(Auth::guard('admin')->user()->level < 4)
        {
            $summary = DB::select("SELECT COUNT(a.id) AS parking, MONTH(outTime) AS monthNumber, YEAR(outTime) AS year 
              FROM parkings a JOIN parking_lots b ON a.parking_lot_id = b.id 
              JOIN admin_parking_lots c ON b.id = c.parking_lot_id 
              WHERE view = 1 
              AND a.auditedActivity <> 'D' 
              AND admin_id = ? 
              GROUP BY monthNumber, year 
              ORDER BY year DESC, monthNumber DESC", [Auth::guard('admin')->user()->id]);
        }
        else
        {
            $summary = DB::select("SELECT SUM(price) AS parking, MONTH(outTime) AS monthNumber, YEAR(outTime) AS year 
              FROM parkings a JOIN parking_lots b ON a.parking_lot_id = b.id AND a.auditedActivity <> 'D' 
              GROUP BY monthNumber, year 
              ORDER BY year DESC, monthNumber DESC");
        }

        $ret = new \stdClass();
        $ret->success = true;
        $ret->data = new \stdClass();
        $ret->data->summary = $summary;

        \Log::info("200::dashboard/monthlyParking/{monthNumber?}/{page?}:: Get weekly parking for month: ".$monthNumber." page: ".$page);
        return response()->json($ret);
    }

    public function getWeeklyParking(Request $request, $weekNumber = 0, $page = 0)
    {
        $skip = $page * config('app.OFFSET');
        if(Auth::guard('admin')->user()->level < 4)
        {
            $summary = DB::select("SELECT COUNT(a.id) AS parking, WEEKOFYEAR(outTime) AS weekNumber, YEAR(outTime) AS year 
              FROM parkings a JOIN parking_lots b ON a.parking_lot_id = b.id 
              JOIN admin_parking_lots c ON b.id = c.parking_lot_id 
              WHERE view = 1 
              AND a.auditedActivity <> 'D' 
              AND admin_id = ? 
              GROUP BY weekNumber, year 
              ORDER BY year DESC, weekNumber DESC", [Auth::guard('admin')->user()->id]);
        }
        else
        {
            $summary = DB::select("SELECT COUNT(a.id) AS parking, WEEKOFYEAR(outTime) AS weekNumber, YEAR(outTime) AS year 
              FROM parkings a JOIN parking_lots b ON a.parking_lot_id = b.id AND a.auditedActivity <> 'D' 
              GROUP BY weekNumber, year 
              ORDER BY year DESC, weekNumber DESC");
        }

        $ret = new \stdClass();
        $ret->success = true;
        $ret->data = new \stdClass();
        $ret->data->summary = $summary;

        \Log::info("200::dashboard/weeklyParking/{weekNumber?}/{page?}:: Get weekly parking for week: ".$weekNumber." page: ".$page);
        return response()->json($ret);
    }

    /**
     * get list of all the time top member (most transaction)
     * @param  Request $request basic request parameter
     * @param  integer $page    page offset of the list
     * @return            
     */
    public function getAllTimeTopMember(Request $request, $page = 0)
    {
        if(Auth::guard('admin')->user()->level < 4)
            return NULL;
    	$skip = $page * config('app.OFFSET');
    	$topMember = DB::select("SELECT SUM(a.total + b.total) AS totalTransaction, a.id AS member_id, g.username, g.email 
          FROM (SELECT IF(d.auditedActivity <> 'D', SUM(d.price), 0) AS total, c.id FROM members c 
          JOIN parkings d ON c.id = d.member_id 
          WHERE c.auditedActivity <> 'D' 
          GROUP BY c.id, d.auditedActivity) a, 
          (SELECT IF(f.auditedActivity <> 'D', SUM(f.payAmount), 0) AS total, e.id 
          FROM members e JOIN top_ups f ON e.id = f.member_id WHERE e.auditedActivity <> 'D' 
          GROUP BY e.id, f.auditedActivity) b 
          JOIN members g ON b.id = g.id 
          WHERE a.id = b.id 
          GROUP BY a.id, g.username, g.email 
          ORDER BY totalTransaction DESC LIMIT ? OFFSET ?", [config('app.OFFSET'), $skip]);
    	
    	$count = DB::select("SELECT COUNT(x.totalTransaction) AS count 
          FROM (SELECT COUNT(g.username), SUM(a.total + b.total) AS totalTransaction, a.id, g.username, g.email 
          FROM (SELECT IF(d.auditedActivity <> 'D', SUM(d.price), 0) AS total, c.id 
          FROM members c JOIN parkings d ON c.id = d.member_id WHERE c.auditedActivity <> 'D' 
          GROUP BY c.id, d.auditedActivity) a, 
          (SELECT IF(f.auditedActivity <> 'D', SUM(f.payAmount), 0) AS total, e.id 
          FROM members e JOIN top_ups f ON e.id = f.member_id WHERE e.auditedActivity <> 'D' 
          GROUP BY e.id, f.auditedActivity) b JOIN members g ON b.id = g.id WHERE a.id = b.id 
          GROUP BY a.id, g.username, g.email) x");
    	$count = $count[0]->count;

    	$ret = new \stdClass();
        $ret->success = true;
        $ret->data = $topMember;
        $ret->total = $count;

        \Log::info("200::dashboard/topMember/allTime:: Get all time top member page: ".$page);
        return response()->json($ret);
    }

    /**
     * get weekly top member list
     * @param  Request $request    basic request parameter
     * @param  integer  $weekNumber week number to get
     * @param  integer $page       page offset
     * @return               
     */
    public function getWeeklyTopMember(Request $request, $weekNumber, $page = 0)
    {
        if(Auth::guard('admin')->user()->level < 4)
            return NULL;
    	$skip = $page * config('app.OFFSET');
    	//unused -> takut lama kalau nanti jumlah member banyak
   		//  	$topMember = DB::select("SELECT SUM(COALESCE(a.total, 0) + COALESCE(b.total, 0)) AS totalTransaction, COALESCE(a.id, b.id) AS member_id, COALESCE(a.weekNumber, b.weekNumber) AS weekNumber, g.username, g.email, COUNT(g.username) AS total FROM (SELECT IF(d.auditedActivity <> 'D', SUM(d.price), 0) AS total, c.id, COALESCE(WEEKOFYEAR(outTime), 0) AS weekNumber FROM members c JOIN parkings d ON c.id = d.member_id WHERE c.auditedActivity <> 'D' GROUP BY c.id, d.auditedActivity, weekNumber) a LEFT JOIN (SELECT IF(f.auditedActivity <> 'D', SUM(f.payAmount), 0) AS total, e.id, COALESCE(WEEKOFYEAR(f.created_at), 0) AS weekNumber FROM members e JOIN top_ups f ON e.id = f.member_id WHERE e.auditedActivity <> 'D' GROUP BY e.id, f.auditedActivity) b ON a.weekNumber = b.weekNumber JOIN members g ON g.id = COALESCE(a.id, b.id) GROUP BY a.id, a.weekNumber, b.weekNumber, g.username, g.email
			// UNION SELECT SUM(COALESCE(a.total, 0) + COALESCE(b.total, 0)) AS totalTransaction, COALESCE(a.id, b.id) AS member_id, COALESCE(a.weekNumber, b.weekNumber) AS weekNumber, g.username, g.email, COUNT(g.username) AS total FROM (SELECT IF(d.auditedActivity <> 'D', SUM(d.price), 0) AS total, c.id, COALESCE(WEEKOFYEAR(outTime), 0) AS weekNumber FROM members c JOIN parkings d ON c.id = d.member_id WHERE c.auditedActivity <> 'D' GROUP BY c.id, d.auditedActivity, weekNumber) a RIGHT JOIN (SELECT IF(f.auditedActivity <> 'D', SUM(f.payAmount), 0) AS total, e.id, COALESCE(WEEKOFYEAR(f.created_at), 0) AS weekNumber FROM members e JOIN top_ups f ON e.id = f.member_id WHERE e.auditedActivity <> 'D' GROUP BY e.id, f.auditedActivity) b ON a.weekNumber = b.weekNumber JOIN members g ON g.id = COALESCE(a.id, b.id) GROUP BY a.id, a.weekNumber, b.weekNumber, g.username, g.email
			// ORDER BY weekNumber, totalTransaction DESC");

    	$topMember = DB::select("SELECT SUM(COALESCE(a.total, 0) + COALESCE(b.total, 0)) AS totalTransaction, 
          COALESCE(a.id, b.id) AS member_id, COALESCE(a.weekNumber, b.weekNumber) AS weekNumber, g.username, g.email 
          FROM (SELECT IF(d.auditedActivity <> 'D', SUM(d.price), 0) AS total, c.id, COALESCE(WEEKOFYEAR(outTime), 0) 
          AS weekNumber FROM members c JOIN parkings d ON c.id = d.member_id WHERE c.auditedActivity <> 'D' 
            GROUP BY c.id, d.auditedActivity, weekNumber) a 
          LEFT JOIN (SELECT IF(f.auditedActivity <> 'D', SUM(f.payAmount), 0) AS total, e.id, COALESCE(WEEKOFYEAR(f.created_at), 0) 
          AS weekNumber FROM members e 
          JOIN top_ups f ON e.id = f.member_id 
          WHERE e.auditedActivity <> 'D' 
          GROUP BY e.id, f.auditedActivity) b ON a.weekNumber = b.weekNumber 
          JOIN members g ON g.id = COALESCE(a.id, b.id) WHERE COALESCE(a.weekNumber, b.weekNumber) = ? 
          GROUP BY a.id, a.weekNumber, b.weekNumber, g.username, g.email UNION 
          SELECT SUM(COALESCE(a.total, 0) + COALESCE(b.total, 0)) AS totalTransaction, COALESCE(a.id, b.id) AS member_id, 
          COALESCE(a.weekNumber, b.weekNumber) AS weekNumber, g.username, g.email 
          FROM (SELECT IF(d.auditedActivity <> 'D', SUM(d.price), 0) AS total, c.id, COALESCE(WEEKOFYEAR(outTime), 0) AS weekNumber 
          FROM members c JOIN parkings d ON c.id = d.member_id WHERE c.auditedActivity <> 'D' 
          GROUP BY c.id, d.auditedActivity, weekNumber) a 
          RIGHT JOIN (SELECT IF(f.auditedActivity <> 'D', SUM(f.payAmount), 0) AS total, e.id, 
          COALESCE(WEEKOFYEAR(f.created_at), 0) AS weekNumber FROM members e JOIN top_ups f ON e.id = f.member_id 
          WHERE e.auditedActivity <> 'D' GROUP BY e.id, f.auditedActivity) b ON a.weekNumber = b.weekNumber 
          JOIN members g ON g.id = COALESCE(a.id, b.id) WHERE COALESCE(a.weekNumber, b.weekNumber) = ? 
          GROUP BY a.id, a.weekNumber, b.weekNumber, g.username, g.email ORDER BY weekNumber, totalTransaction 
          DESC LIMIT ? OFFSET ?", [$weekNumber, $weekNumber, config('app.OFFSET'), $skip]);

    	$count = DB::select("SELECT COUNT(x.totalTransaction) AS count FROM 
          (SELECT SUM(COALESCE(a.total, 0) + COALESCE(b.total, 0)) AS totalTransaction, 
          COALESCE(a.id, b.id) AS member_id, COALESCE(a.weekNumber, b.weekNumber) AS weekNumber, g.username, g.email 
          FROM (SELECT IF(d.auditedActivity <> 'D', SUM(d.price), 0) AS total, c.id, COALESCE(WEEKOFYEAR(outTime), 0) AS weekNumber 
          FROM members c JOIN parkings d ON c.id = d.member_id WHERE c.auditedActivity <> 'D' 
          GROUP BY c.id, d.auditedActivity, weekNumber) a 
          LEFT JOIN (SELECT IF(f.auditedActivity <> 'D', SUM(f.payAmount), 0) AS total, e.id, 
          COALESCE(WEEKOFYEAR(f.created_at), 0) AS weekNumber FROM members e JOIN top_ups f ON e.id = f.member_id 
          WHERE e.auditedActivity <> 'D' GROUP BY e.id, f.auditedActivity) b ON a.weekNumber = b.weekNumber 
          JOIN members g ON g.id = COALESCE(a.id, b.id) WHERE COALESCE(a.weekNumber, b.weekNumber) = ? 
          GROUP BY a.id, a.weekNumber, b.weekNumber, g.username, g.email UNION 
          SELECT SUM(COALESCE(a.total, 0) + COALESCE(b.total, 0)) AS totalTransaction, COALESCE(a.id, b.id) AS member_id, 
          COALESCE(a.weekNumber, b.weekNumber) AS weekNumber, g.username, g.email FROM (SELECT IF(d.auditedActivity <> 'D', 
          SUM(d.price), 0) AS total, c.id, COALESCE(WEEKOFYEAR(outTime), 0) AS weekNumber FROM members c 
          JOIN parkings d ON c.id = d.member_id WHERE c.auditedActivity <> 'D' GROUP BY c.id, d.auditedActivity, weekNumber) a 
          RIGHT JOIN (SELECT IF(f.auditedActivity <> 'D', SUM(f.payAmount), 0) AS total, e.id, 
          COALESCE(WEEKOFYEAR(f.created_at), 0) AS weekNumber FROM members e JOIN top_ups f ON e.id = f.member_id 
          WHERE e.auditedActivity <> 'D' GROUP BY e.id, f.auditedActivity) b ON a.weekNumber = b.weekNumber 
          JOIN members g ON g.id = COALESCE(a.id, b.id) WHERE COALESCE(a.weekNumber, b.weekNumber) = ? 
          GROUP BY a.id, a.weekNumber, b.weekNumber, g.username, g.email) x", [$weekNumber, $weekNumber]);

    	$count = $count[0]->count;
    	$ret = new \stdClass();
        $ret->success = true;
        $ret->data = $topMember;
        $ret->total = $count;

        \Log::info("200::dashboard/topMember/weekly:: Get all time top member week: ".$weekNumber." page: ".$page);
        return response()->json($ret);
    }

    /**
     * get monthly top member list
     * @param  Request $request     basic request parameter
     * @param  integer  $monthNumber number of the month to get
     * @param  integer $page        page offset
     * @return                
     */
    public function getMonthlyTopMember(Request $request, $monthNumber, $page = 0)
    {
        if(Auth::guard('admin')->user()->level < 4)
            return NULL;
    	$skip = $page * config('app.OFFSET');

    	$topMember = DB::select("SELECT SUM(COALESCE(a.total, 0) + COALESCE(b.total, 0)) AS totalTransaction, 
          COALESCE(a.id, b.id) AS member_id, COALESCE(a.monthNumber, b.monthNumber) AS monthNumber, g.username, g.email 
          FROM (SELECT IF(d.auditedActivity <> 'D', SUM(d.price), 0) AS total, c.id, COALESCE(MONTH(outTime), 0) AS monthNumber 
          FROM members c JOIN parkings d ON c.id = d.member_id WHERE c.auditedActivity <> 'D' 
          GROUP BY c.id, d.auditedActivity, monthNumber) a LEFT JOIN (SELECT IF(f.auditedActivity <> 'D', 
          SUM(f.payAmount), 0) AS total, e.id, COALESCE(MONTH(f.created_at), 0) AS monthNumber FROM members e 
          JOIN top_ups f ON e.id = f.member_id WHERE e.auditedActivity <> 'D' 
          GROUP BY e.id, f.auditedActivity) b ON a.monthNumber = b.monthNumber JOIN members g ON g.id = COALESCE(a.id, b.id) 
          WHERE COALESCE(a.monthNumber, b.monthNumber) = ? GROUP BY a.id, a.monthNumber, b.monthNumber, g.username, g.email UNION 
          SELECT SUM(COALESCE(a.total, 0) + COALESCE(b.total, 0)) AS totalTransaction, COALESCE(a.id, b.id) AS member_id, 
          COALESCE(a.monthNumber, b.monthNumber) AS monthNumber, g.username, g.email 
          FROM (SELECT IF(d.auditedActivity <> 'D', SUM(d.price), 0) AS total, c.id, COALESCE(MONTH(outTime), 0) AS monthNumber 
          FROM members c JOIN parkings d ON c.id = d.member_id WHERE c.auditedActivity <> 'D' 
          GROUP BY c.id, d.auditedActivity, monthNumber) a RIGHT JOIN (SELECT IF(f.auditedActivity <> 'D', 
          SUM(f.payAmount), 0) AS total, e.id, COALESCE(MONTH(f.created_at), 0) AS monthNumber FROM members e 
          JOIN top_ups f ON e.id = f.member_id WHERE e.auditedActivity <> 'D' 
          GROUP BY e.id, f.auditedActivity) b ON a.monthNumber = b.monthNumber JOIN members g ON g.id = COALESCE(a.id, b.id) 
          WHERE COALESCE(a.monthNumber, b.monthNumber) = ? GROUP BY a.id, a.monthNumber, b.monthNumber, g.username, g.email 
          ORDER BY monthNumber, totalTransaction DESC LIMIT ? OFFSET ?", [$monthNumber, $monthNumber, config('app.OFFSET'), $skip]);

    	$count = DB::select("SELECT COUNT(x.totalTransaction) AS count FROM 
          (SELECT SUM(COALESCE(a.total, 0) + COALESCE(b.total, 0)) AS totalTransaction, COALESCE(a.id, b.id) AS member_id, 
          COALESCE(a.monthNumber, b.monthNumber) AS monthNumber, g.username, g.email FROM (SELECT IF(d.auditedActivity <> 'D', 
          SUM(d.price), 0) AS total, c.id, COALESCE(MONTH(outTime), 0) AS monthNumber FROM members c 
          JOIN parkings d ON c.id = d.member_id WHERE c.auditedActivity <> 'D' GROUP BY c.id, d.auditedActivity, monthNumber) a 
          LEFT JOIN (SELECT IF(f.auditedActivity <> 'D', SUM(f.payAmount), 0) AS total, e.id, 
          COALESCE(MONTH(f.created_at), 0) AS monthNumber FROM members e JOIN top_ups f ON e.id = f.member_id 
          WHERE e.auditedActivity <> 'D' GROUP BY e.id, f.auditedActivity) b ON a.monthNumber = b.monthNumber 
          JOIN members g ON g.id = COALESCE(a.id, b.id) WHERE COALESCE(a.monthNumber, b.monthNumber) = ? 
          GROUP BY a.id, a.monthNumber, b.monthNumber, g.username, g.email UNION 
          SELECT SUM(COALESCE(a.total, 0) + COALESCE(b.total, 0)) AS totalTransaction, COALESCE(a.id, b.id) AS member_id, 
          COALESCE(a.monthNumber, b.monthNumber) AS monthNumber, g.username, g.email 
          FROM (SELECT IF(d.auditedActivity <> 'D', SUM(d.price), 0) AS total, c.id, COALESCE(MONTH(outTime), 0) AS monthNumber 
          FROM members c JOIN parkings d ON c.id = d.member_id WHERE c.auditedActivity <> 'D' 
          GROUP BY c.id, d.auditedActivity, monthNumber) a RIGHT JOIN (SELECT IF(f.auditedActivity <> 'D', 
          SUM(f.payAmount), 0) AS total, e.id, COALESCE(MONTH(f.created_at), 0) AS monthNumber FROM members e 
          JOIN top_ups f ON e.id = f.member_id WHERE e.auditedActivity <> 'D' 
          GROUP BY e.id, f.auditedActivity) b ON a.monthNumber = b.monthNumber 
          JOIN members g ON g.id = COALESCE(a.id, b.id) WHERE COALESCE(a.monthNumber, b.monthNumber) = ? 
          GROUP BY a.id, a.monthNumber, b.monthNumber, g.username, g.email) x", [$monthNumber, $monthNumber]);

    	$count = $count[0]->count;
    	$ret = new \stdClass();
        $ret->success = true;
        $ret->data = $topMember;
        $ret->total = $count;

        \Log::info("200::dashboard/topMember/monthly:: Get all time top member month: ".$monthNumber." page: ".$page);
        return response()->json($ret);
    }

    /**
     * get parking lot capacity for the parking lot that user allowed to see
     * @param  Request $request basic request parameter
     * @param  integer $page    page offset
     * @return            
     */
    public function getParkingLotCapacity(Request $request, $page = 0)
    {
    	$skip = $page * config('app.OFFSET');
    	$capacity = ParkingLot::selectRaw('parking_lots.id, company_id, (capacity - COUNT(a.id)) AS capacity, companies.name AS companyName, parking_lots.name AS parkingLotName, parking_lots.address')
            ->leftJoin(DB::raw('(SELECT * FROM parkings WHERE auditedActivity <> \'D\' AND outGate = \'\') a'), 'a.parking_lot_id', '=' ,'parking_lots.id')
    		->join('companies','companies.id','=','parking_lots.company_id')
            ->join('admin_parking_lots','admin_parking_lots.parking_lot_id','=','parking_lots.id')
    		->where('parking_lots.auditedActivity','<>','D')
    		->where('companies.auditedActivity','<>','D')
            ->where('admin_parking_lots.admin_id','=',Auth::guard('admin')->user()->id)
    		->groupBy('parking_lots.id')
    		->groupBy('company_id')
    		->groupBy('companyName')
    		->groupBy('parkingLotName')
    		->groupBy('address')
    		->orderBy('capacity')
    		->skip($skip)
            ->take(config('app.OFFSET'))
    		->get();

    	$count = ParkingLot::selectRaw('parking_lots.id, company_id, (capacity - COUNT(a.id)) AS capacity, companies.name AS companyName, parking_lots.name AS parkingLotName, parking_lots.address')
            ->leftJoin(DB::raw('(SELECT * FROM parkings WHERE auditedActivity <> \'D\' AND outGate = \'\') a'), 'a.parking_lot_id', '=' ,'parking_lots.id')
            ->join('companies','companies.id','=','parking_lots.company_id')
            ->where('parking_lots.auditedActivity','<>','D')
            ->where('companies.auditedActivity','<>','D')
    		->count();

    	$ret = new \stdClass();
        $ret->success = true;
        $ret->total = $count;
        $ret->data = $capacity;

        \Log::info("200::dashboard/capacity/{page?}/:: get parking lot capacity page ".$page);
        return response()->json($ret);
    }

    /**
     * get weekly problem count
     * @param  Request $request basic request parameter
     * @return            
     */
    public function getWeeklyProblemCount(Request $request, $start = NULL, $end = NULL)
    {
        if($start == NULL) {
            $weeklyProblem = DB::select("SELECT COUNT(a.id) AS parkingProblemCount, COUNT(b.id) AS generalProblemCount, 
          COALESCE(WEEKOFYEAR(COALESCE(a.created_at, b.created_at)),0) AS weekNumber, 
          COALESCE(YEAR(COALESCE(a.created_at, b.created_at)), 0) AS year FROM parking_problem a 
          LEFT JOIN general_problems b ON COALESCE(WEEKOFYEAR(a.created_at),0) = COALESCE(WEEKOFYEAR(b.created_at),0) 
          GROUP BY weekNumber, year UNION SELECT COUNT(a.id) AS parkingProblemCount, COUNT(b.id) AS generalProblemCount, 
          COALESCE(WEEKOFYEAR(COALESCE(a.created_at, b.created_at)),0) AS weekNumber, 
          COALESCE(YEAR(COALESCE(a.created_at, b.created_at)), 0) AS year FROM general_problems b 
          LEFT JOIN parking_problem a ON COALESCE(WEEKOFYEAR(a.created_at),0) = COALESCE(WEEKOFYEAR(b.created_at),0) 
          GROUP BY weekNumber, year");
        }
        else {
            $weeklyProblem = DB::select("SELECT COUNT(a.id) AS parkingProblemCount, COUNT(b.id) AS generalProblemCount, 
          COALESCE(WEEKOFYEAR(COALESCE(a.created_at, b.created_at)),0) AS weekNumber, 
          COALESCE(YEAR(COALESCE(a.created_at, b.created_at)), 0) AS year FROM parking_problem a 
          LEFT JOIN general_problems b ON COALESCE(WEEKOFYEAR(a.created_at),0) = COALESCE(WEEKOFYEAR(b.created_at),0) 
          WHERE a.created_at > $start AND a.created_at < $end GROUP BY weekNumber, year UNION 
          SELECT COUNT(a.id) AS parkingProblemCount, COUNT(b.id) AS generalProblemCount, 
          COALESCE(WEEKOFYEAR(COALESCE(a.created_at, b.created_at)),0) AS weekNumber, 
          COALESCE(YEAR(COALESCE(a.created_at, b.created_at)), 0) AS year FROM general_problems b 
          LEFT JOIN parking_problem a ON COALESCE(WEEKOFYEAR(a.created_at),0) = COALESCE(WEEKOFYEAR(b.created_at),0) 
          WHERE a.created_at > $start AND a.created_at < $end GROUP BY weekNumber, year");
        }

        $ret = new \stdClass();
        $ret->success = true;
        $ret->data = $weeklyProblem;

        \Log::info("200::dashboard/weeklyProblemCount:: get weekly problem count");
        return response()->json($ret);
    }
}
