<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\AdminParkingLot;
use App\Parking;
use App\ParkingLot;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class ParkingController extends Controller
{
    /**
     * ger pending parking for current admin
     * @param  Request $request basic request parameter
     * @return            
     */
    public function getPendingParkingCount(Request $request)
    {
        if(Auth::guard('admin')->user()->level < 4)
    	{
    		$pendingParkings = AdminParkingLot::selectRaw('COUNT(parkings.id) AS pendingCount, parking_lots.id')
    			->join('parking_lots','admin_parking_lots.parking_lot_id','=','parking_lots.id')
    			->join('parkings','parking_lots.id','=','parkings.parking_lot_id')
    			->leftJoin('transactions','transactions.referenceID','=','parkings.id')
    			->where('parkings.auditedActivity','<>','D')
                ->whereNull('transactions.id')
    			->where('admin_parking_lots.admin_id', Auth::guard('admin')->user()->id)
    			->where('view', 1)
    			->groupBy('parking_lots.id')
    			->first();
    	}
    	else
    	{
    		$pendingParkings = ParkingLot::selectRaw('COUNT(parkings.id) AS pendingCount, parking_lots.id')
    			->join('parkings','parking_lots.id','=','parkings.parking_lot_id')
                ->leftJoin('transactions','transactions.referenceID','=','parkings.id')
    			->whereNull('transactions.id')
    			->where('parkings.auditedActivity','<>','D')
    			->groupBy('parking_lots.id')
    			->first();
    	}

    	$ret = new \stdClass();
        $ret->success = true;
        $ret->data = $pendingParkings;

        \Log::info("200::parking/pendingCount:: Get Pending parking count for particular admin: ");
        return response()->json($ret);
    }

    /**
     * get parking transaction for specific parking lot
     * @param  Request $request      basic request parameter
     * @param  string  $parkingLotID parking lot ID
     * @param  integer $page         page offset
     * @return                 
     */
    public function getParkingByParkingLot(Request $request, $parkingLotID, $page = 0)
    {
        if(Auth::guard('admin')->user()->level < 4)
        {
            $admin = AdminParkingLot::where('admin_id',Auth::guard('admin')->user()->id)
            ->where('parking_lot_id', $parkingLotID)
            ->first();
            if($admin->view == 0)
            {
                \Log::info("401::parking/parkingLot/{parkingLotID}/{page?}/:: Insufficient Access level level");
                return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
            }
        }

        $skip = $page * config('app.OFFSET');
        $parking = Parking::where('parking_lot_id', $parkingLotID)
            ->where('auditedActivity','<>','D')
            ->orderBy('outGate')
            ->orderBy('inTime','DESC')
            ->skip($skip)
            ->take(config('app.OFFSET'))
            ->get();

        $count = Parking::where('parking_lot_id', $parkingLotID)
            ->where('auditedActivity','<>','D')
            ->count();

        $ret = new \stdClass();
        $ret->success = true;
        $ret->data = $parking;
        $ret->total = $count;

        \Log::info("200::parking/parkingLot/{parkingLotID}/{page?}/:: Get parking for parking lot ID: ".$parkingLotID." page: ".$page);
        return response()->json($ret);
    }

    /**
     * get parking for specific company
     * @param  Request $request   basic request parameter
     * @param  string  $companyID id of the company
     * @param  integer $page      page offset
     * @return              
     */
    public function getParkingByCompany(Request $request, $companyID, $page = 0)
    {
        if(Auth::guard('admin')->user()->level < 4)
        {
            // $admin = AdminParkingLot::where('admin_id',Auth::guard('admin')->user()->id)
            // ->where('parking_lot_id', $parkingLotID)
            // ->first();
            // if($admin->view == 0)
            // {
                \Log::info("401::parking/parkingLot/{parkingLotID}/{page?}/:: Insufficient Access level level");
                return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
            // }
        }

        $skip = $page * config('app.OFFSET');
        $parking = Parking::join('parking_lots','parking_lots.id','=','parkings.parking_lot_id')
            ->where('company_id',$companyID)
            ->where('parkings.auditedActivity','<>','D')
            ->orderBy('outGate')
            ->orderBy('inTime','DESC')
            ->skip($skip)
            ->take(config('app.OFFSET'))
            ->get();

        $count = Parking::join('parking_lots','parking_lots.id','=','parkings.parking_lot_id')
            ->where('company_id',$companyID)
            ->where('parkings.auditedActivity','<>','D')
            ->count();

        $ret = new \stdClass();
        $ret->success = true;
        $ret->data = $parking;
        $ret->total = $count;

        \Log::info("200::parking/company/{companyID}/{page?}/:: Get parking for company ID: ".$companyID." page: ".$page);
        return response()->json($ret);
    }

    /**
     * get parking transaction detail
     * @param  Request $request   basic request parameter
     * @param  string  $parkingID id of the parking transaction
     * @return              
     */
    public function getDetail(Request $request, $parkingID)
    {
        $parking = Parking::selectRaw('parkings.*, companies.name AS companyName, companies.address, parking_lots.latitude, parking_lots.longitude, parking_lots.name AS parkingLotName, members.username, payment_types.paymentType, vehicles.plateNumber, vehicles.balance, vehicles.brand, vehicles.color, vehicles.type, vehicles.card_id, vehicle_types.vehicleType, a.name as inGateName')
            ->join('parking_lots','parking_lots.id','=','parkings.parking_lot_id')
            ->join('members','members.id','=','parkings.member_id')
            ->leftJoin('payment_types','payment_types.id','=','parkings.payment_type_id')
            ->join('companies','companies.id','=','parking_lots.company_id')
            ->join('vehicles','vehicles.id','=','parkings.vehicle_id')
            ->leftJoin('vehicle_types','vehicle_types.id','=','vehicles.vehicle_type_id')
            ->leftJoin('gates as a','a.id','=','parkings.inGate')
            ->where('parkings.auditedActivity','<>','D')
            ->where('parkings.id','=',$parkingID)
            ->first();

        $ret = new \stdClass();
        $ret->success = true;
        $ret->data = $parking;

        \Log::info("200::parking/{parkingID}/detail/:: Get parking detail for parking ID: ".$parkingID);
        return response()->json($ret);
    }
}
