<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Problem;
use App\ParkingProblem;
use App\GeneralProblem;
use App\Parking;
use App\Http\Requests;

class ProblemController extends Controller
{
    /**
     * add a new probblem to the problem list
     * @param Request $request request with problem ,description, fine, and isParkingRelated parameter
     */
    public function addProblem(Request $request)
    {
    	if(Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::problem/add/:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
        if($request->has('data'))
        {
        	$data = $request->input('data');
        	if(array_key_exists("problem", $data) && array_key_exists("description", $data) && array_key_exists("fine", $data) && array_key_exists("isParkingRelated", $data))
        	{
        		$problem = new Problem();
                $problem->id = "Pr".substr($data["problem"],0,1).date('Ymdhis').substr(microtime(), 2,3);
        		$problem->problem = $data["problem"];
        		$problem->description = $data["description"];
        		$problem->fine = $data["fine"];
        		$problem->isParkingRelated = $data["isParkingRelated"];
        		$problem->auditedUser = Auth::guard('admin')->user()->id;
        		$problem->auditedActivity = 'I';

        		try
        		{
        			if($problem->save())
        			{
        				$ret = new \stdClass();
        				$ret->success = true;
        				$ret->data = $problem;
        				\Log::info("200::problem/add/:: Problem created with id: ".$problem->id);
    					return response()->json($ret);
        			}
        			else
        			{
						$ret = new \stdClass();
	                    $ret->success = false;
	                    $ret->error = "Something wrong with our server, please try again later";
	                    \Log::error("500::problem/add/:: failed to save");
	                    return response()->json($ret);
        			}
        		}
        		catch (\Illuminate\Database\QueryException $ex)
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::problem/add/:: failed to save: ".$ex);
                    return response()->json($ret);
                }
        	}
        	else
        	{
        		\Log::info("400::problem/add/:: required fields not provided");
                return $this->returnBadRequest("problem, description, fine, and isParkingRelated field are required");
        	}
        }
        \Log::info("400::problem/add/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * edit the problem that exists before
     * @param  Request $request request with problem, description, fine, id, and isParkingRelated
     * @return            
     */
    public function editProblem(Request $request)
    {
    	if(Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::problem/edit/:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
        if($request->has('data'))
        {
        	$data = $request->input('data');
        	if(array_key_exists("problem", $data) && array_key_exists("description", $data) && array_key_exists("fine", $data) && array_key_exists("isParkingRelated", $data) && array_key_exists("id", $data))
        	{
        		$problem = Problem::find($data["id"]);
        		$problem->problem = $data["problem"];
        		$problem->description = $data["description"];
        		$problem->fine = $data["fine"];
        		$problem->isParkingRelated = $data["isParkingRelated"];
        		$problem->auditedUser = Auth::guard('admin')->user()->id;
        		$problem->auditedActivity = 'U';

        		try
        		{
        			if($problem->save())
        			{
        				$ret = new \stdClass();
        				$ret->success = true;
        				$ret->data = $problem;
        				\Log::info("200::problem/edit/:: Problem edited with id: ".$problem->id);
    					return response()->json($ret);
        			}
        			else
        			{
						$ret = new \stdClass();
	                    $ret->success = false;
	                    $ret->error = "Something wrong with our server, please try again later";
	                    \Log::error("500::problem/edit/:: failed to save");
	                    return response()->json($ret);
        			}
        		}
        		catch (\Illuminate\Database\QueryException $ex)
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::problem/edit/:: failed to save: ".$ex);
                    return response()->json($ret);
                }
        	}
        	else
        	{
        		\Log::info("400::problem/edit/:: required fields not provided");
                return $this->returnBadRequest("id, problem, description, fine, and isParkingRelated field are required");
        	}
        }
        \Log::info("400::problem/edit/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * delete problem registered from the DB
     * @param  Request $request request with id parameter
     * @return            
     */
    public function deleteProblem(Request $request)
    {
    	if(Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::problem/delete/:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
        if($request->has('data'))
        {
        	$data = $request->input('data');
        	if(array_key_exists("id", $data))
        	{
        		$problem = Problem::find($data["id"]);
        		$problem->auditedUser = Auth::guard('admin')->user()->id;
        		$problem->auditedActivity = 'D';

        		try
        		{
        			if($problem->save())
        			{
        				$ret = new \stdClass();
        				$ret->success = true;
        				$ret->data = $problem;
        				\Log::info("200::problem/delete/:: Problem deleted with id: ".$problem->id);
    					return response()->json($ret);
        			}
        			else
        			{
						$ret = new \stdClass();
	                    $ret->success = false;
	                    $ret->error = "Something wrong with our server, please try again later";
	                    \Log::error("500::problem/delete/:: failed to save");
	                    return response()->json($ret);
        			}
        		}
        		catch (\Illuminate\Database\QueryException $ex)
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::problem/delete/:: failed to save: ".$ex);
                    return response()->json($ret);
                }
        	}
        	else
        	{
        		\Log::info("400::problem/delete/:: required fields not provided");
                return $this->returnBadRequest("id field is required");
        	}
        }
        \Log::info("400::problem/delete/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * get list of the problem saved in DB
     * @param  Request $request basic request parameter
     * @return            
     */
    public function getProblem(Request $request)
    {
    	$problem = Problem::where('auditedActivity','<>','D')
    		->get();

    	$ret = new \stdClass();
    	$ret->success = true;
    	$ret->data = $problem;

    	\Log::info("200::problem/:: Get problem list");
    	return response()->json($ret);
    }

    /**
     * report the problem occured
     * @param  Request $request request with parameter isParkingRelated, problemID, reason
     * @return            
     */
    public function inputProblem(Request $request)
    {
        if($request->has('data'))
        {
            $data = $request->input('data');
            if(array_key_exists("isParkingRelated", $data) && array_key_exists("problemID", $data) && array_key_exists("reason", $data))
            {
                if($data["isParkingRelated"])
                {
                    //if(array_key_exists("memberID", $data) && array_key_exists("vehicleID", $data))
                    {
                        $problem = new ParkingProblem();
                        if(array_key_exists("parkingID", $data) && $data["parkingID"] != "")
                        {
                            $problem->parking_id = $data["parkingID"];
                            $parking = Parking::find($data["parkingID"]);
                            $problem->vehicle_id = $parking->vehicle_id;
                        }
                        else
                        {
                            if(array_key_exists("cardID", $data))
                            {
                                $parking = Parking::selectRaw('parkings.id, vehicles.id AS vehicle_id')->join('vehicles','vehicles.id','=','parkings.vehicle_id')->where('card_id', $data['cardID'])->orderBy('parkings.inTime','DESC')->first();
                                $problem->parking_id = $parking->id;
                                $problem->vehicle_id = $parking->vehicle_id;
                            }
                            else
                            {
                                \Log::info("400::problem/input/:: required fields not provided: create new parking problem");
                                return $this->returnBadRequest("parkingID or CardID, memberID, and vehicleID fields are required");
                            }
                        }
                        $problem->id = "Ppr".date('Ymdhis').substr(microtime(), 2,3);
                        $problem->problem_id = $data["problemID"];
                        //$problem->vehicle_id = $data["vehicleID"];
                        $problem->reason = $data["reason"];
                        $problem->auditedUser = Auth::guard('admin')->user()->id;
                        $problem->auditedActivity = 'I';
                    }
                }
                else
                {
                    if(array_key_exists("description", $data) && array_key_exists("parkingLotID", $data))
                    {
                        $problem = new GeneralProblem();
                        $problem->id = "Gp".substr($data["description"],2,1).date('Ymdhis').substr(microtime(), 2,3);
                        $problem->problem_id = $data["problemID"];
                        $problem->reason = $data["reason"];
                        $problem->description = $data["description"];
                        $problem->parking_lot_id = $data["parkingLotID"];
                        $problem->auditedUser = Auth::guard('admin')->user()->id;
                        $problem->auditedActivity = 'I';
                    }
                    else
                    {
                        \Log::info("400::problem/input/:: required fields not provided create new general problem");
                        return $this->returnBadRequest("description and parkingLotID field is required");
                    }
                }
                try
                {
                    if($problem->save())
                    {
                        $ret = new \stdClass();
                        $ret->success = true;
                        $ret->data = $problem;
                        \Log::info("200::problem/input/:: problem inputted with id: ".$problem->id);
                        return response()->json($ret);
                    }
                    else
                    {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->error = "Something wrong with our server, please try again later";
                        \Log::error("500::problem/input/:: failed to save");
                        return response()->json($ret);
                    }
                }
                catch (\Illuminate\Database\QueryException $ex)
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::problem/input/:: failed to save: ".$ex);
                    return response()->json($ret);
                }
            }
            else
            {
                \Log::info("400::problem/input/:: required fields not provided");
                return $this->returnBadRequest("isParkingRelated, problemID, reason fields are required");
            }
        }
        \Log::info("400::problem/input/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * get general problem list for the parking lot
     * @param  Request $request      basic request parameter
     * @param  string  $parkingLotID id of the parking lot
     * @param  integer $page         page offset
     * @return                 
     */
    public function getGeneralProblemForParkingLot(Request $request, $parkingLotID, $page = 0)
    {
        if(Auth::guard('admin')->user()->level < 4)
        {
            $admin = AdminParkingLot::where('admin_id',Auth::guard('admin')->user()->id)
            ->where('parking_lot_id', $parkingLotID)
            ->first();
            if($admin->view == 0)
            {
                \Log::info("401::parkingLot/{parkingLotID}/generalProblem:: Insufficient Access level level");
                return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
            }
        }
        $skip = $page * config('app.OFFSET');

        $problem = GeneralProblem::selectRaw('general_problems.*, problems.problem')
            ->join('problems','problems.id','=','general_problems.problem_id')
            ->where('general_problems.auditedActivity','<>','D')
            ->where('parking_lot_id','=',$parkingLotID)
            ->orderBy('general_problems.created_at','DESC')
            ->skip($skip)
            ->take(config('app.OFFSET'))
            ->get();

        $problemCount = GeneralProblem::where('auditedActivity','<>','D')
            ->where('parking_lot_id','=',$parkingLotID)
            ->count();

        $ret = new \stdClass();
        $ret->success = true;
        $ret->data = $problem;
        $ret->total = $problemCount;

        \Log::info("200::parkingLot/{parkingLotID}/generalProblem:: Get general problem for parking lot id: ".$parkingLotID." page: ".$page);
        return response()->json($ret);
    }

    /**
     * get parking problem list for the specific parking lot
     * @param  Request $request      basic request parameter
     * @param  string  $parkingLotID id of the parking lot
     * @param  integer $page         page offset
     * @return                
     */
    public function getParkingProblemForParkingLot(Request $request, $parkingLotID, $page = 0)
    {
        if(Auth::guard('admin')->user()->level < 4)
        {
            $admin = AdminParkingLot::where('admin_id',Auth::guard('admin')->user()->id)
            ->where('parking_lot_id', $parkingLotID)
            ->first();
            if($admin->view == 0)
            {
                \Log::info("401::parkingLot/{parkingLotID}/generalProblem:: Insufficient Access level level");
                return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
            }
        }

        $skip = $page * config('app.OFFSET');

        $problem = ParkingProblem::selectRaw('parking_problem.*, fines AS localFine, fine AS globalFine, problems.problem, problems.description')
            ->join('parkings','parkings.id','=','parking_problem.parking_id')
            ->join('problems','problems.id','=','parking_problem.problem_id')
            ->leftJoin('fines','problems.id','=','fines.problem_id')
            ->where('parkings.parking_lot_id',$parkingLotID)
            ->where('parking_problem.auditedActivity','<>','D')
            ->where('problems.auditedActivity', '<>', 'D')
            ->where(function ($query) {
                $query->whereNull('fines.auditedActivity')
                ->orWhere('fines.auditedActivity', '<>', 'D');
            })
            ->where(function ($query) use($parkingLotID) {
                $query->whereNull('fines.parking_lot_id')
                ->orWhere('fines.parking_lot_id', $parkingLotID);
            })
            ->orderBy('parking_problem.created_at','DESC')
            ->skip($skip)
            ->take(config('app.OFFSET'))
            ->get();

        $problemCount = ParkingProblem::selectRaw('parking_problem.*, fines AS localFine, fine AS globalFine, problems.problem, problems.description')
            ->join('parkings','parkings.id','=','parking_problem.parking_id')
            ->join('problems','problems.id','=','parking_problem.problem_id')
            ->where('parkings.parking_lot_id',$parkingLotID)
            ->where('parking_problem.auditedActivity','<>','D')
            ->leftJoin('fines','problems.id','=','fines.problem_id')
            ->where('problems.auditedActivity', '<>', 'D')
            ->where(function ($query) {
                $query->whereNull('fines.auditedActivity')
                ->orWhere('fines.auditedActivity', '<>', 'D');
            })
            ->where(function ($query) use($parkingLotID) {
                $query->whereNull('fines.parking_lot_id')
                ->orWhere('fines.parking_lot_id', $parkingLotID);
            })
            ->count();

        $ret = new \stdClass();
        $ret->success = true;
        $ret->data = $problem;
        $ret->total = $problemCount;

        \Log::info("200::parkingLot/{parkingLotID}/parkingProblem:: Get parking problem for parking lot id: ".$parkingLotID." page: ".$page);
        return response()->json($ret);
    }
}
