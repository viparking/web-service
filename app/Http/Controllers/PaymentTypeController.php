<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PaymentType;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class PaymentTypeController extends Controller
{
	/**
	 * get all payment type
	 * @param  Request $request basic request parameter
	 * @return            
	 */
    public function getPaymentType(Request $request)
    {
    	$paymentType = PaymentType::get();
    	$ret = new \stdClass();
    	$ret->success = true;
    	$ret->data = $paymentType;
    	\Log::info("200::paymentType/:: Get payment type");
		return response()->json($ret);
    }

    /**
     * add new payment type
     * @param Request $request request with paymentType parameter
     */
    public function addPaymentType(Request $request)
    {
    	if(Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::paymentType/add/:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
        if($request->has('data'))
        {
        	$data = $request->input('data');
        	if(array_key_exists("paymentType", $data))
        	{
        		$paymentType = new PaymentType();
        		$paymentType->paymentType = $data["paymentType"];
        		try 
        		{
        			if($paymentType->save())
        			{
        				$ret = new \stdClass();
        				$ret->success = true;
        				$ret->data = $paymentType;
        				\Log::info("200::paymentType/add/:: paymentType with ID: ".$paymentType->id." added");
    					return response()->json($ret);
        			}
        			else
        			{
        				$ret = new \stdClass();
	                    $ret->success = false;
	                    $ret->error = "Something wrong with our server, please try again later";
	                    \Log::error("500::paymentType/add/:: failed to save");
	                    return response()->json($ret);
        			}
        		} 
        		catch (\Illuminate\Database\QueryException $ex)
    			{
    				$ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::paymentType/add/:: failed to save: ".$ex);
                    return response()->json($ret);
    			}
        	}
        	else
        	{
        		\Log::info("400::paymentType/add/:: required fields not provided");
                return $this->returnBadRequest("paymentType field is required");
        	}
        }
        \Log::info("400::paymentType/add/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * edit payment type with spcific id
     * @param  Request $request request with id and paymentType parameter
     * @return            
     */
    public function editPaymentType(Request $request)
    {
    	if(Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::paymentType/edit/:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
        if($request->has('data'))
        {
        	$data = $request->input('data');
        	if(array_key_exists("id", $data) && array_key_exists("paymentType", $data))
        	{
        		$paymentType = PaymentType::find($data["id"]);
        		$paymentType->paymentType = $data["paymentType"];

        		try
        		{
        			if($paymentType->save())
        			{
        				$ret = new \stdClass();
        				$ret->success = true;
        				$ret->data = $paymentType;
        				\Log::info("200::paymentType/edit/:: paymentType with ID: ".$paymentType->id." edited");
    					return response()->json($ret);
        			}
        			else
        			{
        				$ret = new \stdClass();
	                    $ret->success = false;
	                    $ret->error = "Something wrong with our server, please try again later";
	                    \Log::error("500::paymentType/edit/:: failed to save");
	                    return response()->json($ret);
        			}
        		}
        		catch (\Illuminate\Database\QueryException $ex)
    			{
    				$ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::paymentType/edit/:: failed to save: ".$ex);
                    return response()->json($ret);
    			}
        	}
        	else
        	{
        		\Log::info("400::paymentType/edit/:: required fields not provided");
                return $this->returnBadRequest("paymentType and id field are required");
        	}
        }
        \Log::info("400::paymentType/edit/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * delete payment type at specific id
     * @param  Request $request request with id parameter
     * @return
     */
    public function deletePaymentType(Request $request)
    {
    	if(Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::paymentType/delete/:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
        if($request->has('data'))
        {
        	$data = $request->input("data");
        	if(array_key_exists("id", $data))
        	{
        		$paymentType = PaymentType::find($data["id"]);

        		try
        		{
        			if($paymentType->delete())
        			{
        				$ret = new \stdClass();
        				$ret->success = true;
        				\Log::info("200::paymentType/delete/:: paymentType with ID: ".$paymentType->id." deleted");
    					return response()->json($ret);
        			}
        			else
        			{
        				$ret = new \stdClass();
	                    $ret->success = false;
	                    $ret->error = "Something wrong with our server, please try again later";
	                    \Log::error("500::paymentType/delete/:: failed to save");
	                    return response()->json($ret);
        			}
        		}
        		catch (\Illuminate\Database\QueryException $ex)
    			{
    				$ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::paymentType/delete/:: failed to save: ".$ex);
                    return response()->json($ret);
    			}
        	}
        	else
        	{
        		\Log::info("400::paymentType/delete/:: required fields not provided");
                return $this->returnBadRequest("id field is required");
        	}
        }
        \Log::info("400::paymentType/delete/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }
}
