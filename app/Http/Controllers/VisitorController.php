<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Visitor;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class VisitorController extends Controller
{
    public function register(Request $request) {
        if($request->has('data'))
        {
            $data = $request->input('data');
            if(array_key_exists('cardID', $data) && array_key_exists("name", $data) && array_key_exists("plateNumber", $data)
            && array_key_exists("phoneNumber", $data) && array_key_exists("photo", $data))
            {
                $visitorID = "V".substr($data["cardID"],0,2).date('Ymdhis').substr(microtime(), 2,3);

                $visitor = new Visitor();
                $visitor->id = $visitorID;
                $visitor->cardID = $data["cardID"];
                $visitor->name = $data["name"];
                $visitor->plateNumber = $data["plateNumber"];
                $visitor->phoneNumber = $data["phoneNumber"];
                $visitor->expireTime = Carbon::tomorrow('Asia/Jakarta');

                $tempImage = $this->base64_to_jpeg($data["photo"],"ktp/t".$visitor->id.".jpg");
                $this->compress_image($tempImage, "ktp/".$visitor->id.".jpg", 60);
                unlink("ktp/t".$visitor->id.".jpg");

                try
                {
                    if($visitor->save())
                    {
                        $ret = new \stdClass();
                        $ret->success = true;
                        $ret->data = $visitor;
                        \Log::info("200::visitor/register/:: Visitor registered with id: ".$visitor->id);
                        return response()->json($ret);
                    }
                    else
                    {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->error = "Something wrong with our server, please try again later";
                        \Log::error("500::visitor/register/:: failed to save");
                        return response()->json($ret);
                    }
                }
                catch (\Illuminate\Database\QueryException $ex)
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::visitor/register/:: failed to save: ".$ex);
                    return response()->json($ret);
                }
            }
            else
            {
                \Log::info("400::visitor/register/:: required fields not provided");
                return $this->returnBadRequest("cardID, name, plateNumber, phoneNumber field are required");
            }
        }
        \Log::info("400::visitor/register/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    public function getDetail(Request $request, $cardID) {
        $visitor = Visitor::where('cardID',$cardID)->first();
        if($visitor != null) {
            $ret = new \stdClass();
            $ret->success = true;
            $ret->data = $visitor;
            \Log::info("200::visitor/detail/{cardID}:: Get visitor with cardID: ".$cardID);
            return response()->json($ret);
        }
        else {
            $ret = new \stdClass();
            $ret->success = false;
            $ret->error = "No visitor with card ID ".$cardID;
            \Log::info("200::visitor/detail/{cardID}:: Get visitor failed with cardID: ".$cardID);
            return response()->json($ret);
        }
    }

    public function unregister(Request $request) {
        if($request->has('data'))
        {
            $data = $request->input('data');
            if(array_key_exists('cardID', $data) && array_key_exists('reason',$data))
            {
                $visitor = Visitor::where('cardID',$data["cardID"])->first();
                if($visitor == null || $visitor->takeKTPTime != "0000-00-00 00:00:00") {
                    $ret = new \stdClass();
                    $ret->success = false;
                    if($visitor == null)
                        $ret->error = "No visitor registered with card ID ".$data["cardID"];
                    else
                        $ret->error = "Last Visitor with card ID ".$data["cardID"]." has been out of parking lot";
                    \Log::error("200::visitor/unregister/:: invalid card id or visitor has been out");
                    return response()->json($ret);
                }
                $visitor->takeKTPTime = Carbon::now('Asia/Jakarta');
                $visitor->reason = $data["reason"];

                try
                {
                    if($visitor->save())
                    {
                        $ret = new \stdClass();
                        $ret->success = true;
                        $ret->data = $visitor;
                        \Log::info("200::visitor/unregister/:: Visitor unregistered with id: ".$visitor->id);
                        return response()->json($ret);
                    }
                    else
                    {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->error = "Something wrong with our server, please try again later";
                        \Log::error("500::visitor/unregister/:: failed to save");
                        return response()->json($ret);
                    }
                }
                catch (\Illuminate\Database\QueryException $ex)
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::visitor/unregister/:: failed to save: ".$ex);
                    return response()->json($ret);
                }
            }
            else
            {
                \Log::info("400::visitor/unregister/:: required fields not provided");
                return $this->returnBadRequest("cardID and reason field are required");
            }
        }
        \Log::info("400::visitor/unregister/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * compress uploaded image
     * @param  [type] $source_url      [description]
     * @param  [type] $destination_url [description]
     * @param  [type] $quality         [description]
     * @return [type]                  [description]
     */
    private function compress_image($source_url, $destination_url, $quality)
    {
        $info = getimagesize($source_url);
        if ($info['mime'] == 'image/jpeg')
            $image = imagecreatefromjpeg($source_url);
        else if ($info['mime'] == 'image/gif')
            $image = imagecreatefromgif($source_url);
        else if ($info['mime'] == 'image/png')
            $image = imagecreatefrompng($source_url);
        else
            $image = imagecreatefromjpeg($source_url);
        imagejpeg($image, $destination_url, $quality);
        return $destination_url;
    }

    /**
     * convert image to base64
     * @param  [type] $base64_string [description]
     * @param  [type] $output_file   [description]
     * @return [type]                [description]
     */
    private function base64_to_jpeg($base64_string, $output_file) {
        $ifp = fopen($output_file, "wb+");

        fwrite($ifp, base64_decode($base64_string));
        fclose($ifp);

        return $output_file;
    }
}
