<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\TopUp;
use App\Vehicle;
use App\TopUpAmount;
use App\Parking;
use App\Gate;
use App\Price;
use App\ParkingLot;
use App\Transaction;
use App\ParkingProblem;
use App\Fine;
use App\Visitor;
use Carbon\Carbon;
use DB;

class TransactionController extends Controller
{
    /**
     * top up to the specific vehicle
     * @param  Request $request request with topUpAmountID and cardID
     * @return 
     */
    public function topUp(Request $request)
    {
        if($request->has('data'))
        {
            $data = $request->input('data');
            if(array_key_exists('topUpAmountID', $data) && array_key_exists("cardID", $data))
            {
                $topUpID = "T".substr($data["cardID"],0,2).date('Ymdhis').substr(microtime(), 2,3);

                $vehicle = Vehicle::where('card_id', $data["cardID"])->first();
                $amount = TopUpAmount::find($data["topUpAmountID"]);

                $topUp = new TopUp();
                $topUp->id = $topUpID;
                $topUp->member_id = $vehicle->member_id;
                $topUp->vehicle_id = $vehicle->id;
                $topUp->payAmount = $amount->payAmount;
                $topUp->creditAmount = $amount->creditAmount;
                $topUp->auditedActivity = 'I';
                $topUp->auditedUser = Auth::guard('admin')->user()->id;

                $transaction = new Transaction();
                $transaction->id = "Tr".substr($data["cardID"],1,2).date('Ymdhis').substr(microtime(), 2,3);
                $transaction->transaction_type_id = 1;
                $transaction->referenceID = $topUp->id;
                $transaction->startingBalance = $vehicle->balance;
                $transaction->balance = $vehicle->balance + $amount->creditAmount;
                $transaction->auditedUser = Auth::guard('admin')->user()->id;
                $transaction->auditedActivity = 'I';

                $vehicle->balance += $amount->creditAmount;

                try
                {
                    if($topUp->save() && $vehicle->save())
                    {
                        $ret = new \stdClass();
                        $ret->success = true;
                        $ret->data = new \stdClass();
                        $ret->data->topUp = $topUp;
                        $ret->data->vehicle = $vehicle;
                        \Log::info("200::transaction/topUp/:: Vehicle with id: ".$vehicle->id." Topped up for: ".$amount->creditAmount);
                        return response()->json($ret);
                    }
                    else
                    {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->error = "Something wrong with our server, please try again later";
                        \Log::error("500::transaction/topUp/:: failed to save");
                        return response()->json($ret);
                    }
                }
                catch (\Illuminate\Database\QueryException $ex)
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::transaction/topUp/:: failed to save: ".$ex);
                    return response()->json($ret);
                }
            }
            else
            {
                \Log::info("400::transaction/topup:: required fields not provided");
                return $this->returnBadRequest("cardID and topUpAmountID field are required");
            }
        }
        \Log::info("400::transaction/topUp/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    //backup old
    /*
    public function gateIn(Request $request)
    {
        if($request->has('data'))
        {
            $data = $request->input('data');
            if(array_key_exists("gateID", $data) && array_key_exists("photo", $data))
            {
                $vehicle = null;
                if(array_key_exists("cardID", $data))
                    $vehicle = Vehicle::where('card_id', $data["cardID"])->first();
                else if(array_key_exists("plateNumber", $data))
                    $vehicle = Vehicle::where('plateNumber', $data["plateNumber"])->first();

                if($vehicle == null)
                {
                    if(!array_key_exists("vehicleTypeID", $data))
                    {
                        \Log::info("400::gate/in/:: required fields not provided");
                        return $this->returnBadRequest("vehicleTypeID field is required");
                    }
                    $vehicle = new Vehicle();
                    $vehicle->id = "V".substr($data["gateID"],1,2).date('Ymdhis').substr(microtime(), 2,3);
                    if(array_key_exists("plateNumber", $data))
                        $vehicle->plateNumber = $data['plateNumber'];
                    $vehicle->vehicle_type_id = $data["vehicleTypeID"];
                    $vehicle->member_id = 'guest';
                    $vehicle->card_id = $vehicle->id;

                    try
                    {
                        if(!$vehicle->save())
                        {
                            $ret = new \stdClass();
                            $ret->success = false;
                            $ret->error = "Something wrong with our server, please try again later";
                            \Log::error("500::gate/in/:: failed to save parking: ".$ex);
                            return response()->json($ret);
                        }
                    }
                    catch (\Illuminate\Database\QueryException $ex)
                    {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->error = "Something wrong with our server, please try again later";
                        \Log::error("500::gate/in/:: failed to save parking: ".$ex);
                        return response()->json($ret);
                    }
                }
                $gate = Gate::find($data["gateID"]);
                $parking = new Parking();
                $parking->id = "P".substr($data["gateID"],1,2).date('Ymdhis').substr(microtime(), 2,3);
                $parking->parking_lot_id = $gate->parking_lot_id;
                $parking->vehicle_id = $vehicle->id;
                $parking->member_id = $vehicle->member_id;
                if(array_key_exists("inTime", $data))
                    $parking->inTime =  Carbon::parse($data["inTime"]);
                else
                    $parking->inTime = Carbon::now('Asia/Jakarta');
                $parking->inGate = $data["gateID"];
                $parking->auditedActivity = 'I';
                //$parking->auditedUser = Auth::guard('admin')->user()->id;

                $tempImage = $this->base64_to_jpeg($data["photo"],"gateIn/t".$parking->id.".jpg");
                $this->compress_image($tempImage, "gateIn/".$parking->id.".jpg", 60);
                unlink("gateIn/t".$parking->id.".jpg");

                try
                {
                    if($parking->save())
                    {
                        $ret = new \stdClass();
                        $ret->success = true;
                        $ret->data = $parking;
                        \Log::info("200::gate/in/:: Gate in for parkingID: ".$parking->id);
                        return response()->json($ret);
                    }
                    else
                    {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->error = "Something wrong with our server, please try again later";
                        \Log::error("500::gate/in/:: failed to save parking");
                        return response()->json($ret);
                    }
                }
                catch (\Illuminate\Database\QueryException $ex)
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::gate/in/:: failed to save parking: ".$ex);
                    return response()->json($ret);
                }
            }
            else
            {
                \Log::info("400::gate/in/:: required fields not provided");
                return $this->returnBadRequest("gateID and photo field are required");
            }
        }
        \Log::info("400::gate/in/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    public function gateOut(Request $request)
    {
        if($request->has('data'))
        {
            $data = $request->input('data');
            if(array_key_exists('gateID', $data) &&  array_key_exists("photo", $data))
            {
                $gate = Gate::find($data["gateID"]);
                $parkingLot = ParkingLot::find($gate->parking_lot_id);
                $parking = null;
                $vehicle = null;
                if(array_key_exists("cardID", $data))
                {
                    $vehicle = Vehicle::where('card_id',$data["cardID"])->first();
                    $parking = $vehicle->parking()->orderBy('created_at')
                        ->where('outGate', '')
                        ->first();

                    if($parking == null)
                    {
                        \Log::info("400::gate/out/:: This vehicle has been out from the parking lot");
                        return $this->returnBadRequest("This vehicle has been out from the parking lot");
                    }
                }
                else if(array_key_exists("parkingID", $data))
                {
                    $parking = Parking::find($data["parkingID"]);
                    $vehicle = $parking->vehicle;
                    if(array_key_exists("plateNumber", $data))
                        $vehicle->plateNumber = $data["plateNumber"];
                }
                else if(array_key_exists("plateNumber", $data))
                {
                    $vehicle = Vehicle::where('plateNumber',$data["plateNumber"])->first();
                    $parking = $vehicle->parking()->orderBy('created_at')
                        ->where('outGate', '')
                        ->first();
                }
                else
                {
                    \Log::info("400::gate/out/:: optional required fields not provided");
                    return $this->returnBadRequest("one of cardID, parkingID, or plate Number field are required");
                }

                if(count($parking->parkingProblem) > 0)
                {
                    $transaction = Transaction::where('referenceID', $parking->parkingProblem()->first()->id)->first();
                    if($transaction == null)
                    {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->data = $parking->parkingProblem;
                        $ret->error = "There is a problem with this parking";

                        \Log::error("200::gate/out/:: problem with parking");
                        return response()->json($ret);
                    }
                }

                $tempImage = $this->base64_to_jpeg($data["photo"],"gateOut/t".$parking->id.".jpg");
                $this->compress_image($tempImage, "gateOut/".$parking->id.".jpg", 60);
                unlink("gateOut/t".$parking->id.".jpg");

                $price = Price::where('parking_lot_id',$parking->parking_lot_id)
                    ->where('vehicle_type_id', $vehicle->vehicle_type_id)
                    ->first();

                if(array_key_exists("outTime", $data))
                    $parking->outTime =  Carbon::parse($data["outTime"]);
                else
                    $parking->outTime = Carbon::now('Asia/Jakarta');
                $inTime = Carbon::parse($parking->inTime);
                $outTime = Carbon::parse($parking->outTime);
                $diffInMinutes = $outTime->diffInMinutes($inTime);

                $total = 0;
                if($diffInMinutes < $price->freeDuration)
                    $total = 0;
                else
                {
                    $freeStart = Carbon::now();
                    $freeStart->minute = 0;
                    $freeStart->second = 0;

                    if($freeStart->hour <= $price->freeEnd)
                        $freeStart->day -= 1;
                    $freeStart->hour = $price->freeStart;

                    $freeEnd = Carbon::now();
                    $freeEnd->minute = 0;
                    $freeEnd->second = 0;

                    if($freeEnd->hour >= $price->freeEnd)
                        $freeEnd->day += 1;
                    $freeEnd->hour = $price->freeEnd;

                    if($freeStart->timestamp <= $inTime->timestamp)
                    {
                        //pas masuk udah mulai gratis
                        if($outTime->timestamp <= $freeEnd->timestamp)
                            $total = 0;
                        else
                        {
                            //bayar lebihnya
                            $diffInMinutes = $freeEnd->diffInMinutes($outTime);

                            //bayar lebihnya
                            while($diffInMinutes > 0)
                            {
                                $diffInMinutes -= 60;
                                $diffInMinutes += $price->price;
                            }

                            if($inTime->diffInDays($outTime) > 0)
                                $total += $price->overnightPrice;

                            if($total > $price->maxPrice && $price->maxPrice > 0)
                                $total = $price->maxPrice;
                        }
                    }
                    else if($outTime->timestamp >= $freeStart->timestamp)
                    {
                        //selama di dalem baru mulai gratis
                        $diffInMinutes = $freeStart->diffInMinutes($inTime);

                        $counter = 0;
                        while($counter < $price->initialLength && $diffInMinutes > 0)
                        {
                            $diffInMinutes -= 60;
                            $total += $price->initialPrice;
                            $counter++;
                        }
                        //per hour
                        while($diffInMinutes > 0)
                        {
                            $diffInMinutes -= 60;
                            $total += $price->price;
                        }

                        if($outTime->timestamp >= $freeEnd->timestamp)
                        {
                            $diffInMinutes = $freeEnd->diffInMinutes($outTime);

                            //bayar lebihnya
                            while($diffInMinutes > 0)
                            {
                                $diffInMinutes -= 60;
                                $total += $price->price;
                            }

                            if($inTime->diffInDays($outTime) > 0)
                                $total += $price->overnightPrice;

                            if($total > $price->maxPrice && $price->maxPrice > 0)
                                $total = $price->maxPrice;
                        }
                    }
                    else
                    {
                        //normal
                        //first hour
                        $counter = 0;
                        while($counter < $price->initialLength && $diffInMinutes > 0)
                        {
                            $diffInMinutes -= 60;
                            $total += $price->initialPrice;
                            $counter++;
                        }

                        //per hour
                        while($diffInMinutes > 0)
                        {
                            $diffInMinutes -= 60;
                            $total += $price->price;
                        }

                        $total += $price->overnightPrice * $inTime->diffInDays($outTime);

                        if($total > $price->maxPrice && $price->maxPrice > 0)
                            $total = $price->maxPrice;
                    }
                }
                $parking->price = $total;
                $parking->outGate = $data["gateID"];
                $parking->outTime = $outTime;
                $parking->auditedActivity = 'U';
                //$parking->auditedUser = Auth::guard('admin')->user()->id;

                try
                {
                    if($parking->save())
                    {
                        $parking->vehicle = $vehicle;
                        $ret = new \stdClass();
                        $ret->success = true;
                        $ret->data = $parking;

                        \Log::error("200::gate/out/:: Gate out");
                        return response()->json($ret);
                    }
                    else
                    {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->error = "Something wrong with our server, please try again later";
                        \Log::error("500::gate/out/:: failed to save parking");
                        return response()->json($ret);
                    }
                }
                catch (\Illuminate\Database\QueryException $ex)
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::gate/out/:: failed to save parking: ".$ex);
                    return response()->json($ret);
                }
            }
            else
            {
                \Log::info("400::gate/out/:: required fields not provided");
                return $this->returnBadRequest("gateID, outTime and photo field are required");
            }
        }
        \Log::info("400::gate/out/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }*/

    /* ============================ SEBELUM GM ====================================================== */
    /**
     * create gate in transaction for the parking transcation
     * @param  Request $request request with gateID, photo, cardID, isMember parameter
     * @return            
     */
    public function gateIn(Request $request)
    {
        if($request->has('data'))
        {
            $data = $request->input('data');
            if(array_key_exists("gateID", $data) && array_key_exists("photo", $data) && array_key_exists("cardID", $data)
                && array_key_exists("isMember", $data))
            {
                $vehicle = null;
                if($data["isMember"])
                    $vehicle = Vehicle::where('card_id', $data["cardID"])->first();

                if($vehicle == null)
                {
                    if(!array_key_exists("vehicleTypeID", $data))
                    {
                        \Log::info("400::gate/in/:: required fields not provided");
                        return $this->returnBadRequest("vehicleTypeID field is required");
                    }
                    $vehicle = new Vehicle();
                    $vehicle->id = "V".substr($data["gateID"],1,2).date('Ymdhis').substr(microtime(), 2,3);
                    if(array_key_exists("plateNumber", $data))
                        $vehicle->plateNumber = $data['plateNumber'];
                    $vehicle->vehicle_type_id = $data["vehicleTypeID"];
                    $vehicle->member_id = 'guest';
                    $vehicle->card_id = $data["cardID"];

                    try
                    {
                        if(!$vehicle->save())
                        {
                            $ret = new \stdClass();
                            $ret->success = false;
                            $ret->error = "Something wrong with our server, please try again later";
                            \Log::error("500::gate/in/:: failed to save parking");
                            return response()->json($ret);
                        }
                    }
                    catch (\Illuminate\Database\QueryException $ex)
                    {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->error = "Something wrong with our server, please try again later";
                        \Log::error("500::gate/in/:: failed to save parking: ".$ex);
                        return response()->json($ret);
                    }
                }
                $gate = Gate::find($data["gateID"]);
                $parking = new Parking();
                $parking->id = "P".substr($data["gateID"],1,2).date('Ymdhis').substr(microtime(), 2,3);
                $parking->parking_lot_id = $gate->parking_lot_id;
                $parking->vehicle_id = $vehicle->id;
                $parking->member_id = $vehicle->member_id;
                if(array_key_exists("inTime", $data))
                    $parking->inTime =  Carbon::parse($data["inTime"]);
                else
                    $parking->inTime = Carbon::now('Asia/Jakarta');
                $parking->inGate = $data["gateID"];
                $parking->auditedActivity = 'I';
                //$parking->auditedUser = Auth::guard('admin')->user()->id;

                $tempImage = $this->base64_to_jpeg($data["photo"],"gateIn/t".$parking->id.".jpg");
                $this->compress_image($tempImage, "gateIn/".$parking->id.".jpg", 60);
                unlink("gateIn/t".$parking->id.".jpg");

                if(array_key_exists("photo2", $data)) {
                    $tempImage = $this->base64_to_jpeg($data["photo2"],"gateIn/t".$parking->id."2.jpg");
                    $this->compress_image($tempImage, "gateIn/".$parking->id."2.jpg", 60);
                    unlink("gateIn/t".$parking->id."2.jpg");
                }

                try
                {
                    if($parking->save())
                    {
                        $ret = new \stdClass();
                        $ret->success = true;
                        $ret->data = $parking;
                        \Log::info("200::gate/in/:: Gate in for parkingID: ".$parking->id);
                        return response()->json($ret);
                    }
                    else
                    {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->error = "Something wrong with our server, please try again later";
                        \Log::error("500::gate/in/:: failed to save parking");
                        return response()->json($ret);
                    }
                }
                catch (\Illuminate\Database\QueryException $ex)
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::gate/in/:: failed to save parking: ".$ex);
                    return response()->json($ret);
                }
            }
            else
            {
                \Log::info("400::gate/in/:: required fields not provided");
                return $this->returnBadRequest("gateID and photo field are required!");
            }
        }
        \Log::info("400::gate/in/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * create gate out transaction to close the current parking transaction
     * @param  Request $request request with gateID, photo, isMember, and cardID parameter
     * @return
     */
    public function gateOut(Request $request)
    {
        if($request->has('data'))
        {
            $data = $request->input('data');
            if(array_key_exists('gateID', $data) &&  array_key_exists("photo", $data) && array_key_exists("isMember", $data)&& array_key_exists("cardID", $data))
            {
                $gate = Gate::find($data["gateID"]);
                $parkingLot = ParkingLot::find($gate->parking_lot_id);
                $parking = null;
                $vehicle = null;
                //if(array_key_exists("cardID", $data))
                {
                    $vehicle = Vehicle::where('card_id',$data["cardID"])
                        ->orderBy('created_at','DESC')
                        ->first();
                    $parking = $vehicle->parking()->orderBy('created_at','DESC')
                        ->where('outGate', '')
                        ->first();

                    if(array_key_exists("plateNumber", $data))
                        $vehicle->plateNumber = $data["plateNumber"];

                    if($parking == null)
                    {
                        if(array_key_exists("parkingID", $data)) {
                            $parking = Parking::find($data["parkingID"]);

                            $parking->vehicle = $vehicle;
                            $parking->outTime = Carbon::parse($parking->outTime);
                            $ret = new \stdClass();
                            $ret->success = true;
                            $ret->data = $parking;

                            \Log::error("200::gate/out/:: Gate out From Backend for parkingID: ".$data["parkingID"]);
                            return response()->json($ret);
                        }
                        $parking = $vehicle->parking()->orderBy('created_at','DESC')
                            ->first();
//                        \Log::info("400::gate/out/:: This vehicle has been out from the parking lot");
//                        return $this->returnBadRequest("This vehicle has been out from the parking lot");
                    }
                }
                // else if(array_key_exists("parkingID", $data))
                // {
                //     $parking = Parking::find($data["parkingID"]);
                //     $vehicle = $parking->vehicle;
                //     if(array_key_exists("plateNumber", $data))
                //         $vehicle->plateNumber = $data["plateNumber"];
                // }
                // else if(array_key_exists("plateNumber", $data))
                // {
                //     $vehicle = Vehicle::where('plateNumber',$data["plateNumber"])->first();
                //     $parking = $vehicle->parking()->orderBy('created_at')
                //         ->where('outGate', '')
                //         ->first();
                // }
                // else
                // {
                //     \Log::info("400::gate/out/:: optional required fields not provided");
                //     return $this->returnBadRequest("one of cardID, parkingID, or plate Number field are required");
                // }

                if(count($parking->parkingProblem) > 0)
                {
                    $transaction = Transaction::where('referenceID', $parking->parkingProblem()->first()->id)->first();
                    if($transaction == null)
                    {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->data = $parking->parkingProblem;
                        $ret->error = "There is a problem with this parking";

                        \Log::error("200::gate/out/:: problem with parking");
                        return response()->json($ret);
                    }
                }

                $tempImage = $this->base64_to_jpeg($data["photo"],"gateOut/t".$parking->id.".jpg");
                $this->compress_image($tempImage, "gateOut/".$parking->id.".jpg", 60);
                unlink("gateOut/t".$parking->id.".jpg");

                if(array_key_exists("photo2", $data)) {
                    $tempImage = $this->base64_to_jpeg($data["photo2"],"gateOut/t".$parking->id."2.jpg");
                    $this->compress_image($tempImage, "gateOut/".$parking->id."2.jpg", 60);
                    unlink("gateOut/t".$parking->id."2.jpg");
                }

                $price = Price::where('parking_lot_id',$parking->parking_lot_id)
                    ->where('vehicle_type_id', $vehicle->vehicle_type_id)
                    ->first();

                if(array_key_exists("outTime", $data))
                    $parking->outTime =  Carbon::parse($data["outTime"]);
                else
                    $parking->outTime = Carbon::now('Asia/Jakarta');
                $inTime = Carbon::parse($parking->inTime);
                $outTime = Carbon::parse($parking->outTime);
                $diffInMinutes = $outTime->diffInMinutes($inTime);

                $total = 0;

                $vip = $vehicle->vip()
                    ->where('due_date', '>=', $parking->outTime->toDateString())
                    ->first();

                if($vip != null)
                    $total = 0;
                else if($diffInMinutes < $price->freeDuration)
                    $total = 0;
                else
                {
                    $freeStart = Carbon::now();
                    $freeStart->minute = 0;
                    $freeStart->second = 0;

                    if($freeStart->hour <= $price->freeEnd)
                        $freeStart->day -= 1;
                    $freeStart->hour = $price->freeStart;

                    $freeEnd = Carbon::now();
                    $freeEnd->minute = 0;
                    $freeEnd->second = 0;

                    if($freeEnd->hour >= $price->freeEnd)
                        $freeEnd->day += 1;
                    $freeEnd->hour = $price->freeEnd;

                    if($freeStart->timestamp <= $inTime->timestamp)
                    {
                        //pas masuk udah mulai gratis
                        if($outTime->timestamp <= $freeEnd->timestamp)
                            $total = 0;
                        else
                        {
                            //bayar lebihnya
                            $diffInMinutes = $freeEnd->diffInMinutes($outTime);

                            //bayar lebihnya
                            while($diffInMinutes > 0)
                            {
                                $diffInMinutes -= 60;
                                $diffInMinutes += $price->price;
                            }

                            if($inTime->diffInDays($outTime) > 0)
                                $total += $price->overnightPrice;

                            if($total > $price->maxPrice && $price->maxPrice > 0)
                                $total = $price->maxPrice;
                        }
                    }
                    else if($outTime->timestamp >= $freeStart->timestamp)
                    {
                        //selama di dalem baru mulai gratis
                        $diffInMinutes = $freeStart->diffInMinutes($inTime);

                        $counter = 0;
                        while($counter < $price->initialLength && $diffInMinutes > 0)
                        {
                            $diffInMinutes -= 60;
                            $total += $price->initialPrice;
                            $counter++;
                        }
                        //per hour
                        while($diffInMinutes > 0)
                        {
                            $diffInMinutes -= 60;
                            $total += $price->price;
                        }

                        if($outTime->timestamp >= $freeEnd->timestamp)
                        {
                            $diffInMinutes = $freeEnd->diffInMinutes($outTime);

                            //bayar lebihnya
                            while($diffInMinutes > 0)
                            {
                                $diffInMinutes -= 60;
                                $total += $price->price;
                            }

                            if($inTime->diffInDays($outTime) > 0)
                                $total += $price->overnightPrice;

                            if($total > $price->maxPrice && $price->maxPrice > 0)
                                $total = $price->maxPrice;
                        }
                    }
                    else
                    {
                        //normal
                        //first hour
                        $counter = 0;
                        while($counter < $price->initialLength && $diffInMinutes > 0)
                        {
                            $diffInMinutes -= 60;
                            $total += $price->initialPrice;
                            $counter++;
                        }

                        //per hour
                        while($diffInMinutes > 0)
                        {
                            $diffInMinutes -= 60;
                            $total += $price->price;
                        }

                        $total += $price->overnightPrice * $inTime->diffInDays($outTime);

                        if($total > $price->maxPrice && $price->maxPrice > 0)
                            $total = $price->maxPrice;
                    }
                }
                $parking->price = $total;
                $parking->outGate = $data["gateID"];
                $parking->outTime = $outTime;
                $parking->auditedActivity = 'U';
                //$parking->auditedUser = Auth::guard('admin')->user()->id;

                try
                {
                    if($parking->save())
                    {
                        $parking->vehicle = $vehicle;
                        $ret = new \stdClass();
                        $ret->success = true;
                        $ret->data = $parking;

                        \Log::error("200::gate/out/:: Gate out");
                        return response()->json($ret);
                    }
                    else
                    {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->error = "Something wrong with our server, please try again later";
                        \Log::error("500::gate/out/:: failed to save parking");
                        return response()->json($ret);
                    }
                }
                catch (\Illuminate\Database\QueryException $ex)
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::gate/out/:: failed to save parking: ".$ex);
                    return response()->json($ret);
                }
            }
            else
            {
                \Log::info("400::gate/out/:: required fields not provided");
                return $this->returnBadRequest("gateID, outTime and photo field are required");
            }
        }
        \Log::info("400::gate/out/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }
    /* ============================ SEBELUM GM ====================================================== */

    public function gateInGM(Request $request)
    {
        if($request->has('data'))
        {
            $data = $request->input('data');
            if(array_key_exists("gateID", $data) && array_key_exists("photo", $data) && array_key_exists("cardID", $data))
            {
                $vehicle = null;
                //if($data["isMember"])
                $vehicle = Vehicle::where('card_id', $data["cardID"])->first();

                if($vehicle == null)
                {
                    //visitor
                    $visitor = Visitor::where('cardID',$data["cardID"])
                        ->where('takeKTPTime','=','0000-00-00 00:00:00')
                        ->orderBy('created_at','DESC')
                        ->first();

                    if($visitor == null) {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->error = "The card is not recognized";
                        \Log::error("400::gate/in/:: Invalid card id: ".$data["cardID"]);
                        return response()->json($ret);
                    }

                    $expireTime = Carbon::parse($visitor->expireTime);
                    $expireTime->setTimezone('Asia/Jakarta');
                    if($expireTime->isPast()) {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->error = "The card is expired";
                        \Log::error("400::gate/in/:: Expired card id: ".$data["cardID"]);
                        return response()->json($ret);
                    }

                    $visitor->inTime = Carbon::now('Asia/Jakarta');
                    try
                    {
                        if($visitor->save())
                        {
                            $ret = new \stdClass();
                            $ret->success = true;
                            $ret->data = $visitor;
                            \Log::info("200::gate/in/:: Gate in for visitor id: ".$visitor->id);
                            return response()->json($ret);
                        }
                        else
                        {
                            $ret = new \stdClass();
                            $ret->success = false;
                            $ret->error = "Something wrong with our server, please try again later";
                            \Log::error("500::gate/in/:: failed to save visitor");
                            return response()->json($ret);
                        }
                    }
                    catch (\Illuminate\Database\QueryException $ex)
                    {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->error = "Something wrong with our server, please try again later";
                        \Log::error("500::gate/in/:: failed to save visitor: ".$ex);
                        return response()->json($ret);
                    }
                }

                $parking = $vehicle->parking()
                                ->orderBy('created_at','DESC')
                                ->where('outGate', '=','')
                                ->first();

                // if($parking != null) {
                //     $lot = $parking->parkingLot->name;
                //     $address = $parking->parkingLot->address;
                //     $ret = new \stdClass();
                //     $ret->success = false;
                //     $ret->error = "This vehicle is still registered in the parking lot with name: ".$lot." at ".$address;
                //     \Log::info("200::gate/in/:: Trying for multiple gate in: ".$data["cardID"]);
                //     return response()->json($ret);
                // }

                $gate = Gate::find($data["gateID"]);
                $parking = new Parking();
                $parking->id = "P".substr($data["gateID"],1,2).date('Ymdhis').substr(microtime(), 2,3);
                $parking->parking_lot_id = $gate->parking_lot_id;
                $parking->vehicle_id = $vehicle->id;
                $parking->member_id = $vehicle->member_id;
                if(array_key_exists("inTime", $data))
                    $parking->inTime =  Carbon::parse($data["inTime"]);
                else
                    $parking->inTime = Carbon::now('Asia/Jakarta');
                $parking->inGate = $data["gateID"];
                $parking->auditedActivity = 'I';
                //$parking->auditedUser = Auth::guard('admin')->user()->id;

                $tempImage = $this->base64_to_jpeg($data["photo"],"gateIn/t".$parking->id.".jpg");
                $this->compress_image($tempImage, "gateIn/".$parking->id.".jpg", 60);
                unlink("gateIn/t".$parking->id.".jpg");

                if(array_key_exists("photo2", $data)) {
                    $tempImage = $this->base64_to_jpeg($data["photo2"],"gateIn/t".$parking->id."2.jpg");
                    $this->compress_image($tempImage, "gateIn/".$parking->id."2.jpg", 60);
                    unlink("gateIn/t".$parking->id."2.jpg");
                }

                try
                {
                    if($parking->save())
                    {
                        $ret = new \stdClass();
                        $ret->success = true;
                        $ret->data = $parking;
                        \Log::info("200::gate/in/:: Gate in for parkingID: ".$parking->id);
                        return response()->json($ret);
                    }
                    else
                    {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->error = "Something wrong with our server, please try again later";
                        \Log::error("500::gate/in/:: failed to save parking");
                        return response()->json($ret);
                    }
                }
                catch (\Illuminate\Database\QueryException $ex)
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::gate/in/:: failed to save parking: ".$ex);
                    return response()->json($ret);
                }
            }
            else
            {
                \Log::info("400::gate/in/:: required fields not provided");
                return $this->returnBadRequest("gateID and photo field are required");
            }
        }
        \Log::info("400::gate/in/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * create gate out transaction to close the current parking transaction
     * @param  Request $request request with gateID, photo, isMember, and cardID parameter
     * @return
     */
    public function gateOutGM(Request $request)
    {
        if($request->has('data'))
        {
            $data = $request->input('data');
            if(array_key_exists('gateID', $data) &&  array_key_exists("photo", $data) && array_key_exists("cardID", $data))
            {
                $gate = Gate::find($data["gateID"]);
                $parkingLot = ParkingLot::find($gate->parking_lot_id);
                $parking = null;
                $vehicle = null;
                //if(array_key_exists("cardID", $data))
                {
                    $vehicle = Vehicle::where('card_id',$data["cardID"])
                        ->orderBy('created_at','DESC')
                        ->first();

                    if($vehicle == null) {
                        //visitor
                        $visitor = Visitor::where('cardID',$data["cardID"])
                            ->where('takeKTPTime','=','0000-00-00 00:00:00')
                            ->orderBy('created_at','DESC')
                            ->first();

                        if($visitor == null) {
                            $ret = new \stdClass();
                            $ret->success = false;
                            $ret->error = "Invalid Card ID";
                            \Log::error("400::gate/out/:: Invalid Card ID: ".$data["cardID"]);
                            return response()->json($ret);
                        }

                        $visitor->outTime = Carbon::now('Asia/Jakarta');
                        try
                        {
                            if($visitor->save())
                            {
                                $ret = new \stdClass();
                                $ret->success = true;
                                $ret->data = $visitor;
                                \Log::info("200::gate/out/:: Gate out for visitor id: ".$visitor->id);
                                return response()->json($ret);
                            }
                            else
                            {
                                $ret = new \stdClass();
                                $ret->success = false;
                                $ret->error = "Something wrong with our server, please try again later";
                                \Log::error("500::gate/out/:: failed to save visitor");
                                return response()->json($ret);
                            }
                        }
                        catch (\Illuminate\Database\QueryException $ex)
                        {
                            $ret = new \stdClass();
                            $ret->success = false;
                            $ret->error = "Something wrong with our server, please try again later";
                            \Log::error("500::gate/out/:: failed to save visitor: ".$ex);
                            return response()->json($ret);
                        }
                    }

                    $parking = $vehicle->parking()
                                ->orderBy('created_at','DESC')
                                ->where('outGate', '=','')
                                ->first();

                    if(array_key_exists("plateNumber", $data))
                        $vehicle->plateNumber = $data["plateNumber"];

                    if($parking == null)
                    {
                        if(array_key_exists("parkingID", $data)) {
                            $parking = Parking::find($data["parkingID"]);

                            $parking->vehicle = $vehicle;
                            $parking->outTime = Carbon::parse($parking->outTime);
                            $ret = new \stdClass();
                            $ret->success = true;
                            $ret->data = $parking;

                            \Log::error("200::gate/out/:: Gate out From Backend for parkingID: ".$data["parkingID"]);
                            return response()->json($ret);
                        }
                        $parking = $vehicle->parking()->orderBy('created_at','DESC')
                            ->first();

                        if($parking == null) {
                            $ret = new \stdClass();
                            $ret->success = true;
                            return response()->json($ret);
                        }
                    }
                }

                if(count($parking->parkingProblem) > 0)
                {
                    $transaction = Transaction::where('referenceID', $parking->parkingProblem()->first()->id)->first();
                    if($transaction == null)
                    {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->data = $parking->parkingProblem;
                        $ret->error = "There is a problem with this parking";

                        \Log::error("200::gate/out/:: problem with parking");
                        return response()->json($ret);
                    }
                }

                $tempImage = $this->base64_to_jpeg($data["photo"],"gateOut/t".$parking->id.".jpg");
                $this->compress_image($tempImage, "gateOut/".$parking->id.".jpg", 60);
                unlink("gateOut/t".$parking->id.".jpg");

                if(array_key_exists("photo2", $data)) {
                    $tempImage = $this->base64_to_jpeg($data["photo2"],"gateOut/t".$parking->id."2.jpg");
                    $this->compress_image($tempImage, "gateOut/".$parking->id."2.jpg", 60);
                    unlink("gateOut/t".$parking->id."2.jpg");
                }

                $price = Price::where('parking_lot_id',$parking->parking_lot_id)
                    ->where('vehicle_type_id', $vehicle->vehicle_type_id)
                    ->first();

                if(array_key_exists("outTime", $data))
                    $parking->outTime =  Carbon::parse($data["outTime"]);
                else
                    $parking->outTime = Carbon::now('Asia/Jakarta');
                $inTime = Carbon::parse($parking->inTime);
                $outTime = Carbon::parse($parking->outTime);
                $diffInMinutes = $outTime->diffInMinutes($inTime);

                $total = 0;

                $vip = $vehicle->vip()
                    ->where('due_date', '>=', $parking->outTime->toDateString())
                    ->first();

                if($vip != null)
                    $total = 0;
                else if($diffInMinutes < $price->freeDuration)
                    $total = 0;
                else
                {
                    $freeStart = Carbon::now();
                    $freeStart->minute = 0;
                    $freeStart->second = 0;

                    if($freeStart->hour <= $price->freeEnd)
                        $freeStart->day -= 1;
                    $freeStart->hour = $price->freeStart;

                    $freeEnd = Carbon::now();
                    $freeEnd->minute = 0;
                    $freeEnd->second = 0;

                    if($freeEnd->hour >= $price->freeEnd)
                        $freeEnd->day += 1;
                    $freeEnd->hour = $price->freeEnd;

                    if($freeStart->timestamp <= $inTime->timestamp)
                    {
                        //pas masuk udah mulai gratis
                        if($outTime->timestamp <= $freeEnd->timestamp)
                            $total = 0;
                        else
                        {
                            //bayar lebihnya
                            $diffInMinutes = $freeEnd->diffInMinutes($outTime);

                            //bayar lebihnya
                            while($diffInMinutes > 0)
                            {
                                $diffInMinutes -= 60;
                                $diffInMinutes += $price->price;
                            }

                            if($inTime->diffInDays($outTime) > 0)
                                $total += $price->overnightPrice;

                            if($total > $price->maxPrice && $price->maxPrice > 0)
                                $total = $price->maxPrice;
                        }
                    }
                    else if($outTime->timestamp >= $freeStart->timestamp)
                    {
                        //selama di dalem baru mulai gratis
                        $diffInMinutes = $freeStart->diffInMinutes($inTime);

                        $counter = 0;
                        while($counter < $price->initialLength && $diffInMinutes > 0)
                        {
                            $diffInMinutes -= 60;
                            $total += $price->initialPrice;
                            $counter++;
                        }
                        //per hour
                        while($diffInMinutes > 0)
                        {
                            $diffInMinutes -= 60;
                            $total += $price->price;
                        }

                        if($outTime->timestamp >= $freeEnd->timestamp)
                        {
                            $diffInMinutes = $freeEnd->diffInMinutes($outTime);

                            //bayar lebihnya
                            while($diffInMinutes > 0)
                            {
                                $diffInMinutes -= 60;
                                $total += $price->price;
                            }

                            if($inTime->diffInDays($outTime) > 0)
                                $total += $price->overnightPrice;

                            if($total > $price->maxPrice && $price->maxPrice > 0)
                                $total = $price->maxPrice;
                        }
                    }
                    else
                    {
                        //normal
                        //first hour
                        $counter = 0;
                        while($counter < $price->initialLength && $diffInMinutes > 0)
                        {
                            $diffInMinutes -= 60;
                            $total += $price->initialPrice;
                            $counter++;
                        }

                        //per hour
                        while($diffInMinutes > 0)
                        {
                            $diffInMinutes -= 60;
                            $total += $price->price;
                        }

                        $total += $price->overnightPrice * $inTime->diffInDays($outTime);

                        if($total > $price->maxPrice && $price->maxPrice > 0)
                            $total = $price->maxPrice;
                    }
                }
                $parking->price = $total;
                $parking->outGate = $data["gateID"];
                $parking->outTime = $outTime;
                $parking->auditedActivity = 'U';
                //$parking->auditedUser = Auth::guard('admin')->user()->id;

                try
                {
                    if($parking->save())
                    {
                        $parking->vehicle = $vehicle;
                        $ret = new \stdClass();
                        $ret->success = true;
                        $ret->data = $parking;

                        \Log::error("200::gate/out/:: Gate out");
                        return response()->json($ret);
                    }
                    else
                    {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->error = "Something wrong with our server, please try again later";
                        \Log::error("500::gate/out/:: failed to save parking");
                        return response()->json($ret);
                    }
                }
                catch (\Illuminate\Database\QueryException $ex)
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::gate/out/:: failed to save parking: ".$ex);
                    return response()->json($ret);
                }
            }
            else
            {
                \Log::info("400::gate/out/:: required fields not provided");
                return $this->returnBadRequest("gateID, outTime and photo field are required");
            }
        }
        \Log::info("400::gate/out/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    public function payParking(Request $request)
    {
        if($request->has('data'))
        {
            $data = $request->input('data');
            if(array_key_exists("parkingID", $data) && array_key_exists('payment_type_id', $data))
            {
                $parking = Parking::find($data["parkingID"]);
                if($data["payment_type_id"] == 1)
                {
                    $transaction = new Transaction();
                    $vehicle = $parking->vehicle;
                    
                    if($vehicle->balance < $parking->price)
                    {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->error = "Insufficient amount";

                        \Log::info("200::parking/pay/:: Insufficient amount");
                        return response()->json($ret);
                    }
                    $parking->payment_type_id = $data["payment_type_id"];
                    $parking->member_id = $vehicle->member_id;
                    $transaction->id = "T".substr($parking->id,1,2).date('Ymdhis').substr(microtime(), 2,3);
                    $transaction->transaction_type_id = 2;
                    $transaction->referenceID = $parking->id;
                    $transaction->startingBalance = $vehicle->balance;
                    $vehicle->balance -= $parking->price;
                    $transaction->balance = $vehicle->balance;
                    //$transaction->auditedUser = Auth::guard('admin')->user()->id;
                    $transaction->auditedActivity = 'I';

                    try
                    {
                        if($parking->save() && $transaction->save() && $vehicle->save())
                        {
                            $ret = new \stdClass();
                            $ret->success = true;
                            $ret->data = new \stdClass();
                            $ret->data->parking = $parking;
                            $ret->data->vehicle = $vehicle;
                            $ret->data->transaction = $transaction;

                            \Log::info("200::parking/pay/:: Gate out");
                            return response()->json($ret);
                        }
                        else
                        {
                            $ret = new \stdClass();
                            $ret->success = false;
                            $ret->error = "Something wrong with our server, please try again later";
                            \Log::error("500::parking/pay/:: failed to save parking");
                            return response()->json($ret);
                        }
                    }
                    catch (\Illuminate\Database\QueryException $ex)
                    {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->error = "Something wrong with our server, please try again later";
                        \Log::error("500::parking/pay/:: failed to save parking: ".$ex);
                        return response()->json($ret);
                    }
                }
                else
                {
                    $parking->payment_type_id = $data["payment_type_id"];
                    $parking->auditedActivity = 'U';
                    $transaction = new Transaction();
                    $transaction->id = "T".substr($parking->id,1,2).date('Ymdhis').substr(microtime(), 2,3);
                    $transaction->transaction_type_id = 2;
                    $transaction->referenceID = $parking->id;
                    $transaction->auditedActivity = 'I';
                    try
                    {
                        if($parking->save() && $transaction->save())
                        {
                            $ret = new \stdClass();
                            $ret->success = true;
                            $ret->data = new \stdClass();
                            $ret->data->parking = $parking;
                            $ret->data->vehicle = $parking->vehicle;
                            $ret->data->transaction = $transaction;

                            \Log::info("200::gparking/pay/:: Gate out");
                            return response()->json($ret);
                        }
                        else
                        {
                            $ret = new \stdClass();
                            $ret->success = false;
                            $ret->error = "Something wrong with our server, please try again later";
                            \Log::error("500::parking/pay/:: failed to save parking");
                            return response()->json($ret);
                        }
                    }
                    catch (\Illuminate\Database\QueryException $ex)
                    {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->error = "Something wrong with our server, please try again later";
                        \Log::error("500::parking/pay/:: failed to save parking: ".$ex);
                        return response()->json($ret);
                    }
                }
            }
            else
            {
                \Log::info("400::parking/pay/:: required fields not provided");
                return $this->returnBadRequest("parkingID and payment_type_id field are required");
            }
        }
        \Log::info("400::parking/pay/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * pay specific fine saved
     * @param  Request $request request with parkingProblemID, parkingLotID, and payment_type_id parameter
     * @return            
     */
    public function payFine(Request $request)
    {
        if($request->has('data'))
        {
            $data = $request->input('data');
            if(array_key_exists("parkingProblemID", $data) && array_key_exists('payment_type_id', $data) && array_key_exists("parkingLotID", $data))
            {
                $parkingProblem = ParkingProblem::find($data["parkingProblemID"]);
                $vehicle = Vehicle::find($parkingProblem->vehicle_id);
                $transaction = new Transaction();

                $fine = Fine::where('problem_id', $parkingProblem->problem_id)->where('parking_lot_id', $data["parkingLotID"])->where('auditedActivity', '<>','D')->first();

                $problem = $parkingProblem->problem;
                $amount = $problem->fine;
                if($fine != null)
                    $amount = $fine->fines;

                if($vehicle->balance < $fine->fines && $data['payment_type_id'] == 1)
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Insufficient amount";

                    \Log::info("200::fine/pay/:: Insufficient amount");
                    return response()->json($ret);
                }

                $transaction->id = "T".substr($parkingProblem->id,2,2).date('Ymdhis').substr(microtime(), 2,3);
                $transaction->transaction_type_id = 3;
                $transaction->referenceID = $parkingProblem->id;
                $transaction->startingBalance = $vehicle->balance;
                if($data['payment_type_id'] == 1)
                    $vehicle->balance -= $amount;
                $transaction->balance = $vehicle->balance;
                $transaction->auditedActivity = 'I';

                try
                {
                    if($transaction->save() && $vehicle->save())
                    {
                        $ret = new \stdClass();
                        $ret->success = true;
                        $ret->data = new \stdClass();
                        $ret->data->amount = $amount;
                        $ret->data->vehicle = $vehicle;
                        $ret->data->transaction = $transaction;

                        \Log::info("200::fine/pay/:: fine payment");
                        return response()->json($ret);
                    }
                    else
                    {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->error = "Something wrong with our server, please try again later";
                        \Log::error("500::fine/pay/:: failed to save transaction");
                        return response()->json($ret);
                    }
                }
                catch (\Illuminate\Database\QueryException $ex)
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::fine/pay/:: failed to save transaction: ".$ex);
                    return response()->json($ret);
                }

            }
            else
            {
                \Log::info("400::fine/pay/:: required fields not provided");
                return $this->returnBadRequest("parkingID and payment_type_id field are required");
            }
        }
        \Log::info("400::fine/pay/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * compress uploaded image
     * @param  [type] $source_url      [description]
     * @param  [type] $destination_url [description]
     * @param  [type] $quality         [description]
     * @return [type]                  [description]
     */
    private function compress_image($source_url, $destination_url, $quality) 
    {
        $info = getimagesize($source_url);
        if ($info['mime'] == 'image/jpeg') 
            $image = imagecreatefromjpeg($source_url); 
        else if ($info['mime'] == 'image/gif') 
            $image = imagecreatefromgif($source_url); 
        else if ($info['mime'] == 'image/png')
            $image = imagecreatefrompng($source_url); 
        else
            $image = imagecreatefromjpeg($source_url);
        imagejpeg($image, $destination_url, $quality); 
        return $destination_url; 
    }

    /**
     * convert image to base64
     * @param  [type] $base64_string [description]
     * @param  [type] $output_file   [description]
     * @return [type]                [description]
     */
    private function base64_to_jpeg($base64_string, $output_file) {
        $ifp = fopen($output_file, "wb+");

        fwrite($ifp, base64_decode($base64_string)); 
        fclose($ifp); 

        return $output_file;
    }
}
