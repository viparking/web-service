<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Parking;
use App\Vehicle;
use App\Http\Requests;
use Illuminate\Http\Response;

class PDFController extends Controller
{
    /**
     * download pdf of the report
     * @param  Request $request   basic request parameter
     * @param  string  $vehicleID id of the vehicle
     * @param  string  $startTime string indicating the start filter
     * @param  string  $endTime   string indicating the end filter
     * @return              
     */
	public function downloadPDF(Request $request, $vehicleID, $startTime = null, $endTime = null)
	{
		$this->generatePDF($request, $vehicleID, $startTime, $endTime);
		return response()->download('parking/'.$vehicleID.'.pdf');
	}

    /**
     * generate the pdf to be downloaded
     * @param  Request $request   basic request parameter
     * @param  string  $vehicleID id of the vehicle
     * @param  string  $startTime indicating the start filter
     * @param  string  $endTime   indicating the end filter
     * @return             
     */
    public function generatePDF(Request $request, $vehicleID, $startTime = null, $endTime = null)
    {
    	$html = $this->generateHTML($request, $vehicleID, $startTime, $endTime);
        return view('pdfGenerator', compact(['html','vehicleID']));
    }

    /**
     * generate the html page to be sent via email
     * @param  Request $request   basic request parameter
     * @param  string  $vehicleID id of the vehicle
     * @param  string  $startTime indicating the start filter
     * @param  string  $endTime   indicating the end filter
     * @return              
     */
    public function generateHTML(Request $request, $vehicleID, $startTime = null, $endTime = null)
    {
    	if($startTime != null && $endTime != null)
        {
            $endTime = Carbon::parse($endTime);
            if($endTime->hour == 0)
            {
                $endTime->hour = 23;
                $endTime->minute = 59;
            }
            $parking = Parking::selectRaw("parkings.*, parking_lots.name AS parkingLotName, transactions.startingBalance, transactions.balance")
                ->join('transactions','transactions.referenceID','=','parkings.id')
                ->join('parking_lots','parkings.parking_lot_id','=','parking_lots.id')
                ->where('vehicle_id',$vehicleID)
                ->where('parkings.auditedActivity','<>','D')
                ->where('parkings.outTime', '>=', Carbon::parse($startTime)->toDateTimeString())
                ->where('parkings.outTime', '<=', $endTime->toDateTimeString())
                ->get();
        }
        else
        {
            $parking = Parking::selectRaw("parkings.*, parking_lots.name AS parkingLotName, transactions.startingBalance, transactions.balance")
                ->join('transactions','transactions.referenceID','=','parkings.id')
                ->join('parking_lots','parkings.parking_lot_id','=','parking_lots.id')
                ->where('vehicle_id',$vehicleID)
                ->where('parkings.auditedActivity','<>','D')
                ->get();
        }
        $vehicle = Vehicle::find($vehicleID);
        return view('history', compact(['parking','vehicle']));
    }
}
