<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AdminLevel;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class AdminLevelController extends Controller
{
	/**
	 * Create new admin level and save to database
	 * @param Request $request Request with level and adminLevel parameter
	 */
    public function addLevel(Request $request)
    {
        if(Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::adminLevel/add:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
    	if($request->has("data"))
    	{
    		$data = $request->input("data");
    		if(array_key_exists("level", $data) && array_key_exists("adminLevel", $data))
    		{
    			$level = new AdminLevel();
    			$level->id = $data["level"];
    			$level->adminLevel = $data["adminLevel"];
    			try
    			{
    				$level->save();
    				$ret = new \stdClass();
                    $ret->success = true;
                    $ret->data = new \stdClass();
                    $ret->data->adminLevel = $level;
                    \Log::info("200::adminLevel/add:: admin Level created in with id: ".$level->id);
                    return response()->json($ret);
    			}
    			catch (\Illuminate\Database\QueryException $ex)
    			{
    				$ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Invalid admin level selected";
                    \Log::error("200::adminLevel/add:: admin failed to save, invalid admin level");
                    return response()->json($ret);
    			}
    		}
    		else
    		{
    			\Log::info("400::adminLevel/add:: level or adminLevel field not provided");
			    return $this->returnBadRequest("level and adminLevel field are required");
    		}
    	}
    	else
    	{
    		\Log::info("400::adminLevel/add:: data field not provided");
	    	return $this->returnBadRequest("data field not provided");
    	}
    }

    /**
     * Update the specified level
     * @param  Request $request request with level and adminLevel parameter
     * @return            
     */
    public function updateLevel(Request $request)
    {
        if(Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::adminLevel/update:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
    	if($request->has("data"))
    	{
    		$data = $request->input('data');
    		if(array_key_exists("level",$data) && array_key_exists("adminLevel", $data))
    		{
    			$level = AdminLevel::find($data["level"]);
    			$level->adminLevel = $data["adminLevel"];
    			try
    			{
    				$level->save();
    				$ret = new \stdClass();
                    $ret->success = true;
                    $ret->data = new \stdClass();
                    $ret->data->adminLevel = $level;
                    \Log::info("200::adminLevel/update:: admin Level updated with id: ".$level->id);
                    return response()->json($ret);
    			}
    			catch (\Illuminate\Database\QueryException $ex)
    			{
    				$ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("200::adminLevel/update:: adminLevel failed to save");
                    return response()->json($ret);
    			}
    		}
    		else
    		{
    			\Log::info("400::adminLevel/update:: level or adminLevel field not provided");
			    return $this->returnBadRequest("level and adminLevel field are required");
    		}
    	}
    	else
    	{
    		\Log::info("400::adminLevel/update:: data field not provided");
	    	return $this->returnBadRequest("data field not provided");
    	}
    }

    /**
     * Delete adminLevel at specified level
     * @param  Request $request request with level parameter
     * @return            
     */
    public function deleteLevel(Request $request)
    {
        if(Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::adminLevel/delete:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
    	if($request->has("data"))
    	{
    		$data = $request->input("data");
    		if(array_key_exists("level",$data))
    		{
    			$level = AdminLevel::find($data["level"]);
    			try
    			{
    				$level->delete();
    				$ret = new \stdClass();
                    $ret->success = true;
                    \Log::info("200::adminLevel/delete:: admin Level deleted with id: ".$level->id);
                    return response()->json($ret);
    			}
    			catch (\Illuminate\Database\QueryException $ex)
    			{
    				$ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("200::adminLevel/delete:: adminLevel failed to delete");
                    return response()->json($ret);
    			}
    		}
    		else
    		{
    			\Log::info("400::adminLevel/delete:: level field not provided");
			    return $this->returnBadRequest("level field is required");
    		}
    	}
    	else
    	{
    		\Log::info("400::adminLevel/delete:: data field not provided");
	    	return $this->returnBadRequest("data field not provided");
    	}
    }

    /**
     * get all saved adminLevel in database
     * @param  Request $request basic request object
     * @return            
     */
    public function getLevel(Request $request)
    {
        if(Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::adminLevel/:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
    	$levels = AdminLevel::get();
    	$ret = new \stdClass();
    	$ret->success = true;
    	$ret->data = $levels;

    	\Log::info("200::adminLevel/:: Get AdminLevel");
        return response()->json($ret);
    }
}
