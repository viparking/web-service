<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\ParkingLot;
use App\Admin;
use App\AdminParkingLot;
use App\Price;
use App\Parking;
use App\Fine;
use App\Problem;
use App\Vehicle;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class ParkingLotController extends Controller
{
    /**
     * Add parking lot for specified companyID
     * @param Request $request   request with name, latitude, longitude, and address parameter
     * @param int  $companyID id of the company
     */
    public function addParkingLot(Request $request, $companyID)
    {
        if(Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::vehicle/type/add:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
        if($request->has("data"))
        {
            $data = $request->input("data");
            if(array_key_exists("name", $data) && array_key_exists("latitude", $data) && array_key_exists("longitude", $data) && array_key_exists("address", $data))
            {
                $lot = new ParkingLot();
                $lot->name = $data["name"];
                $lot->company_id = $companyID;
                $lot->latitude = $data["latitude"];
                $lot->longitude = $data["longitude"];
                $lot->address = $data["address"];
                $lot->auditedUser = Auth::guard('admin')->user()->id;
                $lot->auditedActivity = 'I';
                try
                {
                    if($lot->save())
                    {
                        $ret = new \stdClass();
                        $ret->success = true;
                        $ret->data = $lot;
                        \Log::info("200::company/{companyID}/parkingLot/add/:: parkingLot with ID: ".$lot->id." added for company ID: ".$companyID);
                        return response()->json($ret);
                    }
                    else
                    {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->error = "Something wrong with our server, please try again later";
                        \Log::error("500::company/{companyID}/parkingLot/add/:: failed to save");
                        return response()->json($ret);
                    }
                }
                catch (\Illuminate\Database\QueryException $ex)
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::company/{companyID}/parkingLot/add/:: failed to save: ".$ex);
                    return response()->json($ret);
                }
            }
            else
            {
                \Log::info("400::company/{companyID}/parkingLot/add/:: required fields not provided");
                return $this->returnBadRequest("name, latitude, longitude and address field are required");
            }
        }
        \Log::info("400::vcompany/{companyID}/parkingLot/add/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * delete particular parkingLot with specified ID
     * @param  Request $request request with id parameter
     * @return            
     */
    public function deleteParkingLot(Request $request)
    {
        if(Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::parkingLot/delete:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
        if($request->has('data'))
        {
            $data = $request->input("data");
            if(array_key_exists("id", $data))
            {
                $lot = ParkingLot::find($data["id"]);
                $lot->auditedActivity = 'D';
                $lot->auditedUser = Auth::guard('admin')->user()->id;
                try
                {
                    if($lot->save())
                    {
                        $ret = new \stdClass();
                        $ret->success = true;

                        \Log::info("200::parkingLot/delete:: parkingLot with ID: ".$lot->id." added for company ID: ".$lot->company_id);
                        return response()->json($ret);
                    }
                    else
                    {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->error = "Something wrong with our server, please try again later";
                        \Log::error("500::parkingLot/delete:: failed to save");
                        return response()->json($ret);
                    }
                }
                catch (\Illuminate\Database\QueryException $ex)
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::parkingLot/delete:: failed to save: ".$ex);
                    return response()->json($ret);
                }
            }
        }
        \Log::info("400::parkingLot/delete/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * edit parking lot data from specified id
     * @param  Request $request request with id, name, latitude, longitude, and address parameter
     * @return            
     */
    public function editParkingLot(Request $request)
    {
        if(Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::parkingLot/edit/:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
        if($request->has("data"))
        {
            $data = $request->input("data");
            if(array_key_exists("name", $data) && array_key_exists("latitude", $data) && array_key_exists("longitude", $data) && array_key_exists("address", $data) && array_key_exists("id", $data))
            {
                $lot = ParkingLot::find($data["id"]);
                $lot->name = $data["name"];
                $lot->latitude = $data["latitude"];
                $lot->longitude = $data["longitude"];
                $lot->address = $data["address"];
                $lot->auditedUser = Auth::guard('admin')->user()->id;
                $lot->auditedActivity = 'U';
                try
                {
                    if($lot->save())
                    {
                        $ret = new \stdClass();
                        $ret->success = true;
                        $ret->data = $lot;
                        \Log::info("200::parkingLot/edit/:: parkingLot with ID: ".$lot->id." updated");
                        return response()->json($ret);
                    }
                    else
                    {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->error = "Something wrong with our server, please try again later";
                        \Log::error("500::parkingLot/edit/:: failed to save");
                        return response()->json($ret);
                    }
                }
                catch (\Illuminate\Database\QueryException $ex)
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::parkingLot/edit/:: failed to save: ".$ex);
                    return response()->json($ret);
                }
            }
            else
            {
                \Log::info("400::parkingLot/edit/:: required fields not provided");
                return $this->returnBadRequest("id, name, latitude, longitude and address field are required");
            }
        }
        \Log::info("400::parkingLot/edit/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * Get admin list of the particular parking lot
     * @param  Request $request      basic request parameter
     * @param  int  $parkingLotID id of the parking lot
     * @return 
     */
    public function getParkingLotAdmin(Request $request, $parkingLotID, $page = 0)
    {
        $skip = $page * config('app.OFFSET');
        $admin = Admin::join('admin_parking_lots','admins.id','=','admin_parking_lots.admin_id')
            ->where('parking_lot_id',$parkingLotID)
            ->where('admins.auditedActivity','<>','D')
            ->where('admin_parking_lots.auditedActivity','<>','D')
            ->orderBy('level')
            ->skip($skip)
            ->take(config('app.OFFSET'))
            ->get();

        $adminCount = Admin::join('admin_parking_lots','admins.id','=','admin_parking_lots.admin_id')
            ->where('parking_lot_id',$parkingLotID)
            ->where('admins.auditedActivity','<>','D')
            ->where('admin_parking_lots.auditedActivity','<>','D')
            ->count();

        $ret = new \stdClass();
        $ret->success = true;
        $ret->total = $adminCount;
        $ret->data = $admin;

        \Log::info("200::parkingLot/{PakingLotID}/admin:: Get parking lot admin for parking lot ID: ".$parkingLotID);
        return response()->json($ret);
    }

    /**
     * Add an admin with specific id to a parking lot
     * @param Request $request      request with admin_id, view, add, delete, edit parameter
     * @param integer  $parkingLotID id of the parking lot
     */
    public function addAdminToParkingLot(Request $request, $parkingLotID)
    {
        if(Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::parkingLot/{parkingLotID}/admin/add/:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
        if($request->has('data'))
        {
            $data = $request->input("data");
            if(array_key_exists("admin_id", $data) && array_key_exists("view",$data) && array_key_exists("add", $data) && array_key_exists("edit", $data) && array_key_exists("delete", $data))
            {
                $admin = AdminParkingLot::where('admin_id',$data["admin_id"])->where('parking_lot_id',$parkingLotID)->first();
                if($admin != null)
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "This admin has been assigned to selected parking lot";
                    \Log::error("500::parkingLot/{parkingLotID}/admin/add/:: try to reassign");
                    return response()->json($ret);
                }
                $lotAdmin = new AdminParkingLot();
                $lotAdmin->parking_lot_id = $parkingLotID;
                $lotAdmin->admin_id = $data["admin_id"];
                $lotAdmin->view = $data["view"];
                $lotAdmin->add = $data["add"];
                $lotAdmin->edit = $data["edit"];
                $lotAdmin->delete = $data["delete"];
                $lotAdmin->auditedActivity = 'I';
                $lotAdmin->auditedUser = Auth::guard('admin')->user()->id;

                try
                {
                    if($lotAdmin->save())
                    {
                        $ret = new \stdClass();
                        $ret->success = true;
                        $ret->data = $lotAdmin;

                        \Log::info("200::parkingLot/{parkingLotID}/admin/add/:: admin with ID: ".$lotAdmin->admin_id." added for parking lot id: ".$parkingLotID);
                        return response()->json($ret);
                    }
                    else
                    {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->error = "Something wrong with our server, please try again later";
                        \Log::error("500::parkingLot/{parkingLotID}/admin/add/:: failed to save");
                        return response()->json($ret);
                    }
                }
                catch (\Illuminate\Database\QueryException $ex)
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::parkingLot/{parkingLotID}/admin/add/:: failed to save: ".$ex);
                    return response()->json($ret);
                }
            }
            else
            {
                \Log::info("400::parkingLot/{parkingLotID}/admin/add/:: required fields not provided");
                return $this->returnBadRequest("admin_id field is required");
            }
        }
        \Log::info("400::parkingLot/{parkingLotID}/admin/add/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * remove particular admin from specific parking lot
     * @param  Request $request      request with admin_id parameter
     * @param  integer  $parkingLotID id of the parking lot
     * @return 
     */
    public function removeAdminFromParkingLot(Request $request, $parkingLotID)
    {
        if(Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::parkingLot/{parkingLotID}/admin/add/:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
        if($request->has('data'))
        {
            $data = $request->input("data");
            if(array_key_exists("admin_id", $data))
            {
                DB::select(DB::raw("DELETE FROM admin_parking_lots WHERE admin_id = ".$data["admin_id"]." AND parking_lot_id = ".$parkingLotID));
                // $admin = AdminParkingLot::where('admin_id',$data["admin_id"])->where('parking_lot_id',$parkingLotID)->first();
                // $admin->auditedActivity = 'D';
                // $admin->auditedUser = Auth::guard('admin')->user()->id;

                try
                {
                    //if($admin->save())
                    {
                        $ret = new \stdClass();
                        $ret->success = true;

                        \Log::info("200::parkingLot/{parkingLotID}/admin/remove/:: admin with ID: ".$data['admin_id']." removed for parking lot id: ".$parkingLotID);
                        return response()->json($ret);
                    }
                    // else
                    // {
                    //     $ret = new \stdClass();
                    //     $ret->success = false;
                    //     $ret->error = "Something wrong with our server, please try again later";
                    //     \Log::error("500::parkingLot/{parkingLotID}/admin/remove/:: failed to save");
                    //     return response()->json($ret);
                    // }
                }
                catch (\Illuminate\Database\QueryException $ex)
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::parkingLot/{parkingLotID}/admin/remove/:: failed to save: ".$ex);
                    return response()->json($ret);
                }
            }
            else
            {
                \Log::info("400::parkingLot/{parkingLotID}/admin/remove/:: required fields not provided");
                return $this->returnBadRequest("admin_id field is required");
            }
        }
        \Log::info("400::parkingLot/{parkingLotID}/admin/remove/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * get price for specific parking lot
     * @param  Request $request      basic request parameter
     * @param  integer  $parkingLotID id of the parking lot
     * @return                 
     */
    public function getParkingLotPrice(Request $request, $parkingLotID)
    {
        $price = Price::join('vehicle_types','prices.vehicle_type_id','=','vehicle_types.id')
            ->where('parking_lot_id',$parkingLotID)
            ->where('prices.auditedActivity','<>','D')
            ->get();

        $ret = new \stdClass();
        $ret->success = true;
        $ret->data = $price;
        \Log::info("200::parkingLot/{parkingLotID}/price/:: get price for parking lot id: ".$parkingLotID);
        return response()->json($ret);
    }

    /**
     * Set price for specific parking lot
     * @param Request $request      request with all required parameter
     * @param integer  $parkingLotID id of the parking lot
     */
    public function setPriceForParkingLot(Request $request, $parkingLotID)
    {
        if(Auth::guard('admin')->user()->level < 4)
        {
            if(Auth::guard('admin')->user()->level == 3)
            {
                $admin = AdminParkingLot::where('admin_id',Auth::guard('admin')->user()->id)
                ->where('parking_lot_id', $parkingLotID)
                ->first();
                if($admin->edit == 0)
                {
                    \Log::info("401::parkingLot/{parkingLotID}/price/set/:: Insufficient Access level level");
                    return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
                }
            }
            else
            {
                \Log::info("401::parkingLot/{parkingLotID}/price/set/:: Insufficient level");
                return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
            }
        }
        if($request->has('data'))
        {
            $data = $request->input('data');
            if(array_key_exists("vehicle_type_id", $data) && array_key_exists("initialPrice", $data) && array_key_exists("initialLength", $data) && array_key_exists("freeDuration", $data) && array_key_exists("price", $data) && array_key_exists("overnightPrice", $data) && array_key_exists("maxPrice", $data) && array_key_exists("freeStart", $data) && array_key_exists("freeEnd", $data))
            {
                $oldPrice = Price::where('parking_lot_id', $parkingLotID)
                    ->where('vehicle_type_id', $data["vehicle_type_id"])
                    ->where('auditedActivity','<>','D')
                    ->first();
                if($oldPrice != null)
                {
                    DB::delete('DELETE FROM prices WHERE parking_lot_id = ? AND vehicle_type_id = ?', $parkingLotID, $data["vehicle_type_id"]);
                }

                $price = new Price();
                $price->parking_lot_id = $parkingLotID;
                $price->vehicle_type_id = $data["vehicle_type_id"];
                $price->initialPrice = $data["initialPrice"];
                $price->initialLength = $data["initialLength"];
                $price->freeDuration = $data["freeDuration"];
                $price->price = $data["price"];
                $price->overnightPrice = $data["overnightPrice"];
                $price->maxPrice = $data["maxPrice"];
                $price->freeStart = $data["freeStart"];
                $price->freeEnd = $data["freeEnd"];
                $price->auditedActivity = 'I';
                $price->auditedUser = Auth::guard('admin')->user()->id;
                try
                {
                    if($price->save())
                    {
                        $ret = new \stdClass();
                        $ret->success = true;
                        $ret->data = $price;
                        \Log::info("200::parkingLot/{parkingLotID}/price/set/:: price set for parkingLotID: ".$parkingLotID);
                        return response()->json($ret);
                    }
                    else
                    {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->error = "Something wrong with our server, please try again later";
                        \Log::error("500::parkingLot/{parkingLotID}/price/set/:: failed to save");
                        return response()->json($ret);
                    }
                }
                catch (\Illuminate\Database\QueryException $ex)
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::parkingLot/{parkingLotID}/price/set/:: failed to save: ".$ex);
                    return response()->json($ret);
                }
            }
            else
            {
                \Log::info("400::parkingLot/{parkingLotID}/price/set/:: required fields not provided");
                return $this->returnBadRequest("vehicle_type_id, initialPrice, initialLength, freeDuration, price, overnightPrice, maxPrice, freeStart, freeEnd field are required");
            }
        }
        \Log::info("400::parkingLot/{parkingLotID}/price/set/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * Get parking for particular parking lot
     * @param  Request $request      basic request parameter
     * @param  integer  $parkingLotID id of the parking lot
     * @param  integer $page         page offset
     * @return                 
     */
    public function getParkingForParkingLot(Request $request, $parkingLotID, $page = 0)
    {
        if(Auth::guard('admin')->user()->level < 4)
        {
            $admin = AdminParkingLot::where('admin_id',Auth::guard('admin')->user()->id)
                ->where('parking_lot_id', $parkingLotID)
                ->first();
            if($admin->view == 0)
            {
                \Log::info("401::parkingLot/{parkingLotID}/parking/{page?}/:: Insufficient Access level level");
                return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
            }
        }
        $skip = $page * config('app.OFFSET');
        $parking = Parking::selectRaw("parkings.*, members.username, payment_types.paymentType, vehicles.plateNumber, a.name AS inGateName, b.name AS outGateName")
            ->leftJoin('vehicles','vehicles.id','=','parkings.vehicle_id')
            ->leftJoin('members','members.id','=','parkings.member_id')
            ->leftJoin('payment_types','payment_types.id','=','parkings.payment_type_id')
            ->join('gates as a','a.id','=','parkings.inGate')
            ->join('transactions','transactions.referenceID','=','parkings.id')
            ->leftJoin('gates as b','b.id','=','parkings.outGate')
            ->where('parkings.auditedActivity','<>','D')
            ->where('parkings.parking_lot_id', $parkingLotID)
            ->orderBy('parkings.updated_at','DESC')
            ->skip($skip)
            ->take(config('app.OFFSET'))
            ->get();

        $parkingCount = Parking::leftJoin('vehicles','vehicles.id','=','parkings.vehicle_id')
            ->leftJoin('members','members.id','=','parkings.member_id')
            ->leftJoin('payment_types','payment_types.id','=','parkings.payment_type_id')
            ->join('gates as a','a.id','=','parkings.inGate')
            ->join('transactions','transactions.referenceID','=','parkings.id')
            ->leftJoin('gates as b','b.id','=','parkings.outGate')
            ->where('parkings.auditedActivity','<>','D')
            ->where('parkings.parking_lot_id', $parkingLotID)
            ->count();

        $ret = new \stdClass();
        $ret->success = true;
        $ret->total = $parkingCount;
        $ret->data = $parking;

        \Log::info("200::parkingLot/{parkingLotID}/parking/{page?}/:: Get parking for parking lot id:".$parkingLotID." page ".$page);
        return response()->json($ret);
    }

    /**
     * Get pending parking for particular parking lot -> parking that hasn't been out yet
     * @param  Request $request      basic request parameter
     * @param  integer  $parkingLotID id of the parking lot
     * @param  integer  $page       page offset
     * @return                 
     */
    public function getPendingParkingForParkingLot(Request $request, $parkingLotID, $page = 0)
    {
        if(Auth::guard('admin')->user()->level < 4)
        {
            $admin = AdminParkingLot::where('admin_id',Auth::guard('admin')->user()->id)
                ->where('parking_lot_id', $parkingLotID)
                ->first();
            if($admin->view == 0)
            {
                \Log::info("401::parkingLot/{parkingLotID}/parking/{page?}/:: Insufficient Access level level");
                return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
            }
        }
        $skip = $page * config('app.OFFSET');
        $parking = Parking::selectRaw("parkings.*, a.name AS inGateName, b.username")
            ->join('gates as a','a.id','=','parkings.inGate')
            ->join('members as b','b.id','=','parkings.member_id')
            ->leftJoin('transactions','transactions.referenceID','=','parkings.id')
            ->where('parkings.auditedActivity','<>','D')
            ->where('parkings.parking_lot_id', $parkingLotID)
            ->whereNull('transactions.id')
            ->orderBy('parkings.updated_at','DESC')
            ->skip($skip)
            ->take(config('app.OFFSET'))
            ->get();

        $parkingCount = Parking::join('gates as a','a.id','=','parkings.inGate')
            ->leftJoin('transactions','transactions.referenceID','=','parkings.id')
            ->where('parkings.auditedActivity','<>','D')
            ->where('parkings.parking_lot_id', $parkingLotID)
            ->whereNull('transactions.id')
            ->count();

        $ret = new \stdClass();
        $ret->success = true;
        $ret->total = $parkingCount;
        $ret->data = $parking;

        \Log::info("200::parkingLot/{parkingLotID}/parking/{page?}/:: Get pending parking for parking lot id:".$parkingLotID);
        return response()->json($ret);
    }

    /**
     * create and add parking for specific parking lot
     * @param Request $request      request with plateNumber, payment_type_id, member_id, price, inTime, outTime, inGate, outGate, and vehicle_type_id parameter
     * @param integer  $parkingLotID id of the parking lot
     */
    public function addParkingForParkingLot(Request $request, $parkingLotID)
    {
        if(Auth::guard('admin')->user()->level < 4)
        {
            if(Auth::guard('admin')->user()->level == 3)
            {
                $admin = AdminParkingLot::where('admin_id',Auth::guard('admin')->user()->id)
                ->where('parking_lot_id', $parkingLotID)
                ->first();
                if($admin->add == 0)
                {
                    \Log::info("401::parkingLot/{parkingLotID}/price/set/:: Insufficient Access level level");
                    return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
                }
            }
            else
            {
                \Log::info("401::parkingLot/{parkingLotID}/price/set/:: Insufficient level");
                return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
            }
        }
        if($request->has('data'))
        {
            $data = $request->input('data');
            if(array_key_exists("plateNumber", $data) && array_key_exists("payment_type_id", $data) && array_key_exists("member_id", $data) && array_key_exists("price", $data) && array_key_exists("inTime", $data) && array_key_exists("outTime", $data) && array_key_exists("inGate", $data) && array_key_exists("outGate", $data) && array_key_exists("vehicle_type_id", $data))
            {
                $vehicle = Vehicle::where('plateNumber',$data["plateNumber"])
                    ->where('auditedActivity','<>','D')
                    ->first();
                if($vehicle == null)
                {
                    $vehicle = new Vehicle();
                    $vehicle->id = "V".substr($data["plateNumber"],1,2).date('Ymdhis').substr(microtime(), 2,3);
                    $vehicle->plateNumber = $data["plateNumber"];
                    $vehicle->vehicle_type_id = $data["vehicle_type_id"];
                    try
                    {
                        if(!$vehicle->save())
                        {
                            $ret = new \stdClass();
                            $ret->success = false;
                            $ret->error = "Something wrong with our server, please try again later";
                            \Log::error("500::parkingLot/{parkingLotID}/parking/add/:: failed to create vehicle");
                            return response()->json($ret);
                        }
                    }
                    catch (\Illuminate\Database\QueryException $ex)
                    {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->error = "Something wrong with our server, please try again later";
                        \Log::error("500::parkingLot/{parkingLotID}/parking/add/:: failed to create vehicle: ".$ex);
                        return response()->json($ret);
                    }
                }

                $parking = new Parking();
                $parking->vehicle = $vehicle;
                $parking->id = "P".substr($data["plateNumber"],1,2).date('Ymdhis').substr(microtime(), 2,3);
                $parking->payment_type_id = $data["payment_type_id"];
                $parking->member_id = $data["member_id"];
                $parking->price = $data["price"];
                $parking->inTime = Carbon::parse($data["inTime"])->toDateTimeString();
                $parking->outTime = Carbon::parse($data["outTime"])->toDateTimeString();
                $parking->inGate = $data["inGate"];
                $parking->outGate = $data["outGate"];

                try
                {
                    if($parking->save())
                    {
                        $ret = new \stdClass();
                        $ret->success = true;
                        $ret->data = $parking;
                        \Log::info("200::parkingLot/{parkingLotID}/parking/add/:: parking with ID: ".$parking->id." added for parking lot id: ".$parkingLotID);
                        return response()->json($ret);
                    }
                    else
                    {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->error = "Something wrong with our server, please try again later";
                        \Log::error("500::parkingLot/{parkingLotID}/parking/add/:: failed to save");
                        return response()->json($ret);
                    }
                }
                catch (\Illuminate\Database\QueryException $ex)
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::parkingLot/{parkingLotID}/parking/add/:: failed to save: ".$ex);
                    return response()->json($ret);
                }
            }
            else
            {
                \Log::info("400::parkingLot/{parkingLotID}/parking/add/:: required fields not provided");
                return $this->returnBadRequest("plateNumber, payment_type_id, member_id, price, inTime, outTime, inGate, outGate, vehicle_type_id field are required");
            }
        }
        \Log::info("400::parkingLot/{parkingLotID}/parking/add/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * edit parking transaction for parking lot
     * @param  Request $request      request with id, payment_type_id, member_id, price, outTime, and outGate parameter
     * @param  string  $parkingLotID id of the parking lot
     * @return                 
     */
    public function editParkingForParkingLot(Request $request, $parkingLotID)
    {
        if(Auth::guard('admin')->user()->level < 4)
        {
            if(Auth::guard('admin')->user()->level == 3)
            {
                $admin = AdminParkingLot::where('admin_id',Auth::guard('admin')->user()->id)
                ->where('parking_lot_id', $parkingLotID)
                ->first();
                if($admin->view == 0)
                {
                    \Log::info("401::parkingLot/{parkingLotID}/price/set/:: Insufficient Access level level");
                    return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
                }
            }
            else
            {
                \Log::info("401::parkingLot/{parkingLotID}/price/set/:: Insufficient level");
                return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
            }
        }
        if($request->has('data'))
        {
            $data = $request->input('data');
            if(array_key_exists("id",$data) && array_key_exists("payment_type_id", $data) && array_key_exists("member_id", $data) && array_key_exists("price", $data) && array_key_exists("outTime", $data) && array_key_exists("outGate", $data))
            {
                $parking = Parking::find($data["id"]);
                $parking->payment_type_id = $data["payment_type_id"];
                $parking->member_id = $data["member_id"];
                $parking->price = $data["price"];
                $parking->outTime = $data["outTime"];
                $parking->outGate = $data["outGate"];

                try
                {
                    if($parking->save())
                    {
                        $ret = new \stdClass();
                        $ret->success = true;
                        $ret->data = $parking;
                        \Log::info("200::parkingLot/{parkingLotID}/parking/edit/:: parking with ID: ".$parking->id." edited for parking lot id: ".$parkingLotID);
                        return response()->json($ret);
                    }
                    else
                    {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->error = "Something wrong with our server, please try again later";
                        \Log::error("500::parkingLot/{parkingLotID}/parking/edit/:: failed to save");
                        return response()->json($ret);
                    }
                }
                catch (\Illuminate\Database\QueryException $ex)
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::parkingLot/{parkingLotID}/parking/edit/:: failed to save: ".$ex);
                    return response()->json($ret);
                }
            }
            else
            {
                \Log::info("400::parkingLot/{parkingLotID}/parking/edit/:: required fields not provided");
                return $this->returnBadRequest("id, payment_type_id, member_id, price, outTime, outGate field are required");
            }
        }
        \Log::info("400::parkingLot/{parkingLotID}/parking/edit/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * get fines for particular parking lot
     * @param  Request $request      basic request parameter
     * @param  integer  $parkingLotID id of the parking lot
     * @return [type]                [description]
     */
    public function getParkingLotFine(Request $request, $parkingLotID)
    {
        $fine = Problem::selectRaw("id, parking_lot_id, fines AS localFine, problem, description, fine AS globalFine, isParkingRelated")
            ->leftJoin('fines','problems.id','=','fines.problem_id')
            ->where('problems.auditedActivity', '<>', 'D')
            ->where(function ($query) {
                $query->whereNull('fines.auditedActivity')
                      ->orWhere('fines.auditedActivity', '<>', 'D');
            })
            ->where(function ($query) use($parkingLotID) {
                $query->whereNull('fines.parking_lot_id')
                      ->orWhere('fines.parking_lot_id', $parkingLotID);
            })
            ->get();

        $ret = new \stdClass();
        $ret->success = true;
        $ret->data = $fine;
        \Log::info("200::parkingLot/{parkingLotID}/fine/:: Get fines for parking lot id:".$parkingLotID);
        return response()->json($ret);
    }

    /**
     * get fine for specific parking lot
     * @param  Request $request      basic request parameter
     * @param  string  $parkingLotID id of the parking lot
     * @param  string  $problemID    id of the problem
     * @return                 
     */
    public function getParkingLotFineByID(Request $request, $parkingLotID, $problemID)
    {
        $fine = Problem::selectRaw("id, parking_lot_id, fines AS localFine, problem, description, fine AS globalFine, isParkingRelated")
            ->leftJoin('fines','problems.id','=','fines.problem_id')
            ->where('problems.auditedActivity', '<>', 'D')
            ->where(function ($query) {
                $query->whereNull('fines.auditedActivity')
                      ->orWhere('fines.auditedActivity', '<>', 'D');
            })
            ->where(function ($query) use($parkingLotID) {
                $query->whereNull('fines.parking_lot_id')
                      ->orWhere('fines.parking_lot_id', $parkingLotID);
            })
            ->where('problems.id',$problemID)
            ->first();

        $ret = new \stdClass();
        $ret->success = true;
        $ret->data = $fine;
        \Log::info("200::parkingLot/{parkingLotID}/fine/{problemID}:: Get fines for parking lot id:".$parkingLotID);
        return response()->json($ret);
    }

    /**
     * set fine for specific problem at particular parking lot
     * @param Request $request      request with problem_id and fines parameter
     * @param integer  $parkingLotID id of the parking lot
     */
    public function setParkingLotFine(Request $request, $parkingLotID)
    {
        if(Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::parkingLot/{parkingLotID}/fine/set/:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
        if($request->has('data'))
        {
            $data = $request->input('data');
            if(array_key_exists("problem_id", $data) && array_key_exists("fines", $data))
            {
                $oldFine = Fine::where('problem_id', $data["problem_id"])
                    ->where('parking_lot_id', $parkingLotID)
                    ->where('auditedActivity','<>','D')
                    ->first();
                if($oldFine != null)
                {
                    DB::delete('DELETE FROM fines WHERE problem_id = ? AND parking_lot_id = ?', [$data["problem_id"], $parkingLotID]);
                }

                $fine = new Fine();
                $fine->problem_id = $data["problem_id"];
                $fine->fines = $data["fines"];
                $fine->parking_lot_id = $parkingLotID;
                $fine->auditedActivity = 'I';
                $fine->auditedUser = Auth::guard('admin')->user()->id;
                try
                {
                    if($fine->save())
                    {
                        $ret = new \stdClass();
                        $ret->success = true;
                        $ret->data = $fine;
                        \Log::info("200::parkingLot/{parkingLotID}/fine/set/:: fine set for parkingLotID: ".$parkingLotID);
                        return response()->json($ret);
                    }
                    else
                    {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->error = "Something wrong with our server, please try again later";
                        \Log::error("500::parkingLot/{parkingLotID}/fine/set/:: failed to save");
                        return response()->json($ret);
                    }
                }
                catch (\Illuminate\Database\QueryException $ex)
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::parkingLot/{parkingLotID}/fine/set/:: failed to save: ".$ex);
                    return response()->json($ret);
                }
            }
            else
            {
                \Log::info("400::parkingLot/{parkingLotID}/fine/set/:: required fields not provided");
                return $this->returnBadRequest("problem_id and fines field are required");
            }
        }
        \Log::info("400::parkingLot/{parkingLotID}/fine/set/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * Remove local fine for specific problem_id in particular parking lot
     * @param  Request $request      request with problem_id parameter
     * @param  integer  $parkingLotID id of the parking lot
     * @return                 
     */
    public function removeParkingLotFine(Request $request, $parkingLotID)
    {
        if(Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::parkingLot/{parkingLotID}/fine/remove/:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
        if($request->has('data'))
        {
            $data = $request->input('data');
            if(array_key_exists("problem_id", $data))
            {
                $fine = Fine::where('problem_id',$data["problem_id"])
                    ->where('parking_lot_id', $parkingLotID)
                    ->where('auditedActivity','<>','D')
                    ->first();

                $fine->auditedActivity = 'D';
                $fine->auditedUser = Auth::guard('admin')->user()->id;
                try
                {
                    if($fine->save())
                    {
                        $ret = new \stdClass();
                        $ret->success = true;
                        \Log::info("200::parkingLot/{parkingLotID}/fine/remove/:: fine removed for parkingLotID: ".$parkingLotID);
                        return response()->json($ret);
                    }
                    else
                    {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->error = "Something wrong with our server, please try again later";
                        \Log::error("500::parkingLot/{parkingLotID}/fine/remove/:: failed to save");
                        return response()->json($ret);
                    }
                }
                catch (\Illuminate\Database\QueryException $ex)
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::parkingLot/{parkingLotID}/fine/remove/:: failed to save: ".$ex);
                    return response()->json($ret);
                }
            }
            else
            {
                \Log::info("400::parkingLot/{parkingLotID}/fine/remove/:: required fields not provided");
                return $this->returnBadRequest("problem_id field is required");
            }
        }
        \Log::info("400::parkingLot/{parkingLotID}/fine/remove/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * get detail of the parking lot
     * @param  Request $request      basic request parameter
     * @param  string  $parkingLotID id of the parking lot
     * @return                 
     */
    public function detail(Request $request, $parkingLotID)
    {
        if(Auth::guard('admin')->user()->level < 4)
        {
            //if(Auth::guard('admin')->user()->level == 3)
            {
                $admin = AdminParkingLot::where('admin_id',Auth::guard('admin')->user()->id)
                ->where('parking_lot_id', $parkingLotID)
                ->first();
                if($admin->view == 0)
                {
                    \Log::info("401::{parkingLotID}/detail/:: Insufficient Access level level");
                    return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
                }
            }
//            else
//            {
//                \Log::info("401::{parkingLotID}/detail/:: Insufficient level");
//                return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
//            }
        }

        $parkingLot = ParkingLot::where('id', $parkingLotID)
            ->where('auditedActivity','<>','D')
            ->first();

        $ret = new \stdClass();
        $ret->success = true;
        $ret->data = $parkingLot;

        \Log::info("200::{parkingLotID}/detail/:: Get parkingLot detail for id: ".$parkingLotID);
        return response()->json($ret);
    }

    /**
     * get list of manager in specific parking lot
     * @param  Request $request      basic request parameter
     * @param  string  $parkingLotID id of the parking lot
     * @return                 
     */
    public function getManager(Request $request, $parkingLotID)
    {
        //ini cek si manager harus lagi login
        // $admin = Admin::selectRaw('admins.*, admin_parking_lots.*')
        //     ->join('admin_parking_lots','admins.id','=','admin_parking_lots.admin_id')
        //     ->join('admin_logins','admin_logins.admin_id','=','admins.id')
        //     ->where('parking_lot_id',$parkingLotID)
        //     ->where('admins.auditedActivity','<>','D')
        //     ->where('admin_parking_lots.auditedActivity','<>','D')
        //     ->where('admin_logins.auditedActivity','<>','D')
        //     ->where('level',3)
        //     ->get();

        $admin = Admin::selectRaw('admins.*, admin_parking_lots.*')
            ->join('admin_parking_lots','admins.id','=','admin_parking_lots.admin_id')
            ->where('parking_lot_id',$parkingLotID)
            ->where('admins.auditedActivity','<>','D')
            ->where('admin_parking_lots.auditedActivity','<>','D')
            ->where('level',3)
            ->get();

        $ret = new \stdClass();
        $ret->success = true;
        $ret->data = $admin;

        \Log::info("200::parkingLot/{PakingLotID}/mangers:: Get parking lot manager for parking lot ID: ".$parkingLotID);
        return response()->json($ret);
    }
}