<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MemberType;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class MemberTypeController extends Controller
{
	/**
	 * get all member types
	 * @param  Request $request basic request parameter
	 * @return            
	 */
    public function getMemberType(Request $request)
    {
    	$memberType = MemberType::get();
    	$ret = new \stdClass();
    	$ret->success = true;
    	$ret->data = $memberType;
    	\Log::info("200::memberType/:: Get member type");
		return response()->json($ret);
    }

    /**
     * Add member type
     * @param Request $request request with member type parameter
     */
    public function addMemberType(Request $request)
    {
    	if(Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::memberType/add/:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
        if($request->has('data'))
        {
        	$data = $request->input('data');
        	if(array_key_exists("memberType", $data))
        	{
        		$memberType = new MemberType();
        		$memberType->memberType = $data["memberType"];
        		try 
        		{
        			if($memberType->save())
        			{
        				$ret = new \stdClass();
        				$ret->success = true;
        				$ret->data = $memberType;
        				\Log::info("200::memberType/add/:: memberType with ID: ".$memberType->id." added");
    					return response()->json($ret);
        			}
        			else
        			{
        				$ret = new \stdClass();
	                    $ret->success = false;
	                    $ret->error = "Something wrong with our server, please try again later";
	                    \Log::error("500::memberType/add/:: failed to save");
	                    return response()->json($ret);
        			}
        		} 
        		catch (\Illuminate\Database\QueryException $ex)
    			{
    				$ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::memberType/add/:: failed to save: ".$ex);
                    return response()->json($ret);
    			}
        	}
        	else
        	{
        		\Log::info("400::memberType/add/:: required fields not provided");
                return $this->returnBadRequest("memberType field is required");
        	}
        }
        \Log::info("400::memberType/add/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * Edit member type with specific id
     * @param  Request $request request with id and memberType parameter
     * @return 
     */
    public function editMemberType(Request $request)
    {
    	if(Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::memberType/edit/:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
        if($request->has('data'))
        {
        	$data = $request->input('data');
        	if(array_key_exists("id", $data) && array_key_exists("memberType", $data))
        	{
        		$memberType = MemberType::find($data["id"]);
        		$memberType->memberType = $data["memberType"];

        		try
        		{
        			if($memberType->save())
        			{
        				$ret = new \stdClass();
        				$ret->success = true;
        				$ret->data = $memberType;
        				\Log::info("200::memberType/edit/:: memberType with ID: ".$memberType->id." edited");
    					return response()->json($ret);
        			}
        			else
        			{
        				$ret = new \stdClass();
	                    $ret->success = false;
	                    $ret->error = "Something wrong with our server, please try again later";
	                    \Log::error("500::memberType/edit/:: failed to save");
	                    return response()->json($ret);
        			}
        		}
        		catch (\Illuminate\Database\QueryException $ex)
    			{
    				$ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::memberType/edit/:: failed to save: ".$ex);
                    return response()->json($ret);
    			}
        	}
        	else
        	{
        		\Log::info("400::memberType/edit/:: required fields not provided");
                return $this->returnBadRequest("memberType and id field are required");
        	}
        }
        \Log::info("400::memberType/edit/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * Delete member type at specific id
     * @param  Request $request request with id parameter
     * @return            
     */
    public function deleteMemberType(Request $request)
    {
    	if(Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::memberType/delete/:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
        if($request->has('data'))
        {
        	$data = $request->input("data");
        	if(array_key_exists("id", $data))
        	{
        		$memberType = MemberType::find($data["id"]);

        		try
        		{
        			if($memberType->delete())
        			{
        				$ret = new \stdClass();
        				$ret->success = true;
        				\Log::info("200::memberType/delete/:: memberType with ID: ".$memberType->id." deleted");
    					return response()->json($ret);
        			}
        			else
        			{
        				$ret = new \stdClass();
	                    $ret->success = false;
	                    $ret->error = "Something wrong with our server, please try again later";
	                    \Log::error("500::memberType/delete/:: failed to save");
	                    return response()->json($ret);
        			}
        		}
        		catch (\Illuminate\Database\QueryException $ex)
    			{
    				$ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::memberType/delete/:: failed to save: ".$ex);
                    return response()->json($ret);
    			}
        	}
        	else
        	{
        		\Log::info("400::memberType/delete/:: required fields not provided");
                return $this->returnBadRequest("id field is required");
        	}
        }
        \Log::info("400::memberType/delete/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }
}
