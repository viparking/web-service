<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function returnBadRequest($error = "Bad Request")
    {
    	//shorthand for declaring json object
    	return response()->json(['success'=>false, 'error'=>$error], 400);
    }

    //"UL".substr($username,0,1).date('Ymdhis').substr(microtime(), 2,3);
    
    public function getFormattedDate($timeString)
	{
		$t = strtotime($timeString);
		$ret = "";
		$ret = date('H:i d-M-Y',$t);
		// if(date('z')>date('z',$t))
		// {
		// 	$ret = date('H:i d-M-Y',$t);
		// }
		// else
		// {
		// 	$ret = date('H:i',$t);
		// }
		return $ret;
	}

	public function createdDateString($timeString)
	{
		$date1=date_create("now");
		$date2=date_create($timeString);
		$interval = date_diff($date1,$date2);

		$timeString ="";
		if($interval->d>0)
		{
			if($interval->d==1)
				$timeString = "yesterday";
			else if($interval->d<30)
				$timeString = $interval->d." days ago";
			else
				$timeString = "on ".getFormattedDate($timeString);
		}
		else if($interval->h>0)
		{
			$timeString = $interval->h;
			$timeString .= $interval->h==1?" hour ago":" hours ago";
		}
		else if($interval->i>0)
		{
			$timeString = $interval->i;
			$timeString .= $interval->i==1?" minute ago":" minutes ago";
		}
		else
			$timeString = "A few seconds ago";
		return $timeString;
	}
	//other utility method for controller here

}
