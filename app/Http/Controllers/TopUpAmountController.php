<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\TopUpAmount;
use DB;

class TopUpAmountController extends Controller
{
    /**
     * add top up ammount
     * @param Request $request request with payAmount and creditAmount parameter
     */
    public function add(Request $request)
    {
    	if(Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::topUpAmount/add/:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
        if($request->has('data'))
        {
        	$data = $request->input('data');
        	if(array_key_exists("payAmount", $data) && array_key_exists("creditAmount", $data))
        	{
        		$amount = new TopUpAmount();
        		$amount->payAmount = $data["payAmount"];
        		$amount->creditAmount = $data["creditAmount"];
        		$amount->auditedUser = Auth::guard('admin')->user()->id;
        		$amount->auditedActivity = 'I';

        		try
        		{
        			if($amount->save())
        			{
        				$ret = new \stdClass();
        				$ret->success = true;
        				$ret->data = $amount;
        				\Log::info("200::topUpAmount/add/:: Top Up Amount created with id: ".$amount->id);
    					return response()->json($ret);
        			}
        			else
        			{
        				$ret = new \stdClass();
	                    $ret->success = false;
	                    $ret->error = "Something wrong with our server, please try again later";
	                    \Log::error("500::topUpAmount/add/:: failed to save");
	                    return response()->json($ret);
        			}
        		}
        		catch (\Illuminate\Database\QueryException $ex)
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::topUpAmount/add/:: failed to save: ".$ex);
                    return response()->json($ret);
                }
        	}
        	else
        	{
        		\Log::info("400::topUpAmount/add/:: required fields not provided");
                return $this->returnBadRequest("payAmount and creditAmount field are required");
        	}
        }
        \Log::info("400::topUpAmount/add/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * edit the top up amount registered before
     * @param  Request $request request with payAmount, creditAmount, and id parameter
     * @return            
     */
    public function edit(Request $request)
    {
    	if(Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::topUpAmount/edit/:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
        if($request->has('data'))
        {
        	$data = $request->input('data');
        	if(array_key_exists("payAmount", $data) && array_key_exists("creditAmount", $data) && array_key_exists("id", $data))
        	{
        		$amount = TopUpAmount::find($data["id"]);
        		$amount->payAmount = $data["payAmount"];
        		$amount->creditAmount = $data["creditAmount"];
        		$amount->auditedUser = Auth::guard('admin')->user()->id;
        		$amount->auditedActivity = 'U';

        		try
        		{
        			if($amount->save())
        			{
        				$ret = new \stdClass();
        				$ret->success = true;
        				$ret->data = $amount;
        				\Log::info("200::topUpAmount/edit/:: Top Up Amount edited with id: ".$amount->id);
    					return response()->json($ret);
        			}
        			else
        			{
        				$ret = new \stdClass();
	                    $ret->success = false;
	                    $ret->error = "Something wrong with our server, please try again later";
	                    \Log::error("500::topUpAmount/edit/:: failed to save");
	                    return response()->json($ret);
        			}
        		}
        		catch (\Illuminate\Database\QueryException $ex)
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::topUpAmount/edit/:: failed to save: ".$ex);
                    return response()->json($ret);
                }
        	}
        	else
        	{
        		\Log::info("400::topUpAmount/edit/:: required fields not provided");
                return $this->returnBadRequest("payAmount and creditAmount field are required");
        	}
        }
        \Log::info("400::topUpAmount/edit/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * delete registered top up amount saved before
     * @param  Request $request request with id parameter
     * @return            
     */
    public function delete(Request $request)
    {
    	if(Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::topUpAmount/delete/:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
        if($request->has('data'))
        {
        	$data = $request->input('data');
        	if(array_key_exists("id", $data))
        	{
        		$amount = TopUpAmount::find($data["id"]);
        		$amount->auditedUser = Auth::guard('admin')->user()->id;
        		$amount->auditedActivity = 'D';

        		try
        		{
        			if($amount->save())
        			{
        				$ret = new \stdClass();
        				$ret->success = true;
        				\Log::info("200::topUpAmount/delete/:: Top Up Amount deleted with id: ".$amount->id);
    					return response()->json($ret);
        			}
        			else
        			{
        				$ret = new \stdClass();
	                    $ret->success = false;
	                    $ret->error = "Something wrong with our server, please try again later";
	                    \Log::error("500::topUpAmount/delete/:: failed to save");
	                    return response()->json($ret);
        			}
        		}
        		catch (\Illuminate\Database\QueryException $ex)
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::topUpAmount/delete/:: failed to save: ".$ex);
                    return response()->json($ret);
                }
        	}
        	else
        	{
        		\Log::info("400::topUpAmount/delete/:: required fields not provided");
                return $this->returnBadRequest("id field is required");
        	}
        }
        \Log::info("400::topUpAmount/delete/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * get list of the top up amount saved in DB
     * @param  Request $request basic request parameter
     * @return            
     */
    public function get(Request $request)
    {
    	$amount = TopUpAmount::where('auditedActivity','<>','D')
    		->orderBy('payAmount')
    		->get();

    	$ret = new \stdClass();
    	$ret->success = true;
    	$ret->data = $amount;

    	\Log::info("200::topUpAmount/:: Get top up amount list");
    	return response()->json($ret);
    }
}
