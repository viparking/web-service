<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\ContactForm;

class ContactController extends Controller
{
    /**
     * Get list of contact forms filled by customer
     * @param  Request $request basic request parameter
     * @param  integer $page    page offset
     * @return
     */
    public function getContactForms(Request $request, $page = 0)
    {
    	$skip = $page * config('app.OFFSET');
    	$contacts = ContactForm::orderBy('created_at','DESC')
			->skip($skip)
            ->take(config('app.OFFSET'))
    		->get();

        $contactCount = ContactForm::orderBy('created_at','DESC')
            ->count();

    	$ret = new \stdClass();
    	$ret->success = true;
        $ret->total = $contactCount;
    	$ret->data = $contacts;

    	\Log::info("200::contact/getAll:: Get Contact forms page ".$page);
        return response()->json($ret);
    }
}
