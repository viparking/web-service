<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Gate;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class GateController extends Controller
{
	/**
	 * Get gates for specific parking lot
	 * @param  Request $request      basic request parameter
	 * @param  integer  $parkingLotID id of the parking lot
	 * @return                 
	 */
    public function getParkingLotGate(Request $request, $parkingLotID)
    {
    	$gates = Gate::where('parking_lot_id', $parkingLotID)
    		->where('auditedActivity','<>','D')
    		->orderBy('type')
    		->get();

    	$ret = new \stdClass();
    	$ret->success = true;
    	$ret->data = $gates;
    	\Log::info("200::parkingLot/{parkingLotID}/gate/:: get gates for parking lot id: ".$parkingLotID);
        return response()->json($ret);
    }

    /**
     * Add gate for specific parking lot
     * @param Request $request      request parameter with name and type parameter
     * @param integer  $parkingLotID id of the parking lot
     */
    public function addParkingLotGate(Request $request, $parkingLotID)
    {
    	if(Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::parkingLot/{parkingLotID}/gate/add/:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
        if($request->has('data'))
        {
        	$data = $request->input('data');
        	if(array_key_exists("name", $data) && array_key_exists("type", $data))
        	{
        		$gateID = "G".substr($data["name"],0,2).date('Ymdhis').substr(microtime(), 2,3);
        		$gate = new Gate();
        		$gate->id = $gateID;
        		$gate->parking_lot_id = $parkingLotID;
        		$gate->name = $data["name"];
        		$gate->type = $data["type"];
        		$gate->auditedActivity = 'I';
        		$gate->auditedUser = Auth::guard('admin')->user()->id;
        		try
        		{
        			if($gate->save())
        			{
        				$ret = new \stdClass();
        				$ret->success = true;
        				$ret->data = $gate;
        				\Log::info("200::parkingLot/{parkingLotID}/gate/add/:: gate with ID: ".$gate->id." added for parking lot ID: ".$parkingLotID);
    					return response()->json($ret);
        			}
        			else
        			{
        				$ret = new \stdClass();
	                    $ret->success = false;
	                    $ret->error = "Something wrong with our server, please try again later";
	                    \Log::error("500::parkingLot/{parkingLotID}/gate/add/:: failed to save");
	                    return response()->json($ret);
        			}
        		}
        		catch (\Illuminate\Database\QueryException $ex)
    			{
    				$ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::parkingLot/{parkingLotID}/gate/add/:: failed to save: ".$ex);
                    return response()->json($ret);
    			}
        	}
        	else
        	{
        		\Log::info("400::parkingLot/{parkingLotID}/gate/add/:: required fields not provided");
                return $this->returnBadRequest("name and type field are required");
        	}
        }
        \Log::info("400::parkingLot/{parkingLotID}/gate/add/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * Edit gate with specific id for specified parking lot
     * @param  Request $request      request with id, name, and type parameter
     * @param  integer  $parkingLotID id of the parking lot
     * @return                 
     */
    public function editParkingLotGate(Request $request, $parkingLotID)
    {
    	if(Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::parkingLot/{parkingLotID}/gate/edit/:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
        if($request->has('data'))
        {
        	$data = $request->input('data');
        	if(array_key_exists("name", $data) && array_key_exists("type", $data) && array_key_exists("id", $data))
        	{
        		$gate = Gate::find($data["id"]);
        		$gate->name = $data["name"];
        		$gate->type = $data["type"];
        		$gate->auditedUser = Auth::guard('admin')->user()->id;
        		$gate->auditedActivity = 'U';

        		try
        		{
        			if($gate->save())
        			{
        				$ret = new \stdClass();
        				$ret->success = true;
        				$ret->data = $gate;
        				\Log::info("200::parkingLot/{parkingLotID}/gate/edit/:: gate with ID: ".$gate->id." edited for parking lot ID: ".$parkingLotID);
    					return response()->json($ret);
        			}
        			else
        			{
        				$ret = new \stdClass();
	                    $ret->success = false;
	                    $ret->error = "Something wrong with our server, please try again later";
	                    \Log::error("500::parkingLot/{parkingLotID}/gate/edit/:: failed to save");
	                    return response()->json($ret);
        			}
        		}
        		catch (\Illuminate\Database\QueryException $ex)
    			{
    				$ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::parkingLot/{parkingLotID}/gate/edit/:: failed to save: ".$ex);
                    return response()->json($ret);
    			}
        	}
        	else
        	{
        		\Log::info("400::parkingLot/{parkingLotID}/gate/edit/:: required fields not provided");
                return $this->returnBadRequest("name, type and id field are required");
        	}
        }
        \Log::info("400::parkingLot/{parkingLotID}/gate/edit/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * delete gate with specified id for specific parking lot
     * @param  Request $request      request with id parameter
     * @param  integer  $parkingLotID id of the parking lot
     * @return                 
     */
    public function deleteParkingLotGate(Request $request, $parkingLotID)
    {
    	if(Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::parkingLot/{parkingLotID}/gate/delete/:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
        if($request->has('data'))
        {
        	$data = $request->input('data');
        	if(array_key_exists("id", $data))
        	{
        		$gate = Gate::find($data["id"]);
        		$gate->auditedActivity = 'D';
        		$gate->auditedUser = Auth::guard('admin')->user()->id;

        		try
        		{
        			if($gate->save())
        			{
        				$ret = new \stdClass();
        				$ret->success = true;
        				\Log::info("200::parkingLot/{parkingLotID}/gate/delete/:: gate with ID: ".$gate->id." deleted for parking lot ID: ".$parkingLotID);
    					return response()->json($ret);
        			}
        			else
        			{
        				$ret = new \stdClass();
	                    $ret->success = false;
	                    $ret->error = "Something wrong with our server, please try again later";
	                    \Log::error("500::parkingLot/{parkingLotID}/gate/delete/:: failed to save");
	                    return response()->json($ret);
        			}
        		}
        		catch (\Illuminate\Database\QueryException $ex)
    			{
    				$ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::parkingLot/{parkingLotID}/gate/delete/:: failed to save: ".$ex);
                    return response()->json($ret);
    			}
        	}
        	else
        	{
        		\Log::info("400::parkingLot/{parkingLotID}/gate/delete/:: required fields not provided");
                return $this->returnBadRequest("id field is required");
        	}
        }
        \Log::info("400::parkingLot/{parkingLotID}/gate/delete/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }
}
