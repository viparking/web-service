<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\CasshierSession;
use App\Admin;
use App\AdminLogin;
use App\Parking;
use Carbon\Carbon;
use Hash;
use DB;

class SessionController extends Controller
{
    /**
     * start a session for the parking lot casshier
     * @param  Request $request      request with managerPassword, startingBalance, and gateID
     * @param  string  $parkingLotID id of the parking id
     * @param  integer  $managerID    id of the manager authorizing the session
     * @return
     */
    public function start(Request $request, $parkingLotID, $managerID)
    {
        if(Auth::guard('admin')->user()->level > 1)
        {
            \Log::info("401::session/start/:: Incorrect level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }

        $manager = Admin::find($managerID);
        if($manager->level < 3)
        {
            \Log::info("401::session/start/:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }

        if($request->has('data'))
        {
            $data = $request->input('data');
            if(array_key_exists('managerPassword',$data) && array_key_exists("startingBalance", $data) && array_key_exists("gateID", $data))
            {
                if(!Hash::check($data['managerPassword'], $manager->password))
                {
                    \Log::info("200::session/start/:: Invalid Manager password");
                    return response()->json(['success'=>false, 'error'=>'Invalid Manager Password'], 200);
                }
                $session = new CasshierSession();
                $session->id = "SES".date('Ymdhis').substr(microtime(), 2,3);
                $sessionID = "SES".date('Ymdhis').substr(microtime(), 2,3);
                $session->parking_lot_id = $parkingLotID;
                $session->gate_id = $data["gateID"];
                $session->admin_id = Auth::guard('admin')->user()->id;
                $session->login_manager_id = $manager->id;
                $session->loginTime = Carbon::now('Asia/Jakarta');
                $session->startingBalance = $data["startingBalance"];
                $session->auditedUser = Auth::guard('admin')->user()->id;
                $session->auditedActivity = 'I';

                try
                {
                    if($session->save())
                    {
                        $ret = new \stdClass();
                        $ret->success = true;
                        $ret->data = $session;
                        $ret->data->session_id = $sessionID;
                        //$ret->data = $sessionID;
                        \Log::info("200::session/start/:: start session for gateID: ".$data["gateID"]);
                        return response()->json($ret);
                    }
                    else
                    {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->error = "Something wrong with our server, please try again later";
                        \Log::error("500::session/start/:: failed to save parking");
                        return response()->json($ret);
                    }
                }
                catch (\Illuminate\Database\QueryException $ex)
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::session/start/:: failed to save parking: ".$ex);
                    return response()->json($ret);
                }
            }
            else
            {
                \Log::info("400::session/start/:: required fields not provided");
                return $this->returnBadRequest("managerPassword, startingBalance and gateID field are required");
            }
        }
        \Log::info("400::session/start/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * ending the session for the current session of the parking lot casshier
     * @param  Request $request   request with managerPassword, finalBalance, takenBalance parameter
     * @param  string  $sessionID id of the session
     * @param  integer  $managerID id of the manager
     * @return
     */
    public function end(Request $request, $sessionID, $managerID)
    {
        if(Auth::guard('admin')->user()->level > 1)
        {
            \Log::info("401::session/start/:: Incorrect level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }

        $manager = Admin::find($managerID);
        if($manager->level < 3)
        {
            \Log::info("401::session/start/:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }

        if($request->has('data'))
        {
            $data = $request->input('data');
            if(array_key_exists('managerPassword',$data) && array_key_exists("finalBalance", $data) && array_key_exists("takenBalance", $data))
            {
                if(!Hash::check($data['managerPassword'], $manager->password))
                {
                    \Log::info("200::session/end/:: Invalid Manager password".$data["managerPassword"]);
                    return response()->json(['success'=>false, 'error'=>'Invalid Manager Password'], 200);
                }
                $session = CasshierSession::find($sessionID);
                if($session == null)
                {
                    \Log::info("400::session/end/:: Invalid Session ID");
                    return response()->json(['success'=>false, 'error'=>'Invalid Session ID'], 400);
                }

                $income = Parking::selectRaw('SUM(price) AS total')
                    ->where('outTime','>',$session->loginTime)
                    ->where('outGate','=',$session->gate_id)
                    ->where('payment_type_id','=',2)
                    ->where('auditedActivity','<>','D')
                    ->first();
                $income = $income->total;

                if( ($data["finalBalance"] - $session->startingBalance) < $income )
                {
                    \Log::info("200::session/end/:: Balance returned not match");
                    return response()->json(['success'=>false, 'error'=>"Balance doesn't match! Final Balance Should be: ".($session->startingBalance+$income)], 400);
                }

                $session->logout_manager_id = $manager->id;
                $session->logoutTime = Carbon::now('Asia/Jakarta');
                $session->finalBalance = $data["finalBalance"];
                $session->takenBalance = $data["takenBalance"];
                $session->auditedUser = Auth::guard('admin')->user()->id;
                $session->auditedActivity = 'U';

                try
                {
                    if($session->save())
                    {
                        $ret = new \stdClass();
                        $ret->success = true;
                        $ret->data = $session;
                        \Log::info("200::session/end/:: start session for sessionID: ".$sessionID);
                        return response()->json($ret);
                    }
                    else
                    {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->error = "Something wrong with our server, please try again later";
                        \Log::error("500::session/end/:: failed to save parking");
                        return response()->json($ret);
                    }
                }
                catch (\Illuminate\Database\QueryException $ex)
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::session/end/:: failed to save parking: ".$ex);
                    return response()->json($ret);
                }
            }
            else
            {
                \Log::info("400::session/end/:: required fields not provided");
                return $this->returnBadRequest("managerPassword, startingBalance and gateID field are required");
            }
        }
        \Log::info("400::session/end/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * get active session list for the parking lot
     * @param  Request $request      basic request parameter
     * @param  string  $parkingLotID id of the parking lot
     * @param  integer $page         page offset
     * @return
     */
    public function getSessionForParkingLot(Request $request, $parkingLotID, $page = 0)
    {
        if(Auth::guard('admin')->user()->level < 3)
        {
            \Log::info("401::{parkingLotID}/get/{page?}:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }

        $skip = $page * config('app.OFFSET');
        $session = DB::select(DB::raw('SELECT COALESCE(i.income,0) AS income, casshier_sessions.id AS sessionID,
        casshier_sessions.parking_lot_id, casshier_sessions.gate_id, casshier_sessions.admin_id, casshier_sessions.login_manager_id,
        casshier_sessions.logout_manager_id, casshier_sessions.loginTime, casshier_sessions.logoutTime,
         casshier_sessions.startingBalance, casshier_sessions.finalBalance, casshier_sessions.takenBalance,
         casshier_sessions.auditedActivity, casshier_sessions.auditedUser, admins.username, b.username AS loginManagerUsername,
         c.username AS logoutManagerUsername FROM casshier_sessions
  LEFT JOIN (SELECT SUM(price) AS income, s.id FROM parkings p, casshier_sessions s
    WHERE p.parking_lot_id = s.parking_lot_id
    AND outTime > s.loginTime
    AND outTime < IF(s.logoutTime > s.loginTime, s.logoutTime, CURRENT_TIMESTAMP)
    GROUP BY s.id)i ON i.id = casshier_sessions.id
  JOIN admins ON admins.id = casshier_sessions.admin_id
  JOIN admins b on b.id = casshier_sessions.login_manager_id
  LEFT JOIN admins c on c.id = casshier_sessions.logout_manager_id
  WHERE casshier_sessions.parking_lot_id = '.$parkingLotID.'
  ORDER BY loginTime DESC LIMIT '.config('app.OFFSET').' OFFSET '.$skip));

        $sessionCount = CasshierSession::selectRaw('casshier_sessions.id')
            ->where('casshier_sessions.parking_lot_id','=',$parkingLotID)
            ->where('casshier_sessions.auditedActivity','<>','D')
            ->count();

        $ret = new \stdClass();
        $ret->success = true;
        $ret->total = $sessionCount;
        $ret->data = $session;

        \Log::info("200::{parkingLotID}/get/{page?}:: Get parking lot session for parking lot id: ".$parkingLotID." page ".$page);
        return response()->json($ret);
    }

    /**
     * get detail of the session
     * @param  Request $request   basic request parameter
     * @param  string  $sessionID id of the session
     * @param  integer $page      page offset of the transaction detail
     * @return
     */
    public function getSessionDetail(Request $request, $sessionID, $page = 0)
    {
        $skip = $page * config('app.OFFSET');
        $summary = DB::select(DB::raw('SELECT COALESCE(i.income,0) AS income, casshier_sessions.id AS sessionID,
        casshier_sessions.parking_lot_id, casshier_sessions.gate_id, casshier_sessions.admin_id, casshier_sessions.login_manager_id,
        casshier_sessions.logout_manager_id, casshier_sessions.loginTime, casshier_sessions.logoutTime,
         casshier_sessions.startingBalance, casshier_sessions.finalBalance, casshier_sessions.takenBalance,
         casshier_sessions.auditedActivity, casshier_sessions.auditedUser, admins.username, b.username AS loginManagerUsername,
         c.username AS logoutManagerUsername FROM casshier_sessions
  LEFT JOIN (SELECT SUM(price) AS income, s.id FROM parkings p, casshier_sessions s
    WHERE p.parking_lot_id = s.parking_lot_id
    AND outTime > s.loginTime
    AND outTime < IF(s.logoutTime > s.loginTime, s.logoutTime, CURRENT_TIMESTAMP)
    GROUP BY s.id)i ON i.id = casshier_sessions.id
  JOIN admins ON admins.id = casshier_sessions.admin_id
  JOIN admins b on b.id = casshier_sessions.login_manager_id
  LEFT JOIN admins c on c.id = casshier_sessions.logout_manager_id
  WHERE casshier_sessions.id = \''.$sessionID.'\'
  ORDER BY loginTime DESC LIMIT '.config('app.OFFSET').' OFFSET '.$skip));

        $summary = $summary[0];

        if($summary->logoutTime > $summary->loginTime)
        {
            $detail = Parking::where('outGate','=',$summary->gate_id)
                ->where('outTime','>',$summary->loginTime)
                ->where('outTime','<',$summary->logoutTime)
                ->where('auditedActivity','<>','D')
                ->skip($skip)
                ->take(config('app.OFFSET'))
                ->get();

            $count = Parking::where('outGate','=',$summary->gate_id)
                ->where('outTime','>',$summary->loginTime)
                ->where('outTime','<',$summary->logoutTime)
                ->where('auditedActivity','<>','D')
                ->count();
        }
        else
        {
            $detail = Parking::where('outGate','=',$summary->gate_id)
                ->where('outTime','>',$summary->loginTime)
                ->where('auditedActivity','<>','D')
                ->skip($skip)
                ->take(config('app.OFFSET'))
                ->get();

            $count = Parking::where('outGate','=',$summary->gate_id)
                ->where('outTime','>',$summary->loginTime)
                ->where('auditedActivity','<>','D')
                ->count();
        }

        $ret = new \stdClass();
        $ret->success = true;
        $ret->total = $count;
        $ret->data = new \stdClass();
        $ret->data->summary = $summary;
        $ret->data->detail = $detail;

        \Log::info("200::{sessionID}/detail/{page?}:: Get session detail for id: ".$sessionID." page ".$page);
        return response()->json($ret);
    }
}
