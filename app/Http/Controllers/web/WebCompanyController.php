<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use App\Company;

class WebCompanyController extends Controller
{
    public function get(Request $request, $error = null)
    {
    	$companyController = new CompanyController();
    	$companies = $companyController->getCompanyList($request)->getData();
    	return view('company.get', compact(['companies', 'error']));
    }

    public function detail(Request $request, $companyID, $page = null)
    {
    	$companyController = new CompanyController();
    	$company = $companyController->getCompanyDetail($request, $companyID)->getData();
    	$parkingLotList = $companyController->getParkingLot($request, $companyID)->getData();
        $adminListResult = $companyController->getAdmin($request, $companyID, $page)->getData();
        $monthlyParking = $companyController->getMonthlyParking($request, $companyID)->getData();
        $weeklyParking = $companyController->getWeeklyParking($request, $companyID)->getData();
        $monthlyRevenue = $companyController->getMonthlyRevenue($request, $companyID)->getData();
        $weeklyRevenue = $companyController->getWeeklyRevenue($request, $companyID)->getData();

        // Create manual Paginator
        $adminList = new LengthAwarePaginator($adminListResult->data, $adminListResult->total, config('app.OFFSET'), $page);
        $adminList->setPath('detail');

        $level = Auth::guard('admin')->user()->level;
    	return view('company.detail', compact(['company', 'parkingLotList', 'adminList', 'page','level','monthlyParking','monthlyRevenue','weeklyParking','weeklyRevenue']));
    }

    public function add(Request $request, $error = null)
    {
    	return view('company.add', compact(['error']));
    }

    public function edit(Request $request, $companyID, $error = null)
    {
    	$company = Company::find($companyID);
    	return view('company.add', compact(['company', 'error']));
    }

    public function post(Request $request)
    {
		$result = null;
		$companyController = new CompanyController();
		$data = array(
			'name' => $request->input('name'),
			'representativeName' => $request->input('representativeName'),
			'phoneNumber' => $request->input('phoneNumber'),
			'email' => $request->input('email'),
			'address' => $request->input('address')
		);

		// Edit Company
		if ($request->has('id'))
		{
            $companyID = $request->input('id');
			$data['id'] = $companyID;
			$request->merge(array('data' => $data));

			$result = $companyController->editCompany($request)->getData();

            // Return result
            if ($result->success) {
                return redirect('/company');
            }
            else {
                $error = $result->error;
                return $this->edit($request, $companyID, $error);
            }
		}
		// Add New Company
		else
		{
			$request->merge(array('data' => $data));
            
			$result = $companyController->addCompany($request)->getData();

            // Return result
            if ($result->success) {
                return redirect('/company');
            }
            else {
                $error = $result->error;
                return $this->add($request, $error);
            }
		}
    }

    public function delete(Request $request)
    {
    	if ($request->has('id')) 
    	{
    		$data = array('id' => $request->input('id'));
    		$request->merge(array('data' => $data));

    		$companyController = new CompanyController();
    		$result = $companyController->deleteCompany($request)->getData();

    		// Return result
    		if ($result->success) {
    			return redirect('/company');
    		}
    		else {
    			return $this->get($request, $result->error);
    		}
    	}

    	return abort(404);
    }
}
