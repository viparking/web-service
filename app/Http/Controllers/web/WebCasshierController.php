<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use App\Http\Controllers\SessionController;
use App\Http\Controllers\CompanyController;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class WebCasshierController extends Controller
{
    public function getCasshierSession(Request $request)
    {
        $page = 0;
        if($request->has('page'))
            $page = $request->input('page');

    	$companyID = $request->has('companyID') ? $request->input('companyID') : 0;
    	$parkingLotID = $request->has('parkingLotID') ? $request->input('parkingLotID') : 0;

    	$sessionController = new SessionController();
    	$companyController = new CompanyController();

    	$companies = $companyController->getCompanyList($request)->getData();
    	if($companyID > 0)
	    	$parkingLots = $companyController->getParkingLot($request, $companyID)->getData();
	    if($parkingLotID > 0)
	    {
	    	$sessions = $sessionController->getSessionForParkingLot($request, $parkingLotID, $page)->getData();
	    	$sessions = new LengthAwarePaginator($sessions->data, $sessions->total, config('app.OFFSET'), $page);
			$sessions->setPath('casshier?companyID='.$companyID.'&parkingLotID='.$parkingLotID);
	    }
    	return view('session.get', compact(['companies','parkingLots','sessions',
    		'companyID','parkingLotID']));
    }

    public function detail(Request $request, $sessionID)
    {
        $page = 0;
        if($request->has('page'))
            $page = $request->input('page');

        $sessionController = new SessionController();
        $sessions = $sessionController->getSessionDetail($request, $sessionID, $page)->getData();
        $summary = $sessions->data->summary;
        $detail = $sessions->data->detail;
        $detail = new LengthAwarePaginator($detail, $sessions->total, config('app.OFFSET'), $page);
        $detail->setPath('detail');
        return view('session.detail',compact(['detail','summary']));
    }
}
