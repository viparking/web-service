<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\AdminController;
use App\VipMember;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Controllers\VehicleController;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Admin;

class WebVehicleController extends Controller
{
	public function get(Request $request, $page = null)
	{
		$vehicleController = new VehicleController();
        if($request->has("page"))
            $page = $request->input("page");
		$result = $vehicleController->getAllVehicle($request, $page)->getData();

		// Create manual Paginator
		$vehicles = new LengthAwarePaginator($result->data, $result->total, config('app.OFFSET'), $page);
		$vehicles->setPath('vehicle');

		return view('vehicle.get', compact(['vehicles']));
	}

	public function search(Request $request, $page = null)
	{
		$vehicleController = new VehicleController();
		if($request->has("page"))
            $page = $request->input("page");
		$search = $request->input('username');
		$result = $vehicleController->getAllVehicle($request, $page,$search)->getData();
		//echo $request->input('username'); dd();
		// Create manual Paginator
		$vehicles = new LengthAwarePaginator($result->data, $result->total, config('app.OFFSET'), $page);
		$vehicles->setPath('vehicle');

		return view('vehicle.get', compact(['vehicles', 'search']));
	}

	public function addForMember(Request $request, $memberID) {
        $vehicleController = new VehicleController();
        $adminController = new AdminController();
        $vehicleTypes = $vehicleController->getVehicleType($request)->getData();
        $member = $adminController->getMemberDetail($request, $memberID)->getData();
        $member = $member->data[0];

        return view('vehicle.add', compact(['vehicleTypes','member', 'error']));
    }

	public function detail(Request $request, $vehicleID, $error = null)
	{
		$vehicleController = new VehicleController();

		$detail = $vehicleController->getVehicleByID($request, $vehicleID)->getData();
		$parkings =  $vehicleController->getAdminParkingHistory($request, $vehicleID)->getData();
		$topUps = $vehicleController->getAdminTopUp($request, $vehicleID)->getData();

		return view('vehicle.detail', compact(['detail', 'parkings', 'topUps', 'error']));
	}

	public function edit(Request $request, $vehicleID, $error = null)
	{
		$vehicleController = new VehicleController();

		$vehicle = $vehicleController->getVehicleByID($request, $vehicleID)->getData();
		$vehicle = $vehicle->data;
		//print_r($vehicle);
		$vehicleTypes = $vehicleController->getVehicleType($request)->getData();

		return view('vehicle.edit', compact(['vehicle', 'error','vehicleTypes']));
	}

	public function update(Request $request, $vehicleID) {
		$vehicle =\App\Vehicle::find($vehicleID);
		$vehicle->vehicle_type_id = $request->input('vehicleTypeID');
		$vehicle->member_id = $request->input('memberID');
		$vehicle->card_id = $request->input('cardID');
		$vehicle->plateNumber = $request->input('plateNumber');
		$vehicle->address = $request->input('address');
		$vehicle->noSTNK = $request->input('noSTNK');
		$vehicle->brand = $request->input('brand');
		$vehicle->type = $request->input('type');
		$vehicle->color = $request->input('color');

		$vehicle->save();
		return redirect('vehicle/'.$vehicleID.'/detail');
	}

	public function add(Request $request, $error = null)
	{
		$vehicleController = new VehicleController();
		$vehicleTypes = $vehicleController->getVehicleType($request)->getData();

		return view('vehicle.add', compact(['vehicleTypes', 'error']));
	}

	public function post(Request $request)
	{
		$vehicleController = new VehicleController();
		$data = array(
			'vehicleTypeID' => $request->input('vehicleTypeID'),
			'memberID' => $request->input('memberID'),
			'cardID' => $request->input('cardID'),
			'name' => $request->input('name'),
			'plateNumber' => $request->input('plateNumber'),
			'address' => $request->input('address'),
			'noSTNK' => $request->input('noSTNK'),
			'brand' => $request->input('brand'),
			'color' => $request->input('color'),
			'type' => $request->input('type')
		);
		$request->merge(array('data' => $data));

		$result = $vehicleController->addVehicle($request)->getData();

		// Return result
		if ($result->success) {
			return redirect('/vehicle');
		}
		else {
			$error = $result->error;
			return $this->add($request, $error);
		}
	}

	public function delete(Request $request)
	{
		if ($request->has('id'))
		{
			$vehicleID = $request->input('id');
			$data = array('id' => $vehicleID);
			$request->merge(array('data' => $data));

			$vehicleController = new VehicleController();
			$result = $vehicleController->deleteVehicle($request)->getData();

			// Return result
			if ($result->success) {
				return redirect('/vehicle');
			}
			else {
				$error = $result->error;
				return $this->detail($request, $vehicleID, $error);
			}
		}

		return abort(404);
	}

	public function vip(Request $request)
    {
        if ($request->has('vehicleID') && $request->has('dueDate'))
        {
            $vehicleID = $request->input('vehicleID');
            $vip = VipMember::where('vehicle_id','=',$vehicleID)->first();
            if($vip == NULL) {
                $vip = new VipMember();
                $vip->vehicle_id = $vehicleID;
            }
            $vip->due_date = $request->input('dueDate');

            $vip->save();

            return redirect('/vehicle/'.$vehicleID.'/detail');
        }
        return abort(404);
    }
}