<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use App\Http\Controllers\AdminLevelController;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\AdminLevel;

class WebAdminLevelController extends Controller
{
    public function getAdminLevel(Request $request)
    {
    	$adminLevelController = new AdminLevelController();
    	$level = $adminLevelController->getLevel($request);
    	$level = $level->getData();
    	return view('adminLevel.get', compact(['level']));
    }

    public function editAdminLevel(Request $request, $adminLevelID)
    {
    	$adminLevel = AdminLevel::find($adminLevelID);
    	return view('adminLevel.add', compact(['adminLevel']));
    }

    public function post(Request $request)
    {
    	if($request->has('adminLevel'))
    	{
    		$result = null;
    		if($request->has('id'))
    		{
    			$data = [];
    			$data["level"] = $request->input('id');
    			$data["adminLevel"] = $request->input('adminLevel');
    			$request->merge(array('data' => $data));
    			$adminLevelController = new AdminLevelController();
    			$result = $adminLevelController->updateLevel($request);
    		}
    		else
    		{
    			$data = [];
    			$data["level"] = $request->input('id');
    			$data["adminLevel"] = $request->input('adminLevel');
    			$request->merge(array('data' => $data));
    			$adminLevelController = new AdminLevelController();
    			$result = $adminLevelController->addLevel($request);
    		}
    		$result = $result->getData();
    		if($result->success)
	    		return redirect('admin-level/');
	    	else
	    	{
	    		$error = $result->error;
	    		if($request->has('id'))
	    		{
		    		$adminLevel = new \stdClass();
		    		$adminLevel->id = $request->input('id');
		    		$adminLevel->adminLevel = $request->input('adminLevel');
		    		return view('adminLevel.add', compact(['adminLevel','error']));
		    	}
	    		return view('adminLevel.add','error');
	    	}
    	}
    	return redirect('/admin-level');
    }

    public function delete(Request $request)
    {
    	if($request->has('id'))
    	{
    		$data = [];
			$data["level"] = $request->input('id');
			$request->merge(array('data' => $data));
			$adminLevelController = new AdminLevelController();
			$result = $adminLevelController->deleteLevel($request);
			$result = $result->getData();
			if($result->success)
				return redirect('/admin-level');
			$error = $result->error;
			return redirect('/admin-level', compact(['error']));
    	}
    	return redirect('/admin-level');
    }
}
