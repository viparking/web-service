<?php 

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\VehicleController;
use App\Http\Controllers\Controller;
use App\Member;
use App\Http\Requests;

class WebMemberController extends Controller
{
	public function get(Request $request, $page = null)
	{
		$adminController = new AdminController();
		if($request->has("page"))
		    $page = $request->input("page");
		$result = $adminController->getMember($request, $page)->getData();

		// Create manual Paginator
		$members = new LengthAwarePaginator($result->data, $result->total, config('app.OFFSET'), $page);
		$members->setPath('member');

		return view('member.get', compact(['members']));
	}

	public function search(Request $request, $page = null)
	{
		$adminController = new AdminController();
		$search = $request->input('username');
		$result = $adminController->getMember($request, $page,$search)->getData();
		//echo $request->input('username'); dd();
		// Create manual Paginator
		$members = new LengthAwarePaginator($result->data, $result->total, config('app.OFFSET'), $page);
		$members->setPath('member');

		return view('member.get', compact(['members','search']));
	}
	public function detail(Request $request, $memberID, $error = null)
	{
		$adminController = new AdminController();
		$vehicleController = new VehicleController();
		$userController = new UserController();

		$detail = $adminController->getMemberDetail($request, $memberID)->getData();
		$vehicles = $vehicleController->getVehicleForMember($request, $memberID)->getData();
		$transactions = $userController->getTransactionHistory($request, $memberID)->getData();
		
		return view('member.detail', compact(['detail', 'vehicles', 'transactions', 'error']));
	}

	public function edit(Request $request, $memberID, $error = null) {
		$adminController = new AdminController();
		$detail = $adminController->getMemberDetail($request, $memberID)->getData();
		$detail = $detail->data[0];

		return view('member.add', compact(['error','detail', 'memberID']));
	}

	public function update(Request $request, $memberID) {
		$member = Member::find($memberID);
		$member->username = $request->input('username');
		$member->email = $request->input('email');
		$member->phoneNumber = $request->input('phoneNumber');
		$member->alamatTinggal = $request->input('alamatTinggal');
		$member->alamatKTP = $request->input('alamatKTP');
		$member->noKTP = $request->input('noKTP');
		$member->save();
		
		return redirect('/member');
	}

	public function add(Request $request, $error = null)
	{
		return view('member.add', compact(['error']));
	}

	public function post(Request $request)
	{
		$userController = new UserController();
		$data = array(
			'username' => $request->input('username'),
			'password' => $request->input('password'),
			'email' => $request->input('email'),
			'phoneNumber' => $request->input('phoneNumber'),
			'alamatTinggal' => $request->input('alamatTinggal'),
			'alamatKTP' => $request->input('alamatKTP'),
			'noKTP' => $request->input('noKTP')
		);

		$request->merge(array('data' => $data));
		$result = $userController->register($request)->getData();

		// Return result
		if ($result->success) {
			return redirect('/member');
		}
		else {
			$error = $result->error;
			return $this->add($request, $error);
		}
	}

	public function ban(Request $request)
	{
		if ($request->has('id'))
		{
			$memberID = $request->input('id');
			$data = array('memberID' => $memberID);
			$request->merge(array('data' => $data));

			$adminController = new AdminController();
			$result = $adminController->banMember($request)->getData();

			// Return result
			if ($result->success) {
				return redirect('/member');
			}
			else {
				$error = $result->error;
				return $this->detail($request, $memberID, $error);
			}
		}

		return abort(404);
	}

	public function unban(Request $request)
	{
		if ($request->has('id'))
		{
			$memberID = $request->input('id');
			$data = array('memberID' => $memberID);
			$request->merge(array('data' => $data));

			$adminController = new AdminController();
			$result = $adminController->unbanMember($request)->getData();

			// Return result
			if ($result->success) {
				return redirect('/member');
			}
			else {
				$error = $result->error;
				return $this->detail($request, $memberID, $error);
			}
		}

		return abort(404);
	}
}