<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ParkingController;
use App\Http\Controllers\Controller;
use App\Http\Requests;

class WebDashboardController extends Controller
{
	public function get(Request $request)
	{
		$dashboardController = new DashboardController();
		$parkingController = new ParkingController();
		$time = Carbon::now();
        $start = NULL;
        $end = NULL;
        // if($request->has("startDate")) {
        //     $start = $request->input("startDate");
        //     $time = Carbon::parse($start);
        // }
        // if($request->has("endDate"))
        //     $end = $request->input("endDate");
		$week = $time->weekOfYear;
		$month = $time->month;

		$pendingParking 	= $parkingController->getPendingParkingCount($request)->getData();
		$weeklyProblem 		= $dashboardController->getWeeklyProblemCount($request, $start, $end)->getData();
		$parkingLotCapacity = $dashboardController->getParkingLotCapacity($request)->getData();
		$todayRevenue 		= $dashboardController->getTodayRevenue($request)->getData();
		$weeklyRevenue 		= $dashboardController->getWeeklyRevenue($request, $week)->getData();
		$monthlyRevenue 	= $dashboardController->getMonthlyRevenue($request, $month)->getData();
		$weeklyParking		= $dashboardController->getWeeklyParking($request, $week)->getData();
		$monthlyParking		= $dashboardController->getMonthlyParking($request, $month)->getData();
        $temp = $dashboardController->getAllTimeTopMember($request);
        if($temp != null)
		    $allTimeTopMember = $temp->getData();
        else
            $allTimeTopMember = null;
        $temp = $dashboardController->getWeeklyTopMember($request, $week);
        if($temp != null)
		    $weeklyTopMember = $temp->getData();
        else
            $weeklyTopMember = null;
        $temp = $dashboardController->getMonthlyTopMember($request, $month);
        if($temp != null)
		    $monthlyTopMember = $temp->getData();
        else
            $monthlyTopMember = null;

		return view('dashboard', compact([
			'pendingParking', 
			'weeklyProblem',
			'parkingLotCapacity',
			'todayRevenue',
			'weeklyRevenue',
			'monthlyRevenue',
			'allTimeTopMember',
			'weeklyTopMember',
			'monthlyTopMember',
			'monthlyParking',
			'weeklyParking'
		]));
	}
}