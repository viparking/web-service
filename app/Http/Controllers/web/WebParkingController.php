<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use App\Http\Controllers\ParkingController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\GateController;
use App\Http\Controllers\VehicleController;
use App\Http\Controllers\ParkingLotController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\PaymentTypeController;
use App\Gate;
use App\ParkingProblem;
use App\Parking;
use App\Http\Requests;

class WebParkingController extends Controller
{
	public function getParkingDetail(Request $request, $parkingID)
	{
		$parkingController = new ParkingController();
		$parking = $parkingController->getDetail($request, $parkingID)->getData();
		return view('parkingLot.parkingDetail', compact(['parking']));
	}

	public function add(Request $request, $parkingLotID, $error = null)
	{
		$gateController = new GateController();
		$vehicleController = new VehicleController();
		$gates = $gateController->getParkingLotGate($request, $parkingLotID)->getData();
		$vehicleTypes = $vehicleController->getVehicleType($request)->getData();

		return view('parking.add', compact(['parkingLotID', 'gates', 'vehicleTypes', 'error']));
	}

	public function edit(Request $request, $parkingLotID, $parkingID, $error = null)
	{
		$parkingController = new ParkingController();
		$gateController = new GateController();
		$vehicleController = new VehicleController();
		$parking = $parkingController->getDetail($request, $parkingID)->getData();
		$gates = $gateController->getParkingLotGate($request, $parkingLotID)->getData();
		$vehicleTypes = $vehicleController->getVehicleType($request)->getData();

		return view('parking.edit', compact(['parkingLotID', 'parking', 'gates', 'vehicleTypes', 'error']));
	}

	public function detail(Request $request, $parkingLotID, $parkingID, $error = null)
	{
		$parkingController = new ParkingController();
		$parkingLotController = new ParkingLotController();
		$vehicleController = new VehicleController();
		$parkingLot = $parkingLotController->detail($request, $parkingLotID)->getData();
		$parking = $parkingController->getDetail($request, $parkingID)->getData();
		$vehicle = $vehicleController->getVehicleByID($request, $parking->data->vehicle_id)->getData();

		return view('parking.detail', compact(['parkingLotID','parkingLot','parking','vehicle','error']));
	}

	public function post(Request $request)
	{
		$transactionController = new TransactionController();
		$parkingLotID = $request->input('parkingLotID');
		$data = array('photo' => $request->input('photo'));
		$result = null;

		if ($request->has('cardID')) $data['cardID'] = $request->input('cardID');
		if ($request->has('plateNumber')) $data['plateNumber'] = $request->input('plateNumber');
		if ($request->has('parkingID')) $data['parkingID'] = $request->input('parkingID');
		$data["isMember"] = true;

		// Edit Pending Parking (Gate Out)
		if ($request->has('outTime') && $request->has('outGate'))
		{
			$data['gateID'] = $request->input('outGate');
			$data['outTime'] = $request->input('outTime');
			$request->merge(array('data' => $data));

			$result = $transactionController->gateOut($request)->getData();

			// Return result
            if ($result->success) {
                return redirect('/parkingLot/'.$parkingLotID.'/detail');
            }
            else {
                $error = $result->error;
                return $this->edit($request, $parkingLotID, $error);
            }
		}
		// Add Parking (Gate In)
		else 
		{
			$data['gateID'] = $request->input('inGate');
			$data['inTime'] = $request->input('inDate') . ' ' . $request->input('inTime');
			$data['vehicleTypeID'] = $request->input('vehicleTypeID');
			$request->merge(array('data' => $data));

			$result = $transactionController->gateIn($request)->getData();

			// Return result
            if ($result->success) {
                return redirect('/parkingLot/'.$parkingLotID.'/detail');
            }
            else {
                $error = $result->error;
                return $this->add($request, $parkingLotID, $error);
            }
		}
	}

	public function gateOut(Request $request, $parkingLotID, $parkingID, $error = null)
	{
		$photo = "iVBORw0KGgoAAAANSUhEUgAAASwAAAEsCAIAAAD2HxkiAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAACldJREFUeNrs3Wtv03YbwGEoZRyebRLivA22aYhNQki82vf/Apu2Fdi6lh62MppTW0jTloYkz/20kpenFJo4dg72db1AiIMLrn+9/3Yd53yv1zsHTM55EYIIQYSACEGEgAhBhIAIQYSACEGEgAhBhIAIQYSACEGEgAhBhIAIQYSACEGEgAhBhIAIQYSACEGEgAhBhMBsRLi1tbW6umpfM4vu379/8+bNXD/E/Bj+G+12u9ls+nQyi+LozftDzNnLMFkiBBGCCAERgggBEYIIARGCCAERgggBEYIIARGCCAERggiBnM1P+b9vbm7uzp07Pk+MolKpdDodEaaP8OHDhw4jRrG1tTXNEVqOgghBhIAIQYSACEGEgAhBhIAIQYSACEGEgAhBhIAIQYSACEGEgAhBhIAIQYSACEGEgAhBhIAIQYRARubtgkEcHBy02+0LFy5cvXrV3kCE49Dr9ba3txuNRr1ef/v27f/tsvn5a9eu3bp1K36Mn9tXiDB7r169WltbO9Fe4t27d7UjMRjvHYmf2GmIMButVuv58+fx4yB/uNPpRKv//PPP999/f/36dXuPdFyY+VcsPn/++ecBC0wcHh4uLCz89ddfdiAiHEmlUomWUr+p8srKytLSkt2ICFN68+bN4uLiiBt5+fJlLE3tTEQ4tHa7/fTp0263O/qmlpeXo2e7FBEOJ07n4rwuk01FyS9evCjPrsvkKxdlj/Dg4CCWkRlu8PXr1/V6vSR7L86Em82mikQ4kjiLy/zL+d9//12GXbezsxNfv169eqUiEY6kWq1mvs04LYzzzGLvt06ns7i42Ov1KpVK6kvKiPB/35qP5Wjmm41Ds1arFXvXra6u7u/vH9cYHQpJhOnP33LacrHPlGK/9Z9IW5GKML08xuCx4ylRSHEK/ccff8S07/+K4xszIkzp3bt3OW25wOeEyUK0n2EoQsYkJt7Gxsb7vx6nhfl9RRNhkeX3EqSLFy+WYSHa/1suz4gwjStXruS05UuXLhVyIbq3t/eh33XfrAjT+PTTT3Pa8meffVaShWii1Wrld7VZhIUVqeS0brx27VpJFqKGoQhHcv78+Rs3bmS+2f8cKc9CNFGr1Qp/q5AIs/fVV19Fitlu8/79+6VaiPYPTJdnRJhmat29ezfb88xbt24VaSF6fI/ogH/eilSEaXz99ddZPbkwhup3332X+Wid7EJ0qIfuxKp1Z2fHQSXC4Vy6dOnRo0eZlPPtt98W6ZLM4AvRfu6eEWEaUc6DBw9G3Mjt27eLdDY47EI04fKMCFP68ssvf/jhh7m5lDvk3r178deLtEPW19eHffpjUu/m5qYjSoRp3Llz58mTJ5cvXx7qb8X5ZORXsFPBZrM5ypNUXZ4Z7hCyC/p9/vnnP/7448uXL2MOnHlHcozNu3fvfvPNNwW7U3TAb81/xP7+/vb2dsHuWBDhGNcGc3Oxtoy64jCqVqvx44kaY+LF4XX9+vUbN24U8h7R1AvRE8NQhCIcbb/Mz988cu7oCQ7J/SJR3SeffFLg//iIC9FEo9E4PDws9r4S4fhcuHCheDdk57QQ7d/U5uZmwW4eymvxZRdMp93d3fE/Wjdm4OgL0f4VaSY9i5AJiAXw0yPj7DCyz/a9pQ4ODuKM2mdThDNpfX09juCtra1nz56Np8PjhWjmH8v3KkQ4qwvR5GaxRqMxng5jBsbHzXyz8e//0BseI8LptbS01F9dHMfPnz/PtcPMF6KJOCd0K6kIZ0ys395/SES9Xs+vw5wWoomI0OUZEc6Mdru9urp66m/l12FOC9FELEfj5NYnV4SzYXl5+SOvP8ijw1arldNC9MR498kV4QyIVeiZz4aIDn///fesOoxVYq4L0URMwvzecUCEZHZitri4OMifrNVq0WEmZ1kxA8fzxjXxr/XiJhFOu42NjUEeZ5Z0GOvSETuMhej6+vrY/oPunhHhVNvf319bWxvqr4w4D8e2EE0cHh42Gg2faxFOqeXl5RQ9VKvV1B2ObSHar/8tDRHhFKlUKqlHRHSY4kUPY16IJnZ2dgr8to0inFWdTmdlZWXEhofqMP7k4uLi+F+fcc7dMyKcTi9evBj91sqhOtzY2Jjgu+pubm5OpH8Rcrrd3d2sJkN0OMjjCff29j50R854uDwjwilyfH0yw6v2MWQ+/p3G8V8RPZW7Z0Q4LWIGZn7HZnQYmU3nQjSxvb3t8owIJy/OA+NsMKeTrlPn4cQXooahCNPLY/22srLS6XTym7EnOpyShWj/VwqXZ0Q4xNop8we9NBqNvN/E70SHU7IQTbTb7Vqt5ugS4UBiCbe1tbWwsJBVh7Gd5eXl8Zxz/vnnn+eO7ombnoWoFempPHf0Y2PweIDET6LDx48fp367mMTa2trYLkscH+gTeXTimV6/fh2nqVevXnWYmYRnjMH+IH/77bcRj+Y47FK83d+IHU7VQtQwFGGaMZjY2dn59ddfR7mgMqn7xaZTnBjbGyIcdAz2L6JiHqbrMI6595/gVGbtdrtardoPIhx0DPZ3GPPwzHdNe/+AG8/1mNliRSrC4cZgIvqMeThUh7FB7yB96p7M8K0vRFiKMdh/9Aw+D2N4+pL/IV7pK8Khx2Ci2WxGh2fOt263u7S0ZK9+SJwW5nfzkAgLOwaH6nBjYyPXR+vOulhNuDwjwjRjMBGBfaTDg4ODiTxIYrZ4ub0IU47B/g5/+eWXUzuMhai11plin4//qVMiLMgYTLRarejw8PCw/xfr9bpXkRuGIsx9DPZ3GOvSpMMYgK7HDK5SqZR5ySDCUcfgiXl4/OCmtbU1b445uCgw75d3ibDgYzCxt7cX8zBWoWO+UbsAyvytVBFmMwb7O1xYWPC+C8Pa3d2d2hd8iHBmxiCGoQgnPwYZRbVaHfbOeBEag2Sp2+2W8/JM2SM0Bq1IRWgM8q9Wq1XClz6XOkJj0DAUoTHISbVarWwvgC5vhMbgdOp2u5ubmyI0Bpmkst3PXdIIjcFptre3F18lRWgMMkmlWpGWMUJjcPqV6vJM6SI0BmdCqS7PlC5CY3BWlOcbhuWK0BicIfv7+yW5PFOuCI1Bw1CExiBDqNfrJx6fJUJjkLHq9Xpl+MZ9WSI0BmdURFj4Z4WU5e2y4yz/iy++cEzPordv316+fFmEM0+BWI4CIgQRAiIEEQIiBBECIgQRAiIEEQIiBBGCCAERgggBEYIIgYmY9sdb9Hq9ZrPp88Qout2uCNPrdDo//fSTwwjLUUCEIEJAhCBCQIQgQkCEIEJAhCBCQIQgQkCEIEJAhCBCQIQgQkCEIEJAhCBCQIQgQkCEIEJAhCBCQIQgQkCEIEJAhCBCQIQgQkCEIEJAhCBCQIQgQkCEIEJAhCBCECEgQhAhIEIQISBCECEgQhAhIEIQISBCECEgQhAhIEIQISBCECEgQhAhIEIQIZCx871ez14AEYIIARGCCAERgggBEYIIARGCCAERgggBEYIIARGCCAERgggBEYIIARGCCAERgggBEYIIARGCCAERgggBEYIIARHC7PqvAAMA/BkrMLAeft8AAAAASUVORK5CYII=";

		$gate = Gate::where('parking_lot_id', $parkingLotID)
			->where('type','out')
			->first();
		if($gate == null) {
			$parkingController = new ParkingController();
			$gateController = new GateController();
			$vehicleController = new VehicleController();
			$parking = $parkingController->getDetail($request, $parkingID)->getData();
			$gates = $gateController->getParkingLotGate($request, $parkingLotID)->getData();
			$vehicleTypes = $vehicleController->getVehicleType($request)->getData();
			$error = "This Parking lot does not have out gate and thus can't complete this transaction.";

			return view('parking.edit', compact(['parkingLotID', 'parking', 'gates', 'vehicleTypes', 'error']));
		}

        $parking = Parking::find($parkingID);

		$transactionController = new TransactionController();
		$data = array();
		$data["gateID"] = $gate->id;
		$data["photo"] = $photo;
		$data["cardID"] = $parking->vehicle->card_id;
        $data["isMember"] = $parking->member_id != "guest";
        $data["parkingID"] = $parkingID;
		$request->merge(array('data' => $data));

		$paymentTypeController = new PaymentTypeController();

		$result = $transactionController->gateOut($request)->getData();
		$paymentTypes = $paymentTypeController->getPaymentType($request)->getData();
		if($result->success)
			return view('parking.pay', compact(['parkingID','parkingLotID','result','paymentTypes','error']));
		else {
            try {
                return redirect('problem/' . $result->data[0]->id . '/' . $result->data[0]->problem_id . '/' . $result->data[0]->reason . '/' . $parkingLotID . '/resolve');
            }
            catch(\Exception $e) {
                return view('parking.pay', compact(['parkingID','parkingLotID','result','paymentTypes','error']));
            }
        }
	}

	public function resolveProblem(Request $request, $parkingProblemID, $problemID, $reason, $parkingLotID)
	{
		$parkingLotController = new ParkingLotController();
		$paymentTypeController = new PaymentTypeController();
		$problem = $parkingLotController->getParkingLotFineByID($request, $parkingLotID, $problemID)->getData();
		$paymentTypes = $paymentTypeController->getPaymentType($request)->getData();
		$parkingID = ParkingProblem::find($parkingProblemID);
		$parkingID = $parkingID->parking_id;

		return view('parking.resolveProblem',compact(['reason','problem','paymentTypes','problemID','parkingProblemID','parkingLotID','parkingID']));
	}

	public function payFine(Request $request)
	{
		$transactionController = new TransactionController();
		$data = array();
		$data["parkingLotID"] = $request->input('parkingLotID');
		$data["parkingProblemID"] = $request->input("parkingProblemID");
		$data["payment_type_id"] = $request->input('payment_type_id');

		$parkingID = $request->input('parkingID');

		$request->merge(array('data' => $data));		
		$result = $transactionController->payFine($request)->getData();
		if($result->success)
			return redirect('parking/'.$data["parkingLotID"].'/'.$parkingID.'/gate/out');
		else
			return $this->gateOut($request, $data["parkingLotID"], $parkingID, $result->error);
	}

	public function pay(Request $request)
	{
		$transactionController = new TransactionController();
		$data = array();
		$data["parkingID"] = $request->input('parkingID');
		$data["payment_type_id"] = $request->input('payment_type_id');
		$parkingLotID = $request->input('parkingLotID');

		$request->merge(array('data' => $data));
		$result = $transactionController->payParking($request)->getData();

		if($result->success)
			return redirect('parkingLot/'.$parkingLotID.'/detail');
		else
			return redirect('parking/'.$parkingLotID.'/'.$data['parkingID'].'/gate/out/');
	}
}
