<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Controllers\ContactController;
use App\Http\Requests;
use App\ContactForm;
use App\Http\Controllers\Controller;

class WebContactController extends Controller
{
    public function viewAll(Request $request)
    {
    	$page = $request->input('page');
    	$contactController = new ContactController();
    	$contactResult = $contactController->getContactForms($request, $page)->getData();
    	$contacts = new LengthAwarePaginator($contactResult->data, $contactResult->total, config('app.OFFSET'), $page);
        $contacts->setPath('contact');

        return view('contact.get', compact(['contacts']));
    }

    public function detail(Request $request, $id)
    {
    	$contact = ContactForm::find($id);
    	return view('contact.detail', compact(['contact']));
    }
}
