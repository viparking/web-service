<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use App\Http\Controllers\ParkingLotController;
use App\Http\Controllers\ProblemController;
use App\Http\Controllers\Controller;
use App\Http\Requests;

class WebFineController extends Controller
{
	public function add(Request $request, $parkingLotID, $error = null)
	{
		$problemController = new ProblemController();
		$problems = $problemController->getProblem($request)->getData();

		return view('fine.add', compact(['problems', 'parkingLotID', 'error']));
	}

	public function post(Request $request)
	{
		if ($request->has('id'))
		{
			$parkingLotID = $request->input('parkingLotID');
			$data = array(
				'problem_id' => $request->input('id'),
				'fines' => $request->input('fines')
			);
			$request->merge(array('data' => $data));

			$parkingLotController = new ParkingLotController();
			$result = $parkingLotController->setParkingLotFine($request, $parkingLotID)->getData();

			if ($result->success) {
				return redirect('parkingLot/'.$parkingLotID.'/detail');
			}
			else {
				$error = $result->error;
				return $this->add($request, $parkingLotID, $error);
			}
		}

		return abort(404);
	}
}