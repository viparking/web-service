<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use App\Http\Controllers\TopUpAmountController;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\TopUpAmount;

class WebTopUpAmountController extends Controller
{
    public function get(Request $request, $error = null)
    {
    	$topUpController = new TopUpAmountController();
    	$topUps = $topUpController->get($request)->getData();

    	return view('topUpAmount.get', compact(['topUps','error']));
    }

    public function delete(Request $request)
    {
    	if($request->has('id'))
    	{
    		$topUpController = new TopUpAmountController();
    		$data = [];
    		$data['id'] = $request->input('id');
    		$request->merge(array("data" => $data));
    		$result = $topUpController->delete($request);

    		$result = $result->getData();
    		if($result->success)
    			return redirect('topUpAmount');
    		else
    			return $this->get($request, $result->error);
    	}
    	return redirect('topUpAmount');
    }

    public function add(Request $request)
    {
    	return view('topUpAmount.add');
    }

    public function edit(Request $request, $topUpAmountID)
    {
    	$topUp = TopUpAmount::find($topUpAmountID);
    	return view('topUpAmount.add', compact(["topUp"]));
    }

    public function post(Request $request)
    {
    	if($request->has('payAmount') && $request->has('creditAmount'))
    	{
    		$result = null;
    		if($request->has('id'))
    		{
    			$data = [];
    			$data['id'] = $request->input('id');
    			$data['payAmount'] = $request->input('payAmount');
    			$data['creditAmount'] = $request->input('creditAmount');
    			$request->merge(array('data' => $data));
    			$topUpController = new TopUpAmountController();
    			$result = $topUpController->edit($request);
    		}
    		else
    		{
    			$data = [];
    			$data['payAmount'] = $request->input('payAmount');
    			$data['creditAmount'] = $request->input('creditAmount');
    			$request->merge(array('data' => $data));
    			$topUpController = new TopUpAmountController();
    			$result = $topUpController->add($request);
    		}
    		$result = $result->getData();
    		if($result->success)
    			return redirect('/topUpAmount');
    		else
    		{
    			$error = $result->error;
    			if($request->has('id'))
    			{
    				$topUp = TopUpAmount::find($request->input('id'));
    				return view('topUpAmount.add',compact(['error','topUp']));
    			}
    			return view('topUpAmount.add',compact(['error']));
    		}
    	}
    	return redirect('/topUpAmount');
    }
}
