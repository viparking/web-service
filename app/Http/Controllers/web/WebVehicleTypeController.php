<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\VehicleType;
use App\Http\Controllers\VehicleController;

class WebVehicleTypeController extends Controller
{
    public function get(Request $request, $error = null)
    {
    	$vehicleController = new VehicleController();
    	$result = $vehicleController->getVehicleType($request);
    	$vehicleType = $result->getData();
    	return view("vehicleType.get",compact(['vehicleType','error']));
    }

    public function delete(Request $request)
    {
    	if($request->has('id'))
    	{
    		$vehicleController = new VehicleController();
    		$data = [];
    		$data["id"] = $request->input('id');
    		$request->merge(array("data" => $data));
    		$result = $vehicleController->deleteVehicleType($request);

    		$result = $result->getData();
    		if($result->success)
    			return redirect('vehicleType');
    		else
    			return $this->get($request, $result->error);

    	}
    	return redirect('vehicleType');
    }

    public function add(Request $request)
    {
    	return view('vehicleType.add');
    }

    public function edit(Request $request, $vehicleTypeID)
    {
    	$vehicleType = VehicleType::find($vehicleTypeID);
    	return view('vehicleType.add', compact(["vehicleType"]));
    }

    public function post(Request $request)
    {
    	if($request->has('vehicleType'))
    	{
    		$result = null;
    		if($request->has('id'))
    		{
    			$data = [];
    			$data['id'] = $request->input('id');
    			$data['vehicleType'] = $request->input('vehicleType');
    			$request->merge(array('data' => $data));
    			$vehicleController = new VehicleController();
    			$result = $vehicleController->editVehicleType($request);
    		}
    		else
    		{
    			$data = [];
    			$data['vehicleType'] = $request->input('vehicleType');
    			$request->merge(array('data' => $data));
    			$vehicleController = new VehicleController();
    			$result = $vehicleController->addVehicleType($request);
    		}
    		$result = $result->getData();
    		if($result->success)
    			return redirect('/vehicleType');
    		else
    		{
    			$error = $result->error;
    			if($request->has('id'))
    			{
    				$vehicleType = VehicleType::find($request->input('id'));
    				return view('vehicleType.add',compact(['error','vehicleType']));
    			}
    			return view('vehicleType.add',compact(['error']));
    		}
    	}
    	return redirect('/vehicleType');
    }
}
