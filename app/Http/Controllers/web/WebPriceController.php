<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use App\Http\Controllers\ParkingLotController;
use App\Http\Controllers\VehicleController;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\ParkingLot;

class WebPriceController extends Controller
{
	public function add(Request $request, $parkingLotID, $error = null)
	{
		$vehicleController = new VehicleController();
		$vehicleTypes = $vehicleController->getVehicleType($request)->getData();
		$parkingLot = ParkingLot::find($parkingLotID);

		return view('price.add', compact(['parkingLot', 'vehicleTypes', 'error']));
	}

	public function post(Request $request)
	{
		if ($request->has('parkingLotID'))
		{
			$parkingLotController = new ParkingLotController();
			$parkingLotID = $request->input('parkingLotID');
			$data = array(
				'vehicle_type_id' => $request->input('vehicle_type_id'),
				'initialPrice' => $request->input('initialPrice'),
				'initialLength' => $request->input('initialLength'),
				'freeDuration' => $request->input('freeDuration'),
				'price' => $request->input('price'),
				'overnightPrice' => $request->input('overnightPrice'),
				'maxPrice' => $request->input('maxPrice'),
				'freeStart' => $request->input('freeStart'),
				'freeEnd' => $request->input('freeEnd')
			);

			$request->merge(array('data' => $data));
			$result = $parkingLotController->setPriceForParkingLot($request, $parkingLotID)->getData();

			if ($result->success) {
				return redirect('/parkingLot/'.$parkingLotID.'/detail');
			}
			else {
				$error = $result->error;
				return $this->add($request, $parkingLotID, $error);
			}
		}

		return abort(404);
	}
}