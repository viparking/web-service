<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use App\Http\Controllers\ProblemController;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Problem;

class WebProblemTypeController extends Controller
{
	public function get(Request $request, $error = null)
	{
		$problemController = new ProblemController();
		$problemTypes = $problemController->getProblem($request)->getData();

		return view('problemType.get', compact(['problemTypes', 'error']));
	}

	public function edit(Request $request, $problemID, $error = null)
	{
		$problemType = Problem::find($problemID);
		return view('problemType.add', compact(['problemType', 'error']));
	}

	public function add(Request $request, $error = null)
	{
		return view('problemType.add', compact(['error']));
	}

	public function post(Request $request)
	{
		$result = null;
		$problemController = new ProblemController();
		$data = array(
			'problem' => $request->input('problem'),
			'description' => $request->input('description'),
			'fine' => $request->input('fine'),
			'isParkingRelated' => $request->input('isParkingRelated')
		);

		// Edit Problem Type
		if ($request->has('id'))
		{
			$problemID = $request->input('id');
			$data['id'] = $problemID;
			$request->merge(array('data' => $data));

			$result = $problemController->editProblem($request)->getData();

			// Return result
            if ($result->success) {
                return redirect('/problem-type');
            }
            else {
                $error = $result->error;
                return $this->edit($request, $problemID, $error);
            }
		}
		// Add Problem Type
		else
		{
			$request->merge(array('data' => $data));

			$result = $problemController->addProblem($request)->getData();

			// Return result
            if ($result->success) {
                return redirect('/problem-type');
            }
            else {
                $error = $result->error;
                return $this->add($request, $error);
            }
		}
	}

	public function delete(Request $request)
	{
		if ($request->has('id'))
		{
			$data = array('id' => $request->input('id'));
			$request->merge(array('data' => $data));

			$problemController = new ProblemController();
			$result = $problemController->deleteProblem($request)->getData();

			// Return result
			if ($result->success) {
				return redirect('/problem-type');
			}
			else {
				$error = $result->error;
				return $this->get($request, $error);
			}
		}
		return abort(404);
	}
}