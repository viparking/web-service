<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Controllers\ProblemController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\Controller;
use App\ParkingProblem;
use App\Parking;
use App\Problem;
use App\Vehicle;
use App\Transaction;
use App\Fine;
use App\PaymentType;
use App\Http\Requests;

class WebProblemController extends Controller
{
    public function getProblemList(Request $request)
    {
        $page = 0;
        if($request->has('page'))
            $page = $request->input('page');

    	$companyID = $request->has('companyID') ? $request->input('companyID') : 0;
    	$parkingLotID = $request->has('parkingLotID') ? $request->input('parkingLotID') : 0;

    	$problemController = new ProblemController();
    	$companyController = new CompanyController();

    	$companies = $companyController->getCompanyList($request)->getData();
    	if($companyID > 0)
	    	$parkingLots = $companyController->getParkingLot($request, $companyID)->getData();
	    if($parkingLotID > 0)
	    {
	    	$generalProblem = $problemController->getGeneralProblemForParkingLot($request, $parkingLotID, $page)->getData();
	    	$generalProblem = new LengthAwarePaginator($generalProblem->data, $generalProblem->total, config('app.OFFSET'), $page);
			$generalProblem->setPath('problem');

	    	$parkingProblem = $problemController->getParkingProblemForParkingLot($request, $parkingLotID, $page)->getData();
	    	$parkingProblem = new LengthAwarePaginator($parkingProblem->data, $parkingProblem->total, config('app.OFFSET'), $page);
			$parkingProblem->setPath('problem?companyID='.$companyID.'&parkingLotID='.$parkingLotID);
	    }
    	return view('problem.problem', compact(['companies','parkingLots','generalProblem','parkingProblem'
    		,'companyID','parkingLotID']));
    }

    public function detail(Request $request, $parkingProblemID)
    {
        $parkingProblem = ParkingProblem::find($parkingProblemID);
        $vehicle = Vehicle::find($parkingProblem->vehicle_id);
        $member = $vehicle->member;
        $problem = $parkingProblem->problem;
        
        $parking = $parkingProblem->parking;
        $fine = Fine::where('parking_lot_id','=',$parking->parking_lot_id)
            ->where('problem_id','=',$parkingProblem->problem->id)
            ->first();
        $transaction = Transaction::where('referenceID', $parkingProblem->id)->first();
        $paymentType = PaymentType::get();
        return view('problem.detail',compact(['parkingProblem','parking','transaction','fine','paymentType','member','problem']));
    }

    public function add(Request $request, $error = null)
    {
        $companyID = $request->has('companyID') ? $request->input('companyID') : null;
        $parkingLotID = $request->has('parkingLotID') ? $request->input('parkingLotID') : null;
        $problemID = $request->has('problemID') ? $request->input('problemID') : null;

        $companyController = new CompanyController();
        $problemController = new ProblemController();

        $companies = $companyController->getCompanyList($request)->getData();
        $parkingLots = (!empty($companyID)) ? $companyController->getParkingLot($request, $companyID)->getData() : null;
        $problems = $problemController->getProblem($request)->getData();
        $problemDetail = (!empty($problemID)) ? Problem::find($problemID) : null;
        $parkings = (!empty($parkingLotID)) ? Parking::where('parking_lot_id', $parkingLotID)->get() : null;

        return view('problem.add', compact([
            'companies', 'parkingLots', 'problems', 'problemDetail', 'parkings', 
            'companyID', 'parkingLotID', 'problemID', 'error'
        ]));
    }

    public function payFine(Request $request)
    {
        //if(array_key_exists("parkingProblemID", $data) && array_key_exists('payment_type_id') && array_key_exists("parkingLotID", $data))
        $transactionController = new TransactionController();
        $data = array(
            'parkingProblemID' => $request->input('parkingProblemID'),
            'payment_type_id' => $request->input('payment_type_id'),
            'parkingLotID' => $request->input('parkingLotID')
        );

        $request->merge(array('data' => $data));
        
        return redirect('problem');
    }

    public function post(Request $request)
    {
        $problemController = new ProblemController();
        $data = array(
            'isParkingRelated' => $request->input('isParkingRelated'),
            'problemID' => $request->input('problemID'),
            'reason' => $request->input('reason')
        );

        if ($data['isParkingRelated'] == 1)
        {
            if ($request->has('parkingID')) $data['parkingID'] = $request->input('parkingID');
            if ($request->has('cardID')) $data['cardID'] = $request->input('cardID');
        }
        else
        {
            if ($request->has('parkingLotID')) $data['parkingLotID'] = $request->input('parkingLotID');
            if ($request->has('description')) $data['description'] = $request->input('description');
        }

        $request->merge(array('data' => $data));

        $result = $problemController->inputProblem($request)->getData();

        // Return result
        if ($result->success) {
            return redirect('/problem');
        }
        else {
            $error = $result->error;
            return $this->add($request, $error);
        }
    }
}
