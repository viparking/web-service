<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\ParkingLotController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Admin;

class WebAdminController extends Controller
{
	public function get(Request $request, $page = null)
	{
		$adminController = new AdminController();
		$result = $adminController->getAdmin($request, $page)->getData();

		// Create manual Paginator
		$admins = new LengthAwarePaginator($result->data, $result->total, config('app.OFFSET'), $page);
		$admins->setPath('admin');

		return view('admin.get', compact(['admins']));
	}

	public function detail(Request $request, $adminID, $error = null)
	{
		$adminController = new AdminController();
		$detail = $adminController->getAdminDetail($request, $adminID)->getData();
		return view('admin.detail', compact(['detail', 'error']));
	}

	public function add(Request $request, $error = null)
	{
		return view('admin.add', compact(['error']));
	}

	public function assign(Request $request, $adminID, $companyID = null, $error = null)
	{
		$companyController = new CompanyController();
		$companies = $companyController->getCompanyList($request)->getData();

		// Get first company in data (if company ID not exist)
		if (empty($companyID)) {
			$companyID = $companies->data[0]->id;
		}
		
		$parkingLots = $companyController->getParkingLot($request, $companyID)->getData();
		
		return view('admin.assign', compact(['adminID', 'companyID', 'companies', 'parkingLots', 'error']));
	}

	public function post(Request $request)
	{
		$adminController = new AdminController();
		$data = array(
			'username' => $request->input('username'),
			'level' => $request->input('level'),
			'password' => $request->input('password')
		);

		$request->merge(array('data' => $data));
		$result = $adminController->addAdmin($request)->getData();

		// Return result
		if ($result->success) {
			return redirect('/admin');
		}
		else {
			$error = $result->error;
			return $this->add($request, $error);
		}
	}

	public function ban(Request $request)
	{
		if ($request->has('id')) 
		{
			$adminID = $request->input('id');
			$data = array('adminID' => $adminID);
			$request->merge(array('data' => $data));

			$adminController = new AdminController();
			$result = $adminController->banAdmin($request)->getData();

			// Return result
			if ($result->success) {
				return redirect('/admin');
			}
			else {
				$error = $result->error;
				return $this->detail($request, $adminID, $error);
			}
		}

		return abort(404);
	}

	public function unban(Request $request)
	{
		if ($request->has('id')) 
		{
			$adminID = $request->input('id');
			$data = array('adminID' => $adminID);
			$request->merge(array('data' => $data));

			$adminController = new AdminController();
			$result = $adminController->unbanAdmin($request)->getData();

			// Return result
			if ($result->success) {
				return redirect('/admin');
			}
			else {
				$error = $result->error;
				return $this->detail($request, $adminID, $error);
			}
		}

		return abort(404);
	}

	public function removeAccess(Request $request)
	{
		if ($request->has('id') && $request->has('parkingLotID'))
		{
			$parkingLotID = $request->input('parkingLotID');
			$adminID = $request->input('id');
			$data = array('admin_id' => $adminID);
			$request->merge(array('data' => $data));

			$parkingLotController = new ParkingLotController();
			$result = $parkingLotController->removeAdminFromParkingLot($request, $parkingLotID)->getData();

			if ($result->success) {
				return redirect('admin/'.$adminID.'/detail');
			}
			else {
				$error = $result->error;
				return $this->detail($request, $adminID, $error);
			}
		}

		return abort(404);
	}

	public function assignAccess(Request $request)
	{
		if ($request->has('id') && $request->has('parkingLotID'))
		{
			$parkingLotID = $request->input('parkingLotID');
			$adminID = $request->input('id');
			$data = array(
				'admin_id' => $adminID,
				'view' => $request->has('view') ? 1 : 0,
				'add' => $request->has('add') ? 1 : 0,
				'edit' => $request->has('edit') ? 1 : 0,
				'delete' => $request->has('delete') ? 1 : 0
			);
			$request->merge(array('data' => $data));

			$parkingLotController = new ParkingLotController();
			$result = $parkingLotController->addAdminToParkingLot($request, $parkingLotID)->getData();

			// Return result
			if ($result->success) {
				return redirect('admin/'.$adminID.'/detail');
			}
			else {
				$error = $result->error;
				return $this->assign($request, $adminID, null, $error);
			}
		}
	}
}