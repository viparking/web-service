<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Controllers\ParkingLotController;
use App\Http\Controllers\GateController;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Company;
use App\ParkingLot;

class WebParkingLotController extends Controller
{
	public function detail(Request $request, $parkingLotID, $page = null, $error = null)
	{
		$parkingLotController = new ParkingLotController();
		$gateController = new GateController();

		$parkingLot = $parkingLotController->detail($request, $parkingLotID)->getData();
		$gates = $gateController->getParkingLotGate($request, $parkingLotID)->getData();
		$fines = $parkingLotController->getParkingLotFine($request, $parkingLotID)->getData();
		$prices = $parkingLotController->getParkingLotPrice($request, $parkingLotID)->getData();
		$pendingResult = $parkingLotController->getPendingParkingForParkingLot($request, $parkingLotID, $page)->getData();
		$parkingResult = $parkingLotController->getParkingForParkingLot($request, $parkingLotID, $page)->getData();

		$pending = new LengthAwarePaginator($pendingResult->data, $pendingResult->total, config('app.OFFSET'), $page);
		$pending->setPath('detail');
		// Create manual Paginator (Parking table)
		$parking = new LengthAwarePaginator($parkingResult->data, $parkingResult->total, config('app.OFFSET'), $page);
		$parking->setPath('detail');

		return view('parkingLot.detail', compact(['parkingLot', 'gates', 'fines', 'prices', 'pending', 'parking', 'error']));
	}

	public function add(Request $request, $companyID, $error = null)
	{
		$company = Company::find($companyID);
		return view('parkingLot.add', compact(['company', 'error']));
	}

	public function edit(Request $request, $parkingLotID, $error = null)
	{
		$parkingLot = ParkingLot::find($parkingLotID);
		return view('parkingLot.add', compact(['parkingLot', 'error']));	
	}

	public function post(Request $request)
	{
		$result = null;
		$parkingLotController = new ParkingLotController();
		$data = array(
			'name' => $request->input('name'),
			'latitude' => $request->input('latitude'),
			'longitude' => $request->input('longitude'),
			'address' => $request->input('address')
		);

		// Edit Parking Lot (with Company ID)
		if ($request->has('id'))
		{
			$parkingLotID = $request->input('id');
			$data['id'] = $parkingLotID;
			$request->merge(array('data' => $data));
			$result = $parkingLotController->editParkingLot($request)->getData();
			
			if ($result->success) {
				return redirect('/parkingLot/'.$parkingLotID.'/detail');
			}
			else {
				$error = $result->error;
				return $this->edit($request, $parkingLotID, $error);
			}
		}
		// Add New Parking Lot (with Company ID)
		else
		{
			$companyID = $request->input('companyID');
			$request->merge(array('data' => $data));
			$result = $parkingLotController->addParkingLot($request, $companyID)->getData();

			if ($result->success) {
				return redirect('/company/'.$companyID.'/detail');
			}
			else {
				$error = $result->error;
				return $this->add($request, $companyID, $error);
			}
		}
	}

	public function delete(Request $request)
	{
		if ($request->has('id') && $request->has('companyID')) 
    	{
    		$companyID = $request->input('companyID');
    		$parkingLotID = $request->input('id');
    		$data = array('id' => $parkingLotID);
    		$request->merge(array('data' => $data));

    		$parkingLotController = new ParkingLotController();
    		$result = $parkingLotController->deleteParkingLot($request)->getData();

    		// Return result
    		if ($result->success) {
    			return redirect('/company/'.$companyID.'/detail');
    		}
    		else {
    			return $this->detail($request, $parkingLotID, null, $result->error);
    		}
    	}

    	return abort(404);
	}
}