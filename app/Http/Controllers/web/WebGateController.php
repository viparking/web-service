<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use App\Http\Controllers\web\WebParkingLotController;
use App\Http\Controllers\GateController;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Gate;

class WebGateController extends Controller
{
	private function getType()
	{
		return array(
			(object) array('value' => 'in', 'name' => 'In'),
			(object) array('value' => 'out', 'name' => 'Out')
		);
	}

	public function add(Request $request, $parkingLotID, $error = null)
	{
		$types = $this->getType();
		return view('gate.add', compact(['parkingLotID', 'types', 'error']));
	}

	public function edit(Request $request, $gateID, $error = null)
	{
		$types = $this->getType();
		$gate = Gate::find($gateID);
		$selectedType = $gate->type;
		return view('gate.add', compact(['gate', 'types', 'selectedType', 'error']));
	}

	public function post(Request $request)
	{
		$result = null;
		$gateController = new GateController();
		$parkingLotID = $request->input('parkingLotID');
		$data = array(
			'name' => $request->input('name'),
			'type' => $request->input('type')
		);

		// Edit Gate
		if ($request->has('id'))
		{
			$gateID = $request->input('id');
			$data['id'] = $gateID;
			$request->merge(array('data' => $data));
			$result = $gateController->editParkingLotGate($request, $parkingLotID)->getData();

			if ($result->success) {
				return redirect('/parkingLot/'.$parkingLotID.'/detail');
			}
			else {
				$error = $result->error;
				return $this->edit($request, $gateID, $error);
			}
		}
		// Add New Gate
		else
		{
			$request->merge(array('data' => $data));
			$result = $gateController->addParkingLotGate($request, $parkingLotID)->getData();

			if ($result->success) {
				return redirect('/parkingLot/'.$parkingLotID.'/detail');
			}
			else {
				$error = $result->error;
				return $this->add($request, $parkingLotID, $error);
			}
		}
	}

	public function delete(Request $request)
	{
		if ($request->has('id') && $request->has('parkingLotID'))
		{
			$data = array('id' => $request->input('id'));
			$parkingLotID = $request->input('parkingLotID');
			$request->merge(array('data' => $data));

			$gateController = new GateController();
			$result = $gateController->deleteParkingLotGate($request, $parkingLotID)->getData();

			// Return result
    		if ($result->success) {
    			return redirect('/parkingLot/'.$parkingLotID.'/detail');
    		}
    		else {
    			$webParkingLotController = new WebParkingLotController();
				$error = $result->error;
				return $webParkingLotController->detail($request, $parkingLotID, $error);
    		}
		}

		return redirect('/company');
	}
}