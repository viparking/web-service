<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination\LengthAwarePaginator;
use DB;
use App\Member;
use App\MemberLogin;
use App\Http\Requests;
use App\Http\Controllers\UserController;
use App\ContactForm;
use Mail;

class MemberController extends Controller
{
    /**
     * login functionality for member
     * @param  Request $request request with username and password parameter
     * @return
     */
    public function login(Request $request)
    {
        $username = $request->input('username');
        $password = $request->input('password');
        if(Auth::attempt(['username'=>$username, 'password'=>$password]))
        {
            if(Auth::user()->isBanned == 1)
            {
                $error = "You are banned from system";
                return view('login', compact(["error"]));
            }
            $userID = Auth::user()->id;
            $log = MemberLogin::where('member_id', $userID)->where('auditedActivity','<>','D')->update(['auditedActivity'=>'D']);
            $token = "L".substr($username,0,2).date('Ymdhis').substr(microtime(), 2,3);
            $memberLogin = new MemberLogin();
            $memberLogin->id = $token;
            $memberLogin->member_id = $userID;
            $memberLogin->auditedUser = $userID;
            $memberLogin->auditedActivity = 'I';
            $memberLogin->save();
            return redirect('/user/history');
        }
        else
        {
            $error = "Wrong credentials provided, please check your username and password again";
            return view('front.login', compact(["error"]));
        }
    }

    /**
     * get history of the user's transaction
     * @param  Request $request basic request parameter
     * @return
     */
    public function history(Request $request)
    {
        $page = $request->input('page');
        $startTime = $request->input('startTime');
        $endTime = $request->input('endTime');
        $userController = new UserController();
        $historyResult = $userController->getParkingHistory($request, $page, $startTime, $endTime)->getData();

        $history = new LengthAwarePaginator($historyResult->data, $historyResult->total, config('app.OFFSET'), $page);
        $history->setPath('history');

        return view('front.history', compact(['startTime','endTime','history']));
    }

    /**
     * insert a contact form to the DB
     * @param  Request $request request with defined post parameter
     * @return
     */
    public function contact(Request $request)
    {
        $form = new ContactForm();
        $form->email = $request->input('email');
        $form->name = $request->input('name');
        $form->subject = $request->input('subject');
        $form->isRead = false;
        $form->message = $request->input('message');
        $form->save();

        return view('front.contact');
    }

    /**
     * logout functionality for member
     * @param  Request $request basic request parameter
     * @return
     */
    public function logout(Request $request)
    {
        Auth::logout();
        session()->flush();
        return redirect('/');
    }

    /**
     * reset password functionality for member
     * @param  Request $request basic request parameter
     * @return
     */
    public function reset(Request $request)
    {
        $user = Member::where('email',$request->input('email'))->first();
        $user->password = bcrypt($user->id);
        $user->save();

        // Mail::send('reset', ['user' => $user], function($m) use ($user) {
        //     $m->from('no-reply@vi-parking.com',"No-Reply Vi-Parking");
        //     $m->to($user->email, $user->username)->subject("Password Reset");
        //     //$m->attach('parking/'.$vehicle->id.'.pdf');
        // });
        return view('front.resetSuccess', compact(['user']));
    }

    public function import(Request $request)
    {
        if($request->has('data'))
        {
            $data = $request->input('data');
            if(array_key_exists("members", $data))
            {
                foreach($data["members"] as $m) {
                    $member = new Member();
                    $member->id = "M".substr($m[2],0,2).date('Ymdhis').substr(microtime(), 2,3);
                    $member->member_type_id = $m[1];
                    $member->username = $m[2];
                    $member->email = $m[3];
                    $member->password = $m[4];
                    $member->phoneNumber = $m[5];
                    $member->point = $m[6];
                    $member->isBanned = $m[7];
                    $member->alamatTinggal = $m[8];
                    $member->alamatKTP = $m[9];
                    $member->noKTP = $m[10];
                    try
                    {
                        if($member->save())
                        {
                            \Log::info("200::member/import/:: Member with id: ".$member->id." saved");
                        }
                        else
                        {
                            $ret = new \stdClass();
                            $ret->success = false;
                            $ret->error = "Something wrong with our server, please try again later";
                            \Log::error("500::member/import/:: failed to save");
                            return response()->json($ret);
                        }
                    }
                    catch (\Illuminate\Database\QueryException $ex)
                    {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->error = "Something wrong with our server, please try again later";
                        \Log::error("500::member/import/:: failed to save: ".$ex);
                        return response()->json($ret);
                    }
                    usleep(1);
                }

                $ret = new \stdClass();
                $ret->success = true;
                return response()->json($ret);
            }
            else
            {
                \Log::info("400::member/import/:: required fields not provided");
                return $this->returnBadRequest("members field is required");
            }
        }
        \Log::info("400::member/import/:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }
}
