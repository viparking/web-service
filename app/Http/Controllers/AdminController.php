<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Member;
use App\Admin;
use App\AdminLogin;
use App\Http\Requests;
use App\MemberLogin;

class AdminController extends Controller
{
    /**
     * Register new admin to the system, this function only could be called by another admin
     * @param Request $request A request with username, password and level parameter
     */
    public function addAdmin(Request $request)
    {
        if(Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::user/addAdmin:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
        if($request->has("data"))
        {
            $data = $request->input("data");
            if(array_key_exists("username", $data) && array_key_exists("password", $data) && array_key_exists("level", $data))
            {
                $admin = new Admin();
                $admin->username = $data["username"];
                $admin->password = bcrypt($data["password"]);
                $admin->level = $data["level"];
                $admin->isBanned = false;
                $admin->auditedUser = Auth::guard('admin')->user()->id;
                $admin->auditedActivity = 'I';
                try
                {
                    $admin->save();
                    $ret = new \stdClass();
                    $ret->success = true;
                    $ret->data = new \stdClass();
                    $ret->data->admin = $admin;
                    \Log::info("200::user/addAdmin:: admin created in with id: ".$admin->id);
                    return response()->json($ret);
                }
                catch (\Illuminate\Database\QueryException $ex)
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Admin username not available";
                    \Log::info("200::user/addAdmin:: admin failed to save due to unavailable username");
                    return response()->json($ret);
                }
                catch (Exception $ex)
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::user/addAdmin:: admin failed to save");
                    return response()->json($ret);
                }
            }
            else
            {
                \Log::info("400::user/addAdmin:: username or password not provided");
                return $this->returnBadRequest("both username and password field are required");
            }
        }
        \Log::info("400::user/addAdmin:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * Get member list
     * @param  Request $request basic request parameter
     * @param  integer $page    number of page requested
     * @return
     */
    public function getMember(Request $request, $page = 0, $search = "")
    {
        if(Auth::guard('admin')->user()->level < 3)
        {
            \Log::info("401::user/member:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
        $skip = $page * config('app.OFFSET');
        $members = Member::selectRaw('members.*, p.parkingCount, v.vehicleCount')
            ->leftJoin(DB::raw('(SELECT member_id, COUNT(id) AS parkingCount FROM parkings WHERE auditedActivity <> \'D\' GROUP BY member_id) p'),'p.member_id','=','members.id')
            ->leftJoin(DB::raw('(SELECT member_id, COUNT(id) AS vehicleCount FROM vehicles WHERE auditedActivity <> \'D\' GROUP BY member_id) v'), 'v.member_id','=','members.id')
            ->where('members.auditedActivity','<>','D')
            ->where('members.username','like','%'.$search.'%')
            ->orWhere('members.email','like','%'.$search.'%')
            ->skip($skip)
            ->take(config('app.OFFSET'))
            ->get();

        $memberCount = Member::selectRaw('members.*, p.parkingCount, v.vehicleCount')
            ->leftJoin(DB::raw('(SELECT member_id, COUNT(id) AS parkingCount FROM parkings WHERE auditedActivity <> \'D\' GROUP BY member_id) p'),'p.member_id','=','members.id')
            ->leftJoin(DB::raw('(SELECT member_id, COUNT(id) AS vehicleCount FROM vehicles WHERE auditedActivity <> \'D\' GROUP BY member_id) v'), 'v.member_id','=','members.id')
            ->where('members.auditedActivity','<>','D')
            ->where('members.username','like','%'.$search.'%')
            ->where('members.email','like','%'.$search.'%')
            ->count();

        $ret = new \stdClass();
        $ret->success = true;
        $ret->total = $memberCount;
        $ret->data = $members;

        \Log::info("200::user/member:: Get Member page ".$page);
        return response()->json($ret);
    }

    public function getAllMember(Request $request)
    {
        $members = Member::get();
        $ret = new \stdClass();
        $ret->success = true;
        $ret->data = $members;

        \Log::info("200::user/allMember:: Get All Member");
        return response()->json($ret);
    }

    /**
     * Get list of admin
     * @param  Request $request basic request parameter
     * @param  integer $page    number of page requested
     * @return            
     */
    public function getAdmin(Request $request, $page = 0)
    {
        if(Auth::guard('admin')->user()->level < 3)
        {
            \Log::info("401::user/admin:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
        $skip = $page * config('app.OFFSET');
        $admins = Admin::selectRaw('admins.*, Count(a.admin_id) AS accessCount')
            ->leftJoin(DB::raw('(SELECT * FROM admin_parking_lots WHERE auditedActivity <> \'D\') a'), 'a.admin_id', '=' ,'admins.id')
            ->where('admins.auditedActivity','<>','D')
            ->groupBy('admins.id')
            ->skip($skip)
            ->take(config('app.OFFSET'))
            ->get();

        $adminCount = Admin::selectRaw('admins.*, Count(a.admin_id) AS accessCount')
            ->leftJoin(DB::raw('(SELECT * FROM admin_parking_lots WHERE auditedActivity <> \'D\') a'), 'a.admin_id', '=' ,'admins.id')
            ->where('admins.auditedActivity','<>','D')
            ->groupBy('admins.id')
            ->count();

        $ret = new \stdClass();
        $ret->success = true;
        $ret->total = $adminCount;
        $ret->data = $admins;

        \Log::info("200::user/admin:: Get Admin page ".$page);
        return response()->json($ret);
    }

    /**
     * Method to ban a member from the system
     * @param  Request $request The request consist of memberID to be banned
     * @return [type]           [description]
     */
    public function banMember(Request $request)
    {
        if(Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::user/ban:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
        if($request->has("data"))
        {
            $data = $request->input("data");
            if(array_key_exists("memberID", $data))
            {
                $memberID = $data["memberID"];
                $member = Member::find($memberID);
                $member->isBanned = 1;
                MemberLogin::where('member_id', $memberID)->where('auditedActivity','<>','D')->update(['auditedActivity'=>'D']);
                if($member->save())
                {
                    $ret = new \stdClass();
                    $ret->success = true;
                    \Log::info("200::user/ban:: member with ID: ".$memberID." is banned");
                    return response()->json($ret);
                }
                else
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::user/ban:: member failed to save");
                    return response()->json($ret);
                }
            }
            else
            {
                \Log::info("400::user/ban:: memberID not provided");
                return $this->returnBadRequest("memberID field is required");
            }
        }
        \Log::info("400::user/ban:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * Ban admin at specified AdminID
     * @param  Request $request request with adminID parameter
     * @return            
     */
    public function banAdmin(Request $request)
    {
        if(Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::user/banAdmin:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
        if($request->has("data"))
        {
            $data = $request->input("data");
            if(array_key_exists("adminID", $data))
            {
                $adminID = $data["adminID"];
                AdminLogin::where('admin_id', $data["adminID"])->where('auditedActivity','<>','D')->update(['auditedActivity'=>'D']);
                $admin = Admin::find($adminID);
                $admin->isBanned = 1;
                if($admin->save())
                {
                    $ret = new \stdClass();
                    $ret->success = true;
                    \Log::info("200::user/banAdmin:: admin with ID: ".$adminID." is banned");
                    return response()->json($ret);
                }
                else
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::user/banAdmin:: admin failed to save");
                    return response()->json($ret);
                }
            }
            else
            {
                \Log::info("400::user/banAdmin:: adminID not provided");
                return $this->returnBadRequest("adminID field is required");
            }
        }
        \Log::info("400::user/banAdmin:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * Remove ban of a member
     * @param  Request $request request with memberID data
     * @return 
     */
    public function unbanMember(Request $request)
    {
        if(Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::user/unban:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
        if($request->has("data"))
        {
            $data = $request->input("data");
            if(array_key_exists("memberID", $data))
            {
                $memberID = $data["memberID"];
                $member = Member::find($memberID);
                $member->isBanned = 0;
                if($member->save())
                {
                    $ret = new \stdClass();
                    $ret->success = true;
                    \Log::info("200::user/unban:: member with ID: ".$memberID." is unbanned");
                    return response()->json($ret);
                }
                else
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::user/unban:: member failed to save");
                    return response()->json($ret);
                }
            }
            else
            {
                \Log::info("400::user/unban:: memberID not provided");
                return $this->returnBadRequest("memberID field is required");
            }
        }
        \Log::info("400::user/unban:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * remove ban status of admin
     * @param  Request $request request with adminID data
     * @return            
     */
    public function unbanAdmin(Request $request)
    {
        if(Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::user/unbanAdmin:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
        if($request->has("data"))
        {
            $data = $request->input("data");
            if(array_key_exists("adminID", $data))
            {
                $adminID = $data["adminID"];
                $admin = Admin::find($adminID);
                $admin->isBanned = 0;
                if($admin->save())
                {
                    $ret = new \stdClass();
                    $ret->success = true;
                    \Log::info("200::user/unbanAdmin:: admin with ID: ".$adminID." is unbanned");
                    return response()->json($ret);
                }
                else
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::user/unbanAdmin:: admin failed to save");
                    return response()->json($ret);
                }
            }
            else
            {
                \Log::info("400::user/unbanAdmin:: adminID not provided");
                return $this->returnBadRequest("adminID field is required");
            }
        }
        \Log::info("400::user/unbanAdmin:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * Set admin level for specified admin to specified admin level
     * @param Request $request A request with adminID and level as parameter
     */
    public function setAdminLevel(Request $request)
    {
        if(Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::user/setAdminLevel:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
        if($request->has("data"))
        {
            $data = $request->input('data');
            if(array_key_exists("adminID", $data) && array_key_exists("level", $data))
            {
                $admin = Admin::find($data["adminID"]);
                $admin->level = $data["level"];
                try
                {
                    if($admin->save())
                    {
                        $ret = new \stdClass();
                        $ret->success = true;
                        \Log::info("200::user/setAdminLevel:: admin with ID: ".$adminID." is set to level: ".$level);
                        return response()->json($ret);
                    }
                    else
                    {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->error = "Something wrong with our server, please try again later";
                        \Log::error("500::admin/setAdminLevel:: admin failed to save");
                        return response()->json($ret);
                    }
                }
                catch (\Illuminate\Database\QueryException $ex)
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Something wrong with our server, please try again later";
                    \Log::error("500::admin/setAdminLevel:: admin failed to save");
                    return response()->json($ret);
                }
            }
            else
            {
                \Log::info("400::admin/setAdminLevel:: adminID and level not provided");
                return $this->returnBadRequest("adminID and level field are required");
            }
        }
        \Log::info("400::admin/setAdminLevel:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * get detail for particular admin
     * @param  Request $request basic request parameter
     * @param  string  $adminID id of the admin
     * @return            
     */
    public function getAdminDetail(Request $request, $adminID)
    {
        if(Auth::guard('admin')->user()->level < 3)
        {
            \Log::info("401::user/admin/get/{adminID}:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
        $admins = Admin::selectRaw('admins.*, Count(a.admin_id) AS accessCount')
            ->leftJoin(DB::raw('(SELECT * FROM admin_parking_lots WHERE auditedActivity <> \'D\') a'), 'a.admin_id', '=' ,'admins.id')
            ->where('admins.auditedActivity','<>','D')
            ->where('admins.id',$adminID)
            ->first();

        $access = Admin::selectRaw('admins.*, admin_parking_lots.*, parking_lots.name')
            ->join('admin_parking_lots','admins.id','=','admin_parking_lots.admin_id')
            ->join('parking_lots','admin_parking_lots.parking_lot_id','=','parking_lots.id')
            ->where('admins.id',$adminID)
            ->where('admins.auditedActivity','<>','D')
            ->where('admin_parking_lots.auditedActivity','<>','D')
            ->get();

        $ret = new \stdClass();
        $ret->success = true;
        $ret->data = new \stdClass();
        $ret->data->admin = $admins;
        $ret->data->access = $access;

        \Log::info("200::user/admin/get/{adminID}:: Get Admin detail for adminID ".$adminID);
        return response()->json($ret);
    }

    /**
     * get member detail for particular memberID
     * @param  Request $request  basic request parameter
     * @param  string  $memberID id of the member
     * @return             
     */
    public function getMemberDetail(Request $request, $memberID)
    {
        if(Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::user/member/get/{memberID}:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
        $members = Member::selectRaw('members.*, Count(p.id) AS parkingCount, Count(v.id) AS vehicleCount')
            ->leftJoin(DB::raw('(SELECT * FROM parkings WHERE auditedActivity <> \'D\') p'),'p.member_id','=','members.id')
            ->leftJoin(DB::raw('(SELECT * FROM vehicles WHERE auditedActivity <> \'D\') v'), 'v.member_id','=','members.id')
            ->groupBy('members.id')
            ->where('members.id',$memberID)
            ->get();
        $ret = new \stdClass();
        $ret->success = true;
        $ret->data = $members;

        \Log::info("200::user/member/get/{memberID}:: Get Member detail for memberID: ".$memberID);
        return response()->json($ret);
    }

    /**
     * Handle admin login via web page
     * @param  Request $request request with post parameter
     * @return [type]           [description]
     */
    public function webLogin(Request $request)
    {
        $username = $request->input('username');
        $password = $request->input('password');
        if(Auth::guard('admin')->attempt(['username'=>$username, 'password'=>$password]))
        {
            if(Auth::guard('admin')->user()->isBanned == 1)
            {
                $error = "You are banned from system";
                return view('login', compact(["error"]));
            }
            $adminID = Auth::guard('admin')->user()->id;
            $log = AdminLogin::where('admin_id', $adminID)->where('auditedActivity','<>','D')->update(['auditedActivity'=>'D']);
            $token = "L".substr($username,0,2).date('Ymdhis').substr(microtime(), 2,3);
            $adminLogin = new AdminLogin();
            $adminLogin->id = $token;
            $adminLogin->admin_id = $adminID;
            $adminLogin->auditedUser = $adminID;
            $adminLogin->auditedActivity = 'I';
            $adminLogin->save();
            return redirect('/dashboard');
        }
        else
        {
            $error = "Wrong credentials provided, please check your username and password again";
            return view('login', compact(["error"]));
        }
    }

    /**
     * Logout admin
     * @param  Request $request basic request parameter
     * @return            
     */
    public function logout(Request $request)
    {
        Auth::guard('admin')->logout();
        session()->flush();
        return redirect('/');
    }
}
