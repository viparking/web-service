<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Member;
use App\MemberLogin;
use App\Admin;
use App\AdminLogin;
use App\TopUp;
use App\Parking;
use App\ParkingProblem;
use Hash;
use DB;
use Carbon\Carbon;

class UserController extends Controller
{
	/**
	 * Handle user login request and determine whether it is an admin or member
	 * @param  Request $request request consist of username and password
	 * @return JSON           data according to login attempt
	 */
    public function login(Request $request)
    {
    	if($request->has('data'))
    	{
	    	//check all required param first
	    	$data = $request->input('data');
	    	if(array_key_exists('username', $data) && array_key_exists('password', $data))
	    	{
		    	$username = $data['username'];
		    	$password = $data['password'];

		    	if(Auth::attempt(['username'=>$username, 'password'=>$password]))
		    	{
		    		//user logged in
		    		//due to the namespace stdClass initialization should be lead by '\'
		    		if(Auth::user()->isBanned == 1)
		    		{
		    			$ret = new \stdClass();
			            $ret->success = false;
			            $ret->error = "Your account is banned from the system.";
			           	\Log::info("200::user/login:: invalid login attempt");
			            return response()->json($ret);
		    		}
		    		$memberID = Auth::user()->id;
		    		$log = MemberLogin::where('member_id', $memberID)->where('auditedActivity','<>','D')->update(['auditedActivity'=>'D']);
		    		$token = "L".substr($username,0,2).date('Ymdhis').substr(microtime(), 2,3);
		    		$userLogin = new MemberLogin();
		    		$userLogin->id = $token;
		    		$userLogin->member_id = $memberID;
		    		$userLogin->auditedUser = $memberID;
		    		$userLogin->auditedActivity = 'I';
		    		if($userLogin->save())
		    		{
		    			$ret = new \stdClass();
			            $ret->success = true;
			            $ret->token = $token;
			            $ret->data = new \stdClass();
			            $user = Auth::user();
			            $user->memberType = $user->memberType->memberType;
			            $ret->data->user = $user;
			            \Log::info("200::user/login:: user logged in with id: ".$memberID);
			            return response()->json($ret);
		    		}
		    		else
		    		{
		    			$ret = new \stdClass();
			            $ret->success = false;
			            $ret->error = "Something wrong with our server, please try again later";
			            \Log::error("500::user/login:: user login failed to save");
			            return response()->json($ret);
		    		}
		    	} 
		    	else if(Auth::guard('admin')->attempt(['username'=>$username, 'password'=>$password]))
		    	{
		    		if(Auth::guard('admin')->user()->isBanned == 1)
		    		{
		    			$ret = new \stdClass();
			            $ret->success = false;
			            $ret->error = "Your account is banned from the system.";
			           	\Log::info("200::user/login:: invalid login attempt");
			            return response()->json($ret);
		    		}
		    		$adminID = Auth::guard('admin')->user()->id;
		    		$log = AdminLogin::where('admin_id', $adminID)->where('auditedActivity','<>','D')->update(['auditedActivity'=>'D']);
		    		$token = "L".substr($username,0,2).date('Ymdhis').substr(microtime(), 2,3);
		    		$adminLogin = new AdminLogin();
		    		$adminLogin->id = $token;
		    		$adminLogin->admin_id = $adminID;
		    		$adminLogin->auditedUser = $adminID;
		    		$adminLogin->auditedActivity = 'I';
		    		if($adminLogin->save())
		    		{
		    			$ret = new \stdClass();
			            $ret->success = true;
			            $ret->token = $token;
			            $ret->data = new \stdClass();
			            $user = Auth::guard('admin')->user();
			            $ret->data->user = $user;
			            \Log::info("200::user/login:: user logged in with id: ".$adminID);
			            return response()->json($ret);
		    		}
		    		else
		    		{
		    			$ret = new \stdClass();
			            $ret->success = false;
			            $ret->error = "Something wrong with our server, please try again later";
			            \Log::error("500::user/login:: user login failed to save");
			            return response()->json($ret);
		    		}
		    	}
		    	else
		    	{
		    		$ret = new \stdClass();
		            $ret->success = false;
		            $ret->error = "Invalid username and password combination!";
		           	\Log::info("200::user/login:: invalid login attempt");
		            return response()->json($ret);
		    	}
		    }
		    //return 400 otherwise
		    \Log::info("400::user/login:: username or password field not provided");
		    return $this->returnBadRequest("Username and Password field are required");
		}
		//loggin is mandatory!
		\Log::info("400::user/login:: data field not provided");
		return $this->returnBadRequest("data field not provided");
    }

    /**
     * Register function for member, creating new member and save it in database
     * @param  Request $request Request with username, email, phoneNumber, alamatTinggal, alamatKTP, noKTP, and password as parameter
     * @return mixed           
     */
    public function register(Request $request)
    {
        if($request->has("data"))
        {
            $data = $request->input('data');
            if(array_key_exists("username",$data) && array_key_exists("email", $data) && array_key_exists("phoneNumber", $data) && array_key_exists("alamatTinggal", $data) && array_key_exists("alamatKTP", $data) && array_key_exists("noKTP", $data) && array_key_exists("password", $data))
            {
                $member = new Member();
                $memberID = "M".substr($data["username"],0,2).date('Ymdhis').substr(microtime(), 2,3);
                $member->id = $memberID;
                $member->username = $data["username"];
                $member->password = bcrypt($data["password"]);
                $member->email = $data["email"];
                $member->phoneNumber = $data["phoneNumber"];
                $member->alamatTinggal = $data["alamatTinggal"];
                $member->alamatKTP = $data["alamatKTP"];
                $member->noKTP = $data["noKTP"];
                $member->member_type_id = 1;
                $member->auditedActivity = 'I';
                $member->auditedUser = $memberID;
                $member->point = 0;
                $member->isBanned = false;

                if(array_key_exists("photo", $data))
                {
                    $tempImage = $this->base64_to_jpeg($data["photo"],"ktp/t".$member->id.".jpg");
                    $this->compress_image($tempImage, "ktp/".$member->id.".jpg", 60);
                    unlink("ktp/t".$member->id.".jpg");
                }
                try
                {
                    if($member->save())
                    {
                        $ret = new \stdClass();
                        $ret->success = true;
                        $ret->data = new \stdClass();
                        $ret->data->member = $member;
                        \Log::info("200::user/register:: member registered in with id: ".$memberID);
                        return response()->json($ret);
                    }
                    else
                    {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->error = "Something wrong with our server, please try again later";
                        \Log::error("500::user/register:: member failed to save");
                        return response()->json($ret);
                    }
                }
                catch(\Illuminate\Database\QueryException $ex)
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "username not available";
                    \Log::info("200::user/register:: member failed to save due to unavailable username");
                    return response()->json($ret);
                }
            }
            else
            {
                \Log::info("400::user/register:: one of required field not provided");
                return $this->returnBadRequest("One of required field not provided");
            }
        }
        \Log::info("400::user/register:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * Check user token, return the status of the token whether it has been invalidated or not
     * @param  Request $request The request consist token and isAdmin
     * @return [type]           
     */
    public function checkToken(Request $request)
    {
    	if($request->has("data"))
    	{
    		$data = $request->input("data");
    		if(array_key_exists('token',$data) && array_key_exists("isAdmin", $data))
    		{
    			$token = $data["token"];
    			$isAdmin = $data["isAdmin"];
    			if($isAdmin)
    			{
    				$adminLogin = AdminLogin::find($token);
    				if($adminLogin != null)
    				{
    					//token valid
    					$ret = new \stdClass();
    					$ret->success = true;
    					$ret->data = new \stdClass();
    					$ret->data->isValid = true;
    					\Log::info("200::user/checkToken:: valid admin token: ".$token);
			            return response()->json($ret);
    				}
    				else
    				{
    					//token invalid
    					$ret = new \stdClass();
    					$ret->success = true;
    					$ret->data = new \stdClass();
    					$ret->data->isValid = false;
    					\Log::info("200::user/checkToken:: invalid admin token: ".$token);
			            return response()->json($ret);
    				}
    			}
    			else
    			{
    				$memberLogin = MemberLogin::find($token);
    				if($memberLogin != null)
    				{
    					//token valid
    					$ret = new \stdClass();
    					$ret->success = true;
    					$ret->data = new \stdClass();
    					$ret->data->isValid = true;
    					\Log::info("200::user/checkToken:: valid member token: ".$token);
			            return response()->json($ret);
    				}
    				else
    				{
    					//token invalid
    					$ret = new \stdClass();
    					$ret->success = true;
    					$ret->data = new \stdClass();
    					$ret->data->isValid = false;
    					\Log::info("200::user/checkToken:: invalid member token: ".$token);
			            return response()->json($ret);
    				}
    			}
    		}
    		else
    		{
    			\Log::info("400::user/checkToken:: token or isAdmin field not provided");
		    	return $this->returnBadRequest("token and isAdmin field are required");
    		}
    	}
    	\Log::info("400::user/checkToken:: data field not provided");
    	return $this->returnBadRequest("data field not provided");
    }

    /**
     * Check availability of username for registration purpose
     * @param  Request $request request consist of username and isAdmin
     * @return [type]           
     */
    public function checkUsername(Request $request)
    {
    	if($request->has('data'))
    	{
    		$data = $request->input('data');
    		if(array_key_exists("username",$data) && array_key_exists("isAdmin",$data))
    		{
    			$username = $data["username"];
    			$isAdmin = $data["isAdmin"];
    			if($isAdmin)
	    			$user = Admin::where('username',$username)->where('auditedActivity','<>','D')->first();
	    		else
	    			$user = Member::where('username',$username)->where('auditedActivity','<>','D')->first();
	    		if($user != null)
	    		{
	    			$ret = new \stdClass();
	    			$ret->success = true;
	    			$ret->data = new \stdClass();
	    			$ret->data->isAvailable = false;
	    			\Log::info("200::user/checkUsername:: invalid username: ".$username);
		            return response()->json($ret);
	    		}
	    		else
	    		{
	    			$ret = new \stdClass();
	    			$ret->success = true;
	    			$ret->data = new \stdClass();
	    			$ret->data->isAvailable = true;
	    			\Log::info("200::user/checkUsername:: valid username: ".$username);
		            return response()->json($ret);
	    		}
    		}
    		else
    		{
    			\Log::info("400::user/checkUsername:: username or isAdmin field not provided");
			    return $this->returnBadRequest("username and isAdmin field are required");
    		}
    	}
    	\Log::info("400::user/checkUsername:: data field not provided");
    	return $this->returnBadRequest("data field not provided");
    }

    /**
     * get transaction history of the member
     * @param  Request $request  basic request parameter
     * @param  integer $memberID id of the member
     * @param  integer $page     page offset
     * @return             
     */
    public function getTransactionHistory(Request $request, $memberID = 0, $page = 0)
    {
        if(Auth::guard('admin')->user()!=null && Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::me/transactionHistory/:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
        else if(Auth::guard('admin')->user()!=null)
        {
            //do nothing
        }
        else 
        {
            $page = $memberID;
            $memberID = Auth::user()->id;
        }

        $skip = $page * config('app.OFFSET');
        $topUp = TopUp::selectRaw("top_ups.id, vehicle_id, payAmount AS Amount, top_ups.created_at, 'topUp' AS Type, transactions.startingBalance, transactions.balance")
            ->join('transactions','transactions.referenceID','=','top_ups.id')
            ->where('member_id',$memberID)
            ->where('top_ups.auditedActivity','<>','D');

        $fine = ParkingProblem::selectRaw("parking_problem.id, vehicle_id, (transactions.startingBalance - transactions.balance) AS Amount, parking_problem.created_at, 'fine' AS Type, transactions.startingBalance, transactions.balance")
            ->join('transactions','transactions.referenceID','=','parking_problem.id')
            ->where('member_id',$memberID)
            ->where('parking_problem.auditedActivity','<>','D');


        $parking = Parking::selectRaw("parkings.id, vehicle_id, price AS Amount, parkings.created_at, 'parking' AS Type, transactions.startingBalance, transactions.balance")->join('transactions','transactions.referenceID','=','parkings.id')
            ->where('member_id', $memberID)
            ->where('parkings.auditedActivity','<>','D')
            ->union($topUp)
            ->union($fine)
            ->orderBy('created_at', 'DESC')
            ->skip($skip)
            ->take(config('app.OFFSET'))
            ->get();

        $dataCount = Parking::selectRaw("parkings.id, vehicle_id, price AS Amount, parkings.created_at, 'parking' AS Type, transactions.startingBalance, transactions.balance")
            ->join('transactions','transactions.referenceID','=','parkings.id')
            ->where('member_id', $memberID)
            ->where('parkings.auditedActivity','<>','D')
            ->union($topUp)
            ->union($fine)
            ->get();
        $dataCount = count($dataCount);

        $ret = new \stdClass();
        $ret->success = true;
        $ret->total = $dataCount;
        $ret->data = $parking;

        \Log::info("200::user/admin:: Get Transaction for memberID: ".$memberID."Page: ".$page);
        return response()->json($ret);
    }

    /**
     * get parking history of the member
     * @param  Request $request   basic request parameter
     * @param  integer $memberID  id of the member
     * @param  integer $page      page offset
     * @param  string  $startTime start time offset
     * @param  string  $endTime   end time offset
     * @return              
     */
    public function getParkingHistory(Request $request, $memberID = 0, $page = 0, $startTime = null, $endTime = null)
    {
        if(Auth::guard('admin')->user()!=null && Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::me/parkingHistory/:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
        else if(Auth::guard('admin')->user() != null)
        {
            //do nothing
        }
        else
        {
            $endTime = $startTime;
            $startTime = $page;
            $page = $memberID;
            $memberID = Auth::user()->id;
        }

        $skip = $page * config('app.OFFSET');
        if($startTime != null && $endTime != null)
        {
            $endTime = Carbon::parse($endTime);
            if($endTime->hour == 0)
            {
                $endTime->hour = 23;
                $endTime->minute = 59;
            }

            $parking = Parking::selectRaw('parkings.*, transactions.startingBalance, transactions.balance, parking_lots.name')
                ->join('transactions','transactions.referenceID','=','parkings.id')
                ->join('parking_lots','parking_lots.id','=','parkings.parking_lot_id')
                ->where('member_id',$memberID)
                ->where('parkings.auditedActivity','<>','D')
                ->where('parkings.outTime', '>=', Carbon::parse($startTime)->toDateTimeString())
                ->where('parkings.outTime', '<=', Carbon::parse($endTime)->toDateTimeString())
                ->orderBy('created_at', 'DESC')
                ->skip($skip)
                ->take(config('app.OFFSET'))
                ->get();

            $parkingCount = Parking::join('transactions','transactions.referenceID','=','parkings.id')
                ->join('parking_lots','parking_lots.id','=','parkings.parking_lot_id')
                ->where('member_id',$memberID)
                ->where('parkings.auditedActivity','<>','D')
                ->where('parkings.outTime', '>=', Carbon::parse($startTime)->toDateTimeString())
                ->where('parkings.outTime', '<=', Carbon::parse($endTime)->toDateTimeString())
                ->count();
        }
        else
        {
            $parking = Parking::selectRaw('parkings.*, transactions.startingBalance, transactions.balance, parking_lots.name')
                ->join('transactions','transactions.referenceID','=','parkings.id')
                ->join('parking_lots','parking_lots.id','=','parkings.parking_lot_id')
                ->where('member_id',$memberID)
                ->where('parkings.auditedActivity','<>','D')
                ->orderBy('created_at', 'DESC')
                ->skip($skip)
                ->take(config('app.OFFSET'))
                ->get();

            $parkingCount = Parking::join('transactions','transactions.referenceID','=','parkings.id')
                ->join('parking_lots','parking_lots.id','=','parkings.parking_lot_id')
                ->where('member_id',$memberID)
                ->where('parkings.auditedActivity','<>','D')
                ->count();
        }

        $ret = new \stdClass();
        $ret->success = true;
        $ret->total = $parkingCount;
        $ret->data = $parking;

        \Log::info("200::user/admin:: Get Parking for memberID: ".$memberID."Page: ".$page);
        return response()->json($ret);
    }

    /**
     * get fine history of the member
     * @param  Request $request  basic request parameter
     * @param  integer $memberID id of the member
     * @param  integer $page     page offset
     * @return             
     */
    public function getFineHistory(Request $request, $memberID = 0, $page = 0)
    {
        if(Auth::guard('admin')->user()!=null && Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::me/fine/:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
        else 
        {
            $page = $memberID;
            $memberID = Auth::user()->id;
        }

        $skip = $page * config('app.OFFSET');
        $fine = ParkingProblem::selectRaw('parking_problem.*, transactions.startingBalance transactions.balance')
            ->join('transactions','transactions.referenceID','=','parking_problem.id')
            ->where('member_id', $memberID)
            ->where('parking_problem.auditedActivity','<>','D')
            ->orderBy('created_at', 'DESC')
            ->skip($skip)
            ->take(config('app.OFFSET'))
            ->get();

        $fineCount = ParkingProblem::join('transactions','transactions.referenceID','=','parking_problem.id')
            ->where('member_id', $memberID)
            ->where('parking_problem.auditedActivity','<>','D')
            ->count();

        $ret = new \stdClass();
        $ret->success = true;
        $ret->total = $fineCount;
        $ret->data = $fine;

        \Log::info("200::user/admin:: Get fine for memberID: ".$memberID."Page: ".$page);
        return response()->json($ret);
    }

    /**
     * get top up history of the member
     * @param  Request $request  basic request parameter
     * @param  integer $memberID id of the membe
     * @param  integer $page     page offset
     * @return             
     */
    public function getTopUpHistory(Request $request, $memberID = 0, $page = 0)
    {
        if(Auth::guard('admin')->user()!=null && Auth::guard('admin')->user()->level < 4)
        {
            \Log::info("401::me/topup/:: Insufficient level");
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
        else 
        {
            $page = $memberID;
            $memberID = Auth::user()->id;
        }

        $skip = $page * config('app.OFFSET');
        $topUp = TopUp::selectRaw('top_ups.*, transactions.startingBalance, transactions.balance')
            ->join('transactions','transactions.referenceID','=','top_ups.id')
            ->where('member_id', $memberID)
            ->where('top_ups.auditedActivity','<>','D')
            ->orderBy('created_at', 'DESC')
            ->skip($skip)
            ->take(config('app.OFFSET'))
            ->get();

        $topUpCount = TopUp::join('transactions','transactions.referenceID','=','top_ups.id')
            ->where('member_id', $memberID)
            ->where('top_ups.auditedActivity','<>','D')
            ->count();

        $ret = new \stdClass();
        $ret->success = true;
        $ret->total = $topUpCount;
        $ret->data = $topUp;

        \Log::info("200::user/admin:: Get Top Up for memberID: ".$memberID."Page: ".$page);
        return response()->json($ret);
    }

    /**
     * get id of the member by his/her username
     * @param  Request $request  basic request parameter
     * @param  string  $username username of the member
     * @return             
     */
    public function getMemberIDByUsername(Request $request, $username)
    {
        $member = Member::where('username',$username)->first();

        $ret = new \stdClass();
        $ret->success = true;
        $ret->data = $member;

        \Log::info("200::user/admin:: Get member detail for username: ".$username);
        return response()->json($ret);
    }

    /**
     * change password of the member
     * @param  Request $request request with oldPassword and newPassword parameter
     * @return            
     */
    public function changePassword(Request $request)
    {
        if($request->has('data'))
        {
            $data = $request->input('data');
            if(array_key_exists("oldPassword",$data) && array_key_exists("newPassword",$data))
            {
                $member = Auth::user();
                if(Hash::check($data["oldPassword"], $member->password))
                {
                    $member->password = bcrypt($data["newPassword"]);
                    try
                    {
                        $member->save();
                        $ret = new \stdClass();
                        $ret->success = true;
                        \Log::info("200::me/changePassword:: Password changed for memberID: ".$member->id);
                        return response()->json($ret);
                    }
                    catch (\Illuminate\Database\QueryException $ex)
                    {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->error = "Something wrong with our server, please try again later.";
                        \Log::info("200::me/changePassword:: admin failed to save due to unavailable username");
                        return response()->json($ret);
                    }
                    catch (Exception $ex)
                    {
                        $ret = new \stdClass();
                        $ret->success = false;
                        $ret->error = "Something wrong with our server, please try again later";
                        \Log::error("500::me/changePassword:: admin failed to save");
                        return response()->json($ret);
                    }
                }
                else
                {
                    $ret = new \stdClass();
                    $ret->success = false;
                    $ret->error = "Old Password didn't match";
                    \Log::info("200::me/changePassword:: Wrong old password");
                    return response()->json($ret);
                }
            }
            else
            {
                \Log::info("400::me/changePassword:: oldPassword and newPassword field not provided");
                return $this->returnBadRequest("oldPassword and newPassword field are required");
            }
        }
        \Log::info("400::me/changePassword:: data field not provided");
        return $this->returnBadRequest("data field not provided");
    }

    /**
     * compress uploaded image
     * @param  [type] $source_url      [description]
     * @param  [type] $destination_url [description]
     * @param  [type] $quality         [description]
     * @return [type]                  [description]
     */
    private function compress_image($source_url, $destination_url, $quality) 
    {
        $info = getimagesize($source_url); 
        if ($info['mime'] == 'image/jpeg') 
            $image = imagecreatefromjpeg($source_url); 
        else if ($info['mime'] == 'image/gif') 
            $image = imagecreatefromgif($source_url); 
        else if ($info['mime'] == 'image/png')
            $image = imagecreatefrompng($source_url); 
        else
            $image = imagecreatefromjpeg($source_url);
        imagejpeg($image, $destination_url, $quality); 
        return $destination_url; 
    }

    /**
     * convert image to base64
     * @param  [type] $base64_string [description]
     * @param  [type] $output_file   [description]
     * @return [type]                [description]
     */
    private function base64_to_jpeg($base64_string, $output_file) {
        $ifp = fopen($output_file, "wb+");

        fwrite($ifp, base64_decode($base64_string)); 
        fclose($ifp); 

        return $output_file;
    }
}
