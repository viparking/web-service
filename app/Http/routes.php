<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//no route outside!!!
Route::group(['middleware' => ['web']], function() {
	Route::get('/', function() {
 		return view('front.index');
 	});
 	Route::get('/about', function() {
 		return view('front.about');
 	});
 	Route::get('/contact', function() {
 		return view('front.contact');
 	});
 	Route::get('/product', function() {
 		return view('front.product');
 	});
 	Route::get('/forgotPassword', function() {
 		return view('front.forgot');
 	});
 	Route::get('/login', function() {
 		return view('front.login');
 	});
 	Route::group(['middleware' => ['member']], function() {
 		Route::get('user/history', "MemberController@history");
 	});
 	Route::post('userLogin', "MemberController@login");
 	Route::post('/contact', "MemberController@contact");
 	Route::get('userLogout', "MemberController@logout");
 	Route::post('/reset',"MemberController@reset");
});
Route::group(['middleware' => ['web','guest']], function(){
	//login route only
	Route::get('/loginAdmin', function(){
		return view('login');
	});
	Route::post('/login', "AdminController@webLogin");
	Route::get('html/{vehicleID}/{startDate?}/{endDate?}', "PDFController@generateHTML");

	Route::group(["prefix"=>'documentation'], function() {
 		//categorized documentation over here
 		Route::get('/', function(){
 			return view('documentation');
 		});
 		Route::get('user', function(){
			return view('userDocumentation');
		});
 		Route::get('gate', function(){
			return view('gateDocumentation');
		});
		Route::get('memberType', function(){
			return view('memberTypeDocumentation');
		});
		Route::get('parkingLot', function(){
			return view('parkingLotDocumentation');
		});
		Route::get('paymentType', function(){
			return view('paymentTypeDocumentation');
		});
		Route::get('vehicle', function(){
			return view('vehicleDocumentation');
		});
		Route::get('route', function() {
			return view('route');
		});
 	});
});

Route::group(['middleware' => ['web', 'auth']], function () {
 	// Web admin route here
 	Route::get('/dashboard', "web\WebDashboardController@get");

 	// Logout
 	Route::get('/logout', "AdminController@logout");

 	/**
 	 * Company functionality
 	 */
 	Route::group(['prefix' => 'company'], function(){
 		Route::get('/', 'web\WebCompanyController@get');
 		Route::get('/{companyID}/detail', 'web\WebCompanyController@detail');
 		Route::get('/{companyID}/detail?page={page?}', 'web\WebCompanyController@detail');
 		Route::get('/add', 'web\WebCompanyController@add');
 		Route::get('/{companyID}/edit', 'web\WebCompanyController@edit');
 		Route::post('/post', 'web\WebCompanyController@post');
 		Route::post('/delete', 'web\WebCompanyController@delete');
 		Route::get('/{companyID}/parkingLot/add', 'web\WebParkingLotController@add');
 	});

 	/**
 	 * Parking Lot functionality
 	 */
 	Route::group(['prefix' => 'parkingLot'], function() {
 		Route::get('/{parkingLotID}/detail', 'web\WebParkingLotController@detail');
 		Route::get('/{parkingLotID}/detail?page={page?}', 'web\WebParkingLotController@detail');
 		Route::get('/{parkingLotID}/edit', 'web\WebParkingLotController@edit');
 		Route::post('/post', 'web\WebParkingLotController@post');
 		Route::post('/delete', 'web\WebParkingLotController@delete');
 		Route::get('/{parkingLotID}/gate/add', 'web\WebGateController@add');
 		Route::get('/{parkingLotID}/fine/add', 'web\WebFineController@add');
 		Route::get('/{parkingLotID}/price/add', 'web\WebPriceController@add');
 		Route::get('/{parkingLotID}/parking/add', 'web\WebParkingController@add');
 		Route::get('/{parkingLotID}/parking/{parkingID}/edit', 'web\WebParkingController@edit');
 		Route::get('/{parkingLotID}/parking/{parkingID}/detail', 'web\WebParkingController@detail');
 	});
 	Route::group(['prefix' => 'gate'], function() {
 		Route::get('/{gateID}/edit', 'web\WebGateController@edit');
 		Route::post('/post', 'web\WebGateController@post');
 		Route::post('/delete', 'web\WebGateController@delete');
 	});
 	Route::group(['prefix' => 'fine'], function() {
 		Route::post('/post', 'web\WebFineController@post');
 	});
 	Route::group(['prefix' => 'price'], function() {
 		Route::post('/post', 'web\WebPriceController@post');
 	});
 	Route::group(['prefix' => 'parking'], function() {
 		Route::post('/post', 'web\WebParkingController@post');
 		Route::get('{parkingLotID}/{parkingID}/edit','web\WebParkingController@edit');
 		Route::get('{parkingLotID}/{parkingID}/gate/out', 'web\WebParkingController@gateOut');
 		Route::post('payParking', 'web\WebParkingController@pay');
 	});

 	/**
 	 * Problem functionality
 	 */
 	Route::get('problem/add', 'web\WebProblemController@add');
 	Route::get('problem/{parkingProblemID}/{problemID}/{reason}/{parkingLotID}/resolve', "web\WebParkingController@resolveProblem");
 	Route::post('problem/parking/payFine', 'web\WebParkingController@payFine');
 	Route::get('problem','web\WebProblemController@getProblemList');
 	Route::get('problem/{problemID}/detail/','web\WebProblemController@detail');
 	Route::post('problem/payFine','web\WebProblemController@payFine');
 	Route::post('problem/post','web\WebProblemController@post');

 	/**
 	 * Parking functionality
 	 */
 	Route::get('parking/{vehicleID}/detail', "web\WebParkingController@getParkingDetail");

 	/**
 	 * Vehicle functionality
 	 */
 	Route::group(['prefix' => 'vehicle'], function() {
 		Route::get('/', 'web\WebVehicleController@get');
 		//Route::get('/?page={page}', 'web\WebVehicleController@get');
 		Route::get('/{vehicleID}/detail', 'web\WebVehicleController@detail');
 		Route::get('/{vehicleID}/edit', 'web\WebVehicleController@edit');
 		Route::post('/{vehicleID}/edit', 'web\WebVehicleController@update');
 		Route::get('/add', 'web\WebVehicleController@add');
 		Route::get('/add/{memberID}', 'web\WebVehicleController@addForMember');
 		Route::post('/post', 'web\WebVehicleController@post');
 		Route::post('/remove', 'web\WebVehicleController@delete');
        Route::post('/vip', 'web\WebVehicleController@vip');
        Route::post('/search', "web\WebVehicleController@search");
 	});

 	/**
 	 * Member functionality
 	 */
 	Route::group(['prefix' => 'member'], function() {
 		Route::get('/', 'web\WebMemberController@get');
 		//Route::get('/?page={page}', 'web\WebMemberController@get');
 		Route::get('/{memberID}/detail', 'web\WebMemberController@detail');
 		Route::get('/{memberID}/edit', 'web\WebMemberController@edit');
 		Route::post('/{memberID}/edit', 'web\WebMemberController@update');
 		Route::get('/add', 'web\WebMemberController@add');
 		Route::post('/post', 'web\WebMemberController@post');
 		Route::post('/ban', 'web\WebMemberController@ban');
 		Route::post('/unban', "web\WebMemberController@unban");
 		Route::post('/search', "web\WebMemberController@search");
 	});

 	/**
 	 * Admin functionality
 	 */
 	Route::group(['prefix' => 'admin'], function() {
 		Route::get('/', 'web\WebAdminController@get');
 		Route::get('/?page={page}', 'web\WebAdminController@get');
 		Route::get('/{adminID}/detail', 'web\WebAdminController@detail');
 		Route::get('/add', 'web\WebAdminController@add');
 		Route::get('/{adminID}/assign/{companyID?}', 'web\WebAdminController@assign');
 		Route::post('/post', 'web\WebAdminController@post');
 		Route::post('/ban', 'web\WebAdminController@ban');
 		Route::post('/unban', 'web\WebAdminController@unban');
 		Route::post('/access/remove', 'web\WebAdminController@removeAccess');
 		Route::post('/access/assign', 'web\WebAdminController@assignAccess');
 	});

 	/**
 	 * Admin Level functionality
 	 */
 	Route::group(['prefix' => 'admin-level'], function(){
 		Route::get('/',"web\WebAdminLevelController@getAdminLevel");
 		Route::get('edit/{adminLevelID}/',"web\WebAdminLevelController@editAdminLevel");
 		Route::post('/post',"web\WebAdminLevelController@post");
 		Route::post('/delete', "web\WebAdminLevelController@delete");
 	});

 	/**
 	 * Vehicle Type functionality
 	 */
 	Route::group(['prefix' => 'vehicleType'], function(){
 		Route::get('/', "web\WebVehicleTypeController@get");
 		Route::get('/edit/{vehicleTypeID}', "web\WebVehicleTypeController@edit");
 		Route::get('/add', "web\WebVehicleTypeController@add");
 		Route::post('/post', "web\WebVehicleTypeController@post");
 		Route::post('/delete', "web\WebVehicleTypeController@delete");
 	});

 	/**
 	 * Problem Type functionality
 	 */
 	Route::group(['prefix' => 'problem-type'], function() {
 		Route::get('/', 'web\WebProblemTypeController@get');
 		Route::get('/{problemID}/edit', 'web\WebProblemTypeController@edit');
 		Route::get('/add', 'web\WebProblemTypeController@add');
 		Route::post('/post', 'web\WebProblemTypeController@post');
 		Route::post('/delete', 'web\WebProblemTypeController@delete');
 	});

 	// casshier session
 	Route::get('/casshier',"web\WebCasshierController@getCasshierSession");
 	Route::get('casshier/{sessionID}/detail','web\WebCasshierController@detail');

 	//contact forms
 	Route::get('contact/all',"web\WebContactController@viewAll");
 	Route::get('contact/detail/{contactID}','web\WebContactController@detail');

    //top up amount
    Route::group(['prefix' => 'topUpAmount'], function() {
        Route::get('/',"web\WebTopUpAmountController@get");
        Route::get('/add','web\WebTopUpAmountController@add');
        Route::get('/edit/{id}',"web\WebTopUpAmountController@edit");
        Route::post('/post/',"web\WebTopUpAmountController@post");
        Route::post('/delete',"web\WebTopUpAmountController@delete");
    });
});

Route::group(['middleware' => ['api'], 'prefix' => "API"], function(){
	//public route here
	Route::get('vehicle/type', "VehicleController@getVehicleType");
	Route::get('topUpAmount/', "TopUpAmountController@get");
	//not documented yet
	Route::get('problem/get', "ProblemController@getProblem");
	Route::group(['prefix' => 'parkingLot'], function(){
		Route::get('{parkingLotID}/price/',"ParkingLotController@getParkingLotPrice");
		Route::get('{parkingLotID}/gate',"GateController@getParkingLotGate");
		Route::get('{parkingLotID}/fine', "ParkingLotController@getParkingLotFine");
		Route::get('{parkingLotID}/fine/{problemID}',"ParkingLotController@getParkingLotFineByID");
	});
	Route::get('paymentType', "PaymentTypeController@getPaymentType");
	Route::get('memberType', "MemberTypeController@getMemberType");
	Route::get('vehicle/user/{vehicleID}/pdf/{startTime?}/{endTime?}', "PDFController@downloadPDF");

	//route for API here
	Route::group(['middleware' => 'api.key'], function() {
		//route that must utilize api key is here
		//those are routes that provide confidential data
		Route::group(['prefix' => 'user'], function() {
			Route::post('login', "UserController@login");
			Route::post('register', "UserController@register");
			Route::post('checkToken', "UserController@checkToken");
			Route::post('checkUsername', 'UserController@checkUsername');
		});

		Route::group(['prefix' => 'gate'], function(){
			//not documented and should be tested
			Route::post('in', "TransactionController@gateIn");
			Route::post('out', "TransactionController@gateOut");
			Route::post('in/gm', "TransactionController@gateInGM");
            Route::post('out/gm', "TransactionController@gateOutGM");
		});

		Route::post('parking/pay',"TransactionController@payParking");
	});

	Route::group(['middleware' => 'auth.bearer'], function() {
		//route within specific user here
		Route::group(['prefix'=>'vehicle'], function()
		{
			Route::get('me', "VehicleController@getMyVehicle");
			Route::get('history/me', "VehicleController@getMyHistory");
			Route::get('me/{vehicleID}/parking/{page?}/{startTime?}/{endTime?}',"VehicleController@getMemberParkingHistory");
			Route::get('me/{vehicleID}/topUp/{page?}',"VehicleController@getMemberTopUp");

			//not documented yet
			Route::get('card/{cardID}', "VehicleController@getVehicleByCardID");
			Route::get('detail/{vehicleID}', "VehicleController@getVehicleByID");

			Route::get('me/{vehicleID}/email/{startTime?}/{endTime?}', "VehicleController@sendHistoryEmail");
			Route::get('me/{vehicleID}/pdf/{startTime?}/{endTime?}', "PDFController@downloadPDF");
		});

		Route::group(['prefix'=>'visitor'], function() {
            Route::post('unregister', "VisitorController@unregister");
		    Route::post('register', "VisitorController@register");
		    Route::get('detail/{cardID}','VisitorController@getDetail');
        });

		Route::group(['prefix'=>'me'], function(){
			//not documented yet
			Route::get('transactionHistory/{page?}', "UserController@getTransactionHistory");
			Route::get('topUpHistory/{page?}', "Usercontroller@getTopUpHistory");
			Route::get('parkingHistory/{page?}', "UserController@getParkingHistory");
			Route::get('fineHistory/{page?}', "UserController@getFineHistory");

			Route::post('changePassword',"UserController@changePassword");
		});

		Route::group(['middleware' => 'admin'], function(){
			Route::group(['prefix' => 'user'], function(){
				Route::get('admin/{page?}', "AdminController@getAdmin");
				Route::get('member/{page?}', "AdminController@getMember");
				Route::get('allMember', "AdminController@getAllMember");
				Route::post('ban', "AdminController@banMember");
				Route::post('banAdmin', "AdminController@banAdmin");
				Route::post('unban', "AdminController@unbanMember");
				Route::post('unbanAdmin', "AdminController@unbanAdmin");
				Route::post('addAdmin', "AdminController@addAdmin");
				Route::get('member/get/{memberID}', "AdminController@getMemberDetail");
				Route::get('admin/get/{adminID}', "AdminController@getAdminDetail");

				//not documented yet
				Route::get('{memberID}/transactionHistory/{page?}', "UserController@getTransactionHistory");
				Route::get('{memberID}/parkingHistory/{page?}', "UserController@getParkingHistory");
				Route::get('{username}/getMemberID',"UserController@getMemberIDByUsername");
			});

			Route::group(['prefix' => 'adminLevel'], function(){
				Route::post('add',"AdminLevelController@addLevel");
				Route::post('update',"AdminLevelController@updateLevel");
				Route::post('delete',"AdminLevelController@deleteLevel");
				Route::get('/','AdminLevelController@getLevel');
			});
			Route::post('admin/setLevel', 'AdminController@setAdminLevel');

			Route::group(['prefix'=>'vehicle'], function(){
				Route::post('type/add', "VehicleController@addVehicleType");
				Route::post('type/edit',"VehicleController@editVehicleType");
				Route::post('type/delete', "VehicleController@deleteVehicleType");

				Route::get('getAll/{page?}', "VehicleController@getAllVehicle");
				Route::post('delete',"VehicleController@deleteVehicle");
				Route::get('/get/{memberID}', "VehicleController@getVehicleForMember");
				Route::post('/add',"VehicleController@addVehicle");
				Route::post('/restore',"VehicleController@restoreVehicle");

				Route::get('history/member/{memberID}', "VehicleController@getHistoryForMember");
				Route::get('history/all/{page?}', "VehicleController@getAllHistory");

				Route::get('{vehicleID}/parking/{page?}', "VehicleController@getAdminParkingHistory");
				Route::get('{vehicleID}/topUp/{page?}', "VehicleController@getAdminTopUp");
			});

			Route::group(['prefix' => 'company'], function(){
				Route::post('{companyID}/parkingLot/add', "ParkingLotController@addParkingLot");
				//not documented yet
				Route::post('add', "CompanyController@addCompany");
				Route::post('delete', "CompanyController@deleteCompany");
				Route::post('edit',"CompanyController@editCompany");
				Route::get('/', "CompanyController@getCompanyList");
				Route::get('/{companyID}/', "CompanyController@getCompanyDetail");
				Route::get('{companyID}/parkingLot',"CompanyController@getParkingLot");
				Route::get('/{companyID}/admin/{page?}', "CompanyController@getAdmin");
			});

			Route::group(['prefix' => 'parkingLot'], function(){
				Route::post('delete/', "ParkingLotController@deleteParkingLot");
				Route::post('edit/', "ParkingLotController@editParkingLot");

				Route::get('{parkingLotID}/admin/get/{page?}', "ParkingLotController@getParkingLotAdmin");
				Route::post('{pakingLotID}/admin/add', "ParkingLotController@addAdminToParkingLot");
				Route::post('{parkingLotID}/admin/remove', "ParkingLotController@removeAdminFromParkingLot");

				Route::post('{parkingLotID}/price/set', "ParkingLotController@setPriceForParkingLot");

				Route::get('{parkingLotID}/parking/get/{page?}', "ParkingLotController@getParkingForParkingLot");
				Route::post('{parkingLotID}/parking/add',"ParkingLotController@addParkingForParkingLot");
				Route::post('{parkingLogID}/parking/edit',"ParkingLotController@editParkingForParkingLot");
				Route::get('{parkingLotID}/parking/pending/{page?}',"ParkingLotController@getPendingParkingForParkingLot");

				Route::post('{parkingLotID}/gate/add',"GateController@addParkingLotGate");
				Route::post('{parkingLotID}/gate/edit',"GateController@editParkingLotGate");
				Route::post('{parkingLotID}/gate/delete', "GateController@deleteParkingLotGate");

				Route::post('{parkingLotID}/fine/set', "ParkingLotController@setParkingLotFine");
				Route::post('{parkingLotID}/fine/remove', "ParkingLotController@removeParkingLotFine");

				//not documented yet
				Route::get('{parkingLotID}/detail', "ParkingLotController@detail");
				Route::get('{parkingLotID}/managers', "ParkingLotController@getManager");
				Route::get('{parkingLotID}/generalProblem/{page?}',"ProblemController@getGeneralProblemForParkingLot");
				Route::get('{parkingLotID}/parkingProblem/{page?}',"ProblemController@getParkingProblemForParkingLot");
			});

			Route::group(['prefix' => 'problem'], function(){
				Route::post('add/', "ProblemController@addProblem");
				Route::post('edit/', "ProblemController@editProblem");
				Route::post('delete/', "ProblemController@deleteProblem");

				Route::post('input', "ProblemController@inputProblem");
			});

			Route::group(['prefix' => 'paymentType'], function(){
				Route::post('add', "PaymentTypeController@addPaymentType");
				Route::post('edit', "PaymentTypeController@editPaymentType");
				Route::post('delete', "PaymentTypeController@deletePaymentType");
			});

			Route::group(['prefix' => 'memberType'], function(){
				Route::post('add', "MemberTypeController@addMemberType");
				Route::post('edit', "MemberTypeController@editMemberType");
				Route::post('delete', "MemberTypeController@deleteMemberType");
			});

			Route::group(['prefix' => 'transaction'], function(){
				//not documented yet
				Route::post('topUp', "TransactionController@topUp");
			});

			Route::group(['prefix' => 'topUpAmount'], function(){
				//not documented yet
				Route::post('add/', "TopUpAmountController@add");
				Route::post('delete/', "TopUpAmountController@delete");
				Route::post('edit/', "TopUpAmountController@edit");
			});

			Route::group(["prefix" => 'parking'], function(){
				//not documented yet
				Route::post('pendingCount', "ParkingController@getPendingParkingCount");
				Route::get('get/parkingLot/{parkingLotID}/{page?}', "ParkingController@getParkingByParkingLot");
				Route::get('get/company/{companyID}/{page?}', "ParkingController@getParkingByCompany");
				Route::get('{parkingID}/detail',"ParkingController@getDetail");
			});

			Route::group(['prefix' => 'dashboard'], function(){
				//not documented yet
				Route::get('todayRevenue', "DashboardController@getTodayRevenue");
				Route::get('weeklyRevenue/{weekNumber?}/{page?}', "DashboardController@getWeeklyRevenue");
				Route::get('monthlyRevenue/{monthNumber?}/{page?}', "DashboardController@getMonthlyRevenue");
				Route::get('topMember/allTime/{page?}', "DashboardController@getAllTimeTopMember");
				Route::get('topMember/weekly/{weekNumber}/{page?}', "DashboardController@getWeeklyTopMember");
				Route::get('capacity/{page?}', "DashboardController@getParkingLotCapacity");
				Route::get('weeklyProblemCount/', "DashboardController@getWeeklyProblemCount");
			});

			Route::group(['prefix' => 'session'], function(){
				//not documented yet
				Route::post('{parkingLotID}/start/{managerID}', "SessionController@start");
				Route::post('{sessionID}/end/{managerID}', "SessionController@end");
				Route::get('{parkingLotID}/get/{page?}',"SessionController@getSessionForParkingLot");
				Route::get('{sessionID}/detail/{page?}', "SessionController@getSessionSummary");
			});

			Route::post('fine/pay', "TransactionController@payFine");

			Route::post('member/import',"MemberController@import");
		});
	});
});
//no route outside!!!