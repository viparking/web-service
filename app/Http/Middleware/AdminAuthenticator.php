<?php

namespace App\Http\Middleware;

use Closure;

class AdminAuthenticator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('Authorization');
        $token = substr($token, 7);
        $token = base64_decode($token);
        $param = explode(':', $token);

        $userLogin = null;
        if(is_numeric($param[1]))
        {
            return $next($request);
        }
        else
        {
            \Log::info("401::BearerAuthenticator:: invalid user id or token for user id ".$param[1]);
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
    }
}