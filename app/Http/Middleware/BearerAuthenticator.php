<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\MemberLogin;
use App\AdminLogin;

class BearerAuthenticator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $token = $request->header('Authorization');
        $token = substr($token, 7);
        $token = base64_decode($token);
        $param = explode(':', $token);

        $userLogin = null;
        if(is_numeric($param[1]))
            $userLogin = AdminLogin::find($param[0]);
        else
            $userLogin = MemberLogin::find($param[0]);

        if($userLogin == null)
        {
            \Log::info("401::BearerAuthenticator:: invalid user id or token for user id ".$param[1]);
            return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
        }
        else if($userLogin instanceof AdminLogin)
        {
            if($userLogin->admin_id == $param[1] && $userLogin->auditedActivity != 'D')
            {
                if($param[2]==config('app.API_KEY'))
                {
                    Auth::guard('admin')->loginUsingId($param[1]);
                    return $next($request);
                }
                else
                {
                    \Log::info("401::BearerAuthenticator:: invalid API Key by user id ".$param[1]);
                    return response()->json(['success'=>false, 'error'=>'INVALID API KEY'], 401);
                }
            }
        }
        else if($userLogin instanceof MemberLogin)
        {
            if($userLogin->member_id == $param[1] && $userLogin->auditedActivity != 'D')
            {
                if($param[2]==config('app.API_KEY'))
                {
                    Auth::loginUsingId($param[1]);
                    return $next($request);
                }
                else
                {
                    \Log::info("401::BearerAuthenticator:: invalid API Key by user id ".$param[1]);
                    return response()->json(['success'=>false, 'error'=>'INVALID API KEY'], 401);
                }
            }
        }
        \Log::info("401::BearerAuthenticator:: invalid user id or token for user id ".$param[1]);
        return response()->json(['success'=>false, 'error'=>'UNAUTHORIZED'], 401);
    }
}
