<?php

namespace App\Http\Middleware;

use Closure;

class ApiKeyChecker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->has('apiKey'))
        {
            if($request->input('apiKey') == config('app.API_KEY'))
            {
                return $next($request);
            }
        }
        \Log::info("401::ApiKeyChecker:: Invalid API Key");
        return response()->json(['success'=>false, 'error'=>'INVALID API KEY'], 401);
    }
}
