<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Problem extends Model
{
	public $incrementing = false;
    public function fine(){
    	return $this->hasMany('App\Fine');
    }

    public function parkingProblem() {
    	return $this->hasMany('App\ParkingProblem');
    }
}
