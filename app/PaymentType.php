<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentType extends Model
{
    public function parking() {
    	return $this->hasMany('App\Parking');
    }
}
