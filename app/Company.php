<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    public function parkingLot(){
    	return $this->hasMany('App\ParkingLot');
    }
}
