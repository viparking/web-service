<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParkingLot extends Model
{
    protected $hidden = [
        'updated_at','created_at','auditedActivity','auditedUser'
    ];
    public function company() {
    	return $this->belongsTo('App\Company');
    }
    public function price() {
    	return $this->hasMany('App\Price');
    }
    public function fine() {
    	return $this->hasMany('App\Fine');
    }
    public function parking() {
    	return $this->hasMany('App\Parking');
    }
    public function gate(){
        return $this->hasMany('App\Gate');
    }
}