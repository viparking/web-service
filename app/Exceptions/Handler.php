<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Response;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        //return response()->json(['name' => 'Abigail', 'state' => 'CA']);
        // $ret = new stdClass();
        //     $ret->success = false;
        //     $ret->error = "Method Not Allowed";
        //     return Response::json($ret, 200);
        if ($this->isHttpException($e) && ($request->ajax() || $request->wantsJson()))
        {
            //Wrong method except (get)
            //shorthand for declaring json object
            //return response()->json(['success'=>false, 'error'=>'Bad Request'], 400);
            $ret = new \stdClass();
            $ret->success = false;
            $ret->error = "Method Not Allowed!!!";
            return response()->json($ret, 405);
        }
        else if(method_exists($e, 'getStatusCode') && $e->getStatusCode()==405)
        {
            //handle not defined (not allowed method)
            $ret = new \stdClass();
            $ret->success = false;
            $ret->error = "Method Not Allowed";
            return response()->json($ret, 405);
        }
        // else
        // {
        //     $ret = new stdClass();
        //     $ret->success = false;
        //     $ret->error = "Method Not Allowed";
        //     return Response::json($ret, 200);
        // }
        return parent::render($request, $e);
    }
}
