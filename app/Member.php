<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Member extends Authenticatable
{
    public $incrementing = false;
    protected $hidden = [
        'password','created_at','updated_at', 'auditedActivity','auditedUser'
    ];

    public function parkingProblem() {
    	return $this->hasMany('App\ParkingProblem');
    }
    public function parking(){
    	return $this->hasMany('App\Parking');
    }
    public function memberType() {
    	return $this->belongsTo('App\MemberType');
    }
    public function memberLogin() {
    	return $this->hasMany('App\MemberLogin');
    }
    public function vehicle() {
    	return $this->hasMany('App\Vehicle');
    }
    public function topUp(){
    	return $this->hasMany('App\TopUp');
    }
}
