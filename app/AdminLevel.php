<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminLevel extends Model
{
	protected $hidden = [
        'updated_at','created_at'
    ];

    public function admin()
    {
    	return $this->hasMany('App/Admin');
    }
}
