<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GeneralProblem extends Model
{
    protected $hidden = [
        'created_at','auditedActivity','auditedUser'
    ];
    public $incrementing = false;
}
