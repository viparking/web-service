<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParkingProblemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parking_problem', function (Blueprint $table) {
            $table->char('id',20);
            $table->char('problem_id',20)->index();
            $table->char('parking_id',20)->index();
            $table->char('member_id',20)->index();
            $table->char('vehicle_id',20)->index();
            $table->text('reason');
            $table->char('auditedUser',20);
            $table->char('auditedActivity',1);
            $table->timestamps();

            $table->primary('id');
        });

        DB::table('parking_problem')->insert([
                ['id' => 'test parking problem',
                'problem_id' => 'test problem',
                'parking_id' => 'test parking',
                'member_id' => 'test',
                'vehicle_id' => 'test vehicle',
                'reason' => 'ini alasannya',
                'auditedUser' => 'test',
                'auditedActivity' => 'I']
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('parking_problem');
    }
}
