<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCasshierSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('casshier_sessions', function (Blueprint $table) {
            $table->char('id',20);
            $table->integer('parking_lot_id')->length(10)->unsigned()->index();
            $table->char('gate_id',20)->index();
            $table->integer('admin_id')->length(10)->unsigned()->index();
            $table->integer('login_manager_id')->length(10)->unsigned()->index();
            $table->integer('logout_manager_id')->length(10)->unsigned()->index();
            $table->dateTime('loginTime');
            $table->dateTime('logoutTime');
            $table->integer('startingBalance');
            $table->integer('finalBalance');
            $table->integer('takenBalance');
            $table->char('auditedUser',20);
            $table->char('auditedActivity',1);
            $table->timestamps();

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('casshier_sessions');
    }
}
