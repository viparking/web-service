<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPakingProblemConstraint extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parking_problem', function (Blueprint $table) {
            $table->foreign('problem_id')->references('id')->on('problems');
            $table->foreign('parking_id')->references('id')->on('parkings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parking_problem', function (Blueprint $table) {
            $table->dropForeign('parking_problem_problem_id_foreign');
            $table->dropForeign('parking_problem_parking_id_foreign');
        });
    }
}
