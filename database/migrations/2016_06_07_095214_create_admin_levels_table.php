<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_levels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('adminLevel');
            $table->timestamps();
        });

        DB::table('admin_levels')->insert([
                ['adminLevel'=>'Kasir'],
                ['adminLevel'=>'Staff Registrasi'],
                ['adminLevel'=>'Manager'],
                ['adminLevel'=>'Staff Kantor'],
                ['adminLevel'=>'Super Admin']
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('admin_levels');
    }
}
