<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVisitorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visitors', function (Blueprint $table) {
            $table->char('id',20);
            $table->string('cardID');
            $table->string('name');
            $table->string('plateNumber', 10);
            $table->string('phoneNumber',30);
            $table->text('reason');
            $table->datetime('expireTime');
            $table->datetime('inTime');
            $table->datetime('outTime');
            $table->datetime('takeKTPTime');
            $table->timestamps();

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('visitors');
    }
}
