<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParkingsConstraint extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parkings', function (Blueprint $table) {
            $table->foreign('parking_lot_id')->references('id')->on('parking_lots');
            $table->foreign('vehicle_id')->references('id')->on('vehicles');
            $table->foreign('payment_type_id')->references('id')->on('payment_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parkings', function (Blueprint $table) {
            $table->dropForeign('parkings_parking_lot_id_foreign');
            $table->dropForeign('parkings_vehicle_id_foreign');
            $table->dropForeign('parkings_payment_type_id_foreign');
        });
    }
}
