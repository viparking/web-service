<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fines', function (Blueprint $table) {
            $table->char('problem_id',20)->index();
            $table->integer('parking_lot_id')->length(10)->unsigned()->index();
            $table->integer('fines');
            
            $table->char('auditedUser',20);
            $table->char('auditedActivity',1);
            $table->timestamps();
        });

        DB::table('fines')->insert([
                ['problem_id' => 'test problem',
                'parking_lot_id' => 1,
                'fines' => 123123,
                'auditedUser' => 'test',
                'auditedActivity' => 'I']
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fines');
    }
}
