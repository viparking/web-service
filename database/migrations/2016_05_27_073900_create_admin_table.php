<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username',50)->unique();
            $table->string('password',500);
            $table->integer('level');
            $table->boolean('isBanned');
            $table->char('auditedUser',20);
            $table->char('auditedActivity',1);
            $table->timestamps();
            $table->rememberToken();
        });
        DB::table('admins')->insert([
                ['username' => 'admin',
                'password' => '$2y$10$MVYyqLuPPX6PWerG6J3Cnu8MHC9Zmu3PxJJJ8B./bOmnDsVaqA.9K',
                'level' => 5,
                'isBanned' => false,
                'auditedUser' => 'test',
                'auditedActivity' => 'I']
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('admins');
    }
}
