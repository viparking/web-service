<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminMenuMappingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_menu_mappings', function (Blueprint $table) {
            $table->integer('admin_level_id');
            $table->string('menu');
        });

        DB::table('admin_menu_mappings')->insert([
            ['admin_level_id' => 5,
            'menu' => 'Dashboard'],
            ['admin_level_id' => 5,
                'menu' => 'Company'],
            ['admin_level_id' => 5,
                'menu' => 'Problem'],
            ['admin_level_id' => 5,
                'menu' => 'Casshier'],
            ['admin_level_id' => 5,
                'menu' => 'Vehicle'],
            ['admin_level_id' => 5,
                'menu' => 'Member'],
            ['admin_level_id' => 5,
                'menu' => 'Admin'],
            ['admin_level_id' => 5,
                'menu' => 'Admin Level'],
            ['admin_level_id' => 5,
                'menu' => 'Vehicle Type'],
            ['admin_level_id' => 5,
                'menu' => 'Member Type'],
            ['admin_level_id' => 5,
                'menu' => 'Problem Type'],
            ['admin_level_id' => 5,
                'menu' => 'Top Up Amount'],
            ['admin_level_id' => 5,
                'menu' => 'Contact Forms'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('admin_menu_mappings');
    }
}
