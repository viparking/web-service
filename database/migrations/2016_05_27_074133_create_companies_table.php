<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('representativeName');
            $table->string('phoneNumber',35);
            $table->string('emailAddress');
            $table->text('address');
            $table->char('auditedUser',20);
            $table->char('auditedActivity',1);
            $table->timestamps();
        });
        DB::table('companies')->insert([
                ['name'=>'test company',
                'representativeName' => 'representative',
                'phoneNumber' => '081237192321',
                'emailAddress' => 'fer@fer.com',
                'address' => 'asdfaewr awrea asdf ',
                'auditedUser' => 'test',
                'auditedActivity' => 'I']
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('companies');
    }
}
