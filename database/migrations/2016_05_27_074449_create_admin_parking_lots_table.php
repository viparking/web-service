<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminParkingLotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_parking_lots', function (Blueprint $table) {
            $table->integer('admin_id')->length(10)->unsigned()->index();
            $table->integer('parking_lot_id')->length(10)->unsigned()->index();
            $table->boolean('view');
            $table->boolean('add');
            $table->boolean('edit');
            $table->boolean('delete');
            $table->char('auditedUser',20);
            $table->char('auditedActivity',1);
            $table->timestamps();
        });

        DB::table('admin_parking_lots')->insert([
                ['admin_id' => 1,
                'parking_lot_id' => 1,
                'view' => true,
                'add' => true,
                'edit' => true,
                'delete' => true,
                'auditedUser' => 'test',
                'auditedActivity' => 'I']
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('admin_parking_lot');
    }
}
