<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prices', function (Blueprint $table) {
            $table->integer('parking_lot_id')->length(10)->unsigned()->index();
            $table->integer('vehicle_type_id')->length(10)->unsigned()->index();
            $table->integer('initialPrice');
            $table->integer('initialLength');
            $table->integer('freeDuration');
            $table->integer('price');
            $table->integer('overnightPrice');
            $table->integer('maxPrice');
            $table->integer('freeStart');
            $table->integer('freeEnd');
            $table->char('auditedUser',20);
            $table->char('auditedActivity',1);
            $table->timestamps();
        });

        DB::table('prices')->insert([
                ['parking_lot_id' => 1,
                'vehicle_type_id' => 1,
                'initialPrice' => 5000,
                'initialLength' => 1,
                'freeDuration' => 15,
                'price' => 4000,
                'overnightPrice' => 25000,
                'maxPrice' => 100000,
                'freeStart' => 25,
                'freeEnd' => 25,
                'auditedUser' => 'test',
                'auditedActivity' => 'I']
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('prices');
    }
}
