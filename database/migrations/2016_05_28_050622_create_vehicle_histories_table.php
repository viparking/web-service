<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_histories', function (Blueprint $table) {
            $table->char('id',20);
            $table->char('vehicle_id',20)->index();
            $table->integer('vehicle_type_id')->length(10)->unsigned()->index();
            $table->char('user_id',20)->index();
            $table->string('name');
            $table->string('plateNumber',15);
            $table->integer('balance');
            $table->char('auditedActivity',1);
            $table->timestamps();

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vehicle_histories');
    }
}
