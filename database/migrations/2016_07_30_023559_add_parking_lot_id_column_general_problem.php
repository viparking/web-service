<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParkingLotIdColumnGeneralProblem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('general_problems', function (Blueprint $table) {
            $table->integer('parking_lot_id')->length(10)->unsigned()->index();
        });

        DB::table('general_problems')->insert([
                ['id' => 'test general2',
                'problem_id' => 'prob',
                'reason' => 'alasan',
                'description' => 'misal ada bomb',
                'parking_lot_id' => 1,
                'auditedUser' => 'test',
                'auditedActivity' => 'I']
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('general_problems', function (Blueprint $table) {
            //
        });
    }
}
