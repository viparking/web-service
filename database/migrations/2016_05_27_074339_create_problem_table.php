<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProblemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('problems', function (Blueprint $table) {
            $table->char('id',20);
            $table->string('problem');
            $table->text('description');
            $table->integer('fine');
            $table->boolean('isParkingRelated');
            $table->char('auditedUser',20);
            $table->char('auditedActivity',1);
            $table->timestamps();

            $table->primary('id');
        });

        DB::table('problems')->insert([
                ['id' => 'test problem',
                'problem' => 'ini masalah',
                'description' => 'description dari masalah',
                'fine' => 120000,
                'isParkingRelated' => false,
                'auditedUser' => 'test',
                'auditedActivity' => 'I']
            ]);

        DB::table('problems')->insert([
                ['id' => 'prob',
                'problem' => 'ini masalah222',
                'description' => 'ini test isi description dari masalah',
                'fine' => 55555,
                'isParkingRelated' => true,
                'auditedUser' => 'test',
                'auditedActivity' => 'I']
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('problems');
    }
}
