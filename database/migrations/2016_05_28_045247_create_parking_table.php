<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParkingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parkings', function (Blueprint $table) {
            $table->char('id',20);
            $table->integer('parking_lot_id')->length(10)->unsigned()->index();
            $table->char('vehicle_id',20)->index();
            $table->integer('payment_type_id')->length(10)->unsigned()->index();
            $table->char('member_id',20)->index();
            $table->integer('price');
            $table->datetime('inTime');
            $table->datetime('outTime');
            $table->char('inGate',20);
            $table->char('outGate',20);
            $table->char('auditedUser',20);
            $table->char('auditedActivity',1);
            $table->timestamps();

            $table->primary('id');
        });

        DB::table('parkings')->insert([
                ['id'=>'test parking',
                'parking_lot_id' => 1,
                'vehicle_id' => 'test vehicle',
                'member_id' => 'test',
                'payment_type_id' => 1,
                'price' => 10000,
                'inGate'=> 'g1',
                'outGate'=>'g2']
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('parkings');
    }
}
