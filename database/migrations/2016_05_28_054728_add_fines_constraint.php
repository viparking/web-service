<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFinesConstraint extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fines', function (Blueprint $table) {
            $table->foreign('problem_id')->references('id')->on('problems');
            $table->foreign('parking_lot_id')->references('id')->on('parking_lots');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fines', function (Blueprint $table) {
            $table->dropForeign('fines_problem_id_foreign');
            $table->dropForeign('fines_parking_lot_id_foreign');
        });
    }
}
