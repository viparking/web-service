<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTransactionTypeData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaction_types', function (Blueprint $table) {
            //
        });

        DB::table('transaction_types')->where('id',2)->update(['transactionType'=>'parking']);

        DB::table('transaction_types')->insert([
            ['transactionType'=>'fine']
         ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaction_types', function (Blueprint $table) {
            //
        });
    }
}
