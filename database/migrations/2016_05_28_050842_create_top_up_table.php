<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTopUpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('top_ups', function (Blueprint $table) {
            $table->char('id',20);
            $table->char('member_id',20)->index();
            $table->char('vehicle_id',20)->index();
            $table->integer('payAmount');
            $table->integer('creditAmount');
            $table->char('auditedUser',20);
            $table->char('auditedActivity',1);
            $table->timestamps();

            $table->primary('id');
        });
        DB::table('top_ups')->insert([
                ['id' => 'test top up',
                'member_id' => 'test',
                'vehicle_id' => 'test vehicle',
                'payAmount' => 100,
                'creditAmount' => 100000,
                'auditedUser' => '1',
                'auditedActivity' => 'I']
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('top_ups');
    }
}
