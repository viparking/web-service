<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gates', function (Blueprint $table) {
            $table->char('id',20);
            $table->integer('parking_lot_id')->length(10)->unsigned()->index();
            $table->string('name');
            $table->string('type',3);
            $table->char('auditedUser',20);
            $table->char('auditedActivity',1);
            $table->timestamps();

            $table->primary('id');
        });
        DB::table('gates')->insert([
                ['id'=>'g1',
                'parking_lot_id'=>1,
                'type'=>'in',
                'name'=>'first gate'],
                ['id'=>'g2',
                'parking_lot_id'=>1,
                'type'=>'out',
                'name'=>'second gate'],
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gates');
    }
}
