<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->char('id',20);
            $table->integer('member_type_id')->length(10)->unsigned()->index();
            $table->string('username',20);
            $table->string('email');
            $table->string('password');
            $table->string('phoneNumber',30);
            $table->integer('point');
            $table->boolean('isBanned');
            $table->text('alamatTinggal');
            $table->text('alamatKTP');
            $table->string('noKTP');
            $table->char('auditedUser',20);
            $table->char('auditedActivity',1);
            $table->timestamps();

            $table->primary('id');
        });

        DB::table('members')->insert([
                ['id' => 'test',
                'member_type_id'=>1,
                'username'=>'ferico55',
                'password'=>'$2y$10$nNlyPPKppYe.XwUx0deBG.410wrpDbWvSaDYwFT8AYK9fRFbgS43i',
                'email'=>'ferico55@gmail.com',
                'phoneNumber'=>'0812387123',
                'isBanned'=>false,
                'auditedUser'=>'test',
                'auditedActivity'=>'I'],
                ['id' => 'guest',
                'member_type_id'=>1,
                'username'=>'guest',
                'password'=>'$2y$10$nNlyPPKppYe.XwUx0deBG.410wrpDbWvSaDYwFT8AYK9fRFbgS43i',
                'email'=>'ferico55@gmail.com',
                'phoneNumber'=>'0812387123',
                'isBanned'=>false,
                'auditedUser'=>'test',
                'auditedActivity'=>'I']
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('members');
    }
}
