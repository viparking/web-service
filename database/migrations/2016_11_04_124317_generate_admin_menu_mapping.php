<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenerateAdminMenuMapping extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('admin_menu_mappings')->insert([
            ['admin_level_id' => 1,
                'menu' => 'Dashboard'],
            ['admin_level_id' => 1,
                'menu' => 'Company'],
            ['admin_level_id' => 1,
                'menu' => 'Problem'],
        ]);

        DB::table('admin_menu_mappings')->insert([
            ['admin_level_id' => 2,
                'menu' => 'Dashboard'],
            ['admin_level_id' => 2,
                'menu' => 'Company'],
            ['admin_level_id' => 2,
                'menu' => 'Problem'],
            ['admin_level_id' => 2,
                'menu' => 'Vehicle'],
            ['admin_level_id' => 2,
                'menu' => 'Member'],
        ]);
        
        DB::table('admin_menu_mappings')->insert([
            ['admin_level_id' => 3,
                'menu' => 'Dashboard'],
            ['admin_level_id' => 3,
                'menu' => 'Company'],
            ['admin_level_id' => 3,
                'menu' => 'Problem'],
            ['admin_level_id' => 3,
                'menu' => 'Casshier'],
            ['admin_level_id' => 3,
                'menu' => 'Vehicle'],
            ['admin_level_id' => 3,
                'menu' => 'Member'],
            ['admin_level_id' => 3,
                'menu' => 'Admin'],
        ]);

        DB::table('admin_menu_mappings')->insert([
            ['admin_level_id' => 4,
                'menu' => 'Dashboard'],
            ['admin_level_id' => 4,
                'menu' => 'Company'],
            ['admin_level_id' => 4,
                'menu' => 'Problem'],
            ['admin_level_id' => 4,
                'menu' => 'Casshier'],
            ['admin_level_id' => 4,
                'menu' => 'Vehicle'],
            ['admin_level_id' => 4,
                'menu' => 'Member'],
            ['admin_level_id' => 4,
                'menu' => 'Admin'],
            ['admin_level_id' => 4,
                'menu' => 'Admin Level'],
            ['admin_level_id' => 4,
                'menu' => 'Vehicle Type'],
            ['admin_level_id' => 4,
                'menu' => 'Member Type'],
            ['admin_level_id' => 4,
                'menu' => 'Problem Type'],
            ['admin_level_id' => 4,
                'menu' => 'Top Up Amount'],
            ['admin_level_id' => 4,
                'menu' => 'Contact Forms'],
        ]);
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
