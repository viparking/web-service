<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->char('id',20);
            $table->integer('vehicle_type_id')->length(10)->unsigned()->index();
            $table->char('member_id',20)->index();
            $table->string('card_id');
            $table->string('name');
            $table->string('plateNumber',15);
            $table->integer('balance');
            $table->text('address');
            $table->string('noSTNK',100);
            $table->string('brand',150);
            $table->string('color',100);
            $table->string('type',100);
            $table->char('auditedUser',20);
            $table->char('auditedActivity',1);
            $table->timestamps();

            $table->primary('id');
        });

        DB::table('vehicles')->insert([
                ['id'=>'test vehicle',
                'vehicle_type_id'=>1,
                'member_id'=>'test',
                "name"=>'vehicle',
                "card_id"=>"test",
                'plateNumber' => 'B6198UOO',
                'balance'=>123123123,
                'auditedActivity'=>'I',
                'address' => 'jalan jalan',
                'noSTNK' => 'stnk',
                'brand' => 'yamaha',
                'color' => 'metalic white',
                'type' => 'xenia',
                'auditedUser'=>'test'],
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vehicles');
    }
}
