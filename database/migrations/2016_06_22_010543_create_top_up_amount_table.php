<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTopUpAmountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('top_up_amounts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('payAmount');
            $table->integer('creditAmount');
            $table->char('auditedUser',20);
            $table->char('auditedActivity',1);
        });

        DB::table('top_up_amounts')->insert([
                ['payAmount' => 100,
                'creditAmount' => 100000,
                'auditedUser' => '1',
                'auditedActivity' => 'I']
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('top_up_amounts');
    }
}
