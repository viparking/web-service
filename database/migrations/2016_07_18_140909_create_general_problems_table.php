<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeneralProblemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('general_problems', function (Blueprint $table) {
            $table->char('id',20);
            $table->char('problem_id',20)->index();
            $table->text('reason');
            $table->text('description');
            $table->char('auditedUser',20);
            $table->char('auditedActivity',1);
            $table->timestamps();

            $table->primary('id');
        });

        DB::table('general_problems')->insert([
                ['id' => 'test general',
                'problem_id' => 'prob',
                'reason' => 'ini alasannya',
                'description' => 'misal mati lampu jam 5 - 7',
                'auditedUser' => 'test',
                'auditedActivity' => 'I']
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('general_problems');
    }
}
