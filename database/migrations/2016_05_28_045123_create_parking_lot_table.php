<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParkingLotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parking_lots', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->length(10)->unsigned()->index();
            $table->string('name');
            $table->double('latitude');
            $table->double('longitude');
            $table->text('address');
            $table->char('auditedUser',20);
            $table->char('auditedActivity',1);
            $table->timestamps();
        });

        DB::table('parking_lots')->insert([
                ['name' => 'parking lot name',
                'company_id' => 1,
                'latitude' => 6,
                'longitude' => 102,
                'address' => '12 usdajf n32fas dfa',
                'auditedUser' => 'test',
                'auditedActivity' => 'I']
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('parking_lots');
    }
}
